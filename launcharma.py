from pathlib import Path
import subprocess
import winreg
import sys

r"""

"B:\Steam\steamapps\common\Arma 3\arma3.exe"  -noAsserts -noSteam
"F:\Documents\Arma 3 - Other Profiles\Adanteh\missions\test.Malden\mission.sqm" 
-name=Adanteh -showScriptErrors -filePatching -window -nosplash -world=empty
"""


def find_arma():
    flags = winreg.KEY_READ
    try:
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\\Wow6432Node\\bohemia interactive\\arma 3", 0, flags)
        (value, valuetype) = winreg.QueryValueEx(key, "main")
        key.Close()
        return value
    except Exception:
        raise Exception("Arma not found")


def start(buldozer=True):
    arma = find_arma()
    modpath = Path(arma) / "x" / "plopper"
    armapath = str(Path(arma) / "arma3_x64.exe")
    mission = r"C:\Users\anpos\Documents\Arma 3 - Other Profiles\Adanteh\missions\plop.Malden\mission.sqm"

    mods = [r"B:\Steam\steamapps\common\Arma 3\!Workshop\@Pythia"]
    mods.append(str(modpath))

    import os
    import time

    os.chdir(arma)
    time.sleep(1)

    sys.path.insert(0, arma)
    _mod = "-mod=" + ";".join(mods)

    if buldozer:
        arguments = [
            armapath,
            "-buldozer+addons",
            "-name=Buldozer",
            "-window",
            "-exThreads=0",
            "-disableSteam",
            "-noAsserts",
            "-cfg=p:\buldozer.cfg",
            "-showScriptErrors",
            "-mod=@Plopper;@Pythia",
        ]
        arma = "P:\\"
    else:
        arguments = [
            armapath,
            mission,
            "-name=Adanteh",
            "-showScriptErrors",
            "-filePatching",
            "-window",
            "-nosplash",
            "-nopause",
            _mod,
        ]

    print(arguments)
    process = subprocess.Popen(arguments, cwd=arma)


if __name__ == "__main__":
    start(buldozer=False)
