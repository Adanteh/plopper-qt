class EventHandlers
{
  class ADDON
  {
    onUndo="['onUndo', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onRedo="['onRedo', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onHistoryChange="['onHistoryChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetToggle="['onWidgetToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetNone="['onWidgetNone', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetTranslation="['onWidgetTranslation', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetRotation="['onWidgetRotation', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetScale="['onWidgetScale', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWidgetArea="['onWidgetArea', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onGridChange="['onGridChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMoveGridToggle="['onMoveGridToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onRotateGridToggle="['onRotateGridToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onScaleGridToggle="['onScaleGridToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onVerticalToggle="['onVerticalToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onSurfaceSnapToggle="['onSurfaceSnapToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onWorkspacePartSwitch="['onWorkspacePartSwitch', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onModeChange="['onModeChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onSubmodeChange="['onSubmodeChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMapOpened="['onMapOpened', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMapClosed="['onMapClosed', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onSearchEdit="['onSearchEdit', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onSearchCreate="['onSearchCreate', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionListChange="['onMissionListChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionPreview="['onMissionPreview', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionPreviewEnd="['onMissionPreviewEnd', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    init="['init', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onTerrainNew="['onTerrainNew', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionNew="['onMissionNew', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionLoad="['onMissionLoad', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMissionAutosave="['onMissionAutosave', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onMessage="['onMessage', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onServerToggle="['onServerToggle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onEntityMenu="['onEntityMenu', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onSelectionChange="['onSelectionChange', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onConnectingStart="['onConnectingStart', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onConnectingEnd="['onConnectingEnd', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
    onTogglePlaceEmptyVehicle="['onTogglePlaceEmptyVehicle', _this] call (uiNamespace getVariable 'ADA_eden_fnc_onEdenEvent')";
  };
};
