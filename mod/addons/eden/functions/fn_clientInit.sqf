/*
    Function:       ADA_Eden_fnc_clientInit
    Author:         Adanteh
    Description:    WIP Function
*/
#define __INCLUDE_EDEN_IDC
#include "macros.hpp"

if !(is3DEN) exitWith { };

GVAR(namespace) = false call CFUNC(createNamespace);

//////////////////
// -- TOOLS HANDLING
//////////////////


// -- This will create a tools changed event
GVAR(panelToggle) = [false, false];
GVAR(currentCamera) = get3DENCamera;
[{
    // -- Eden panes opened / closed
    private _leftPanelState = ctrlEnabled ((findDisplay IDD_DISPLAY3DEN) displayCtrl IDC_DISPLAY3DEN_PANELLEFT);
    private _rightPanelState = ctrlEnabled ((findDisplay IDD_DISPLAY3DEN) displayCtrl IDC_DISPLAY3DEN_PANELRIGHT);
    if !([_leftPanelState, _rightPanelState] isEqualTo GVAR(panelToggle)) then {
        ["eden.panelStateChanged", [[_leftPanelState, _rightPanelState], GVAR(panelToggle)]] call CFUNC(localEvent);
        GVAR(panelToggle) = [_leftPanelState, _rightPanelState];
    };

    // -- Detect surface painter open/close
    private _data = if (!isNil "SP_var_camera") then { SP_var_camera } else { get3DENCamera };
    if !(_data isEqualTo GVAR(currentCamera)) then {
        ["sp.cameraChanged", [_data, GVAR(currentCamera)]] call CFUNC(localEvent);
        GVAR(currentCamera) = _data;
    };

}, 0] call CFUNC(addPerFrameHandler);

["object.import", {
    (_this select 0) call FUNC(importEdenEntity);
}] call CFUNC(addEventHandler);
