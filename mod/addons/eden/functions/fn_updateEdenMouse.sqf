/*
    Function:       ADA_Eden_fnc_updateEdenMouse
    Author:         Adanteh
    Description:    Disables mouse events on eden display, by overlaying a structured text item on it
*/
#define __INCLUDE_EDEN_IDC
#include "macros.hpp"

params [["_enable", true]];


if (_enable) then {
    disableSerialization;
    private _blocker = (uiNamespace getVariable [QGVAR(edenBlocker), controlNull]);

    if !(GVAR(disableEdenDrag)) then {
        GVAR(disableEdenDrag) = true;
        if (isNull _blocker) then {
            _blocker = (findDisplay IDD_DISPLAY3DEN) ctrlCreate ["RscStructuredText", -1];
            uiNamespace setVariable [QGVAR(edenBlocker), _blocker];
        };
    };

    // -- Adjust to within the 'mouse' area of eden.
    private _toolbarPos = ctrlPosition ((findDisplay IDD_DISPLAY3DEN) displayCtrl IDC_DISPLAY3DEN_TOOLBAR);
    private _toolbarBottomPos = (_toolbarPos select 1) + (_toolbarPos select 3);
    /*
    private _leftPos = [safeZone] ctrlEnabled ((findDisplay IDD_DISPLAY3DEN) displayCtrl IDC_DISPLAY3DEN_PANELLEFT);
    private _sidebarLeftEnd = (_sidebarLeftPos select 0) + (_sidebarLeftPos select 2);
    private _sidebarRightPos = ctrlPosition ((findDisplay IDD_DISPLAY3DEN) displayCtrl IDC_DISPLAY3DEN_PANELRIGHT);
    private _sidebarRightPos = (_sidebarRightPos select 0);
    */


    private _position = [
        safeZoneX,
        _toolbarBottomPos, // Menubar + toolbar
        safeZoneW,
        safeZoneH - _toolbarBottomPos
    ];
    _blocker ctrlSetPosition _position;
    _blocker ctrlCommit 0;

} else {
    if (GVAR(disableEdenDrag)) then {
        GVAR(disableEdenDrag) = false;
        ctrlDelete (uiNamespace getVariable [QGVAR(edenBlocker), controlNull]);
    };
};
