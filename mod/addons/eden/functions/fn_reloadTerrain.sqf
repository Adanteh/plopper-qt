/*
    Function:       ADA_Eden_fnc_reloadTerrain
    Author:         Adanteh
    Description:    Reloads the terrain
*/
#include "macros.hpp"

// -- We need this to work outside missions and between a map reload, so spawn in UInamespace
with (uiNamespace) do {
    [] spawn {
        disableSerialization;
        // -- Save the current settings
        private _currentWorld = worldName;
        private _displayName = getText (configFile >> "CfgWorlds" >> _currentWorld >> "displayName");
        private _cameraVectorDirAndUp = [vectorDir get3DENCamera, vectorUp get3DENCamera];
        private _cameraPos = getPos get3DENCamera;

        // -- Save the current mission
        do3DENAction "MissionSaveAs";
        private _textField = (findDisplay 314) displayCtrl 102;
        _textField ctrlSetText "ats_terrainview";
        ctrlActivate ((findDisplay 314) displayCtrl 1);

        // -- Switch to virtual reality
        do3DENAction "MissionNew";
        waitUntil { !(isNull (findDisplay 316)) };
        // -- Figure out which terrain is VR terrain
        private _islandList = ((findDisplay 316) displayCtrl 101);
        for "_i" from 0 to (lbSize _islandList - 1) do {
            private _terrainName = toLower (_islandList lbText _i);
            if (_terrainName find "virtual" != -1) exitWith {
                 _islandList lbSetCurSel _i;
            };
        };
        ctrlActivate ((findDisplay 316) displayCtrl 1);

        // -- Switch back to the previous terrain
        waitUntil { !(isNull (findDisplay 313)) };

        do3DENAction "MissionLoad";
        waitUntil { !(isNull (findDisplay 314)) };

        private _folderList = ((findDisplay 314) displayCtrl 101);
        private _missionList = ((findDisplay 314) displayCtrl 103);
        private _children = _folderList tvCount [0];
        for "_i" from 0 to (_children - 1) do {
            systemChat str (_folderList tvText [0, _i]);
        };

        {
            for "_j" from 0 to (lbSize _missionList - 1) do {
                private _missionName = toLower (_missionList lnbText [0, _i]);
                systemChat _missionName;
                if (_missionName == "ats_terrainview") exitWith {
                     _missionList lbSetCurSel _i;
                };
            };
        } forEach [];
        ctrlActivate ((findDisplay 314) displayCtrl 1);


        // -- Reapply the old camera settings
        move3DENCamera [_cameraPos, false];
        get3DENCamera setVectorDirAndUp _cameraVectorDirAndUp;
    };
};


// -- Load mission
with (uiNamespace) do {
    [] spawn {
        disableSerialization;

    };
};
