/*
    Function:       ADA_Eden_fnc_replaceByEntity
    Author:         Adanteh
    Description:    Replaces an object by an eden entity
*/
#include "macros.hpp"

params ["_object"];

private _objectClass = typeOf _object;
[format ["%1: %2 [%3]", _fnc_scriptNameShort, _object, _objectClass], "purple", 5, 50] call CFUNC(debugMessage);
if (_objectClass != "") then {
    private _pos = getPosATL _object;
    private _scale = _object getVariable [QMVAR(Scale), 1];
    private _vectorDirAndUp = [vectorDir _object, vectorUp _object];
    private _entity = create3DENEntity ["object", _objectClass, _pos, true];

    // -- Replace created objects by newly crated entity
    SP_var_createdObjects set [(SP_var_createdObjects find _object), _entity];

    private _rotation = [_object] call FUNC(calcPitchBankYaw);
    deleteVehicle _object;

    _entity setVectorDirAndUp _vectorDirAndUp;
    _entity set3DENAttribute ["rotation", _rotation];
    if (_scale != 1) then { _entity setVariable [QMVAR(Scale), _scale, true]; };
    _object = _entity;
    _object setVariable ["entity", true];
};

_object
