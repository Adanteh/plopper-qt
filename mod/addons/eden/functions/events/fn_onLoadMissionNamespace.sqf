/*
    Function:       ADA_Eden_fnc_onLoadMissionNamespace
    Author:         Adanteh
    Description:    Runs whenever an event eden event is run, with a fresh missionnamespaces. Invokes recompile of functions to missionNamespace and autoinit
*/
#include "macros.hpp"


// -- Not available till a mission is loaded in, so do it manually
{
    private _function = format [QCFUNC(%1), _x];
    private _code = uiNamespace getVariable [_function, {}];
    missionNamespace setVariable [_function, _code];
} forEach [
    "compile",
    "moduleLoad",
    "moduleQueue",
    "moduleQueueExecute"
];

// -- Load additional functions
{
    private _code = uiNamespace getVariable [_x, {}];
    missionNamespace setVariable [_x, _code];
} forEach [
    "py3_fnc_callExtension"
];


// -- Couple of events where new missionNamespace triggers: onInit, onTerrainNew, onMissionLoad, onMissionPreviewEnd and more
[""] call (uiNamespace getVariable QCFUNC(moduleLoad));
if !(MODULELOADED(MODULE)) exitWith { };

with missionNamespace do {
    ["preinit"] call (uiNamespace getVariable QCFUNC(moduleQueueExecute));
};
