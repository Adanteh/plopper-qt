/*
    Function:       ADA_Eden_fnc_onInit
    Author:         Adanteh
    Description:    Run when eden is started
*/
#include "macros.hpp"

if (isNull (findDisplay 313)) exitWith { };

disableSerialization;
private _display = findDisplay 313;

["addKeybinds.display", [_display]] call CFUNC(localEvent);
{
    _x params ["_event", "_code"];
    private _ehHandle = _display getVariable [QCVAR(keybind) + _event, -1];
    if (_ehHandle != -1) then {
        _display displayRemoveEventHandler [_event, _ehHandle];
    };
    _display setVariable [QCVAR(keybind) + _event, (_display displayAddEventHandler [_event, _code])];
} forEach [
    ["MouseButtonDown", (format ["if (_this select 1 == 0) then { call ADA_object_fnc_mouseDown; }; true"])],
    ["mouseButtonUp", (format ["if (_this select 1 == 0) then { call ADA_object_fnc_mouseUp; }; true"])]
];
