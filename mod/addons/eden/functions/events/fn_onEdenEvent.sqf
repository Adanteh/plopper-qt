/*
    Function:       ADA_Eden_fnc_onEdenEvent
    Author:         Adanteh
    Description:    Wrapper for all possible edden functions, so we don't need to reload config
*/
#include "macros.hpp"

params [["_event", ""], ["_args", []]];

if !(is3DEN) exitWith { };

private _functionsAvaiable = missionNamespace getVariable [QGVAR(init), false];
if !(_functionsAvaiable) then {
    missionNamespace setVariable [QGVAR(init), true];
    _this call (uiNamespace getVariable QFUNC(onLoadMissionNamespace));
};

if !(MODULELOADED(MODULE)) exitWith { };

_this call FUNC(onInit);

diag_log text format ["[%1] Eden event: '%2' - %3 in '%5' - %4", QUOTE(PREFIX), _event, _args, diag_tickTime, [currentNamespace] call (uiNamespace getVariable QCFUNC(namespaceName))];

switch (_event) do {
    case "onUndo": {

    };
    case "onRedo": {

    };
    case "onHistoryChange": {

    };
    case "onWidgetToggle": {

    };
    case "onWidgetNone": {

    };
    case "onWidgetTranslation": {

    };
    case "onWidgetRotation": {

    };
    case "onWidgetScale": {

    };
    case "onWidgetArea": {

    };
    case "onGridChange": {

    };
    case "onMoveGridToggle": {

    };
    case "onRotateGridToggle": {

    };
    case "onScaleGridToggle": {

    };
    case "onVerticalToggle": {

    };
    case "onSurfaceSnapToggle": {

    };
    case "onWorkspacePartSwitch": {

    };
    case "onModeChange": {

    };
    case "onSubmodeChange": {

    };
    case "onMapOpened": {

    };
    case "onMapClosed": {

    };
    case "onSearchEdit": {

    };
    case "onSearchCreate": {

    };
    case "onMissionListChange": {

    };
    case "onMissionPreview": {

    };
    case "onMissionPreviewEnd": {

    };
    case "init": { // -- This is the actual creation of display, and where we want to add keybinds
        _this call (uiNamespace getVariable QFUNC(onInit));
    };

    case "onTerrainNew": {

    };
    case "onMissionNew": {

    };
    case "onMissionLoad": {

    };
    case "onMissionAutosave": {

    };
    case "onMessage": {

    };
    case "onServerToggle": {

    };
    case "onEntityMenu": {

    };
    case "onSelectionChange": {

    };
    case "onConnectingStart": {

    };
    case "onConnectingEnd": {

    };
    case "onTogglePlaceEmptyVehicle": {

    };
};

["eden." + _event, [_args]] call CFUNC(localEvent);
