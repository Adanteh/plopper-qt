/*
    Function:       ADA_Eden_fnc_someFunction
    Author:         Adanteh
    Description:    Does a thing
*/
#include "macros.hpp"

params ["_data", "_keys"];
_data params _keys; //["_modelPath", "_pos", "_scale", "_pitchBankYaw"];

// -- Cache this shit
private _fnc_matchModelToClass = {
    params ["_modelPath"];
    private _class = GVAR(namespace) getVariable ["class.path#" + _modelPath, "##"];
    if (_class == "##") then {
        private _condition = format ["(getText (_x >> 'model') == '%1')", _modelPath];
        private _classes = _condition configClasses (configFile >> "CfgVehicles");
        _class = (_classes param [0, configNull]);
        _class = [configName _class, ""] select (isNull _class);
        GVAR(namespace) setVariable ["class.path#" + _modelPath, _class];
    };
    _class;
};

private _modelClass = [_modelPath] call _fnc_matchModelToClass;
if (_modelClass != "") then {
    [_modelClass, _pos, _pitchBankYaw, _layer] call FUNC(createEntity);
} else {
    [format ["'%1' doesn't have a cfgVehicle class, skipping"], "red"] call CFUNC(debugMessage);
};
