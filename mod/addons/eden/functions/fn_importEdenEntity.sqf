/*
    Function:       ADA_Eden_fnc_importEdenEntity
    Author:         Adanteh
    Description:    Does a thing
*/
#include "macros.hpp"

params ["_data", "_keys"];
_data params _keys; //["_modelPath", "_pos", "_scale", "_pitchBankYaw"];

// -- This is a workaround to patch old project files that use floats, instead of fixed strings
if ((_pos select 0) isEqualType "") then { _pos = _pos apply { parseNumber _x } };
if (_scale isEqualType "") then { _scale = parseNumber _scale };
if ((_pitchBankYaw select 0) isEqualType "") then { _pitchBankYaw = _pitchBankYaw apply { parseNumber _x } };

private _modelClass = [_modelPath] call CFUNC(getClassFromModel);
if (_modelClass != "") then {
    [_modelClass, _pos, _pitchBankYaw, _layer] call FUNC(createEntity);
} else {
    [format ["'%1' doesn't have a cfgVehicle class, skipping", _modelPath], "red", nil, -1] call CFUNC(debugMessage);
};
