/*
    Function:       ADA_Eden_fnc_createEntity
    Author:         Adanteh
    Description:    Creates eden entity
*/
#include "macros.hpp"

params ["_modelClass", "_pos", "_pitchBankYaw", ["_layer", ""]];
[[_fnc_scriptNameShort, _modelClass, _pos], "orange"] call CFUNC(debugMessage);


_pos set [2, (_pos select 2) - (getTerrainHeightASL _pos)];
private _entity = create3DENEntity ["object", _modelClass, _pos, true];
//_entity setPosWorld _pos;

[_entity, _pitchBankYaw] call CFUNC(setPitchBankYaw);
_entity set3DENAttribute ["rotation", _pitchBankYaw];
_entity set3DENAttribute ["pos", getPosATL _entity];


if (_layer != "") then {
    private _layerEden = GVAR(namespace) getVariable ["layer." + _layer, -1];
    if (_layerEden isEqualTo -1) then {
        _layerEden = -1 add3DENLayer _layer;
        GVAR(namespace) setVariable ["layer." + _layer, _layerEden];
    };
    _entity set3DENLayer _layerEden;
};
