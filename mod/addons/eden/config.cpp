#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main"};

			class clientInit;
			class clientInitWIP;
            class createEntity;
            class importEdenEntity;
			class reloadTerrain;
			class replaceByEntity;
			class someFunction;
			class updateEdenMouse;

			class Events {
				class onEdenEvent;
				class onInit;
				class onLoadMissionNamespace;
				class onLoadTerrain;
			};
		};
	};
};

class Cfg3DEN {
	#include "edenevents.hpp"
};

class RscDisplaySurfacePainterCamera;
class ScrollBar;


class ctrlMenuStrip;
class display3DEN {
	class Controls {
		class MenuStrip: ctrlMenuStrip {
			class Items {
				class Tools {
					items[] += {QADDON}; // += must be used; you want to expand the array, not override it!
				};
				class ADDON {
					text = "ObjectPlacement"; // Item text
					items[] = {QGVAR(Projects), QGVAR(Recompile)};
					picture = ICON(action,build);
				};
                class GVAR(Projects) {
                    text = "Project Load";
					action = "['projects', true, true, true, (findDisplay 313)] call ADA_Main_fnc_uiPaneOpen";
                };
				class GVAR(Recompile) {
					text = "Recompile Functions";
					action = "[''] call ADA_Core_fnc_moduleLoad";
				};
			};
		};
	};
};
