/*
    Function:       mod_loader.sqf
    Author:         Adanteh
    Description:    A custom function compiler for buldozer, requiring nothing outside of what's available in A3 Core
*/

#define F11 0x57

testvars = allVariables missionNamespace;

_keybinds = [] spawn {

    private _code = {
        _this execVM "plopper\key_handler.sqf";
        false
    };

    _code = str(_code);
	keybind_ada = (findDisplay -1) displayAddEventHandler ["KeyDown", (_code select [1, count _code -2])];
};


// -- Compile functions
private _preinit = [];
private _postinit = [];
private _prestart = [];

private _fnc_compile = {
    params ["_fnc_name", "_file"];

    private _code = compile preprocessFileLineNumbers _file;
    {
        _x setVariable [_fnc_name, _code];
        nil
    } count [missionNamespace, uiNamespace, parsingNamespace];
};


{
    private _addon = _x;
    private _tag = configName _addon;
    {
        private _module = _x;
        private _path = getText (_x >> "file");
        {
            private _function = _x;
            private _fnc_name = format ["%1_fnc_%2", _tag, configName _x];

            diag_log [_fnc_name];

            if (getNumber (_function >> "preinit") == 1) then {
                _preinit pushBack _fnc_name;
            };
            if (getNumber (_function >> "postinit") == 1) then {
                _postinit pushBack _fnc_name;
            };

            private _file = format ["%1\fn_%2.sqf", _path, configName _x];
            [_fnc_name, _file] call _fnc_compile;

        } forEach (configProperties [_module, "isClass _x", true]);
    } forEach (configProperties [_addon, "isClass _x", true]);
} forEach (configProperties [configFile >> "CfgFunctions", "isClass _x", true]);

{
    diag_log ["@Plopper preinit: ", _x];
    ["preinit"] call (missionNamespace getVariable _x);
} forEach _preinit;
{
    diag_log ["@Plopper postinit: ", _x];
    ["postinit"] call (missionNamespace getVariable _x);
} forEach _preinit;
