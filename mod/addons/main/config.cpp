#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Core)};
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core"};

            class addKeybind;
            class clientInit;
            class clientInitKeybinds;
            class clientInitPlopper;
            class getObjectData;
            class getSelectionData;
            class handleAxisButton;
            class modifierPressed;
            class someFunction;
            class startMod;
            class startTesting;

            class Camera {
                class camCreate;
                class camMouseBehaviour;
                class camMove;
                class camRotate;
                class clientInitCam;
                class getCamObject;
            };

            #include "functions\command\functions.hpp"
            #include "functions\events\functions.hpp"
            #include "functions\objects\functions.hpp"

            class Rotations {
                class getTerrainBuilderMatrix;
                class matrixVectorMultiply;
            };

            class UI {
                class Map {
                    class clientInitMap;
                    class clientInitMarkers;
                    // class mapMarkers;
                    class uiMapToggle;
                    class uiMapEvent;
                };

                class handleLoadingScreen;
                class uiHandleFocus;
                class uiCreateUI;
            };
        };
    };
};

#include "ui\ui.hpp"
#include "cfgMarkers.hpp"
