/*
    Function:       ADA_Main_fnc_modifierPressed
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_modifier"];
(GVAR(namespaceKeys) getVariable [_modifier, -1]) > time;
