/*
    Function:       ADA_Main_fnc_clientInitKeybinds
    Author:         Adanteh
    Description:    WIP Function
*/
#define __INCLUDE_DIK
#define __INCLUDE_IDCS
#include "macros.hpp"

GVAR(namespaceKeys) = false call CFUNC(createNamespace);

[
    "Object Mode", QSVAR(recompile),
    ["Recompile", "Recompiles all ADA functions"],
    { true },
    {
        [""] call CFUNC(moduleLoad);
        ["Recompiled all functions", "red", 5, 1] call CFUNC(debugMessage);
        true
    },
    [DIK_GRAVE, [true, false, false]], false, true
] call CFUNC(addKeybind);


["ui.onLoad", {
    ["addKeybinds.control", [__GUI_VIEWPORT]] call CFUNC(localEvent);
    ["addKeybinds.control", [__GUI_MAP]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

[
    "General", QSVAR(DeselectAll), ["Deselect All", "Deselects all selected objects"],
    { true },
    { [[]] call FUNC(selectObjects); false },
    [DIK_D, [false, true, false]], false, true
] call FUNC(addKeybind);

[
    "Buldozer", QSVAR(BDPlace), ["LMB Simulation", "Uses keybind for left mouse button hold"],
    { [true] call FUNC(onBdPlace); },
    { [false] call FUNC(onBdPlace); },
    [DIK_SPACE, [nil, nil, nil]], false, false
] call FUNC(addKeybind);

// -- Keybind system for holding some modifier keys shared between modules
[
    "Modifiers", QSVAR(Deselect), ["Deselect/Delete", "Removes from selection or delete point"],
    { GVAR(namespaceKeys) setVariable ["Deselect", time + 0.5]; false },
    { GVAR(namespaceKeys) setVariable ["Deselect", -1]; false },
    [DIK_LALT, [nil, nil, nil]], false, false
] call FUNC(addKeybind);
[
    "Modifiers", QSVAR(Selecting), ["Add Selection", "Adds to current selection"],
    { GVAR(namespaceKeys) setVariable ["Selecting", time + 0.5]; false },
    { GVAR(namespaceKeys) setVariable ["Selecting", -1]; false },
    [DIK_LCONTROL, [nil, nil, nil]], false, false
] call FUNC(addKeybind);
[
    "Modifiers", QSVAR(scrollModifier), ["Scroll Modifier", "Changes effect of scroll actions"],
    { GVAR(namespaceKeys) setVariable ["scrollModifier", time + 0.5]; false },
    { GVAR(namespaceKeys) setVariable ["scrollModifier", -1]; false },
    [DIK_LSHIFT, [nil, nil, nil]], false, false
] call FUNC(addKeybind);


[
    "Object Mode", QSVAR(SetTemplate), ["Set Template", "Sets current template from selected object"],
    { true },
    { [] call FUNC(setCurrentClass); false },
    [DIK_F, [false, false, false]], false, true
] call FUNC(addKeybind);

// -- Manipulation keybinds. These work as long as you have something selected
[
    "Object Mode", QSVAR(delete),
    ["Delete selected", "Deletes selected objects (If nothing selected, delete mouseover)"],
    { false },
    { [] call FUNC(deleteObjects); false },
    [DIK_DELETE, [nil, nil, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(scale),
    ["Scale selected", "Changes scale for selected (Drag mouse or scrollwheel)"],
    { ['scale', true] call FUNC(handleAxisButton); false },
    { ['scale', false] call FUNC(handleAxisButton); false },
    [DIK_R, [nil, false, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(xMove),
    ["Move along X-Axis", "Restricts movement to left/right only"],
    { ['axis.move.x', true] call FUNC(handleAxisButton); false },
    { ['axis.move.x', false] call FUNC(handleAxisButton); false },
    [DIK_X, [nil, false, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(yMove),
    ["Move along Y-Axis", "Restricts movement to front/back only"],
    { ['axis.move.y', true] call FUNC(handleAxisButton); false },
    { ['axis.move.y', false] call FUNC(handleAxisButton); false },
    [DIK_Y, [nil, false, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(raising),
    ["Raise selected", "Raises/lowers selected objects"],
    { ['axis.move.z', true] call FUNC(handleAxisButton); false },
    { ['axis.move.z', false] call FUNC(handleAxisButton); false },
    [DIK_LALT, [nil, nil, nil]], false, false
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(pitch),
    ["Tilt selected", "Changes pitch for selected"],
    { ['axis.rotate.x', true] call FUNC(handleAxisButton); false },
    { ['axis.rotate.x', false] call FUNC(handleAxisButton); false },
    [DIK_C, [nil, nil, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(bank),
    ["Roll selected", "Changes bank for selected"],
    { ['axis.rotate.y', true] call FUNC(handleAxisButton); false },
    { ['axis.rotate.y', false] call FUNC(handleAxisButton); false },
    [DIK_V, [nil, nil, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(rotate),
    ["Rotate selected", "Rotates selected objects"],
    { ["axis.rotate.z", true] call FUNC(handleAxisButton); false },
    { ["axis.rotate.z", false] call FUNC(handleAxisButton); false },
    [DIK_E, [nil, nil, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(precision),
    ["Precision", "Reduces effect of all manipulation options"],
    { ["precisionMode", true] call FUNC(handleAxisButton); false },
    { ["precisionMode", false] call FUNC(handleAxisButton); false },
    [DIK_SPACE, [nil, nil, nil]], false, true
] call FUNC(addKeybind);


[
    "Object Mode", QSVAR(aligntomouseover),
    ["Align to mouseover", "Aligns whatever axis button you have selected of selected to mouseover target"],
    { false },
    { ['aligntomouseover'] call FUNC(handleToolbarButtons); false },
    [DIK_B, [nil, nil, nil]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(Cut), ["Cut selected", "Cuts selected objects and puts it on clipboard"],
    { true },
    { ["cut"] call FUNC(cutCopyPaste); false },
    [DIK_X, [false, true, false]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(Copy), ["Copy selected", "Copies selected objects and puts it on clipboard"],
    { true },
    { ["copy"] call FUNC(cutCopyPaste); false },
    [DIK_C, [false, true, false]], false, true
] call FUNC(addKeybind);

[
    "Object Mode", QSVAR(Paste), ["Paste clipboard", "Pastes clipboard contents on mouse cursor location"],
    { true },
    { ["paste"] call FUNC(cutCopyPaste); false },
    [DIK_V, [false, true, false]], false, true
] call FUNC(addKeybind);

