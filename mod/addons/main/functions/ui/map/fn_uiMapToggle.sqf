/*
    Function:       ADA_Main_fnc_uiMapToggle
    Author:         Adanteh
    Description:    Toggles the map in our UI
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

[_fnc_scriptNameshort] call CFUNC(debugMessage);

//if (isNull __GUI_WINDOW) exitWith { };

params ["_show"];
if (isNil "_show") then {
    _show = !ctrlShown __GUI_MAP;
};

if (_show) then {
    __GUI_MAP ctrlShow _show;
    __GUI_MAP ctrlEnable _show;
    // -- Recenter the map
    __GUI_MAP mapCenterOnCamera true;
    __GUI_MAP mapCenterOnCamera false;
    ctrlSetFocus __GUI_MAP;
} else {
    __GUI_MAP ctrlShow _show;
    __GUI_MAP ctrlEnable _show;
};
