/*
    Function:       ADA_Main_fnc_uiMapEvent
    Author:         Adanteh
    Description:    Toggles the map in our UI
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params [["_mode", "keyDown"], ["_args", []]];


switch (toLower _mode) do {

    case "onload": {
        _args params ["_map"];
        [QMVAR(mapLoad), [_map]] call CFUNC(localEvent);
    };

    // -- Teleport (Normal camera, alt to p)
    case "onmousebuttondblclick": {
        //[[_fnc_scriptNameShort, _mode, _args], "orange", 5] call CFUNC(debugMessage);

        _args params ["_map", "", "_xPos", "_yPos"];
        private _worldPos = _map ctrlMapScreenToWorld [_xPos, _yPos];
        // -- If Alt double click is used teleport player]
        if ([DIK_LALT] call CFUNC(keyPressed)) then {
            player setPos _worldPos;
        } else {
            private _camera = call FUNC(getCamObject);
            private _currentHeight = (getPosATL _camera) select 2;
            _worldPos set [2, _currentHeight];
            _camera setPosATL _worldPos;
        };
    };
};
