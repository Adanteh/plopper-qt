/*
    Function:       ADA_Main_fnc_clientInitMarkers
    Author:         Adanteh
    Description:    Handles updating markers for player location and camera location + target
*/
#include "macros.hpp"

[{
    {
        // -- Update the marker for the player
        private _centerUnit = _x;
        private _markerTag = [_centerUnit] call CFUNC(name);


        private _markerName = format [QGVAR(playerMarker_%1), _markerTag];
        if !([_markerName] call CFUNC(markerExists)) then {
            _markerName = createMarker [_markerName, getPosWorld _centerUnit];
            _markerName setMarkerColor "ColorBlack";
            _markerName setMarkerText _markerTag;
            _markerName setMarkerShape "ICON";
            _markerName setMarkerType QGVAR(markerMan);
        };
        _markerName setMarkerPos (getPosWorld _centerUnit);
        _markerName setMarkerDir (getDir _centerUnit);
    } forEach allUnits;

    {
        _x params ["_camera", "_markerTag"];
        if !(isnull _camera) then {
        // -- Update the marker for the camera
            private _markerName = format [QGVAR(camMarker_%1), _markerTag];
            if !([_markerName] call CFUNC(markerExists)) then {
                _markerName = createMarker [_markerName, getPosWorld _camera];
                _markerName setMarkerColor "ColorBlue";
                _markerName setMarkerText (format ["Cam %1", _markerTag]);
                _markerName setMarkerShape "ICON";
                _markerName setMarkerType QGVAR(markerCam);
            };
            _markerName setMarkerPos (getPosWorld _camera);
            _markerName setMarkerDir (getDir _camera);
        };

    } forEach GVAR(cameras);
}, 0.5] call CFUNC(addPerFrameHandler);
