/*
    Function:       ADA_Main_fnc_clientInitMap
    Author:         Adanteh
    Description:    Toggles the map in our UI
*/
#include "macros.hpp"

// -- KEYBINDS
["ui.onLoad", {
    [false] call FUNC(uiMapToggle);
}] call CFUNC(addEventHandler);
