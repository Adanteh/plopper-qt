/*
    Function:       ADA_Main_fnc_mapMarkers
    Author:         Adanteh
    Description:    Does a thing
    Example:        ["init"] call ADA_Main_fnc_mapMarkers
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

params ["_mode", ["_args", []]];

switch (toLower _mode) do {
    case "init": {
        [QMVAR(mapLoad), {
            (_this select 0) params ["_map"];
            ["mapload", [_map]] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);

        ["object.created", {
            (_this select 0) params ["_object"];
            ["objectcreated", [_object]] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);

        ["object.predelete", {
            (_this select 0) params ["_object"];
            private _varname = _object getVariable [QMVAR(objectIndex), ""];
            if (_varname != "") then {
                [_varname] call CFUNC(markerRemoveGroup);
            };
        }] call CFUNC(localEvent);

        ["markerclick", {
            ["markerclick", _this select 0] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);

        ["map.LMB.drag.end", {
            ["mapdrag", _this select 0] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);

        ["selection.changed", {
            ["selection.changed", _this select 0] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);

        ["selection.reset", {
            ["selection.reset", _this select 0] call MFUNC(mapMarkers);
        }] call CFUNC(addEventHandler);
    };

    case "mapload": {
        _args params ["_map"];
        [_map] call CFUNC(markerRegisterMap);
        [_map] call CFUNC(registerMapClick);
    };

    case "objectcreated": {
        _args params ["_object"];

        private _varname = _object getVariable [QMVAR(objectIndex), ""];
        if (_varname isEqualTo "") exitWith { };


        private _templateName = [_object] call MFUNC(getTemplateName);
        private _data = EGVAR(library,libraryTML) getVariable [_templateName, []];
        if (_data isEqualTo []) exitWith { };

        _data params ["", "", "", "_colorNormal", "_rectangle"];

        (boundingBoxReal _object) params ["_p1", "_p2"];
        private _maxWidth = abs ((_p2 select 0) - (_p1 select 0));
        private _maxLength = abs ((_p2 select 1) - (_p1 select 1));
        private _colorHover = [0.56, 0.56, 0.56, 0.95];
        private _shape = ['ellipse', 'rectangle'] select _rectangle;

        [
            _varname,
            [[
                _shape,
                _object,
                _maxWidth * 0.5,
                _maxLength * 0.5,
                _object,
                [1, 1, 1, 1],
                _colorNormal
            ]]
        ] call CFUNC(markerAddGroup);

        private _colorString = format ["#(rgb,8,8,3)color(%1,%2,%3,%4)", _colorNormal select 0, _colorNormal select 1, _colorNormal select 2, _colorNormal param [3, 1]];
        _object setVariable [QMVAR(colorString), _colorString];

        [_varname + "_hoverout", {
            ["hoverout", _this select 0, _this select 1] call MFUNC(mapMarkers);
        }, [_object, _colorNormal, _colorHover], CVAR(markerNamespaceEvents)] call CFUNC(addEventHandler);

        [_varname + "_hoverin", {
            ["hoverin", _this select 0, _this select 1] call MFUNC(mapMarkers);
        }, [_object, _colorNormal, _colorHover], CVAR(markerNamespaceEvents)] call CFUNC(addEventHandler);
    };

    case "hoverin": {
        _args params ["_map", "_xPos", "_yPos"];
        params ["_object", "_colorNormal", "_colorHover"];
        ["debugMessage", [[_fnc_scriptNameShort, "hoverin", _args, _object], "blue"]] call CFUNC(localEvent);
    };

    case "hoverout": {
        _args params ["_map", "_xPos", "_yPos"];
        params ["_object", "_colorNormal", "_colorHover"];
        ["debugMessage", [[_fnc_scriptNameShort, "hoverout", _args, _object], "blue"]] call CFUNC(localEvent);
    };

    case "selection.reset": {
        _args params ["_selection"];
        {
            private _varname = _x getVariable [QMVAR(ObjectIndex), ""];
            if (_varname != "") then {
                _colorString = _x getVariable [QMVAR(colorString), "#(rgb,8,8,3)color(1,0.5,0,0.5)"];
                ((CVAR(markerNamespace) getVariable _varName) select 1 select 0) set [6, _colorString];
            };
        } forEach _selection;
    };

    case "selection.changed": {
        _args params ["_selection"];
        {
            private _varname = _x getVariable [QMVAR(ObjectIndex), ""];
            if (_varname != "") then {
                ((CVAR(markerNamespace) getVariable _varName) select 1 select 0) set [6, "#(rgb,8,8,3)color(0.56,0.56,0.56,0.95)"];
            };
        } forEach _selection;
    };

    case "markerclick": {
        _args params ["_marker"];

        private _object = MVAR(namespaceObjects) getVariable [_marker, objNull];
        if (isNull _object) exitWith {
            [[]] call MFUNC(selectObjects);
        };

        private _addToSelection = ["Selecting"] call MFUNC(modifierPressed);
        [[_object], _addToSelection] call MFUNC(selectObjects);
    };

    case "mapdrag": {
        _args params ["_ctrl", "", "_startPosWorld", "", "_endPosWorld"];

        private _size = _startPosWorld vectorDiff _endPosWorld apply { (abs _x) / 2 };
        private _center = _startPosWorld vectorAdd _endPosWorld apply { _x / 2 };
        private _array = [] call MFUNC(getObjectsPlaced);
        private _objects = _array inAreaArray [_center, _size select 0, _size select 1, 0, true, -1];

        private _addToSelection = ["Selecting"] call MFUNC(modifierPressed);
        [_objects, _addToSelection] call MFUNC(selectObjects);
    };
};
