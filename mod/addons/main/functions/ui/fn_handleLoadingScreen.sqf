/*
    Function:       ADA_Main_fnc_handleLoadingScreen
    Author:         Adanteh
    Description:    Handles loading screen. Show logo, do other fancy things
    Example:        ["start"] call ADA_Main_fnc_handleLoadingScreen;
*/
#include "macros.hpp"

params [["_mode", ""], ["_args", []]];

switch (toLower _mode) do {
    case "start": {
        _args params [["_name", "default"], ["_loadingScreenClass", QGVAR(CtrlLoadingScreen)], ["_loadingText", "Loading!"]];
        startLoadingScreen [_loadingText, _loadingScreenClass];
    };

    case "end": {
        _args params [["_name", "default"]];
        endLoadingScreen;
    };
};
