/*
    Function:       ADA_Main_fnc_uiCreateUI
    Author:         Adanteh
    Description:    Creates the basic UI for ObjectPlacement
    Example:        [] call ADA_Main_fnc_uiCreateUI;
*/
#include "macros.hpp"



if (GVAR(buldozer)) then {
    private _display = findDisplay -1;
    ["addKeybinds.display", [_display]] call CFUNC(localEvent);
    uiNamespace setVariable [QSVAR(interface), _display];
    systemChat str(_display);
    // private _cfg = (configFile >> QSVAR(Interface) >> "ControlsBackground" >> "MouseArea");
    // {
    //     private _function = getText (_cfg >> _x);
    //     if (_function != "") then {
    //         private _eh = _x select [2, count _x - 1];
    //         private _handle = _display displayAddEventHandler [_eh, _function];
    //         diag_log [_eh, _function, _handle];
    //     };
    // } foreach [
    //     // "onMouseMoving",
    //     // "onMouseButtonDown",
    //     // "onMouseButtonUp",
    //     // "onMouseButtonDblClick",
    //     // "onMouseZChanged"
    // ];

    if !(MVAR(started)) then {
        [{
            private _cursorPos = getPosASL GVAR(bdCursor);
            if !(_cursorPos isEqualTo (GVAR(namespace) getVariable ["bdCursorPos", [0, 0, 0]])) then {
                GVAR(namespace) setVariable ["bdCursorPos", _cursorPos];
                setMousePosition [0.5, 0.5];
                [findDisplay -1, 0.5, 0.5] call FUNC(onMouseMoving);
            };
        }] call CFUNC(addPerFrameHandler);
    };
    // _display displayAddEventHandler ["keydown", "systemChat str _this"];
    // _display displayAddEventHandler ["mousezchanged", "systemChat str _this"];

    // _display displaySetEventHandler ["MouseButtonDown", "systemChat str _this"];
    // ["ui.onLoad"] call CFUNC(localEvent);
} else {
    createDialog QSVAR(Interface);
};
