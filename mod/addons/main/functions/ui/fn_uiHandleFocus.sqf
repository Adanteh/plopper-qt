/*
    Function:       ADA_Main_fnc_uiHandleFocus
    Author:         Adanteh
    Description:    Handles setting focus from and to viewport, because none of these events made by BIS actually work
*/
#include "macros.hpp"

params [["_mode", "enter"]];

switch (toLower _mode) do {
    case "enter": {
        ["ui.view.mousein"] call CFUNC(localEvent);
    };

    case "exit": {
        ["ui.view.mouseout"] call CFUNC(localEvent);
    };
};
