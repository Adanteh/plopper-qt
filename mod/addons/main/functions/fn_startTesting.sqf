/*
    Function:       ADA_Main_fnc_startTesting
    Author:         Adanteh
    Description:    Does some tests to see if the pythia modules are loading okay
*/
#include "macros.hpp"

private _returnCode = ["plopper_install.test"] call py3_fnc_callExtension;

if !(_returnCode isEqualTo 0) exitWith {
    ["Error in loading Python backend, check .rpt for details", "red", 30, -1] call CFUNC(debugMessage);

    for "_i" from 0 to 3 do {
        diag_log ["#########   ", ADDONNAME, "   #########"];
    };
    diag_log ["#########   ", "ERROR START", "   #########"];

    private _errorMessage = _returnCode;
    if (_errorMessage isEqualTo []) then {
        _errorMessage = "No return from Pythia call, usually means BattleEye is blocking extension, relaunch without BE";
    };

    diag_log _errorMessage;
    [_errorMessage, "red", 30, -1] call CFUNC(debugMessage);
    diag_log ["#########   ", "ERROR END", "   #########"];
    true;
};


true;
