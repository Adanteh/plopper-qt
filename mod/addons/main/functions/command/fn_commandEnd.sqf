#include "macros.hpp"
/*
   Function:       ADA_main_fnc_commandEnd
   Author:         Adanteh
   Description:    
*/

params ["_command", ["_objects", []]];

private _data = [];
switch (_command) do {
    case "move": {
        _data = _objects apply { [_x getVariable QMVAR(ObjectIndex), getPosWorld _x] };
    };
};

["plopper.plop", ["command", _command, _data]] call PY3_fnc_callExtension;