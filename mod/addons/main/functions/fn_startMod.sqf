/*
    Function:       ADA_Main_fnc_startMod
    Author:         Adanteh
    Description:    Starts the mod.
    Example:        [] call ADA_Main_fnc_startMod
*/
#include "macros.hpp"

// -- Test the very basic core of things to see waddap
params [["_mode", "arma"]];

private _success = [] call FUNC(startTesting);
if !(_success) exitWith { };

if (_mode == "buldozer") then {
    CVAR(disableHint) = true;
    MVAR(buldozer) = true;
    MVAR(bdCursor) = cameraOn;
};

[""] call CFUNC(moduleLoad);

[_mode] spawn {
    params ["_mode"];
    private ["_msg", "_progress"];

    if !(MVAR(started)) then {
        uiSleep 0.05;

        // Check if the requirements are installed
        ["plopper_install.install"] call PY3_fnc_callExtension;
        
        private _ticks = 0;
        private _array = [];

        waitUntil { 
            uiSleep 1;
            private _return = (["plopper_install.installed"] call PY3_fnc_callExtension);
            _ticks = _ticks + 1;
            _array resize (_ticks mod 5); 
            [format ["Installing requirements%1", (_array apply { " " }) joinString "."], true] call CFUNC(hint);
            
            _msg = _return select 1;
            _progress = _return select 0;
            (_progress >= 1) || (_progress == -1);
        };
    } else {
        _progress = 1;
    };



    if (_progress == -1) then {
        diag_log ["#########   ", "ERROR PLOPPER", "   #########"];
        systemChat _msg;
        diag_log _msg;
        [format ["Error with Plopper requirements install. %1. Please try manual install", _msg]] call CFUNC(hint);

    } else {
        if (GVAR(buldozer)) then {
            startLoadingScreen ["Loading"];
        } else {
            ["start"] call FUNC(handleLoadingScreen);
        };


        uiSleep 0.05;
        ["", true] call CFUNC(hint);
        [] call FUNC(uiCreateUI);

        private _running = ["plopper.running"] call PY3_fnc_callExtension;
        if (_running isEqualTo [true, true]) then {
            ["plopper.plop", ["ui.toggle_visibility", true]] call PY3_fnc_callExtension;
            ["plop.tool_changed", [MVAR(set_tool), ""]] call CFUNC(localEvent);
        } else {
            private _name = ["Arma 3", "Buldozer"] select GVAR(buldozer);
            ["plopper.start", [_name, _mode]] call PY3_fnc_callExtension;
            ["plop.started"] call CFUNC(localEvent);
        };

        uiSleep 0.05;
        if (GVAR(buldozer)) then {
            endLoadingScreen;
        } else {
            ["end"] call FUNC(handleLoadingScreen);
        };

    };
};

