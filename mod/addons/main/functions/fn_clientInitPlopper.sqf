/*
    Function:       ADA_Main_fnc_clientInitPlopper
    Author:         Adanteh
    Description:    WIP Function
*/
#include "macros.hpp"

MVAR(set_tool) = "";
MVAR(plop_cache_handle) = -1;
MVAR(started) = false;

["plop.send_settings", {
    {
        ["plop.setting_changed", _x] call CFUNC(localEvent);
    } forEach (_this select 0);
}] call CFUNC(addEventHandler);

["plop.setting_changed", {
    (_this select 0) params ["_setting", "_value"];
    _setting = [_setting, "/", "_"] call CFUNC(replaceInString);
    missionNamespace setVariable [format [QGVAR(set_%1), _setting], _value];
}] call CFUNC(addEventHandler);


["plop.setting_changed", {
    (_this select 0) params ["_setting", "_value"];
    if (_setting == "debug/showmessages") then { CVAR(dbgLogLevel) = [4, 999] select _value }
}] call CFUNC(addEventHandler);


["plop.run_function", {
    (_this select 0) params ["_function", ["_args", []]];
    _args call ([_function] call CFUNC(parseToCode));
}] call CFUNC(addEventHandler);

["plop.open_window", {
    (_this select 0) params ["_window"];
    ['show', _window] call CFUNC(settingsWindow);
}] call CFUNC(addEventHandler);

["plop.class_changed", {
    (_this select 0) params ["_new"];
    GVAR(lastSelectedClass) = _new;

    ["object.mainclass.changed", [GVAR(lastSelectedClass)]] call CFUNC(localEvent);
}] call CFUNC(addEventHandler);

["plop.property_event", {
    (_this select 0) params ["_event"];
    [_event] call FUNC(handleToolbarButtons);
}] call CFUNC(addEventHandler);

["plop.add_keybind", {
    
    (_this select 0) params [
        "_category", 
        "_keybind",
        "_tag",
        "_down",
        "_up",
        ["_modifierRelease", true],
        ["_condition", { true }]
    ];
    [_category, _tag, "", _down, _up, _keybind, nil, _modifierRelease, _condition] call FUNC(addKeybind);
}] call CFUNC(addEventHandler);

["favorites.add_from_selection", { 
    private _data = [(_this select 0)] call MFUNC(getSelectionData);
    ["plopper.plop", ["favorites.add_from_selection", _data]] call PY3_fnc_callExtension;
}] call CFUNC(addEventHandler);

// This is our way of python sending things to Arma. We can't force arma to callExtension from python
// So instead we poll a python list every 0.1 seconds and treat them as event where needed
["plop.started", {
    MVAR(started) = true;
    uiNamespace setVariable [QGVAR(started), true];

    diag_log "Started event";
    if (MVAR(plop_cache_handle) isEqualTo -1) then {
        MVAR(plop_cache_handle) = [{
            private _eventCache = ["plopper.plop", ["cache_out"]] call PY3_fnc_callExtension;

            {
                _x params ["_event", ["_calls", []]];
                {
                    [_event, _x] call CFUNC(localEvent);
                } forEach _calls;
            } forEach _eventCache;
        }, 0.1] call CFUNC(addPerFrameHandler);
    };
}] call CFUNC(addEventHandler);

["plopper.quit", {
    [MVAR(plop_cache_handle)] call CFUNC(removePerFrameHandler);
    MVAR(plop_cache_handle) = 1;
}] call CFUNC(addEventHandler);

// -- This only triggers when we go back to editor. Does not trigger when clicking restart in preview
[{
    // -- Close it if mod was still running
    if (uiNamespace getVariable [QGVAR(started), false]) then {
        private _running = ["plopper.running"] call PY3_fnc_callExtension;
        if (_running isEqualTo [true, true]) then {
            ["plopper.deinit"] call (uiNamespace getVariable 'PY3_fnc_callExtension');
        };
    };


    (findDisplay 46) displayAddEventHandler ["unLoad", {
        if (MVAR(started)) then {
            with uiNamespace do {
                ["plopper.deinit"] call (uiNamespace getVariable 'PY3_fnc_callExtension');
            };
        };
    }];
}, { !(isNull (findDisplay 46)) }] call CFUNC(waitUntil);
