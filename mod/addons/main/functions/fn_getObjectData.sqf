#include "macros.hpp"
/*
    Function:       ADA_Main_fnc_getObjectData
    Author:         Adanteh
    Description:    Gets data from object, used for easy access from python
*/

params ["_obj", ["_tags", []]];

private _data = [];

{
    private _tag = toLower _x;
    private _value = switch (_tag) do {
        case "model": { (getModelInfo _obj) select 1 };
        case "scale": { 
            private _scale = _obj getVariable QMVAR(Scale);
            if (isNil "_scale") then {
                _scale = [_obj] call CFUNC(getObjectScale);
            };
            _scale
        };
        case "size": {  // size
            (boundingBoxReal _obj) params ["_size1", "_size2"];
            private _maxWidth = abs ((_size2 select 0) - (_size1 select 0));
            private _maxLength = abs ((_size2 select 1) - (_size1 select 1));
            (_maxWidth max _maxLength);
        };

        case "dir": {  // direction
            getDir _obj;
        };

        case "posw": { // getPosWorld
            (getPosWorld _obj)
        };

        case "posw_offset": {
            private _pos = (getPosWorld _obj);
            _pos set [2, (_pos select 2) - (boundingCenter _obj select 2)];
            _pos;
        };

        case "posw_t": { // Used to getPosATL, but in a way that's accurate when object is pitched/banked
            private _posW = (getPosWorld _obj);
            _posW set [2, (_posW select 2) - (getTerrainHeightASL _posW) - (boundingCenter _obj select 2)];
            _posW
        };

        case "pitchbank": { // Used to getPosATL, but in a way that's accurate when object is pitched/banked
            private _pitchBankYaw = [vectorDir _obj, vectorUp _obj] call CFUNC(vectorToPby);
            _pitchBankYaw
        };

        default {
            [["Unknown tag in getObjectData ", _tag], "red", 10, -1] call CFUNC(debugMessage);
            nil;
        }
    };

    if !(isNil "_value") then {
        _data pushBack [_tag, _value];
    };

} forEach _tags;

_data;