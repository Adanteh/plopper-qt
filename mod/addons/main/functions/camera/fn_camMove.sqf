/*
    Function:       ADA_Main_fnc_camMove
    Author:         Adanteh
    Description:    Handles camera movement
*/
#define __INCLUDE_DIK
#define __DEFAULTSTEP 0.10
#include "macros.hpp"

private _movementDirection = [0, 0, 0];
{
	_x params ["_actionKey", "_movement"];
	if ((GVAR(namespaceKeys) getVariable [_actionKey, -1]) > time) then {
		_movementDirection = _movementDirection vectorAdd _movement;
	};
} forEach [
	["cameraMoveForward", [0, 1, 0]],
	["cameraMoveBackward", [0, -1, 0]],
	["cameraMoveLeft", [-1, 0, 0]],
	["cameraMoveRight", [1, 0, 0]],
	["cameraMoveUp", [0, 0, 1]],
	["cameraMoveDown", [0, 0, -1]]
];

if (_movementDirection isEqualTo [0, 0, 0]) exitWith { };
(_movementDirection apply { (_x min 1) max -1}) params ["_dX", "_dY", "_dZ"];

private _cameraSpeed = GVAR(set_cam_speed);
private _camera = GVAR(namespace) getVariable ["cam.object", objNull];
private _camDir = getDir _camera;//(GVAR(namespace) getVariable ["cam.yaw", 0]);
private _camPosOld = getPosASL _camera;

if (["camSpeed"] call MFUNC(modifierPressed)) then {
	_cameraSpeed = _cameraSpeed * GVAR(set_cam_speed_modifier);
};

// -- Move camera faster at higher altitudes
private _terrainHeightOld = (getTerrainheightASL _camPosOld) max 1;
if (MVAR(set_cam_dynamicspeed)) then {
    private _modifier = ((0.5 + ((_camPosOld select 2) - _terrainHeightOld) * 0.1)) max 1;
	_cameraSpeed = _cameraSpeed * _modifier;
};

// -- This creates 'dolly' camera behaviour. Rather than W moving on a vertical plane, it'll move closer to whatever your looking at
private _camPos = if (MVAR(set_cam_dollymode)) then {
    _camera modelToWorldWorld ([_dx, _dY, 0] apply { _x * (_cameraSpeed * __DEFAULTSTEP) });
} else {
    [_camPosOld, ([_dx*-1, _dY, 0] apply { _x * (_cameraSpeed * __DEFAULTSTEP) }), _camDir] call CFUNC(calcRelativePosition);
};

// --
if (MVAR(set_cam_followterrain)) then {
	private _terrainHeightDifference = ((getTerrainHeightASL _camPos) max 0) - _terrainHeightOld;
	_camPos set [2, (_camPos select 2) + _terrainHeightDifference];
};

private _camPosTest = +_camPos;
if (GVAR(buldozer)) then {
    _camPos set [2, (_camPosOld select 2) + _dZ * (_cameraSpeed * __DEFAULTSTEP)];
} else {
    // -- Prevent camera from going below terrain level
    _camPos set [2, (_camPos select 2) max (getTerrainHeightASL _camPos) + _dZ * (_cameraSpeed * __DEFAULTSTEP)];
};


_camera setPosWorld _camPos;
// private _posASL = getPosASL _camera;
// _posASL set [2, 100];
// _camera setPosASL _posASL;

