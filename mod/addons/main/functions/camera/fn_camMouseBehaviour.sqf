/*
    Function:       ADA_Main_fnc_camMouseBehaviour
    Author:         Adanteh
    Description:    Handles the mouse behaviour (Right click hold to move camera around)
*/
#include "macros.hpp"

params [["_mode", "down"], ["_args", []]];

switch (toLower _mode) do {
    case "down": {
        _args params ["", "_key"];
        GVAR(namespace) setVariable ["mouse." + str _key, true];
    };

    case "up": {
        _args params ["", "_key"];
        GVAR(namespace) setVariable ["mouse." + str _key, false];
    };

    case "moving": {
        _args params ["", "_xPos", "_yPos"];
        private _newPos = [_xPos, _yPos];
        if (GVAR(namespace) getVariable ["mouse.1", false]) then {
            private _oldPos =+ (GVAR(namespace) getVariable ["mouse.pos", [0.5, 0.5]]);
            [_newPos, _oldPos] call FUNC(camRotate);
        };
        GVAR(namespace) setVariable ["mouse.pos", _newPos];
    };
};
