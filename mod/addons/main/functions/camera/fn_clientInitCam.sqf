/*
    Function:       ADA_Main_fnc_clientInitCamSettings
    Author:         Adanteh
    Description:    Inits some camera related things
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(camUpdatePFH) = -1;
if (isNil QGVAR(cameras)) then { GVAR(cameras) = [] }; // MP synced

["ui.onLoad", {
    if !(GVAR(buldozer)) then {
        player allowDamage false;
    };
}] call CFUNC(addEventHandler);

["ui.onUnload", {
    if !(GVAR(buldozer)) then {
        player switchCamera "Internal";
    };

    [GVAR(camUpdatePFH)] call CFUNC(removePerFrameHandler);
    GVAR(camUpdatePFH) = -1;
}] call CFUNC(addEventHandler);

// -- Create a loop for camera movement, needed for smooth movement as opposed to repeated trigger
["ui.onLoad", {
    [] call FUNC(camCreate);
    if (GVAR(camUpdatePFH) != -1) exitWith { };
    GVAR(camUpdatePFH) = [{ call FUNC(camMove); }, 0] call CFUNC(addPerFrameHandler);
}] call CFUNC(addEventHandler);


// -- Reset camera keys when mouseout
["ui.view.mouseout", {
    {
        GVAR(namespace) setVariable [_x, false];
    } forEach [
        "cameraMoveForward",
        "cameraMoveBackward",
        "cameraMoveLeft",
        "cameraMoveRight",
        "cameraMoveUp",
        "cameraMoveDown"
    ];
}] call CFUNC(addEventHandler);


// -- This moves the camera if you copy an object
["object.copied", {
    (_this select 0) params [["_object", objNull], ["_copyObject", objNull]];

    if (isNull _copyObject) exitWith { };
    if !(MVAR(set_movemoncopy)) exitWith { };

    private _camera = call FUNC(getCamObject);
    private _positionOffset = (getPosWorld _object) vectorDiff (getPosWorld _copyObject);
    private _newPos = (getPosWorld _camera) vectorAdd _positionOffset;
    _camera setPosWorld _newPos;
}] call CFUNC(addEventHandler);


// -- Camera keybinds
[
    "Camera", QSVAR(camSpeed), ["Speed Modifier", "Moves the camera faster"],
    { GVAR(namespaceKeys) setVariable ["camSpeed", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["camSpeed", -1]; MVAR(uiShown) },
    [DIK_LSHIFT, [nil, nil, nil]], false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveForward), ["Forward", "Moves the camera forward"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveForward", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveForward", -1]; MVAR(uiShown) },
    "cameraMoveForward", false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveBackward), ["Backward", "Moves the camera backward"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveBackward", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveBackward", -1]; MVAR(uiShown) },
    "cameraMoveBackward", false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveLeft), ["Left", "Moves the camera left"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveLeft", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveLeft", -1]; MVAR(uiShown) },
    "cameraMoveLeft", false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveRight), ["Right", "Moves the camera right"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveRight", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveRight", -1]; MVAR(uiShown) },
    "cameraMoveRight", false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveUp), ["Up", "Moves the camera up"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveUp", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveUp", -1]; MVAR(uiShown) },
    "cameraMoveUp", false, false
] call FUNC(addKeybind);
[
    "Camera", QSVAR(cameraMoveDown), ["Down", "Moves the camera down"],
    { GVAR(namespaceKeys) setVariable ["cameraMoveDown", time + 0.5]; MVAR(uiShown) },
    { GVAR(namespaceKeys) setVariable ["cameraMoveDown", -1]; MVAR(uiShown) },
    "cameraMoveDown", false, false
] call FUNC(addKeybind);



