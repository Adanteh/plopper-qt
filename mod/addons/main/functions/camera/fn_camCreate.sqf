/*
    Function:       ADA_Main_fnc_camCreate
    Author:         Adanteh
    Description:    Creates the camera
*/
#include "macros.hpp"

// -- Use 3den camera if in eden mode
if (is3DEN) exitWith {
    _camera = get3DENCamera;
    GVAR(namespace) setVariable ["cam.object", _camera];
    _camera
};

if (GVAR(buldozer)) exitWith {
    _camera = GVAR(bdCursor);
    GVAR(namespace) setVariable ["cam.object", _camera];

    [[_fnc_scriptNameShort, _camera]] call CFUNC(debugMessage);
    _camera
};

// -- Use existing camera
private _camera = GVAR(namespace) getVariable ["cam.object", objNull];
if (isNull _camera) then {
    private _camPos = (cameraOn) modelToWorld [0, 0, 50];
    _camera = "camera" camCreate _camPos;
    GVAR(namespace) setVariable ["cam.object", _camera];
    GVAR(cameras) pushBack [_camera, [player] call CFUNC(name)]; // -- setvar doesnt work, so use nested aray
    publicVariable QGVAR(cameras);
};

_camera switchCamera "Internal";
["ui.cameraswitched", [_camera]] call CFUNC(localEvent);
_camera
