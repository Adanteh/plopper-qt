/*
    Function:       ADA_Main_fnc_camRotate
    Author:         Adanteh
    Description:    Handles camera rotation (Mouse usage)
*/
#include "macros.hpp"
#define __ROTATESPEED 75

params ["_newPos", "_oldPos"];
_deltaX = ((_newPos select 0) - (_oldPos select 0)) * __ROTATESPEED;
_deltaY = ((_newPos select 1) - (_oldPos select 1)) * __ROTATESPEED;

private _camera = GVAR(namespace) getVariable ["cam.object", objNull];

// -- Yaw is rotation / direction. Pitch is rolling forward (Nose down)
private _yaw = ((GVAR(namespace) getVariable ["cam.yaw", 0]) + _deltaX);
private _pitch = ((GVAR(namespace) getVariable ["cam.pitch", 0]) - _deltaY) max -90 min 90;
if (_yaw > 360) then {
    _yaw = _yaw - 360;
};
if (_yaw >= 360) then {
    _yaw = _yaw % 360;
};
if (_yaw < 0) then {
    _yaw = 360 + _yaw % 360;
};

GVAR(namespace) setVariable ["cam.yaw", _yaw];
GVAR(namespace) setVariable ["cam.pitch", _pitch];

private _vectorDirAndUp = [_pitch, 0, _yaw] call CFUNC(calcVectorDirAndUp);
_camera setVectorDirAndUp _vectorDirAndUp;
