/*
    Function:       ADA_Main_fnc_getCamObject
    Author:         Adanteh
    Description:    Gets camera object that's currently used
*/
#include "macros.hpp"

(GVAR(namespace) getVariable ["cam.object", objNull]);
