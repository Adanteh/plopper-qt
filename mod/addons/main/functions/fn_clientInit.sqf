/*
    Function:       ADA_Main_fnc_clientInit
    Author:         Adanteh
    Description:    WIP Function
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
MVAR(uiShown) = false;
MVAR(buldozer) = false;
MVAR(bdCursor) = objNull;

// ["init"] call MFUNC(mapMarkers);
if (isNil QGVAR(namespaceObjects)) then {
    GVAR(namespaceObjects) = true call CFUNC(createNamespace);
    publicVariable QGVAR(namespaceObjects);
};


// -- Reset focus on viewport so we can move camera around
["ui.view.mousein", {
    ctrlSetFocus __GUI_VIEWPORT;
    ["plopper.plop", ["ui.set_focus"]] call PY3_fnc_callExtension;
    // There is some magic going on with this. It does work when we have selected our QDockWidgets
    // and do a mousein, but not when we selected sidebar, toolbar, menubar etc
}] call CFUNC(addEventHandler);


["plop.closeEvent", {
    if (MVAR(uiShown)) then {
        closeDialog 0;
    };
}] call CFUNC(addEventHandler);


["ui.onUnload", {
    ["plop.tool_changed", ["", MVAR(set_tool)]] call CFUNC(localEvent);
    ["plopper.plop", ["ui.toggle_visibility", false]] call PY3_fnc_callExtension;
    
    MVAR(uiShown) = false;
}] call CFUNC(addEventHandler);


["ui.onLoad", {
    MVAR(uiShown) = true;
}] call CFUNC(addEventHandler);