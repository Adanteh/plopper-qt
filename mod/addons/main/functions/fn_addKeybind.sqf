/*
    Function:       ADA_Main_fnc_addKeybind
    Author:         Adanteh
    Description:    Passthrough function for Plopper keybinds, will add UI open condition
*/
#include "macros.hpp"
params ["_category", "_tag", "_displayName", "_downCode", "_upCode", "_keybindDefault", "_something", ["_modifierRelease", true], ["_condition", { true }]];

// private _conditionMerged = ["%1 && { %2 }", QMVAR(uiShown), _condition];
private _idd = [-1, 14001] select (isNil "bis_buldozer_cursor");
[_category, _tag, _displayName, _downCode, _upCode, _keybindDefault, _something, _modifierRelease, _condition, _idd] call CFUNC(addKeybind); 