/*
    Function:       ADA_Main_fnc_onBdPlace
    Author:         Adanteh
*/
#include "macros.hpp"

if (MVAR(buldozer)) then {
    params ["_down"];

    _pos = ASLToAGL (getPosASL MVAR(bdCursor));
    setMousePosition (worldToScreen _pos);

    private _return = false;
    if (_down) then {
        if (GVAR(namespaceMouse) getVariable ["bdPlaceDown", false]) exitWith { };

        GVAR(namespaceMouse) setVariable ["bdPlaceDown", true];
        // private _args = [0, [0.5, 0.5], _pos]; // This just simulates left mouse, so pass button 0
        // _return = ["bdPlaceStart", _args] call FUNC(callEvents);
        private _mouseArgs = [nil, 0];
        _return = _mouseArgs call FUNC(onMouseButtonDown);
        if (isNil "_return") exitWith { _return = true };
    } else {
        GVAR(namespaceMouse) setVariable ["bdPlaceDown", false];
        // private _args = [0, [0.5, 0.5], _pos, [0.5, 0.5], _pos]; // This just simulates left mouse, so pass button 0
        // _return = ["bdPlaceEnd", _args] call FUNC(callEvents);
        private _mouseArgs = [nil, 0];
        _return = _mouseArgs call FUNC(onMouseButtonUp);
        if (isNil "_return") exitWith { _return = true };
    };

    _return
} else {
    false
};

