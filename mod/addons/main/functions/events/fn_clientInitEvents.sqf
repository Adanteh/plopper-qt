/*
    Function:       ADA_Main_fnc_clientInitEvents
    Author:         Adanteh
    Description:    Handles running mode and tool events
*/
#include "macros.hpp"


/*
    Modes are unique, for example Object mode which controls single objects
    Brush mode which allows you to Brush
    Terrain mode which allows terrain manipulation and so on


    Tools are a tool for one of these modes and are shared between different modes, for example:
        - Quick setting (Easy options under a quick keykind)
        - Bounding box (Drawing lines around single objects)
*/

GVAR(namespaceMouse) = false call CFUNC(createNamespace);
GVAR(namespaceModeEvents) = false call CFUNC(createNamespace);
GVAR(namespaceToolEvents) = false call CFUNC(createNamespace);

["plop.tool_register", {
    (_this select 0) params ["_tool"];
    missionNamespace setVariable [format ["%1_%2_modeActive", QUOTE(PREFIX), _tool], false];
}] call CFUNC(addEventHandler);

["plop.tool_changed", {
    (_this select 0) params ["_newMode", "_oldMode"];

    if (_newMode != "") then {
        missionNamespace setVariable [format ["%1_%2_modeActive", QUOTE(PREFIX), _newMode], true];
    };
    if (_oldMode != "") then {
        missionNamespace setVariable [format ["%1_%2_modeActive", QUOTE(PREFIX), _oldMode], false];
    };
}] call CFUNC(addEventHandler);

["plop.tool_changed", {
    (_this select 0) params ["_newMode", "_oldMode"];

    private _toolsNew = getArray (configFile >> "ObjectPlacement" >> "Modes" >> _newMode >> "tools");
    private _toolsOld = getArray (configFile >> "ObjectPlacement" >> "Modes" >> _oldMode >> "tools");
    ["tools.changed", [_toolsNew, _toolsOld]] call CFUNC(localEvent);

    // -- Call deactivation events if we arent'switching to same tool (Might happen on UI restart)
    if (_newMode != _oldMode) then {
        private _events = GVAR(namespaceModeEvents) getVariable ["OnDeactivate", []];
        {
            _x call (uiNamespace getVariable _x);
        } forEach _events;
    };

    // -- Reset the namespace so we cleanup all events that might not exist for the new mode
    [GVAR(namespaceModeEvents)] call CFUNC(resetNamespace);

    // -- Readd all the events for the new mode, and add them by configName
    private _modeEvents = "true" configClasses (configFile >> "ObjectPlacement" >> "Modes" >> _newMode >> "events");
    {
        private _function = getText (_x >> "function");
        // [[_fnc_scriptNameShort, _newMode, configName _x], "orange", 2, 5] call CFUNC(debugMessage);
        if (_function != "") then {
            private _eventType = configName _x;
            private _currentEvents = GVAR(namespaceModeEvents) getVariable [_eventType, []];
            _currentEvents pushBackUnique _function;
            GVAR(namespaceModeEvents) setVariable [_eventType, _currentEvents];
        };
    } forEach _modeEvents;

    // -- Call activation events that were added
    private _events = GVAR(namespaceModeEvents) getVariable ["OnActivate", []];
    {
        _x call (uiNamespace getVariable _x);
    } forEach _events;
    
}] call CFUNC(addEventHandler);


["tools.changed", {
    (_this select 0) params ["_toolsNew", "_toolsOld"];
    private _toolsToActivate = _toolsNew - _toolsOld;
    private _toolsToDeactive = _toolsOld - _toolsNew;
    //[[_fnc_scriptNameShort, _toolsToActivate, _toolsToDeactive], "purple"] call CFUNC(debugMessage);

    {
        private _varName = format["%1.OnDeactivate", _x];
        private _events = GVAR(namespaceToolEvents) getVariable [_varName, []];
        //[[_fnc_scriptNameShort, _varName, _events], "blue"] call CFUNC(debugMessage);
        {
            _x call (uiNamespace getVariable _x);
        } forEach _events;
    } forEach _toolsToDeactive;

    [GVAR(namespaceToolEvents)] call CFUNC(resetNamespace);
    {
        private _tool = _x;
        private _toolEvents = "true" configClasses (configFile >> "ObjectPlacement" >> "Tools" >> _tool >> "Events");
        {
            private _function = getText (_x >> "function");
            if (_function != "") then {
                private _eventType = configName _x;
                // -- Activate and deactivate are fully unique and should be able to be triggered seperately
                if (toLower _eventType in ["onactivate", "ondeactivate"]) then {
                    _eventType = format ["%1.%2", _tool, _eventType];
                };
                private _currentEvents = GVAR(namespaceToolEvents) getVariable [_eventType, []];
                _currentEvents pushBackUnique _function;
                GVAR(namespaceToolEvents) setVariable [_eventType, _currentEvents];
            };
        } forEach _toolEvents;
    } forEach _toolsNew;

    // -- Call activation events
    {
        private _varName = format["%1.OnActivate", _x];
        private _events = GVAR(namespaceToolEvents) getVariable [_varName, []];
        {
            _x call (uiNamespace getVariable _x);
        } forEach _events;
    } forEach _toolsNew;


}] call CFUNC(addEventHandler);
