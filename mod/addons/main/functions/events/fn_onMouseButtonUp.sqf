/*
    Function:       ADA_Main_fnc_onMouseButtonUp
    Author:         Adanteh
*/
#include "macros.hpp"

params ["", "_button"];

private _return = [_fnc_scriptNameShort, _this] call FUNC(callEvents);
if ((isNil "_return") || { (_return isEqualTo false) }) then {
    ['up', _this] call FUNC(camMouseBehaviour);
};

private _mousePosition = getMousePosition;
private _mousePositionWorld = screenToWorld _mousePosition;
private _clickAt = GVAR(namespaceMouse) getVariable [__MOUSEVAR(Down), -1];
private _timeSincePress = diag_tickTime - _clickAt;
GVAR(namespaceMouse) setVariable [__MOUSEVAR(DownNow), false];

// -- Check double click
private _lastUp = GVAR(namespaceMouse) getVariable [__MOUSEVAR(Up), 0];
private _timeSinceUp = diag_tickTime - _lastUp;
GVAR(namespaceMouse) setVariable [__MOUSEVAR(Up), diag_tickTime];

// -- This is arma calling mouseButtonUp down when we clicked in Arma windowed mode OUTSIDE THE ARMA window
// This causes phantom clicks when refocussing arma, calling however many times we clicked outside arma
private _ignored = false;
if (_timeSincePress < 0.01 || (_timeSinceUp < 0.01)) then { _ignored = true; };

if (_ignored) exitWith {
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(Up), diag_tickTime + DOUBLECLICKTIME];
};

private _eventArgs = [_button, _mousePosition, _mousePositionWorld];
// -- Two quick releases means it's a double click
if (_timeSinceUp <= DOUBLECLICKTIME) exitWith {
    ["MouseDoubleClick", _eventArgs] call FUNC(callEvents);
    ["mouse.doubleClick", _eventArgs] call CFUNC(localEvent);
};

// -- It wasn't a mouse click, but a mouse hold (Don't run the normal clicking events)
if (_timeSincePress >= HOLDTIME) exitWith {
    ["end", _button] call FUNC(onMouseDrag);
};

["MouseClick", _eventArgs] call FUNC(callEvents);
["mouse.click", _eventArgs] call CFUNC(localEvent);

_return