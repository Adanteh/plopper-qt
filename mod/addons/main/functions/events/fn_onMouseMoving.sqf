/*
    Function:       ADA_Main_fnc_onMouseMoviving
    Author:         Adanteh
*/
#include "macros.hpp"

private _return = [_fnc_scriptNameShort, _this] call FUNC(callEvents);
if ((isNil "_return") || { (_return isEqualTo false) }) then {
    ['moving', _this] call FUNC(camMouseBehaviour);
};

// -- Detect dragging actions
{
    private _button = _x;
    private _held = GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownNow), false];
    if (_held) then {
        private _clickAt = GVAR(namespaceMouse) getVariable [__MOUSEVAR(Down), -1];
        private _timeSincePress = diag_tickTime - _clickAt;
        if (_timeSincePress >= HOLDTIME) then {
            ["moving", _button] call FUNC(onMouseDrag);
        };
    };
} forEach [0, 1];
