/*
    Function:       ADA_Main_fnc_callEvents
    Author:         Adanteh
    Description:    Calls stacked events for whichever tool and modes are actives
*/
#include "macros.hpp"

params ["_event", "_args"];

private "_return";
private _events = +(GVAR(namespaceModeEvents) getVariable [_event, []]);
_events append (GVAR(namespaceToolEvents) getVariable [_event, []]);
{
    _return = _args call (uiNamespace getVariable _x);
} forEach _events;

_return