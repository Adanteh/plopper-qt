/*
    Function:       ADA_Main_fnc_onMouseZChanged
    Author:         Adanteh
*/
#include "macros.hpp"

params ["", "_scrollAmount"];
[_this] call CFUNC(debugMessage);

private _return = [_fnc_scriptNameShort, _this] call FUNC(callEvents);

private _delta = (_scrollAmount / 1.2);  // -- This is 1.2 by default, which is annoying
if (__AXISRESTRAINT("axis.rotate.z")) then { // -- Shift
    // [[_fnc_scriptNameShort, _delta], "orange"] call CFUNC(debugMessage);
    [[_delta]] call FUNC(rotate);
};

if (__AXISRESTRAINT("axis.rotate.x")) then { // -- X
    [[_delta]] call FUNC(pitch);
};

if (__AXISRESTRAINT("axis.rotate.y")) then { // -- C
    [[_delta]] call FUNC(bank);
};

if (__AXISRESTRAINT("axis.move.z")) then { // -- Ctrl
    [[_delta]] call FUNC(changeHeight);
};

if (__AXISRESTRAINT("scale")) then { // -- Alt
    [[_delta]] call FUNC(scale);
};

if (__AXISRESTRAINT("axis.move.x")) then { // -- Ctrl
    [[_delta], [true, false]] call FUNC(moveScroll);
};

if (__AXISRESTRAINT("axis.move.y")) then { // -- Ctrl
    [[_delta], [false, true]] call FUNC(moveScroll);
};
