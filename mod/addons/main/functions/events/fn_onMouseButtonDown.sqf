/*
    Function:       ADA_Main_fnc_onMouseButtonDown
    Author:         Adanteh
*/
#include "macros.hpp"

// THIS DOES NOT TRIGGER ON MOUSE BUTTON DOUBLE CLICK (The second mousedown)
params ["_ctrl", "_button"];

private _outsideScreen = ({ _x < 0 } count getMousePosition) == 2;

if !(GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownNow), false]) then {
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(DownNow), true];
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(Down), diag_tickTime];
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(DownPos), getMousePosition];
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(DownPosWorld), screenToWorld getMousePosition];
    GVAR(namespaceMouse) setVariable [__MOUSEVAR(DragPosLast), getMousePosition];
};


private _lastUp = GVAR(namespaceMouse) getVariable [__MOUSEVAR(Up), 0];
private _timeSinceUp = diag_tickTime - _lastUp;

// -- This is arma calling mouseButtonUp down when we clicked in Arma windowed mode OUTSIDE THE ARMA window
// This causes phantom clicks when refocussing arma, calling however many times we clicked outside arma
private _ignored = false;
if (_timeSinceUp < 0.01) then { _ignored = true };
// private _buttonName = (["LEFT", "RIGHT"] select _button);
// [["DOWN", _button, diag_tickTime, "       ", _timeSinceUp], ["blue", "white"] select _ignored] call CFUNC(debugMessage);

if (_ignored) exitWith { };

private _return = [_fnc_scriptNameShort, _this] call FUNC(callEvents);
if ((isNil "_return") || { (_return isEqualTo false) }) then {
    ['down', _this] call FUNC(camMouseBehaviour);
};

_return