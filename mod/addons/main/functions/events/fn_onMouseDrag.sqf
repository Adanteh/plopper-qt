/*
    Function:       ADA_Main_fnc_onMouseDrag
    Author:         Adanteh
    Description:    Event for holding a mouse button and moving the mouse around
*/
#include "macros.hpp"

params [["_mode", "start"], ["_button", 0]];

switch (toLower _mode) do {
    case "start": {
        // -- Determine if we started this drag with a mouse on an object, if so, we want to move/rotate/scale items, if not then we want to create a selection box
        GVAR(namespaceMouse) setVariable ["mouseDragging", true];
        private _startPos = GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownPos), getMousePosition];
        private _startPosWorld = GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownPosWorld), screenToWorld _startPos];
        ["mouse.drag.start", [_button, _startPos, _startPosWorld]] call CFUNC(localEvent);

        private _return = ["MouseDragStart", [_button, _startPos, _startPosWorld]] call FUNC(callEvents);
    };

    case "moving": {
        private _alreadyDragging = GVAR(namespaceMouse) getVariable ["mouseDragging", false];
        if !(_alreadyDragging) then {
            ["start", _button] call FUNC(onMouseDrag);
        };
        private _posAGL = screenToWorld getMousePosition;
        GVAR(namespaceMouse) setVariable ["dragPosWorld", _posAGL];
    };

    case "end": {
        GVAR(namespaceMouse) setVariable ["mouseDragging", false];
        private _startPos = GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownPos), getMousePosition];
        private _startPosWorld = GVAR(namespaceMouse) getVariable [__MOUSEVAR(DownPosWorld), screenToWorld _startPos];
        private _endPos = getMousePosition;
        private _endPosWorld = screenToWorld _endPos;
        ["mouse.drag.end", [_button, _startPos, _startPosWorld, _endPos, _endPosWorld]] call CFUNC(localEvent);

        private _return = ["MouseDragEnd", [_button, _startPos, _startPosWorld, _endPos, _endPosWorld]] call FUNC(callEvents);
    };
};
