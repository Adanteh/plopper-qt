/*
    Function:       ADA_Main_fnc_setCurrentClass
    Author:         Adanteh
    Description:    Sets the current class from selection
*/
#include "macros.hpp"

private _selected = [] call FUNC(getSelectedObjects);
if (_selected isEqualTo []) exitWith { };

private _last = [_selected] call CFUNC(arrayPeek);
private _templateName = [_last] call MFUNC(getTemplateName);
[format ["Set class to '%1'", _templateName], "cyan", 3, -1] call CFUNC(debugMessage);

private _modelPath = (getModelInfo _last) select 1;
if (_modelPath != "") then {
    private _templateName = [_last] call MFUNC(getTemplateName);
    if !([_templateName, _modelPath] isEqualTo GVAR(lastSelectedClass)) then {
        ["plopper.plop", ["set_main_alt", _templateName, _modelPath]] call PY3_fnc_callExtension;
    };
};