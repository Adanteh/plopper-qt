#include "macros.hpp"
/*
   Function:       ADA_main_fnc_setObjectValue
   Author:         Adanteh
   Description:    Called by our object.value_change event. Allows changing values from Details window
*/

params ["_id", "_key", "_value"];

LOGTHIS;

private _obj = GVAR(namespaceObjects) getVariable [_id, objNull];
if (isNull _obj) exitWith {};


switch (_key) do {
    case "posx";
    case "posy";
    case "posz": {
        private _index = ["posx", "posy", "posz"] find _key;
        private _pos = getPosWorld _obj;
        _pos set [_index, _value];
        _obj setPosWorld _pos;
    };
    case "scale": { 
        _obj setVariable [QMVAR(scale), _value, true];
        [_obj] call EFUNC(toolBounding,boxScale);
    };
    case "pitch";
    case "bank";
    case "yaw": {
        private _index = ["pitch", "bank", "yaw"] find _key;
        private _pby = _obj getVariable [QMVAR(pitchBankYaw), [0, 0, 0]];
        _pby set [_index, _value];
        _obj setVariable [QMVAR(PitchBankYaw), _pby];
        [_obj, _pby] call CFUNC(setPitchBankYaw);
    };
};