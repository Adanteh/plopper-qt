/*
    Function:       ADA_Main_fnc_cutCopyPaste
    Author:         Adanteh
    Description:    Handles cut, copy and paste behaviour
*/
#include "macros.hpp"

params [["_mode", ""], ["_args", []]];

if (isNil QGVAR(namespaceCopy)) then {
    GVAR(namespaceCopy) = false call CFUNC(createNamespace);
};

switch (toLower _mode) do {
    case "cut";
    case "copy": {
        _args params [["_selection", []]];

        if (_selection isEqualTo []) then {
            _selection = call MFUNC(getSelectedObjects);
        };
        if (_selection isEqualTo []) exitWith {
            [LOCALIZE("NOTHING_TO_COPY"), "orange", 5, -1] call CFUNC(debugMessage);
        };

        GVAR(namespaceCopy) setVariable ["clipboard", _selection];
        GVAR(namespaceCopy) setVariable ["mode", _mode];
    };

    case "paste": {
        private _clipboard = GVAR(namespaceCopy) getVariable ["clipboard", []];
        private _pasteMode = GVAR(namespaceCopy) getVariable ["mode", _mode];
        if (_clipboard isEqualTo []) exitWith {
            [LOCALIZE("NOTHING_TO_PASTE"), "orange", 5, -1] call CFUNC(debugMessage);
        };

        private _currentPos = screenToWorld getMousePosition;
        private _centerPoint = [0, 0, 0];
        _clipboard apply { _centerPoint = _centerPoint vectorAdd (getPosWorld _x) };
        _centerPoint = _centerPoint vectorMultiply (1 / (count _clipboard));

        private _followTerrain = MVAR(set_followTerrain);
        private _keepHorizontal = !MVAR(set_slopeAlign);
        private _screenPos = (screenToWorld getMousePosition);
        _screenPos set [2, _centerPoint select 2];
        private _offset = _screenPos vectorDiff _centerPoint;
        [[_fnc_scriptNameShort, _offset], 'red'] call CFUNC(debugMessage);

        // -- Cutting will simply move the objects (Ignore terrain objects)
        if (_pasteMode == "cut") then {
            {
                private _oldPos = getPosWorld _x;
                private _newPos = _oldPos vectorAdd _offset;

                if (_followTerrain) then {
                    private _heightDifference = (getTerrainHeightASL _newPos) - (getTerrainHeightASL _oldPos);
                    _newPos set [2, (_oldPos select 2) + _heightDifference];
                };

                _x setPosWorld _newPos;
            } forEach (_clipboard select { [_x] call MFUNC(canEditObject) });
            GVAR(namespaceCopy) setVariable ["clipboard", []];

        // -- Copying will create a new object, with the same position offsets as the originals
        } else {
            private _objectsCreated = [];
            {
                private _oldPos = getPosWorld _x;
                _oldPos set [2, (_oldPos select 2) - (boundingCenter _x select 2)];
                private _newPos = _oldPos vectorAdd _offset;
                private _direction = getDir _x;
                private _modelPath = getModelInfo _x select 1;
                private _objectLayer = _x getVariable [QMVAR(ObjectLayer), ""];
                private _pitchBankYaw = +(_x getVariable [QMVAR(PitchBankYaw), [0, 0, _direction]]);
                private _scale = _x getVariable [QMVAR(Scale), 1];
                if (_followTerrain) then {
                    private _heightDifference = (getTerrainHeightASL _newPos) - (getTerrainHeightASL _oldPos);
                    _newPos set [2, (_oldPos select 2) + _heightDifference];
                };

                _objectsCreated pushBack ([
                    _modelPath,
                    _newPos,
                    _direction,
                    _scale,
                    _keepHorizontal,
                    _pitchBankYaw,
                    false,
                    [QMVAR(ObjectLayer), _objectLayer]
                ] call MFUNC(createObject));
            } forEach _clipboard;
            [_objectsCreated] call MFUNC(selectObjects);
        };
    };
};
