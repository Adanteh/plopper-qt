/*
    Function:       ADA_Main_fnc_getClassSelected
    Author:         Adanteh
    Description:    Gets the current selected class (Whatever is last clicked, whether it be library, favorites or used objects)
*/
#include "macros.hpp"

GVAR(lastSelectedClass);
