/*
    Function:       ADA_Main_fnc_getSelectedObjects
    Author:         Adanteh
    Description:    Gets currently selected
*/
#include "macros.hpp"

GVAR(namespace) getVariable ["selectedObjects", []];
