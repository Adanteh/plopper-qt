/*
    Function:       ADA_Main_fnc_canEditObject
    Author:         Adanteh
    Description:    Sees if you can edit this object (Locked, terrain object, whatever)
*/
#include "macros.hpp"

params ["_object"];
if ([_object] call CFUNC(isTerrainObject)) exitWith { false };
if (_object getVariable [QMVAR(ObjectLocked), false]) exitWith { false };
if (_object getVariable [QMVAR(ObjectHidden), false]) exitWith { false };

true;
