/*
    Function:       ADA_Main_fnc_detectUnderCursor
    Author:         Adanteh
    Description:    Gets the object under the cursor, closest towards the camera
*/
#include "macros.hpp"

params [["_mousePosition", getMousePosition], ["_camera", objNull]];

if (isNull _camera) then {
    _camera = call MFUNC(getCamObject);
};

private _worldPosASL = AGLtoASL (screenToWorld _mousePosition);
private _cameraPos = AGLtoASL positionCameraToWorld [0, 0, 0];
private _objects = lineIntersectsSurfaces [_cameraPos, _worldPosASL, _camera];

_objects = _objects select { !(_x select 2 isKindOf QSVAR(Bounding_box)) };
if (_objects isEqualTo []) exitWith { objNull };

private _lastObjectEntry = [_objects] call CFUNC(arrayPeek);
private _object = (_lastObjectEntry select 2);
private _underCursor = (_object getVariable ["localMoving", _object]); // -- If object found is local moving object, get the one it's connected to

//[format ["%1: %2", _fnc_scriptNameShort, _underCursor], "lime", 5, __DEBUGSELECT] call CFUNC(debugMessage);


_underCursor
