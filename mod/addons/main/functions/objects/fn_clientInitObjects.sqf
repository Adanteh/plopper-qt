#include "macros.hpp"
/*
    Function:       ADA_Main_fnc_clientInitObjects
    Author:         Adanteh
    Description:    Handles all objects for a project
*/

GVAR(objectsPlaced) = [];
GVAR(objectIndex) = 0;
GVAR(lastSelectedClass) = [];

// -- Keep an index of the client, always unique
if (isNil QGVAR(clientIndexServer)) then {
    GVAR(clientIndexServer) = 0;
} else {
    GVAR(clientIndexServer) = GVAR(clientIndexServer) + 1;
};

publicVariable QGVAR(clientIndexServer);
GVAR(clientIndex) = GVAR(clientIndexServer);

// -- Add to array
["object.created", {
    (_this select 0) params ["_object", "", "_client"];
    if !(_client isEqualTo player) exitWith { };

    GVAR(objectsPlaced) pushBackUnique _object;
}] call CFUNC(addEventHandler);


// -- Object delete call from Plop
["object.delete", {
    (_this select 0) params ["_ids"];
    {
        private _obj = GVAR(namespaceObjects) getVariable [_x, objNull];
        if !(isNull _obj) then {
            ["object.predelete", [_obj]] call CFUNC(globalEvent);
            deleteVehicle _obj;
        };
    } forEach _ids;
}] call CFUNC(addEventHandler);


// -- Remove from array
["object.predelete", {
    (_this select 0) params ["_object"];
    private _index = (GVAR(objectsPlaced) find _object);
    if (_index != -1) then {
        GVAR(objectsPlaced) deleteAt _index;
    };
}] call CFUNC(addEventHandler);


["object.value_change", {
    (_this select 0) call FUNC(setObjectValue);
}] call CFUNC(addEventHandler);


// -- Cleans up local mover (createVehicleLocal object we do attachTo) for local position workaround
["object.predelete", {
    (_this select 0) params ["_object"];
    private _localMover = _object getVariable ["localMover", objNull];
    if !(isNull _object) then {
        deleteVehicle _object;
    };
}] call CFUNC(addEventHandler);


// -- Used from import functions (Project module mostly)
["object.import", {
    if (is3DEN) exitWith { };

    (_this select 0) params ["_data", "_keys"];
    _data params _keys; //["_modelPath", "_pos", "_scale", "_pitchBankYaw"];
    if (_modelPath == "") exitWith { };
    _isHidden = _isHidden call CFUNC(parseToBool);
    _isLocked = _isLocked call CFUNC(parseToBool);

    // -- This is a workaround to patch old project files that use floats, instead of fixed strings
    if ((_pos select 0) isEqualType "") then { _pos = _pos apply { parseNumber _x } };
    if (_scale isEqualType "") then { _scale = parseNumber _scale };
    if ((_pitchBankYaw select 0) isEqualType "") then { _pitchBankYaw = _pitchBankYaw apply { parseNumber _x } };

    private _vars = [[QMVAR(ObjectLayer), _layerName], [QMVAR(ObjectHidden), _isHidden], [QMVAR(ObjectLocked), _isLocked]];
    private _object = [_modelPath, _pos, _yaw, _scale, false, _pitchBankYaw, false, _vars] call MFUNC(createObject);
    if (_isHidden) then {
        ["object.hideObject", [_object, true]] call CFUNC(localEvent);
    };
    if (_isLocked) then {
        ["object.lockObject", [_object, true]] call CFUNC(localEvent);
    };
    _importedObject = _object; // -- No private, we want this as return
}] call CFUNC(addEventHandler);


// -- Hidden
["object.hideObject", {
    (_this select 0) params ["_object", "_isHidden"];
    _object hideObject _isHidden;
}] call CFUNC(addEventHandler);


// -- Locked, will be ignored will all actions (Moving, deleting, etc)
["object.lockObject", {
    (_this select 0) params ["_object", "_isLocked"];
}] call CFUNC(addEventHandler);

// ["selection.changed", {
//     (_this select 0) params ["_objects", ["_add", false]];
//     private _obj_ids = (_objects apply { _x getVariable [QMVAR(ObjectIndex), ""] }) - [""];
//     ["plopper.plop", ["selection", _obj_ids]] call PY3_fnc_callExtension;
// }] call CFUNC(addEventHandler);

// -- Changes our main selected class/ has some extra stuff in it so it works for terrain objects too
// ["selection.changed", {
//     (_this select 0) params ["_objects", ["_add", false]];

//     if (_add) exitWith { };
//     if (count _objects == 0) exitWith { };

//     private _lastObject = [_objects] call CFUNC(arrayPeek);
//     if (isNull _lastObject) exitWith { };

//     // -- Get model and template name. If template name isn't set (Import/terrain) cache it
//     //    private _modelPath = (getModelInfo _lastObject) select 1;
//     //    if (_modelPath != "") then {
//     //        private _templateName = [_lastObject] call MFUNC(getTemplateName);
//     //        if !([_templateName, _modelPath] isEqualTo GVAR(lastSelectedClass)) then {
//     //            ["plopper.plop", ["set_main_alt", _templateName, _modelPath]] call PY3_fnc_callExtension;
//     //        };
//     //    };
// }] call CFUNC(addEventHandler);
