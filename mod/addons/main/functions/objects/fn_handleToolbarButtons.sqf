/*
    Function:       ADA_Main_fnc_handleToolbarButtons
    Author:         Adanteh
    Description:    Shared fucntion for random snips for code called from buttons in toolbar
*/
#include "macros.hpp"

params [["_mode", "start"]];

private _selection = (call MFUNC(getSelectedObjects)) select { [_x] call MFUNC(canEditObject) };
[[_fnc_scriptNameShort, _this], "cyan"] call CFUNC(debugMessage);

switch (toLower _mode) do {
    case "resetheight": {
        {
            private _pos = getPosWorld _x;
            _pos set [2, getTerrainHeightASL _pos + ((boundingCenter _x) select 2)];
            _x setPosWorld _pos;
        } forEach _selection;
    };

    case "alignheight": {
        private _heights = _selection apply { (getPosWorld _x select 2) - (boundingCenter _x select 2) };
        private _maxHeight = selectMax _heights;
        {
            private _pos = getPosWorld _x;
            _pos set [2, _maxHeight + ((boundingCenter _x) select 2)];
            _x setPosWorld _pos;
        } forEach _selection;
    };

    case "scalereset": {
        {
            _x setVariable [QMVAR(Scale), 1, true];
            [_x] call EFUNC(toolBounding,boxDelete);
        } forEach _selection;
    };

    // -- Will set the height to the height of mouseover
    case "aligntomouseover": {
        private _mouseOver = [nil] call MFUNC(detectUnderCursor);
        if (isNull _mouseOver) exitWith { };
        private _targetPos = getPosWorld _mouseOver;
        private _targetPitchBankYaw = _mouseOver getVariable [QMVAR(PitchBankYaw), [0, 0, getDir _mouseOver]];
        _targetPos set [2, (_targetPos select 2) - (boundingCenter _mouseOver select 2)];
        {
            private _pos = getPosWorld _x;
            if (__AXISRESTRAINT("axis.move.x")) then { _pos set [0, (_targetPos select 0)]; };
            if (__AXISRESTRAINT("axis.move.y")) then { _pos set [1, (_targetPos select 1)]; };
            if (__AXISRESTRAINT("axis.move.z")) then { _pos set [2, (_targetPos select 2) + ((boundingCenter _x) select 2)] };
            _x setPosWorld _pos;

            private _pitchBankYaw = _x getVariable [QMVAR(PitchBankYaw), [0, 0, getDir _x]];
            if (__AXISRESTRAINT("axis.rotate.x")) then { _pitchBankYaw set [0, (_targetPitchBankYaw select 0)] };
            if (__AXISRESTRAINT("axis.rotate.y")) then { _pitchBankYaw set [1, (_targetPitchBankYaw select 1)] };
            if (__AXISRESTRAINT("axis.rotate.z")) then { _pitchBankYaw set [2, (_targetPitchBankYaw select 2)] };

            [_x, _pitchBankYaw] call CFUNC(setPitchBankYaw);
            _x setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];
        } forEach (_selection - [_mouseover]);
    };

    case "alignhorizon": {
        [nil, nil, false] call FUNC(resetPitchBankYaw);
    };

    case "alignterrain": {
        [nil, nil, true] call FUNC(resetPitchBankYaw);
    };
};
