/*
    Function:       ADA_Main_fnc_resetPitchBankYaw
    Author:         Adanteh
    Description:    Does a thing
*/
#include "macros.hpp"

params ["", ["_selection", []], ["_toTerrain", false]];
if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };

{
    private _terrainObject = [_x] call CFUNC(isTerrainObject);
    if !(_terrainObject) then {

        if (_toTerrain) then {

            private _pitchBankYaw = _x getVariable [QMVAR(PitchBankYaw), [0, 0, 0]];
            private _vectorDirAndUp = [(getPosWorld _x), _pitchBankYaw select 2] call CFUNC(getTerrainVector);
            _x setVectorDirAndUp _vectorDirAndUp;
            _x setVariable [QMVAR(PitchBankYaw), _vectorDirAndUp call CFUNC(vectorToPby), true];
        } else {
            _x setDir (getDir _x);
            _x setVectorUp [0, 0, 1];
            _x setVariable [QMVAR(PitchBankYaw), [0, 0, getDir _x], true];
        };
    };
} forEach (_selection select { [_x] call MFUNC(canEditObject) });
