/*
    Function:       ADA_Main_fnc_scale
    Author:         Adanteh
    Description:    Changes scale for object
*/
#include "macros.hpp"
#define __MANIPULATIONMODIFIER 0.05

params [["_delta", [0, 0]], ["_selection", []]];

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };
if (count _delta > 1) then {
    _delta = (_delta select 0) * (0.5 + (_delta select 1));
} else {
    _delta = _delta select 0;
};

private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 10;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _strength = _strength / 10;
};

// -- Scale each object
{
    private _terrainObject = [_x] call CFUNC(isTerrainObject);
    if !(_terrainObject) then {
        private _scale = _x getVariable [QMVAR(Scale), 1];
        _scale = 0 max (_scale + (_delta * _strength));
        _x setVariable [QMVAR(Scale), _scale, true];
        [_x] call EFUNC(toolBounding,boxScale);
    };
} forEach (_selection select { [_x] call MFUNC(canEditObject) });
