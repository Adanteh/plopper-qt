/*
    Function:       ADA_Main_fnc_changeHeight
    Author:         Adanteh
    Description:    Changes height for an object
*/
#include "macros.hpp"
#define __MANIPULATIONMODIFIER 0.1

params [["_delta", [0, 0]], ["_selection", []]];

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };
if (count _delta > 1) then {
    _delta = -1*(_delta select 0) * (0.5 + abs(_delta select 1)* 3);
} else {
    _delta = _delta select 0;
};

if (_delta isEqualTo 0) exitWith { };
private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 10;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _strength = _strength / 10;
};

// -- Raise height for each object
{
    private _terrainObject = [_x] call CFUNC(isTerrainObject);
    if !(_terrainObject) then {
        private _pos = getPosWorld _x;
        _pos set [2, (_pos select 2) + (_delta * _strength)];
        _x setPosWorld _pos;
    };
} forEach (_selection select { [_x] call MFUNC(canEditObject) });
