#include "macros.hpp"
/*
    Function:       ADA_Main_fnc_rotateToPoint
    Author:         Adanteh
    Description:    Rotates / aka YAW the object on it's X plane
*/
#define __MANIPULATIONMODIFIER 5

params [["_delta", [0, 0]], ["_selection", []], ["_mode", "scroll"]];

if (_mode == "scroll") exitWith { };

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };

private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 9;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _strength = _strength / 10;
};

private _objectsToManipulate = _selection select { [_x] call MFUNC(canEditObject) };
private _followTerrain = MVAR(set_followTerrain);

// -- Grouped rotate means that you rotate around the middle of all selected objects, instead of each object around it's own axis
// -- This is the default behaviour, ungrouped is more for trees that you want to keep in place and rotate
private _centerPoint = [0, 0, 0];
private _groupedRotation = false;
if (count _objectsToManipulate > 1) then {
    if (MVAR(set_grouped_rotate)) then {
        _objectsToManipulate apply { _centerPoint = _centerPoint vectorAdd (getPosWorld _x ) };
        _centerPoint = _centerPoint vectorMultiply (1 / (count _objectsToManipulate));
        _centerPoint set [2, 0];
        _groupedRotation = true;
    };
};

// -- Figure out if we need to create a new starting point, or this is still the same drag action
private _dragIndex = EGVAR(object,namespace) getVariable ["drag.dragIndex", 0];
private _dragIndexCurrent = GVAR(namespace) getVariable ["drag.dragIndex.rotatehandled", -1];

[[_fnc_scriptNameShort, _dragIndex, _dragIndexCurrent], nil, nil, -1] call CFUNC(debugMessage);
private _newAction = false;
if !(_dragIndex isEqualTo _dragIndexCurrent) then {
    GVAR(namespace) setVariable ["drag.dragIndex.rotatehandled", _dragIndex];
    _newAction = true;
};


// -- Someone that is less retarded than I am (Adanteh) could probably clean this up
{
    #define __INDEX 2
    private _pitchBankYaw = _x getVariable [QMVAR(PitchBankYaw), [0, 0, 0]];

    // -- Figure around the point on which we want to rotate (Single object = offset, grouped object = middle point)
    private "_offsetArray";
    private _rotationPos = getPosWorld _x;

    if (_groupedRotation) then {
        _rotationPos = _centerPoint;
        _offsetArray = _x worldToModel _centerPoint;
    } else {
        private _templateName = [_x] call MFUNC(getTemplateName);
        private _data = [_templateName] call EFUNC(object,getClassData);
        _data params (call EFUNC(object,getDataStructure));
        _str = str [_templateName, _data];
        
        _offsetArray = [_rotationOffsetX, _rotationOffsetY, _rotationOffsetZ];
        if !(_offsetArray isEqualTo [0, 0, 0]) then {
            _rotationPos = _x modelToWorld _offsetArray;
        };
    };

    // -- If this is a new dragging action, figure out the object relative direction to the original mouse position, so we can use that as 0-point
    if (_newAction) then {
        private _startPosWorld = GVAR(namespace) getVariable ["drag.startPosWorld", screenToWorld getMousePosition];
        private _dirOffSetNew = [_rotationPos, (screenToWorld getMousePosition)] call CFUNC(getRelDir);
        _x setVariable [QGVAR(RotateMouseOffset), _dirOffSetNew];
        

    };

    private _dirOffsetOld = _x getVariable [QGVAR(RotateMouseOffset), 0];
    private _dirOffSetNew = ([_rotationPos, (screenToWorld getMousePosition)] call CFUNC(getRelDir));
    _x setVariable [QGVAR(RotateMouseOffset), _dirOffSetNew];

    private _yawAdjust = (_dirOffSetNew - _dirOffsetOld);
    private _newYaw = (_pitchBankYaw select __INDEX) + _yawAdjust;
    /* if (_newYaw > 360) then {
        _newYaw = _newYaw - 360;
    };
    if (_newYaw >= 360) then {
        _newYaw = _newYaw % 360;
    };
    if (_newYaw < 0) then {
        _newYaw = 360 + _newYaw % 360;
    }; */

    _pitchBankYaw set [__INDEX, _newYaw];

    // -- Rotate around an certain point, figuring out the new coordinates for our object
    if !(_offsetArray isEqualTo [0, 0, 0]) then {
        private _rotationPos = _x modelToWorld _offsetArray;
        private _centerPos = getPosWorld _x;
        _px = _centerPos select 0;
        _py = _centerPos select 1;
        _mpx = _rotationPos select 0;
        _mpy = _rotationPos select 1;
        _rpx = ( (_px - _mpx) * cos(_yawAdjust) ) + ( (_py - _mpy) * sin(_yawAdjust) ) + _mpx;
        _rpy = (-(_px - _mpx) * sin(_yawAdjust) ) + ( (_py - _mpy) * cos(_yawAdjust) ) + _mpy;
        private _newPos = [_rpx, _rpy, (_centerPos select 2)];
        if (_followTerrain) then {
            private _heightDifference = (getTerrainHeightASL _newPos) - (getTerrainHeightASL _centerPos);
            _newPos set [2, (_centerPos select 2) + _heightDifference];
        };
        _x setPosWorld _newPos;
    };

    [_x, _pitchBankYaw] call CFUNC(setPitchBankYaw);
    _x setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];

} forEach _objectsToManipulate;
