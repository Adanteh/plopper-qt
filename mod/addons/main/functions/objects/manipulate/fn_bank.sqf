/*
    Function:       ADA_Main_fnc_bank
    Author:         Adanteh
    Description:    Tilts the object left and right (Plane rolling around)
*/
#include "macros.hpp"
#define __MANIPULATIONMODIFIER 5

params [["_delta", [0, 0]], ["_selection", []]];

//[format ["%1: %2", _fnc_scriptName, _this], "magenta", 5, __DEBUGTOOL] call CFUNC(debugMessage);
if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };
if (count _delta > 1) then {
    _delta = (_delta select 0) * (0.5 + (_delta select 1));
} else {
    _delta = _delta select 0;
};

if (_delta isEqualTo 0) exitWith { };

private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 9;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _strength = _strength / 10;
};

private _slopeContactRotate = MVAR(set_slopecontact_rotate);
{

    if (_slopeContactRotate || { !(_x getVariable [QMVAR(SlopeLandContact), false]) }) then {
        #define __INDEX 1
        private _pitchBankYaw = _x getVariable [QMVAR(PitchBankYaw), [0, 0, 0]];
        _pitchBankYaw set [__INDEX, (_pitchBankYaw select __INDEX) + _delta * _strength];

        [_x, _pitchBankYaw] call CFUNC(setPitchBankYaw);
        _x setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];
    };

} forEach (_selection select { [_x] call MFUNC(canEditObject) });
