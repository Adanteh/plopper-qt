/*
    Function:       ADA_Main_fnc_moveScroll
    Author:         Adanteh
    Description:    Moves an object by scrolling (Axis usage only)
*/
#include "macros.hpp"
#define __MANIPULATIONMODIFIER 0.1

params [["_delta", [0, 0]], ["_axis", [false, false]]];

if (count _delta > 1) then {
    _delta = -1*(_delta select 0) * (0.5 + abs(_delta select 1)* 3);
} else {
    _delta = _delta select 0;
};

if (_delta isEqualTo 0) exitWith { };
private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 10;
};

[nil, nil, ([nil, nil, nil] apply { _delta * _strength }), _axis] call EFUNC(object,move);
