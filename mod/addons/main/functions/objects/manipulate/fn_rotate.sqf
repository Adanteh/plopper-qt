/*
    Function:       ADA_Main_fnc_rotate
    Author:         Adanteh
    Description:    Rotates / aka YAW the object on it's X plane
*/
#include "macros.hpp"
#define __MANIPULATIONMODIFIER 5

params [["_delta", [0, 0]], ["_selection", []], ["_mode", "scroll"]];

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };
if (count _delta > 1) then {
    _delta = (_delta select 0) * (0.5 + (_delta select 1));
} else {
    _delta = _delta select 0;
};

if (_delta isEqualTo 0) exitWith { };
private _strength = __MANIPULATIONMODIFIER;
if (__AXISRESTRAINT("scrollModifier")) then {
    _strength = _strength * 9;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _strength = _strength / 10;
};
private _objectsToManipulate = _selection select { [_x] call MFUNC(canEditObject) };
private _followTerrain = MVAR(set_followTerrain);

// -- Grouped rotate means that you rotate around the middle of all selected objects, instead of each object around it's own axis
private _centerPoint = [0, 0, 0];
private _groupedRotation = false;
if (count _objectsToManipulate > 1) then {
    if (MVAR(set_grouped_rotate)) then {
        _objectsToManipulate apply { _centerPoint = _centerPoint vectorAdd (getPosWorld _x ) };
        _centerPoint = _centerPoint vectorMultiply (1 / (count _objectsToManipulate));
        _centerPoint set [2, 0];
        _groupedRotation = true;
    };
};


// -- TODO: A cool thign could be relative direction, from start point to end point. So rather than all adjusting with the same difference, you instead all turn objects towards a point
// -- Rotate each object

{

    #define __INDEX 2
    private _pitchBankYaw = _x getVariable [QMVAR(PitchBankYaw), [0, 0, 0]];
    private _yawAdjust = _delta * _strength;
    _pitchBankYaw set [__INDEX, (_pitchBankYaw select __INDEX) + _yawAdjust];

    // -- Check rotation. If offset are all 0, just do ezpz rotate around center point. If not, then calculate new position

    private "_offsetArray";
    if (_groupedRotation) then {
        _offsetArray = (_x worldToModel _centerPoint);
    } else {
        private _templateName = [_x] call MFUNC(getTemplateName);
        private _data = [_templateName] call EFUNC(object,getClassData);
        private _keys = call EFUNC(object,getDataStructure);
        _data params _keys;
        _offsetArray = [_rotationOffsetX, _rotationOffsetY, _rotationOffsetZ];
    };

    // -- Rotate around an certain point, figuring out the new coordinates for our object
    if !(_offsetArray isEqualTo [0, 0, 0]) then {
        private _rotationPos = _x modelToWorld _offsetArray;
        private _centerPos = getPosWorld _x;
        _px = _centerPos select 0;
        _py = _centerPos select 1;
        _mpx = _rotationPos select 0;
        _mpy = _rotationPos select 1;
        _rpx = ( (_px - _mpx) * cos(_yawAdjust) ) + ( (_py - _mpy) * sin(_yawAdjust) ) + _mpx;
        _rpy = (-(_px - _mpx) * sin(_yawAdjust) ) + ( (_py - _mpy) * cos(_yawAdjust) ) + _mpy;
        private _newPos = [_rpx, _rpy, (_centerPos select 2)];
        if (_followTerrain) then {
            private _heightDifference = (getTerrainHeightASL _newPos) - (getTerrainHeightASL _centerPos);
            _newPos set [2, (_centerPos select 2) + _heightDifference];
        };
        _x setPosWorld _newPos;
    };

    [_x, _pitchBankYaw] call CFUNC(setPitchBankYaw);
    _x setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];

} forEach _objectsToManipulate;
