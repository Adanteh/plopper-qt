/*
    Function:       ADA_Main_fnc_getTemplateName
    Author:         Adanteh
    Description:    Gets template name for selected object (Base model name minus extension, cached for repeated call)
*/
#include "macros.hpp"

params ["_object"];
if (isNull _object) exitWith { "" };

private _templateName = _object getVariable QMVAR(TemplateName);
if (isNil "_templateName") then {
    _templateName = (((getModelInfo _object) select 0) splitString ".") select 0;
    _object setVariable [QMVAR(TemplateName), _templateName];
};

_templateName;
