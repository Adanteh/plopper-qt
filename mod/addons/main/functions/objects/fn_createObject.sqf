/*
    Function:       ADA_Main_fnc_createObject
    Author:         Adanteh
    Description:    Places an object

    Position is
*/
#include "macros.hpp"

// -- Expected position is ATL
params [
    ["_modelPath", "", [""]],
    ["_posASL", []],
    ["_direction", 0],
    ["_scale", 1],
    ["_keepHorizontal", true],
    ["_pitchBankYaw", []],
    ["_select", true],
    ["_extraVars", []]
];

[[_fnc_scriptNameShort, _modelPath], "orange", 1, 500] call CFUNC(debugMessage);

// -- TOD: Validate parameters here
if (_posASL isEqualTo []) then {
    _posASL = [0, 0, 0];
};

private _object = createSimpleObject [_modelPath, _posASL];
if (isNull _object) exitWith {
    [format ["%1: %2", LOCALIZE("COULDNT_CREATE"), _modelPath], "red", 5, -1] call CFUNC(debugMessage);
    _object
};

_posASL set [2, (_posASL select 2) + (boundingCenter _object select 2)];
_object setPosWorld _posASL; // -- Test if this is needed
_object enableSimulation false;

if !(_keepHorizontal) then {
    if (_pitchBankYaw isEqualTo []) then { // -- Only set to surface if no pitch,bank,yaw is given
        private _surfaceDirAndUp = [_posASL, _direction] call CFUNC(getTerrainVector);
        _pitchBankYaw = _surfaceDirAndUp call CFUNC(vectorToPby);
    };
};

if (_pitchBankYaw isEqualTo []) then {
    _pitchBankYaw = [0, 0, _direction];
};

_pitchBankYaw params [["_pitch", 0], ["_bank", 0], ["_yaw", 0]];

// -- Keep unique index for object, and save template name, because that won't ever change
private _templateName = (((getModelInfo _object) select 0) splitString ".") select 0;
private _objectIndex = format ["%1_%2", GVAR(clientIndex), GVAR(objectIndex)];
GVAR(objectIndex) = GVAR(objectIndex) + 1;

private _slopeLandContact = (namedProperties _object findIf { _x isEqualTo ["placement", "slopelandcontact"] }) >= 0;

// -- Don't rotate slopeLandContact items (unless we specifically mark that this is okay)
if (_slopeLandContact && { !(MVAR(set_slopecontact_rotate)) }) then {
    _pitchBankYaw set [0, 0];
    _pitchBankYaw set [1, 0];
};

_object setVariable [QMVAR(Scale), _scale, true];
_object setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];
_object setVariable [QMVAR(TemplateName), _templateName, true];
_object setVariable [QMVAR(ObjectIndex), _objectIndex, true];
_object setVariable [QMVAR(SlopeLandContact), _slopeLandContact, true];

GVAR(namespaceObjects) setVariable [_objectIndex, _object, true];

// -- Not a nested array
if !((_extraVars param [0, []]) isEqualType []) then { _extraVars = [_extraVars] };
{
    _x params ["_varName", "_value", ["_global", false]];
    _object setVariable [_varName, _value, _global];
} forEach _extraVars;

[_object, _pitchBankYaw] call CFUNC(setPitchBankYaw);
["object.created", [_object, _select, player]] call CFUNC(globalEvent);

_object
