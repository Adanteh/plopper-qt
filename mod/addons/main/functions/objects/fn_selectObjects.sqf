/*
    Function:       ADA_Main_fnc_selectObjects
    Author:         Adanteh
    Description:    Selects an object (or more than one)
*/
#include "macros.hpp"

params [["_objects", [], [[], objNull]], ["_add", false]];

//[[_fnc_scriptNameShort, _objects], 'olive'] call CFUNC(debugMessage);
// -- If a single object is given, save it as an array. We always want to select as arrays
if (_objects isEqualType objNull) then {
    _objects = [_objects];
};

// -- Delete current bounding boxes (Unless we are adding to selection)
if !(_add) then {
    ["selection.reset", [GVAR(namespace) getVariable ["selectedObjects", []]]] call CFUNC(localEvent);
    GVAR(namespace) setVariable ["selectedObjects", []];
};

["selection.changed", [_objects, _add]] call CFUNC(localEvent);

// -- Deselect all
if (_objects isEqualTo []) exitWith { };
if (_objects isEqualTo [objNull]) exitWith { };

// -- Create new bounding boxes
private _selection = call FUNC(getSelectedObjects);
{
    // -- Color bb based on state of object (Terrain, object, eden entity)
    ["selection.addobject", [_x]] call CFUNC(localEvent);
    _selection pushBack _x;
} forEach (_objects - _selection); // -- Filter out objects that are already in the selection

if (count _selection > 0) then {
    if (isMultiplayer) then {
        ["setOwner", [_selection, clientOwner]] call CFUNC(localEvent);
        ["debugMessage", [[_fnc_scriptNameShort, { local _x } count _selection], "red"]] call CFUNC(localEvent);
    };
};

GVAR(namespace) setVariable ["selection", _selection];
