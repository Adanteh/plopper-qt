/*
    Function:       ADA_Main_fnc_addExistingObjects
    Author:         Adanteh
    Description:    Goes through all placed objects in mission and adds the one that aren't in yet
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

private _objects = (allMissionObjects "") - GVAR(objectsPlaced);
private _count = 0;
{
    private _object = _x;

    if (true) then {
        if (isNull _object) exitWith { }; // fuck BIS, why is this in the return of allMissionObjects?
        if (isNil { getModelInfo _object }) exitWith { }; // lol BIS

        private _templateName = (((getModelInfo _object) select 0) splitString ".") param [0, "--"];
        private _inLibrary = ["plopper.plop", ["library.in_library", _templateName]] call PY3_fnc_callExtension;
        private _objectIndex = format ["%1_%2", GVAR(clientIndex), GVAR(objectIndex)];
        [[_object, _templateName], "orange", 10] call CFUNC(debugMessage);

        if !(_inLibrary) exitWith { };
        GVAR(objectIndex) = GVAR(objectIndex) + 1;

        private _scale = [_object] call CFUNC(getObjectScale);
        private _pitchBankYaw = [vectorDir _object, vectorUp _object] call CFUNC(vectorToPby);
        private _slopeLandContact = (namedProperties _object findIf { _x isEqualTo ["placement", "slopelandcontact"] }) >= 0;

        _object setVariable [QMVAR(Scale), _scale, true];
        _object setVariable [QMVAR(PitchBankYaw), _pitchBankYaw, true];
        _object setVariable [QMVAR(TemplateName), _templateName, true];
        _object setVariable [QMVAR(ObjectIndex), _objectIndex, true];
        _object setVariable [QMVAR(SlopeLandContact), _slopeLandContact];

        GVAR(namespaceObjects) setVariable [_objectIndex, _object, true];

        ["object.created", [_object, false]] call CFUNC(globalEvent);
        _count = _count + 1;

    };



} forEach _objects;

[format ["%1 objects added", _count], "green", 5, -1] call CFUNC(debugMessage);
