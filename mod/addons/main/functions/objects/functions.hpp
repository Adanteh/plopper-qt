class Objects {
    class addExistingObjects;
    class canEditObject;
    class clientInitObjects;
    class createObject;
    class cutCopyPaste;
    class deleteObjects;
    class detectUnderCursor;
    class getClassSelected;
    class getObjectsPlaced;
    class getSelectedObjects;
    class getTemplateName;
    class handleToolbarButtons;
    class selectObjects;
    class setCurrentClass;
    class setObjectValue;

    class Manipulate {
        class changeHeight;
        class moveScroll;
        class resetPitchBankYaw;
        class rotate;
        class rotateToPoint;
        class scale;
        class bank;
        class pitch;
    };
};