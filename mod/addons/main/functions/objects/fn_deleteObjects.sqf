/*
    Function:       ADA_Main_fnc_deleteObjects
    Author:         Adanteh
    Description:    Deletes selected objects, if no objects are selected deletes mouseover
*/
#include "macros.hpp"

private _selection = (call MFUNC(getSelectedObjects)) select { !(isNull _x) };
if (_selection isEqualTo []) then {
    private _mouseOver = [nil] call MFUNC(detectUnderCursor);
    if !(isnull _mouseOver) then {
        _selection = [_mouseOver];
    } else {
        _selection = get3DENselected "object";
    };
};

if !(isNil QEGVAR(toolGizmo,gizmoObject)) then {
    _selection = _selection - [EGVAR(toolGizmo,gizmoObject)];
};

{
    ["object.predelete", [_x]] call CFUNC(globalEvent); // Keep this in front, so we dont end up with null
    deleteVehicle _x;
} forEach (_selection select { [_x] call MFUNC(canEditObject) });
