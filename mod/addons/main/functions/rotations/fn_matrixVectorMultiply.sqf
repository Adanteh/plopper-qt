// [
//     [vector _rowX, vector _rowY, vector _rowZ] _matrix,
//     vector _vector
// ], vector
//
// Returns the product of the 3x3 "_matrix" to the vector "_vector".

// ianbanks: https://foxhound.international/sqf-snippets/export-arma-3-to-terrain-builder

params ["_matrix", "_v"];

_matrix params ["_x", "_y", "_z"];

[
    _x vectorDotProduct _v,
    _y vectorDotProduct _v,
    _z vectorDotProduct _v
]
