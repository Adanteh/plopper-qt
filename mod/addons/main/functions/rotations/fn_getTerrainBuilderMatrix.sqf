// [
//     number _rotationX,
//     number _rotationY,
//     number _rotationZ
// ], [vector _rowX, vector _rowY, vector _rowZ]
//
// Returns a 3x3 matrix using the rotation order and direction that is
// used by Terrain Builder.
//
// The coordinates and rotation axes match those used by "getPos" in
// game, not the Terrain Builder frame. In Terrain Builder,
// "Pitch (X): deg", "Roll (Z): deg" and "Yaw (Y): deg" match up to
// "_rotationX", "_rotationY" and "_rotationZ" in this function
// respectively.
//
// Given rotations from Terrain Builder, R * v where R is the orientation
// matrix returned by this function will change the object space
// vector (v) to world space.

// ianbanks: https://foxhound.international/sqf-snippets/export-arma-3-to-terrain-builder

params ["_x", "_y", "_z"];

[
    [cos _y * cos _z - sin _x * sin _y * sin _z, cos _z * sin _x * sin _y + cos _y * sin _z, cos _x * sin _y],
    [-cos _x * sin _z, cos _x * cos _z, -sin _x],
    [-cos _z * sin _y - cos _y * sin _x * sin _z, cos _y * cos _z * sin _x - sin _y * sin _z, cos _x * cos _y]
]
