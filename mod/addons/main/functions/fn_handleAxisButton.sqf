/*
    Function:       ADA_Main_fnc_handleAxisButton
    Author:         Adanteh
    Description:    Handles the buttons for axi movement (Can toggle a certain axis)
*/
#include "macros.hpp"

params [["_axis", ""], ["_keydown", false]];

if !(_keydown) then { // -- Keyup
    GVAR(namespaceKeys) setVariable [_axis, -1];
} else {
    GVAR(namespaceKeys) setVariable [_axis, time + 0.5];
};

if (toLower _axis in ["precisionmode", "scale"]) exitWith { };
if (_keydown) then {
    [_axis] call EFUNC(toolGizmo,gizmoHighlight);
} else {
    [_axis, false] call EFUNC(toolGizmo,gizmoHighlight);
};
