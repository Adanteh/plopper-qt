#include "macros.hpp"
/*
    Function:       ADA_Main_fnc_getSelectionData
    Author:         Adanteh
    Description:    Gets data from object, used 
*/

params ["_tags"];

LOGTHIS;

private _selectedObjects = call MFUNC(getSelectedObjects);
private _var = ["Selected ", _selectedObjects];

if (_selectedObjects isEqualTo []) exitWith {
    [LOCALIZE("NO_OBJECTS_SELECTED"), "red", nil, -1] call CFUNC(debugMessage);
    []
};

private _data = [];
{
    _data pushBack ([_x, _tags] call MFUNC(getObjectData));
} forEach _selectedObjects;

_data;