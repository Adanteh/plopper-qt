#include "module.hpp"
#include "..\core\macros.hpp"

#ifdef __INCLUDE_IDCS
    #include "idcs.hpp"
#endif

#define __AXISRESTRAINT(var1) (GVAR(namespaceKeys) getVariable [var1, -1]) > time
