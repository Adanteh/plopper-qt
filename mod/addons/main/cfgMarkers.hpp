class CfgMarkers {
    class GVAR(markerCam) {
        name = "Camera Marker";
        icon = TEXTURE(cameraTexture_ca.paa);
        texture = TEXTURE(cameraTexture_ca.paa);
        side = 0;
        size = 29;
        shadow = 0;
        scope = 0;
        color[] = {1, 1, 1, 1};
    };

    class GVAR(markerMan) {
        color[] = {1, 1, 1, 1};
        name = "iconMan";\
        icon = "\A3\ui_f\data\map\vehicleicons\iconMan_ca.paa";
        scope = 0;
        shadow = 0;
        size = 23;
    };
};

class CfgMarkerColors {
    class ColorBlue {
        name = "Blue";
        color[] = {0, 0, 1, 1};
        scope = 2;
    };
};