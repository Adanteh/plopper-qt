class GVAR(CtrlLoadingScreen) {
	idd = 667;
	duration = 10e10;
	fadein = 0;
	fadeout = 0;
	name = "Loading Screen";
	class controlsBackground {
		class blackBG : ctrlStatic {
			x = safezoneX;
			y = safezoneY;
			w = safezoneW;
			h = safezoneH;
			text = "";
			colorText[] = {0,0,0,0};
			colorBackground[] = {0.14, 0.13, 0.13,1};
		};
		class baseLogo : ctrlStaticPictureKeepAspect {
            idc = __IDC_ELEMENT_1;
			x = safezoneX + safezoneW * 0.25;
			y = safezoneY + safezoneH * 0.25;
			w = safezoneW * 0.5;
			h = safezoneH * 0.5;
			text = TEXTURE(logo_big_ca.paa);
		};
        class pythiaLogo : ctrlStaticPictureKeepAspect {
            idc = __IDC_ELEMENT_2;
            x = safezoneX + safezoneW * 0.05;
            y = safezoneY + safezoneH * 0.85;
            w = safezoneW * 0.1;
            h = safezoneH * 0.1;
            text = TEXTURE(logo_pythia.paa);
        };
	};
	class controls {
		class Title1 : RscLoadingText {
			text = ""; // "Loading" text in the middle of the screen
		};
		class CA_Progress : RscProgress {
			idc = 104;
			type = 8; // CT_PROGRESS
			style = 0; // ST_SINGLE
			texture = "#(rgb,8,8,3)color(0,1,0,0.75)";
		};
		class CA_Progress2 : RscProgressNotFreeze {
			idc = 103;
		};
		class Name2: RscText {
			idc = 101;
			style = 0x02;
			x = safezoneX + safezoneW * 0.05;
			y = safezoneY + safezoneH * 0.85;
			w = safezoneW * 0.9;
			h = safezoneH * 0.1;
			text = "";
			sizeEx = 0.05;
			colorText[] = {1.0,1.0,1.0,1.0};
		};
	};
};
