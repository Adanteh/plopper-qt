class SVAR(Interface)
{
    idd = 123;
    movingenable = false;
    onLoad = "uiNamespace setVariable ['ADA_Interface', (_this select 0)]; ['ui.onLoad'] call ADA_Core_fnc_localEvent;";
    onUnload = "['ui.onUnload'] call ADA_Core_fnc_localEvent;";

    class controlsBackground {
        class MouseArea: RscText {
            idc = __GUI_VIEW_IDC;
            x = "safezoneX";
            y = "safezoneY";
            w = "safezoneW * 0.8";
            h = "safezoneH * 0.97";
            colorBackground[] = {0, 0, 0, 0};
            colorShadow[] = {0, 0, 0, 0.5};
            linespacing = 1;
            tooltipColorText[] = {1, 1, 1, 1};
            tooltipColorBox[] = {1, 1, 1, 1};
            tooltipColorShade[] = {0, 0, 0, 0.65};
            text = "";
            style = 0x10;
            onMouseMoving = "_this call ADA_Main_fnc_onMouseMoving";
            onMouseButtonDown = "_this call ADA_Main_fnc_onMouseButtonDown";
            onMouseButtonUp = "_this call ADA_Main_fnc_onMouseButtonUp";
            onMouseButtonDblClick = "";
            onMouseZChanged = "_this call ADA_Main_fnc_onMouseZChanged";

            onKeyDown = "";
            onKeyUp = "";
            onMouseEnter = "['enter', _this] call ADA_Main_fnc_uiHandleFocus";
            onMouseExit = "['exit', _this] call ADA_Main_fnc_uiHandleFocus";
        };

    };

    class controls {
        class Map: RscMapControl {
            idc = __GUI_MAP_IDC;
            x = safeZoneX + GRID_TOOLBAR_X(2);
            y = safezoneY + __GUI_MENUBAR_H + __GUI_TOGGLES_H;
            w = safeZoneW - GRID_TOOLBAR_X(2);
            h = safeZoneH - __GUI_MENUBAR_H + __GUI_TOGGLES_H;

            alphaFadeStartScale = 1.1; // Scale at which satellite map starts appearing
            alphaFadeEndScale = 1.1; // Scale at which satellite map is fully rendered
            maxSatelliteAlpha = 0.85; // Maximum alpha of satellite map
            showCountourInterval = 0;

            onLoad = "['onLoad', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseButtonDblClick = "['onMouseButtonDblClick', _this] call ADA_Main_fnc_uiMapEvent;";
            onSetFocus = "['onSetFocus', _this] call ADA_Main_fnc_uiMapEvent;";
            onKillFocus = "['onKillFocus', _this] call ADA_Main_fnc_uiMapEvent;";
            onKeyDown = "['onKeyDown', _this] call ADA_Main_fnc_uiMapEvent;";
            onKeyUp = "['onKeyUp', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseButtonDown = "['onMouseButtonDown', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseButtonUp = "['onMouseButtonUp', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseButtonClick = "['onMouseButtonClick', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseZChanged = "['onMouseZChanged', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseMoving = "['onMouseMoving', _this] call ADA_Main_fnc_uiMapEvent;";
            onMouseHolding = "['onMouseHolding', _this] call ADA_Main_fnc_uiMapEvent;";
        };
    };
};
