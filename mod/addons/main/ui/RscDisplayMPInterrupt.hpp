class SVAR(InteruptMenuButton): RscButtonMenu {
    default = 0;
    font = __FONT_LIGHT;
    fontSecondary = __FONT_LIGHT;
    period = 10;
    periodFocus = 10;
    periodOver = 10;
    style = 0;
    colorBackground[] = __COLOR_BUTTON_BG_ACCENT;
    colorBackground2[] = __COLOR_BUTTON_BG_ACCENT;
    colorBackgroundFocused[] = __COLOR_BUTTON_BG_ACCENT;
    color[] = {1, 1, 1, 1};
    colorFocused[] = {1, 1, 1, 1};
    color2[] = {1, 1, 1, 1};
    colorText[] = {1, 1, 1, 1};
    colorDisabled[] = {0.25, 0.25, 0.25, 1};
    colorSecondary[] = {1, 1, 1, 1};
    color2Secondary[] = {1, 1, 1, 1};
    colorFocusedSecondary[] = {1, 1, 1, 1};
    class Attributes {
        color = "#FFFFFF";
        font = __FONT_LIGHT;
        shadow = "false";
        align = "left";
    };
    text = "Plopper";
    action = "(findDisplay 49) closeDisplay 0; call ADA_Main_fnc_startMod;";
    tooltip = "Starts Plopper mod";
    x = safeZoneX + __GUI_GRIDX(1);
    y = safeZoneY + __GUI_GRIDY(1);
    h = __GUI_GRIDY(3);
    w = __GUI_GRIDX(15);
    size = __GUI_GRIDX(3);
    sizeEx = __GUI_GRIDX(3);
    onLoad = "";
};

class SVAR(ResetSettingsButton): RscButtonMenu {
    text = "Reset UI";
    x = safeZoneX + __GUI_GRIDX(1);
    y = safeZoneY + __GUI_GRIDY(4);
    h = __GUI_GRIDY(1);
    w = __GUI_GRIDX(15);
    size = __GUI_GRIDX(1);
    sizeEx = __GUI_GRIDX(1);
    action = "['plopper.misc.reset_settings', []] call PY3_fnc_callExtension;"
};

class RscDisplayMPInterrupt: RscStandardDisplay {
    class controls {
        class SVAR(InteruptMenuButton): SVAR(InteruptMenuButton) {};
        class SVAR(ResetSettingsButton): SVAR(ResetSettingsButton) {};
    };
};

class RscDisplayInterrupt: RscStandardDisplay {
    class controls {
        class SVAR(InteruptMenuButton): SVAR(InteruptMenuButton) {};
        class SVAR(ResetSettingsButton): SVAR(ResetSettingsButton) {};
    };
};

class RscDisplayInterruptEditor3D: RscStandardDisplay {
    class controls {
        class SVAR(InteruptMenuButton): SVAR(InteruptMenuButton) {};
        class SVAR(ResetSettingsButton): SVAR(ResetSettingsButton) {};
    };
};

class RscDisplayInterruptEditorPreview: RscStandardDisplay {
    class controls {
        class SVAR(InteruptMenuButton): SVAR(InteruptMenuButton) {};
        class SVAR(ResetSettingsButton): SVAR(ResetSettingsButton) {};
    };
};
