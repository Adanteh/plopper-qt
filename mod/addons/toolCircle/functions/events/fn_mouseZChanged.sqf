/*
    Function:       ADA_toolCircle_fnc_mouseZChanged
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"
#define __BASEMODIFIER 0.5

params ["", "_scrollAmount"];

private _shift = ["scrollModifier"] call MFUNC(modifierPressed);
private _multiplier = ([1, 10] select _shift) * 0.5;
private _radius = MVAR(set_brush_circleSize);
_radius = (_radius + (_multiplier * __BASEMODIFIER * _scrollAmount)) max 1;

MVAR(set_brush_circleSize) = _radius;
call FUNC(calculateCirclePoints);
["plopper.plop", ["set_setting", "brush/circlesize", _radius]] call PY3_fnc_callExtension;

false; // -- Don't cancel other keybinds
