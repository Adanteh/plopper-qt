/*
    Function:       ADA_toolCircle_fnc_toolActivate
    Author:         Adanteh
    Description:    Run when tool is activated
*/
#include "macros.hpp"

call FUNC(calculateCirclePoints);
private _handle = missionNamespace getVariable [QGVAR(pfh), -1];

// -- Handles drawing lines in 3D, for bounding boxes
if (_handle == -1) then {
    _handle = [{ _this call FUNC(drawCircle) }, 0] call CFUNC(addPerFrameHandler);
    missionNamespace setVariable [QGVAR(pfh), _handle];
};
