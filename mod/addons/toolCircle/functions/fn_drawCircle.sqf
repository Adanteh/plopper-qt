/*
    Function:       ADA_toolCircle_fnc_drawCircle
    Author:         Adanteh
    Description:    Draws the circle in shiny colors
*/
#define __INCLUDE_DIK
#include "macros.hpp"

private _circlePoints = GVAR(namespace) getVariable ["circlePoints", []];
if (_circlePoints isEqualTo []) exitWith { };

private _color = [GVAR(color), [1, 0.1, 0, 1]] select (["Deselect"] call MFUNC(modifierPressed));
_color = [_color, [0, 0, 1, 1]] select (["Selecting"] call MFUNC(modifierPressed));

private _pointCount = (count _circlePoints) - 1;
private _mousePosition = screenToWorld getMousePosition;
drawLine3D [
    _mousePosition vectorAdd (_circlePoints select _pointCount),
    _mousePosition vectorAdd (_circlePoints select 0),
    _color
];

for "_i" from 1 to _pointCount do {
    drawLine3D [
        _mousePosition vectorAdd (_circlePoints select _i),
        _mousePosition vectorAdd (_circlePoints select (_i - 1)),
        _color
    ];
};
