/*
    Function:       ADA_toolCircle_fnc_getCircleSize
    Author:         Adanteh
    Description:    Gets the radius of the brushing circle
*/
#include "macros.hpp"

MVAR(set_brush_circleSize);
