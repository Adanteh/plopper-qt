/*
    Function:       ADA_toolCircle_fnc_calculateCirclePoints
    Author:         Adanteh
    Description:    Creates circle points to draw
*/
#include "macros.hpp"
#define Z_FACTOR 0.015

private _radius = call FUNC(getCircleSize);
private _circlePoints = [];

for [{private _i = 0}, {_i < 360}, {_i = _i + 15}] do {
	private _point = [
        (sin _i) * _radius,
	    (cos _i) * _radius,
	    _radius * Z_FACTOR
    ];
	_circlePoints pushBack _point;
};

GVAR(namespace) setVariable ["circlePoints", _circlePoints];
