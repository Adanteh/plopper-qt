/*
    Function:       ADA_toolCircle_fnc_setCircleColor
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params ["_color"];
if (isNil "_color") then {
    _color = [0, 1, 0, 1];
};

GVAR(color) = _color;
