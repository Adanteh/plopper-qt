/*
    Function:       ADA_toolCircle_fnc_clientInit
    Author:         Adanteh
    Description:    Creates namespaces
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(color) = [0, 1, 0, 1];
MVAR(set_brush_circleSize) = 1;


["plop.setting_changed", {
    (_this select 0) params ["_setting", "_value"];
    if (_setting == "brush/circlesize") then {
        call FUNC(calculateCirclePoints);
    };
}] call CFUNC(addEventHandler);
