#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main", "Object"};
            class clear;
            class clientInit;
            class getDataStructure;
            class getObjectData;
            class fileLoad;

            class Export {
                class exportLine;
                class exportToTB;
                class importFromTB;
            };
		};
	};
};
