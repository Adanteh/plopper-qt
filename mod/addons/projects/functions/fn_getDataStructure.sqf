/*
    Function:       ADA_Projects_fnc_getDataStructure
    Author:         Adanteh
    Description:    Gets the named local variables to export and use for params command
*/
#include "macros.hpp"

["_modelpath", ["_pos", [0, 0, 0]], ["_scale", 1], ["_pitchBankYaw", [0, 0, 0]], ["_layerName", ""], ["_isHidden", 0], ["_isLocked", 0]]
