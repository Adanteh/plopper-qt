/*
    Function:       ADA_Projects_fnc_resetProject
    Author:         Adanteh
    Description:    Resets the current local project (deletes all objects, resets object vars)
*/
#include "macros.hpp"

private _objects = call MFUNC(getObjectsPlaced);
{
    deleteVehicle _x;
} forEach _objects;
_objects resize 0; // -- Reset the var from getObjectsPlaced

