/*
    Function:       ADA_Projects_fnc_getObjectData
    Author:         Adanteh
    Description:    Gets all data required to save a project with
*/
#include "macros.hpp"
#define DECIMALS_POS	6
#define DECIMALS_ANG	6
#define DECIMALS_SCALE	3

private _objects = call MFUNC(getObjectsPlaced);
private _data = [];
{
    if !(isNull _x) then {
        private _object = _x;
        private _templateName = [_object] call MFUNC(getTemplateName);
        if (_templateName == "") exitWith { };
        // -- use posworld, because it's the only thing that's reliable when rotating, but subtract bounding center.
        // -- Bounding center is automatically added when creating objects (No other way)
        private _position = (getPosWorld _object);
        _position set [2, (_position select 2) - (boundingCenter _object select 2)];


        // -- These objects auto slope and should never export their pitch bank
        private _slopeLandContact = _object getVariable [QMVAR(SlopeLandContact), false];
        private _pitchBankYaw = _object getVariable [QMVAR(PitchBankYaw), [0, 0, 0]] apply { round (_x * 10000) / 10000 };
        /*
        if (_slopeLandContact) then {
            _pitchBankYaw set [0, 0];
            _pitchBankYaw set [1, 0];
        };
        */

        private _entry = [
            (getModelInfo _object) select 1,
            _position apply { (round (_x * 10000) / 10000) toFixed DECIMALS_POS }, // -- Round on 5 decimals
            (_object getVariable [QMVAR(scale), 1]) toFixed DECIMALS_SCALE,
            _pitchBankYaw apply { _x toFixed DECIMALS_ANG },
            _object getVariable [QMVAR(ObjectLayer), ""],
            parseNumber (_object getVariable [QMVAR(ObjectHidden), false]),
            parseNumber (_object getVariable [QMVAR(ObjectLocked), false])
        ];

        // -- This will make sure you don't get duplicate objects when importing the same object multiple times
        if (MVAR(set_project_preventDuplicates)) then {
            _data pushbackUnique _entry;
        } else {
            _data pushBack _entry;
        };
    };
} forEach _objects;
_data
