/*
    Function:       ADA_Projects_fnc_exportLine
    Author:         Adanteh, ianbanks
    Description:    This will create a proper string per object we want to export
*/
#include "macros.hpp"
#define DECIMALS_POS	6
#define DECIMALS_ANG	6
#define DECIMALS_SCALE	3

params [["_object", objNull]];

if (isNull _object) exitWith { "" };

private _templateName = [_object] call MFUNC(getTemplateName);
if (_templateName == "") exitWith { "" };

private _center = getPosWorld _object;

// -- Export only within the mapframe
private _worldSize = worldSize;
if ((_center select 0) <= 0 || {(_center select 0) >= _worldSize}) exitWith { "" };
if ((_center select 1) <= 0 || {(_center select 1) >= _worldSize}) exitWith { "" };
private _scale = _object getVariable [QMVAR(scale), 1];

// -- These objects auto slope and should never export their pitch bankt
private _slopeLandContact = _object getVariable [QMVAR(SlopeLandContact), false];
([_object] call CFUNC(calcPitchBankYaw)) params ["_xRot", "_yRot", "_zRot"];

private "_modelPosition";
private _color = "red";
private _terrainBuilderScaledGround = [0, 0, 0];

if (_slopeLandContact) then {
    _modelPosition = _center vectorDiff [0, 0, boundingCenter _object select 2];
    _xRot = 0;
    _yRot = 0;
} else {
    if ([_xRot, _yRot] isEqualTo [0, 0]) then {
        _modelPosition = getPosASL _object;
        _color = "green";
    } else {
        // -- This calculation is by Ianbanks from:
        // https://foxhound.international/sqf-snippets/export-arma-3-to-terrain-builder
        private _terrainBuilderMatrix = [_xRot, _yRot, _zRot] call MFUNC(getTerrainBuilderMatrix);
        private _boundingCenter = boundingCenter _object;
        private _terrainBuilderGround = [_terrainBuilderMatrix, _boundingCenter] call MFUNC(matrixVectorMultiply);
        _terrainBuilderScaledGround = _terrainBuilderGround;
        // _terrainBuilderScaledGround = _terrainBuilderGround vectorMultiply (1 / _scale);
        // _modelPosition = _center vectorDiff [0, 0, _terrainBuilderScaledGround select 2 - ((boundingCenter _object) select 2)];
        _modelPosition = _center vectorDiff [0, 0, _terrainBuilderScaledGround select 2];
        // _modelPosition set [2, _groundPos];
    };
};

// [[_fnc_scriptNameShort, [_terrainBuilderScaledGround, _modelPosition, boundingCenter _object]], _color, nil, -1] call CFUNC(debugMessage);
private _mapFrameOffset = [200000, 0, 0];
(_modelPosition vectorAdd _mapFrameOffset) params ["_xPos", "_yPos", "_zPos"];

private _line = format [
    '"%1";%2;%3;%4;%5;%6;%7;%8;',
    _templateName, // 0-class
    _xPos toFixed DECIMALS_POS, // 1-X
    _yPos toFixed DECIMALS_POS, // 2-Y
    _zRot toFixed DECIMALS_ANG, // 3-Yaw
    _xRot toFixed DECIMALS_ANG, // 4-Pitch
    _yRot toFixed DECIMALS_ANG, // 5-Bank
    _scale toFixed DECIMALS_SCALE, // 6-SCALE
    _zPos toFixed DECIMALS_POS // 7-Z
];

_line;
