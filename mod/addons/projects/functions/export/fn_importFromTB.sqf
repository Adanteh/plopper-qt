/*
    Function:       ADA_Projects_fnc_importFromTB
    Author:         Adanteh
    Description:    Imports a TB text file and converts it to a regular project file to load
*/
#include "macros.hpp"

params ["_data", ["_quiet", false]];

// -- Convert format to our regular project data.
private _keys = call FUNC(getDataStructure);//["_modelpath", ["_pos", [0, 0, 0]], ["_scale", 1], ["_pitchBankYaw", [0, 0, 0]]]
private _keysTerrainBuilder = ["_templateName", "_posX", "_posY", "_yaw", "_pitch", "_bank", "_scale", "_posZ"];
private _dataProject = [];
private _missingTemplates = [];
private _mapframeX = 200000;
private _mapframeY = 0;

{
    _x params _keysTerrainBuilder;
    private _modelPath = ["plopper.plop", ["library.get_model", _templateName]] call PY3_fnc_callExtension;
    if (_modelPath != "") then {
        private _pos = [_posX - _mapframeX, _posY - _mapframeY, _posZ];
        _pos set [2, (_pos select 2) + getTerrainHeightASL _pos];

        private _pitchBankYaw = [_pitch, _bank, _yaw];
        _dataProject pushBack [_modelPath, _pos, _scale, _pitchBankYaw];

        ["object.import", [[_modelPath, _pos, _scale, _pitchBankYaw], _keys]] call CFUNC(localEvent);
        private _terrainBuilderMatrix = _pitchBankYaw call MFUNC(getTerrainBuilderMatrix);
    } else {
        _missingTemplates pushBackUnique _templateName;
    };

} forEach _fileData;

// -- Call global import function
["project.imported", [_dataProject, _keys]] call CFUNC(localEvent);

{
    [format ["Missing template %1", _x], "red", 4, -1] call CFUNC(debugMessage);
} forEach _missingTemplates;

if !(_quiet) then {
    [LOCALIZE("PROJECT_LOAD_OK"), "lime", 5, -1] call CFUNC(debugMessage);
};
