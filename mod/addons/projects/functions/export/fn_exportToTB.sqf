/*
    Function:       ADA_Projects_fnc_exportToTB
    Author:         Adanteh
    Description:    Exports the current project in terrain builder format
*/
#include "macros.hpp"

// -- Create data for export
private _objects = call MFUNC(getObjectsPlaced);
private _exportData = [];
{
    private _line = [_x] call FUNC(exportLine);
    if (_line != "") then {
        _exportData pushBack _line;
    };
} forEach _objects;
_exportData
