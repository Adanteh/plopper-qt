/*
    Function:       ADA_Projects_fnc_fileLoad
    Author:         Adanteh
    Description:    Selects an item from the file list
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

params ["_data"];

[LOCALIZE("LOADING_PROJECT"), "lime", 5, -1] call CFUNC(debugMessage);
if (_data isEqualTo []) exitWith { };

private _keys = call FUNC(getDataStructure);
[
    _data,
    {
        ["object.import", [_element, _args select 0]] call CFUNC(localEvent);
    },
    {
        _args params ["_keys"];
        ["project.imported", [[], _keys]] call CFUNC(localEvent);
    },
    [_keys],
    10
] call CFUNC(addBatch);
