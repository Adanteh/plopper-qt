/*
    Function:       ADA_Projects_fnc_clientInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#define __INCLUDE_DIK
#include "macros.hpp"

["project.load_project", {
    [(_this select 0)] call FUNC(fileLoad);
}] call CFUNC(addEventHandler);

["project.import_tb", {
    [(_this select 0)] call FUNC(importFromTB);
}] call CFUNC(addEventHandler);

["project.save_project", {
    private _data = call FUNC(getObjectData);
    private _success = ["plopper.plop", ["project.save_project", _data, worldName]] call py3_fnc_callExtension;
}] call CFUNC(addEventHandler);

["project.export_project", {
    private _data = call FUNC(exportToTb);
    private _success = ["plopper.plop", ["project.export_project", _data]] call py3_fnc_callExtension;
}] call CFUNC(addEventHandler);

["project.clear", {
    [] call FUNC(clear);
}] call CFUNC(addEventHandler);
