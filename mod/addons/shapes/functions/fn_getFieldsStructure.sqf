/*
    Function:       ADA_Shapes_fnc_getFieldsStructure
    Author:         Adanteh
    Description:    Gets the fields structure for this shapefile
*/
#include "macros.hpp"

GVAR(shapeFields)
