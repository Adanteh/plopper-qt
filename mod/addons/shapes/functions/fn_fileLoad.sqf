/*
    Function:       ADA_Shapes_fnc_fileLoad
    Author:         Adanteh
    Description:    Selects an item from the file list
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

params ["_ctrlGroup", "", "_ctrlCombo", ["_baseName", "##"], ["_quiet", false]];
if (_baseName == "##") then {
    _basename = _ctrlCombo lbText (lbCurSel _ctrlCombo);
};

private _path = format ["Projects\%1\Shapes\%2", MVAR(currentProject), _baseName];
private _fileData = ["ObjectPlacement.read_shapefile", [_path]] call py3_fnc_callExtension;
private _paneCtrl = ctrlParentControlsGroup _ctrlGroup;
private _treeviewCtrl = _paneCtrl controlsGroupCtrl __IDC_ELEMENT_2;
["clear", [_treeviewCtrl]] call FUNC(handleToolbarButtons);

// -- Import
private _keys = call FUNC(getDataStructure);
_fileData params _keys;
GVAR(shapeFields) = _fields;
GVAR(shapeType) = _shapeType;

{
    GVAR(shapeIndex) = GVAR(shapeIndex) + 1;

    _x params ["_record", "_points"];
    private _polyPoints = [];
    {
        _polypoints pushBack ([_x] call FUNC(createPoint));
    } forEach _points;
    GVAR(namespaceShapes) setVariable [str GVAR(shapeIndex), _polyPoints];
    GVAR(namespace) setVariable [str GVAR(shapeIndex) + "_record", _record];
} forEach _shapeRecords;


['load', [_paneCtrl]] call FUNC(handleToolbarButtons);

if !(_quiet) then {
    [LOCALIZE("FILE_LOAD_OK"), "lime", 5, -1] call CFUNC(debugMessage);
};
