/*
    Function:       ADA_Shapes_fnc_fileSave
    Author:         Adanteh
    Description:    Save favorites list to file
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

params ["_ctrlGroup", "_ctrlButton", "_ctrlCombo", "_fileName"];
// private _paneCtrl = ctrlParentControlsGroup _ctrlGroup;
// private _listbox = _paneCtrl controlsGroupCtrl __IDC_ELEMENT_2;

private _keys = call FUNC(getDataStructure);
private _shapeRecords = allVariables GVAR(namespaceShapes);
private _shapePoints = [];
{
    private _polyPoints = GVAR(namespaceShapes) getVariable [_x, []];
    if (_polyPoints isEqualType []) then {
        if (_polyPoints isEqualTo []) exitWith { };

        private _record = GVAR(namespace) getVariable [_x + "_record", ['objectplacement_layer']];
        private _points = [];
        {
            private _coordinate = getPosASL _x;
            _points pushBack [_coordinate select 0, _coordinate select 1];
        } forEach _polyPoints;
        _shapePoints pushBack [_record, _points];
    };
} forEach _shapeRecords;

private _data = [GVAR(shapeType), GVAR(shapeFields), _shapePoints];
GVAR(testData) = _data;

private _path = format ["Projects\%1\Shapes\%2", MVAR(currentProject), _fileName];
private _return = ["ObjectPlacement.write_shapefile", [_path, _data]] call py3_fnc_callExtension;
if (_return isEqualTo 0) then {
    [LOCALIZE("FILE_SAVE_OK"), "lime", 5, -1] call CFUNC(debugMessage);
} else {
    [format ["%1<br />%2", LOCALIZE("FILE_SAVE_ERROR"), _return], "red", 5, -1] call CFUNC(debugMessage);
};
