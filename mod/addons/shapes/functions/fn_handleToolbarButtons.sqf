/*
    Function:       ADA_Shapes_fnc_handleToolbarButtons
    Author:         Adanteh
    Description:    Shared fucntion for random snips for code called from buttons in toolbar
*/
#define __INCLUDE_IDCS
#include "macros.hpp"
#define CTRL(x) (_paneCtrl controlsGroupCtrl x)

params [["_mode", "start"], ["_args", []]];
[_this, 'purple'] call CFUNC(debugMessage);

private _return = false;
switch (toLower _mode) do {
    case "add": {
        _args params ["_index"];
        if (_index isEqualType 0) then { _index = str _index };
        private _paneCtrl = uiNamespace getVariable [QGVAR(list), controlNull];
        private _listBox = CTRL(__IDC_ELEMENT_2);
        private _entryIndex = _listbox lbAdd format ["%1 (%2)", _index, -1];
        _listbox lbSetData [_entryindex, _index];
        _listbox lbSetTooltip [_entryIndex, _index];
        _listbox lbSetCurSel _entryIndex;
    };

    case "remove": {
        _args params ["_ctrlButton"];
        private _paneCtrl = ctrlParentControlsGroup _ctrlButton;
        private _listbox = CTRL(__IDC_ELEMENT_2);
        private _entryindex = lbCurSel _listbox;
        if (_entryindex != -1) then {
            private _dataEntry = _listbox lbData _entryindex;
            _listbox lbDelete _entryindex;
            _listbox lbSetCurSel (_entryindex - 1);
            [_dataEntry] call FUNC(clearShape);
        };
    };

    case "clear": {
        _args params ["_ctrlButton"];
        private _paneCtrl = ctrlParentControlsGroup _ctrlButton;
        private _listbox = CTRL(__IDC_ELEMENT_2);
        lbClear _listbox;

        {
            [_x] call FUNC(clearShape);
        } forEach (allVariables GVAR(namespaceShapes));
    };

    // -- Pane open. Restore all the loaded brush items
    case "load": {
        _args params ["_paneCtrl"];
        private _listbox = CTRL(__IDC_ELEMENT_2);
        lbClear _listbox;
        {
            private _shape = GVAR(namespaceShapes) getVariable _x;
            if !(isNil "_shape" || { _shape isEqualTo [] }) then {
                private _data = GVAR(namespace) getVariable [_x + "_record", []];
                _data params [["_name", ""], "", "", ["_id", -1]];
                private _entryIndex = _listbox lbAdd format ["%1 (%2)", _name, _id];
                _listbox lbSetData [_entryindex, _x];
                _listbox lbSetTooltip [_entryIndex, _x];
            };
        } forEach (allVariables GVAR(namespaceShapes));
    };

    // -- Selection in the brush list changed
    case "lbselchanged": {
        _args params ["_listbox", "_selectionIndex"];
        // -- Unhighlight old entry
        {
            _x setObjectTexture [0, __YELLOWTEX];
        } forEach (GVAR(namespaceShapes) getVariable [GVAR(shapeActive), []]);

        GVAR(selectionIndex) = _selectionIndex;
        GVAR(shapeActive) = _listbox lbData _selectionIndex;
        _settingsGroup = ctrlParentControlsGroup (ctrlParentControlsGroup _listbox);
        // -- Highlight new entry
        {
            _x setObjectTexture [0, __BLUETEX];
        } forEach (GVAR(namespaceShapes) getVariable [GVAR(shapeActive), []]);

        // -- This will set all values belonging to the newly selected brush to the setttings options
        private _settingArray = [GVAR(shapeFields)] call CFUNC(arrayGetKeys);
        {
            private _setting = _x;
            private _controlSetting = GVAR(namespace) getVariable ["setting.ctrl." + _setting, [controlNull]] select 0;

            private _settingIndex = _settingArray find (toLower _setting);
            private _selectedItem = GVAR(namespace) getVariable [GVAR(shapeActive) + "_record", []];
            private _value = _selectedItem param [_settingIndex, 0];
            // -- Figure out control type so we know what to do
            private _controlValue = (_controlSetting controlsGroupCtrl 100);
            private _controlType = _controlValue getVariable ["type", "text"];
            switch (toLower _controlType) do {
                case "listbox": { _controlValue lbSetCurSel _value };
                case "text";
                default {
                    if !(_value isEqualType "") then { _value = str _value };
                    _controlValue ctrlSetText _value;
                };
            };
        } forEach _settingArray;

    };

    // -- Gets setting from brush item (called by settingsAddControl)
    case 'value.ctrl.get': {
        _args params ["_setting", ["_defaultValue", 0], ["_control", controlNull]];
        // -- Change data sturucture to only return variable names. Find our given setting class in the data structure array
        private _settingArray = [GVAR(shapeFields)] call CFUNC(arrayGetKeys);
        private _settingIndex = _settingArray find (toLower _setting);
        private _selectedItem = GVAR(namespace) getVariable [GVAR(shapeActive) + "_record", []];
        _return = _selectedItem param [_settingIndex, _defaultValue];

        // -- Save the control this get function was called by, so we known which controls exist
        _control ctrlSetText _setting;
        GVAR(namespace) setVariable ["setting.ctrl." + _setting, [_control]];
    };

    // -- Sets setting to brush item (called by changing value in control)
    case 'value.ctrl.set': {
        _args params ["_setting", ["_value", 0, ["", [], 0]]];
        // -- Change data sturucture to only return variable names. Find our given setting class in the data structure array
        private _settingArray = [GVAR(shapeFields)] call CFUNC(arrayGetKeys);
        private _settingIndex = _settingArray find (toLower _setting);
        if (_settingIndex != -1) then {
            private _selectedItem = GVAR(namespace) getVariable [GVAR(shapeActive) + "_record", []];
            _selectedItem set [_settingIndex, _value];
            GVAR(namespace) setVariable [GVAR(shapeActive) + "_record", _selectedItem];
        };
    };

    case "shapefiletype.get": {
        _args params ['_dropdown'];
        { // Selection changed
            _x params ["_text", "_type"];
            private _index = _dropdown lbAdd _text;
            _dropdown lbSetData [_index, str _type];
            if (_type isEqualTo GVAR(shapeType)) then {
                _dropdown lbSetCurSel _index;
            };
        } forEach [
            ["Polyline", 3],
            ["Polygon", 5]
        ];
    };

    case "shapefiletype.set": {
        _args params ["_dropdown", "_index"];
        if (_index isEqualTo -1) exitWith { };
        private _data = _dropdown lbData _index;
        if (_data isEqualTo "") exitWith { };
        private _type = parseNumber _data;

        GVAR(shapeType) = _type;
    };

    // -- Will change selection to whatever index shape we give
    case "changesel": {
        _args params ["_index"];
        if (_index isEqualType 0) then { _index = str _index };
        private _paneCtrl = uiNamespace getVariable [QGVAR(list), controlNull];
        private _listbox = CTRL(__IDC_ELEMENT_2);
        for "_i" from 0 to (lbSize _listbox - 1) do {
            if (_index == (_listbox lbData _i)) exitWith {
                _listbox lbSetCurSel _i;
            };
        };
    };
    // -- Double click to pan to first item in the shape
    case "lbdblclick": {
        _args params ["_listbox", "_selectionIndex"];
        private _index = _listbox lbData _selectionIndex;
        private _shape = GVAR(namespaceShapes) getVariable _index;
        if !(isNil "_shape") then {
            private _object = _shape param [0, objNull];
            private _camera = call MFUNC(getCamObject);
            private _positionOffset = (getPosATL _camera) vectorDiff (screenToWorld [0.5, 0.5]);
            private _newPos = (getPosATL _object) vectorAdd _positionOffset;
            _newPos set [2, (getPosATL _camera) select 2];
            _camera setPosATL _newPos;
        };
    };

};

_return;
