/*
    Function:       ADA_Shapes_fnc_handleToolbarPane
    Author:         Adanteh
    Description:    Function to configure brush control in toolbar (called from SettingsWindow)
*/
#define __INCLUDE_IDCS
#include "macros.hpp"

params ["_baseArgs", "_control", "_subcategoryControlsgroupIDC", "_controlsGroupIDC"];
_baseArgs params ["_displayName", "_tooltip", "_controlType", "_defaultValue", "_condition", "_action", ["_values", []]];

uiNamespace setVariable [QGVAR(list), _control];

private _lastFileSelected = "";
private _filesCtrl = _control controlsGroupCtrl __IDC_ELEMENT_4;

private _listbox = _control controlsGroupCtrl __IDC_ELEMENT_2;
_listbox ctrlSetEventHandler ["LbSelChanged", format ["['lbselchanged', _this] call %1", QFUNC(handleToolbarButtons)]];
_listbox ctrlSetEventHandler ["LbDblClick", format ["['LbDblClick', _this] call %1", QFUNC(handleToolbarButtons)]];

// -- Add behaviour for add / remove buttons
(_control controlsGroupCtrl __IDC_BUTTON_1) ctrlSetEventHandler ["ButtonClick", format ["['remove', _this] call %1", QFUNC(handleToolbarButtons)]];
(_control controlsGroupCtrl __IDC_BUTTON_2) ctrlSetEventHandler ["ButtonClick", format ["['clear', _this] call %1", QFUNC(handleToolbarButtons)]];
["load", [_control]] call FUNC(handleToolbarButtons);
