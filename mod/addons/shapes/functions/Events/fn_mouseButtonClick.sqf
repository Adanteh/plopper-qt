/*
    Function:       ADA_Shapes_fnc_mouseButtonClick
    Author:         Adanteh
    Description:    Handles left mouse button down
*/
#include "macros.hpp"

params [["_button", 0], "_mousePosition", "_positionWorld"];
if (_button != 0) exitWith { };


private _removePoint = ["Deselect"] call MFUNC(modifierPressed);

if !(GVAR(placingShape)) exitWith {
    private _mouseOver = [nil] call MFUNC(detectUnderCursor);
    private _shapeIndex = _mouseOver getVariable QGVAR(shapeIndex);
    [[_fnc_scriptNameShort, _mouseOver, _shapeIndex], "red", 5] call CFUNC(debugMessage);

    GVAR(pointDrag) = _mouseOver;
    if !(isNil "_shapeIndex") then {
        if (_shapeIndex isEqualType 0) then { _shapeIndex = str _shapeIndex };
        if (_removePoint) then {
            deleteVehicle _mouseOver;
            private _currentShapes = (GVAR(namespaceShapes) getVariable [_shapeIndex, []]) - [_mouseOver];
            GVAR(namespaceShapes) setVariable [_shapeIndex, _currentShapes];
        } else {
            ["changesel", [_shapeIndex]] call FUNC(handleToolbarButtons);
        };
    };
};


private _polyPoints = GVAR(namespaceShapes) getVariable [str GVAR(shapeIndex), []];

// -- Control click will finish this shape
private _closeLoop = GVAR(namespace) getVariable ["closeLoopKey", false];
if (_closeLoop) exitWith {
    if (count _polyPoints > 1) then {
        GVAR(placingShape) = false;
        ["changesel", [GVAR(shapeIndex)]] call FUNC(handleToolbarButtons);
    };
};

_positionWorld set [2, (_positionWorld select 2) + 0.1]; // -- Increase height a little so its easier to see
_polyPoints pushBack ([_positionWorld] call FUNC(createPoint));
GVAR(namespace) setVariable ["currentShape", _polyPoints];
GVAR(namespaceShapes) setVariable [str GVAR(shapeIndex), _polyPoints];
