/*
    Function:       ADA_Shapes_fnc_mouseDragEnd
    Author:         Adanteh
    Description:    End of drag event for brush mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld", "_endPos", "_endPosWorld"];
if (_button != 0) exitWith { };

// -- End loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
[_handle] call CFUNC(removePerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", -1];
GVAR(pointDrag) = objNull;
