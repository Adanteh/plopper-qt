/*
    Function:       ADA_Shapes_fnc_mouseDragMoving
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _currentPos = getMousePosition;
private _lastPos = GVAR(namespace) getVariable ["mouseDragPosLast", [-1, -1]];

 // -- Don't place when mouse not moving. Maybe make this a setting
if (_lastPos isEqualTo _currentPos) exitWith { };

GVAR(namespace) setVariable ["lastUsedAt", diag_tickTime];
GVAR(namespace) setVariable ["mouseDragPosLast", _currentPos];

if (isNull GVAR(pointDrag)) exitWith { };

// -- Move based on the world position change for the cursor
private _previousPos = GVAR(namespace) getVariable ["lastPosMove", screenToWorld _currentPos];
private _currentPos = screenToWorld _currentPos;
private _offset = _currentPos vectorDiff _previousPos;
GVAR(namespace) setVariable ["lastPosMove", _currentPos];

// -- Move the point
private _oldPos = (getPosWorld GVAR(pointDrag));
private _newPos = (_oldPos vectorAdd _offset);
_newPos set [2, __POLYPOINTOFFSET];
GVAR(pointDrag) setPosATL _newPos;
