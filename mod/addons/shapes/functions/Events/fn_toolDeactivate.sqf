/*
    Function:       ADA_Shapes_fnc_toolDeactivate
    Author:         Adanteh
    Description:    Run when tool is Deselected
*/
#include "macros.hpp"

GVAR(placingShape) = false;

private _handle = GVAR(namespace) getVariable ["drawLinePFH", -1];
if (_handle != -1) then {
    [_handle] call CFUNC(removePerFrameHandler);
};
GVAR(namespace) setVariable ["drawLinePFH", nil];
