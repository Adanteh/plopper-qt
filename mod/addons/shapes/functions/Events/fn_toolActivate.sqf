/*
    Function:       ADA_Shapes_fnc_toolActivate
    Author:         Adanteh
    Description:    Run when tool is selected
*/
#include "macros.hpp"

// -- Handles drawing lines in 3D, for bounding boxes
private _handle = GVAR(namespace) getVariable ["drawLinePFH", -1];
if (_handle == -1) then {
    _handle = [{ _this call FUNC(drawShapes) }, 0] call CFUNC(addPerFrameHandler);
    GVAR(namespace) setVariable ["drawLinePFH", _handle];
};
