/*
    Function:       ADA_Shapes_fnc_mouseButtonDblClick
    Author:         Adanteh
    Description:    Handles mouse double click
*/
#include "macros.hpp"

params ["_button", "_mousePosition", "_mousePositionWorld"];
if (_button != 0) exitWith { };

if !(GVAR(placingShape)) then {
    GVAR(placingShape) = true;
    GVAR(shapeIndex) = GVAR(shapeIndex) + 1;
    ["add", [GVAR(shapeIndex)]] call FUNC(handleToolbarButtons);
    _this call FUNC(mouseButtonClick);
} else {
    GVAR(placingShape) = false;
    ["changesel", [GVAR(shapeIndex)]] call FUNC(handleToolbarButtons);
};
