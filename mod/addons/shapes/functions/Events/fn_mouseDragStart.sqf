/*
    Function:       ADA_Shapes_fnc_mouseDragStart
    Author:         Adanteh
    Description:    Event for mouse dragging start (When button is hold) for brush mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld"];
if (_button != 0) exitWith { };

GVAR(namespace) setVariable ["lastPosMove", _startPosWorld];
GVAR(namespace) setVariable ["drag.startPosWorld", _startPosWorld];


// -- Allow direct drag on active shape
private _mouseOver = [_startPos] call MFUNC(detectUnderCursor);
private _shapeIndex = _mouseOver getVariable QGVAR(shapeIndex);

[[_fnc_scriptNameShort, _mouseOver, _shapeIndex], "purple"] call CFUNC(debugMessage);
if !(isNil "_shapeIndex") then {
    if (str _shapeIndex isEqualTo GVAR(shapeActive)) then {
        GVAR(pointDrag) = _mouseOver;
    };
};

private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
private _handle = [{ _this call FUNC(mouseDragMoving) }, 0] call CFUNC(addPerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", _handle];
