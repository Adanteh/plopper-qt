/*
    Function:       ADA_Shapes_fnc_getDataStructure
    Author:         Adanteh
    Description:    Gets the named local variables to export and use for params command
*/
#include "macros.hpp"

[
    ["_shapeType", 3],
    ["_fields", [["__LAYER","C",30,0],["ID","N",30,0],["ORDER","N",30,0]]],
    ["_shapeRecords", []]
];
