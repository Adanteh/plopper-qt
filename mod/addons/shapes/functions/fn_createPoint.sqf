/*
    Function:       SPE_Shapes_fnc_createPoint
    Author:         Adanteh
    Description:    Creates a point for a polyline / polygon
*/
#include "macros.hpp"

params [["_coordinate", []], ["_index", GVAR(shapeIndex)]];

private _object = createSimpleObject [QSVAR(Sphere), [_coordinate select 0, _coordinate select 1, 0]];
_object setPosATL [_coordinate select 0, _coordinate select 1, __POLYPOINTOFFSET];
_object setVariable [QGVAR(shapeIndex), _index];
_object
