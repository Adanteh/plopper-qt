/*
    Function:       SPE_Shapes_fnc_clearShape
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params [["_index", ""]];
if (_index isEqualType 0) then { _index = str _index };

{
    deleteVehicle _x;
} forEach (GVAR(namespaceShapes) getVariable [_index, []]);
GVAR(namespaceShapes) setVariable [_index, nil];
