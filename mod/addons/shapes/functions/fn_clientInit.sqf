#define __INCLUDE_DIK
#include "macros.hpp"
/*
    Function:       ADA_Shapes_fnc_clientInit
    Author:         Adanteh
    Description:    Client init brush mode
*/

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(namespaceShapes) = false call CFUNC(createNamespace);
GVAR(selectionIndex) = 0;

[] params (call FUNC(getDataStructure));
GVAR(shapeIndex) = 0;
GVAR(shapeActive) = "";
GVAR(shapeFields) = _fields;
GVAR(shapeType) = _shapeType;

// -- Points placing/moving
GVAR(placingShape) = false;
GVAR(pointDrag) = objNull;

[
    "Line Tool", QGVAR(closeLoop), ["Close Loop Modifier", "Hold this key and click to close the current polygon"],
    { GVAR(namespace) setVariable ["closeLoopKey", true]; false },
    { GVAR(namespace) setVariable ["closeLoopKey", false]; false },
    [DIK_LCONTROL, [nil, nil, nil]], false, false
] call MFUNC(addKeybind);
