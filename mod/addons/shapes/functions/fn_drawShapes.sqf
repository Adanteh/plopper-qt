/*
    Function:       ADA_Shapes_fnc_drawShapes
    Author:         Adanteh
    Description:    Draws lines between current selected shape
*/
#include "macros.hpp"

if (GVAR(shapeActive) != "") then {
    private _polyPoints = GVAR(namespaceShapes) getVariable [GVAR(shapeActive), []];
    if !(_polyPoints isEqualType []) exitWith { };
    if (_polyPoints isEqualTo []) exitWith { };

    for "_i" from 1 to ((count _polyPoints) - 1) do {
        drawLine3D [
            ASLToAGL getPosASL (_polyPoints select _i),
            ASLToAGL getPosASL (_polyPoints select (_i - 1)),
            [0, 0, 1, 1]
        ];
    };
};

// -- If this is set to true we want the last point to connect back to the first point (in different color)
if (GVAR(placingShape)) then {
    private _polyPoints = GVAR(namespace) getVariable ["currentShape", []];
    private _pointCount = (count _polyPoints) - 1;
    if (_pointCount >= 0) then {
        private _drawToMouse = GVAR(namespace) getVariable ["drawToMouse", true];
        if (_drawToMouse) then {
            private _mousePos = screenToWorld getMousePosition;
            _mousePos set [2, 0.3];
            drawLine3D [
                ASLToAGL getPosASL (_polyPoints select _pointCount),
                _mousePos,
                [1, 0, 0, 1]
            ];
        };
    };
};
