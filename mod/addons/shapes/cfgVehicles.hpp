class CfgVehicles {
	class NonStrategic;
	class SVAR(Sphere): NonStrategic {
		_generalMacro = QSVAR(Sphere);
		displayName = "Sphere (Clickable)";
		model = TEXTURE(ada_sphere.p3d);
		hiddenSelections[] = {"surface"};
        hiddenSelectionsTextures[] = {__YELLOWTEX};
        featureType = 2; // -- Show on max range
        author = "Mondkalb";
        scope = 2;
        scopeCurator = 2;
        icon = "iconObject_circle";
        accuracy = 1000;
	};
};
