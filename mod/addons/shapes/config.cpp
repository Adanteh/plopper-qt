#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            priority = 15;
            scope = 2;
            action = "['shapes'] call ADA_Main_fnc_toolbarButtonChangeMode;";
            tools[] = {};
            picture = ICON(action,dashboard);
            tooltip = "Shape Mode";
            type = "mode";
            default = 0;
            enable = 1;

            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
                class mouseDragStart { function = QFUNC(mouseDragStart); };
                class mouseDragEnd { function = QFUNC(mouseDragEnd); };
                class mouseClick { function = QFUNC(mouseButtonClick); };
                class MouseDoubleClick { function = QFUNC(mouseButtonDblClick); };
            };

            #include "cfgToolbarSettings.hpp"
        };
    };
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main"};

            class clearShape;
			class clientInit;
            class createPoint;
            class drawShapes;
            class fileLoad;
            class fileSave;
            class getDataStructure;
            class getFieldsStructure;
            class handleToolbarButtons;
            class handleToolbarPane;
            class testFunc;

            class Events {
				class mouseButtonClick;
                class mouseButtonDblClick;
				class mouseDragEnd;
                class mouseDragMoving;
				class mouseDragStart;
			};
		};
	};
};

#include "ui\ui.hpp"
#include "cfgVehicles.hpp"
