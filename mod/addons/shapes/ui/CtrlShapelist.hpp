
// -- THis is a settings group for settings framework, loaded into the toolbar.
#define __BASEWIDTH __GUI_PANE_W
class GVAR(CtrlShapeList): ctrlControlsGroupNoScrollbars {
    settingLoadCode = QFUNC(handleToolbarPane);
    x = __GUI_SETTING_SPACING_X;
    y = (1 * GRID_H);
	w = __GUI_PANE_W;
	h = (__GUI_SETTING_LINE_H) * 2 + (81.5 * GRID_H);

	class Controls {
        // -- Allow selecting saved files
        class FileSelectorNew: SVAR(CtrlFileSelector) {
            idc = __IDC_ELEMENT_4;
            x = 0;
			w = __BASEWIDTH;
			h = __GUI_SETTING_LINE_H;
        };

        class BrushList: SVAR(CtrlListBox) {
            idc = __IDC_ELEMENT_2;
            x = 0;
            y = __GUI_SETTING_LINE_H + (1.5 * GRID_H);
            h = (80 * GRID_H);
            w = __BASEWIDTH - (6.5 * GRID_W) - (2 * __GUI_SETTING_SPACING_X);
            colorBorder[] = {0, 0, 0, 1};
        };

        class ButtonRemove : SVAR(ctrlButtonPictureKeepAspect) {
            idc = __IDC_BUTTON_1;
            x = __BASEWIDTH - (5 * GRID_W) - (2 * __GUI_SETTING_SPACING_X);
            y = __GUI_SETTING_LINE_H + (1.5 * GRID_H);
            h = (5 * GRID_H);
            w = (5 * GRID_W);
            text = ICON(content,remove);
            tooltip = "Remove selected entry from list";
        };

        class ButtonClear: ButtonRemove {
            idc = __IDC_BUTTON_2;
            y = __GUI_SETTING_LINE_H + (7 * GRID_H);
            text = ICON(content,clear);
            tooltip = "Removes all items from current shape";
        };
	};
};
