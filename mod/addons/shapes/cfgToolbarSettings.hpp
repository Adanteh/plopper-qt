class ToolbarSettings {
    class BrushList {
        settingClass = QGVAR(CtrlShapeList);
    };
    class ShapeType {
        displayName = "Shape Type";
        tooltip = "Sort of shapes (Polyline for roads, polygon for filling in stuff";
        settingClass = QCVAR(CtrlCombo);
        settingChangeCode = "['shapefiletype.set', _this] call ADA_Shapes_fnc_handleToolbarButtons;";
        currentValueCode = "['shapefiletype.get', [_dropdown]] call ADA_Shapes_fnc_handleToolbarButtons";
    };
    class LayerName {
        displayName = "Layer";
        tooltip = "Layername for this record";
        settingClass = QCVAR(CtrlTextEdit);
        settingChangeCode = "['value.ctrl.set', ['__LAYER', _value]] call ADA_Shapes_fnc_handleToolbarButtons;";
        currentValueCode = "['value.ctrl.get', ['__LAYER', '', _control]] call ADA_Shapes_fnc_handleToolbarButtons";
    };
    class RoadType {
        displayName = "Road Type";
        tooltip = "Index of road in roadslib.cfg";
        settingClass = QCVAR(CtrlNumberEdit);
        settingChangeCode = "['value.ctrl.set', ['ID', _value]] call ADA_Shapes_fnc_handleToolbarButtons;";
        currentValueCode = "['value.ctrl.get', ['ID', 1, _control]] call ADA_Shapes_fnc_handleToolbarButtons";
    };
    class RoadOrder: RoadType {
        displayName = "Z-Order";
        tooltip = "Z-order of roads (Lower goes further on top)";
        settingChangeCode = "['value.ctrl.set', ['ORDER', _value]] call ADA_Shapes_fnc_handleToolbarButtons;";
        currentValueCode = "['value.ctrl.get', ['ORDER', 25, _control]] call ADA_Shapes_fnc_handleToolbarButtons";
    };
};
