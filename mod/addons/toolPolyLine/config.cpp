#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class ObjectPlacement {
    class Tools {
        class ADDON {
            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
                class mouseClick { function = QFUNC(mouseButtonClick); };
                class mouseDoubleClick { function = QFUNC(mouseButtonDblClick); };
            };
        };
    };
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main"};

			class clientInit { noImport = 1; };
			class drawPolyLine { noImport = 1; };
            class getPolyLinePoints;

			class Events {
                class mouseButtonClick { noImport = 1; };
                class mouseButtonDblClick { noImport = 1; };
				class toolActivate { noImport = 1; };
				class toolDeactivate { noImport = 1; };
			};
		};
	};
};
