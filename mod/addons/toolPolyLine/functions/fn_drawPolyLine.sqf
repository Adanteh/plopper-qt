/*
    Function:       ADA_toolPolyLine_fnc_drawPolyLine
    Author:         Adanteh
    Description:    Draws our line
*/
#include "macros.hpp"

private _polyPoints = GVAR(namespace) getVariable ["polyPoints", []];
if (_polyPoints isEqualTo []) exitWith { };

private _color = [1, 0, 0, 1];
private _pointCount = (count _polyPoints) - 1;
for "_i" from 1 to _pointCount do {
    drawLine3D [
        (_polyPoints select _i),
        (_polyPoints select (_i - 1)),
        _color
    ];
};

// -- If this is set to true we want the last point to connect back to the first point (in different color)
private _drawToFirst = GVAR(namespace) getVariable ["drawPoly", false];
if (_drawToFirst) then {
    drawLine3D [
        (_polyPoints select _pointCount),
        (_polyPoints select 0),
        [0, 1, 1, 1]
    ];
};

if (_pointCount >= 0) then {
    private _drawToMouse = GVAR(namespace) getVariable ["drawToMouse", true];
    if (_drawToMouse) then {
        private _mousePos = screenToWorld getMousePosition;
        _mousePos set [2, 0.3];
        drawLine3D [
            (_polyPoints select _pointCount),
            _mousePos,
            [1, 0, 0, 1]
        ];
    };
};
