/*
    Function:       ADA_toolPolyline_fnc_getPolyLinePoints
    Author:         Adanteh
    Description:    Gets the coordinates of our polyline points, mainly used as imported function
*/
#include "macros.hpp"

GVAR(namespace) getVariable ["polyPoints", []];
