/*
    Function:       ADA_toolPolyLine_fnc_toolDeactivate
    Author:         Adanteh
    Description:    Run when tool is deactivated
*/
#include "macros.hpp"

GVAR(namespace) setVariable ["polyPoints", nil];

private _handle = missionNamespace getVariable [QGVAR(pfh), -1];
if (_handle != -1) then {
    [_handle] call CFUNC(removePerFrameHandler);
};
missionNamespace setVariable [QGVAR(pfh), nil];
