/*
    Function:       ADA_Filler_fnc_mouseButtonDblClick
    Author:         Adanteh
    Description:    Handles mouse double click
*/
#include "macros.hpp"

params ["_button", "_mousePosition", "_mousePositionWorld"];
if (_button != 0) exitWith { };

if ([QGVAR(doubleclick), 1] call MFUNC(uiGetSetting) > 0) then {
    GVAR(namespace) setVariable ["polyPoints", nil];
};


