/*
    Function:       ADA_toolPolyLine_fnc_mouseButtonClick
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params [["_button", 0], "_mousePosition", "_positionWorld"];
switch (_button) do {
    case 0: {
        // -- Alt click removes last point
        private _polyPoints = GVAR(namespace) getVariable ["polyPoints", []];
        private _removePoint = ["Deselect"] call MFUNC(modifierPressed);
        if (_removePoint) exitWith {
            if (_polyPoints isEqualTo []) exitWith { };
            _polyPoints deleteAt (count _polyPoints - 1);
            GVAR(namespace) setVariable ["polyPoints", _polyPoints];
        };

        _positionWorld set [2, (_positionWorld select 2) + 0.1]; // -- Increase height a little so its easier to see

        // -- Control click will set the position to the first point in the line (Only if there's more than one point already)
        private _closeLoop = GVAR(namespace) getVariable ["closeLoopKey", false];
        if (_closeLoop) then {
            if (count _polyPoints > 1) then {
                _positionWorld = _polyPoints select 0;
            };
        };

        // -- Save click position as next point to draw at
        _polyPoints pushBack _positionWorld;
        GVAR(namespace) setVariable ["polyPoints", _polyPoints];
    };

    case 1: {
        GVAR(namespace) setVariable ["polyPoints", nil];
    };
};
