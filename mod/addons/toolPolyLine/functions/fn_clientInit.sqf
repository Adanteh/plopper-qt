/*
    Function:       ADA_toolPolyLine_fnc_clientInit
    Author:         Adanteh
    Description:    Creates namespaces
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);

[
    "Line Tool", QGVAR(closeLoop), ["Close Loop Modifier", "Hold this key and click to close the current polygon"],
    { GVAR(namespace) setVariable ["closeLoopKey", true]; false },
    { GVAR(namespace) setVariable ["closeLoopKey", false]; false },
    [DIK_LCONTROL, [nil, nil, nil]], false, false
] call MFUNC(addKeybind);
