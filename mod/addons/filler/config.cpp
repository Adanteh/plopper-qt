#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            tools[] = {QSVAR(toolPolyLine), QSVAR(toolBounding)};
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main", "toolPolyLine"};

            class clientInit;
            class fillPolygon;
            class getDataStructure;
        };
    };
};
