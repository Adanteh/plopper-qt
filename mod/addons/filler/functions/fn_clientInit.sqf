/*
    Function:       ADA_Filler_fnc_clientInit
    Author:         Adanteh
    Description:    Client init Filler mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(modelList) = [];
GVAR(selectionIndex) = 0;
GVAR(fillIndex) = 0;
GVAR(fillLayer) = "";

["filler.add_from_selection", { 
    private _data = [(_this select 0)] call MFUNC(getSelectionData);
    ["plopper.plop", ["tools.filler.add_from_selection", _data]] call PY3_fnc_callExtension;
}] call CFUNC(addEventHandler);


["filler.generate", { _this call FUNC(fillPolygon) }] call CFUNC(addEventHandler);


