#include "macros.hpp"
/*
    Function:       ADA_filler_fnc_fillPolygon
    Author:         Adanteh
    Description:    Fills entire polygon with shiny objects, from our list in toolbar
*/

// -- Settings
private _data = ["plopper.plop", ["tools.filler.get_brush"]] call PY3_fnc_callExtension;
private _flow = _data deleteAt 0;
private _allowOnRoad = MVAR(set_filler_allowOnRoad);
private _allowOnWater = MVAR(set_filler_allowOnWater);
private _spaceToTerrain = MVAR(set_filler_spaceToTerrain);
private _alignToTerrain = MVAR(set_slopeAlign);
private _weight = _data apply { _x select 2 };

private _elements = [];


// -- Get the area to place things in
private _polypoints = call EFUNC(toolPolyline,getPolyLinePoints);
private _origin = [_polypoints] call CFUNC(calcPolygonCoords);
_origin params ["_bottomLeft", "_topRight", "_width", "_height"];

GVAR(fillIndex) = GVAR(fillIndex) + 1;
GVAR(fillLayer) = format ["Fill #%1", GVAR(fillIndex)];

private ["_gridX", "_gridY"];
for "_gridX" from 0 to _width step _flow do {
    for "_gridY" from 0 to _height step _flow do {
        call {
            private _placePos = _bottomLeft vectorAdd [RANDOMOFFSET(_gridX,_flow), RANDOMOFFSET(_gridY,_flow), 0];
            if (!_allowOnWater && {surfaceIsWater _placePos}) exitWith { [["surfaceIsWater"]] call CFUNC(debugMessage); };
            if (!_allowOnRoad && {isOnRoad _placePos}) exitWith { [["isOnRoad"]] call CFUNC(debugMessage); };
            if !(_placePos inPolygon _polypoints) exitWith { };
            _elements pushBack _placePos;

        };
    };
};

if (count _elements == 0) exitWith {};

[_elements, {
    // _element from Batch
    _args params ["_objectsPlaced", "_data", "_weight", "_alignToTerrain", "_spaceToTerrain"];
    private _entry = _data selectRandomWeighted _weight;
    _entry params (call FUNC(getDataStructure));

    private _nearby = (call MFUNC(getObjectsPlaced)) inAreaArray [_element, _spacing, _spacing, 0, false, -1];
    if (count _nearby > 0) exitWith { };

    if (_spaceToTerrain && { (count (nearestTerrainObjects [ASLtoAGL (ATLToASL _element), [], _spacing]) > 0) }) exitWith { };

    private _direction = (0) + random _dir_random;
    private _scale = _min_scale + random (_max_scale - _min_scale);
    private _pitchBankYaw = [random _rotation_random, random _rotation_random, _direction];
    _element set [2, getTerrainHeightASL _element];

    // 0 horizon setting uses global align, 1 sets it to horizontal, 2 to slope align
    private _horizontal = !_alignToTerrain;
    if (_keepHorizontal > 0) then {
        _horizontal = (_keepHorizontal == 1);
    };
    if !(_horizontal) then {
        _pitchBankYaw = [];
    };

    private _vars = [[QMVAR(ObjectLayer), GVAR(fillLayer)]];
    _objectsPlaced pushBack ([_modelPath, _element, _direction, _scale, _horizontal, _pitchBankYaw, false, _vars] call MFUNC(createObject));
}, {
    if (MVAR(set_filler_selectOnDone)) then {
        [(_args select 0)] call MFUNC(selectObjects);
    };
}, [[], _data, _weight, _alignToTerrain, _spaceToTerrain], 10] call CFUNC(addBatch);



true;
