/*
    Function:       ADA_toolGizmo_fnc_updateGizmo
    Author:         Adanteh
    Description:    Updates the position of our gizmo indicator
*/
#include "macros.hpp"

params [["_object", objNull], ["_offset", [0, 0, 0]], ["_multiSelect", false]];
if (isnull _object) exitWith {
    private _handle = GVAR(namespace) getVariable ["posUpdatePFH", -1];
    if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
    GVAR(namespace) setVariable ["posUpdatePFH", -1];
    GVAR(gizmoObject) setPosASL [-1500, -1500, 0];
};

GVAR(gizmoData) = [_object, _offset, _multiSelect];
if (_multiSelect) then {
    [GVAR(gizmoObject), [0, 0, 0]] call CFUNC(setPitchBankYaw);
};

private _handle = GVAR(namespace) getVariable ["posUpdatePFH", -1];
if (_handle == -1) then {

    // -- AttachTo doesn't work for simpleObject, you can attach, but it won't update properly, so we use PFH instead
    // -- Also attach messed up our pitchbankyaw system in ways i can't explain, so screw it
    _handle = [{
        
        GVAR(gizmoData) params ["_object", "_offset", "_multiSelect"];
        private _pos = (_object modelToWorld _offset);
        _pos set [2, (getTerrainHeightASL _pos + 0.5)];

        if (isNull GVAR(gizmoObject)) then {
            GVAR(gizmoObject) = "ADA_Gizmo" createVehicleLocal [0, 0, 0];
        };
        GVAR(gizmoObject) setPosWorld _pos;
        private _pitchBankYaw = [0, 0, (_object getVariable [QMVAR(PitchBankYaw), [0, 0, 0]]) select 2];
        [GVAR(gizmoObject), _pitchBankYaw] call CFUNC(setPitchBankYaw);

    }, 0, []] call CFUNC(addPerFrameHandler);
    GVAR(namespace) setVariable ["posUpdatePFH", _handle];
};
