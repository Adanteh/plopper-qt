#include "macros.hpp"
/*
    Function:       ADA_toolGizmo_fnc_gizmoHighlight
    Author:         Adanteh
    Description:    Highlights one of the axis
*/

params [["_axis", 0, [0, ""]], ["_highlight", true]];

if (_axis isEqualType "") then {
    _axis = ["axis.move.x", "axis.move.y", "axis.move.z", "axis.rotate.y", "axis.rotate.x", "axis.rotate.z"] find (toLower _axis);
};

#define __REDTEX_HIGHLIGHT "#(rgb,8,8,3)color(0.53,0.08,0.02,1,co)"
#define __GREENTEX_HIGHLIGHT "#(rgb,8,8,3)color(0.05,0.55,0.01,1,co)"
#define __BLUETEX_HIGHLIGHT "#(rgb,8,8,3)color(0.06,0.2,0.55,1,co)"
// vec3(0.53,0.08,0.02)
// vec3(0.05,0.55,0.01)
// vec3(0.06,0.2,0.55)

// -- Get the proper color for our thing
private _color = if (_highlight) then {
    [__REDTEX_HIGHLIGHT, __GREENTEX_HIGHLIGHT, __BLUETEX_HIGHLIGHT] select (_axis mod 3);
} else {
    [__REDTEX, __GREENTEX, __BLUETEX] select (_axis mod 3);
};
GVAR(gizmoObject) setObjectTexture [_axis, _color];
