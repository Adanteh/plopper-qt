/*
    Function:       ADA_toolGizmo_fnc_clientInit
    Author:         Adanteh
    Description:    Used to show a rotation / manipulation gizmo. Mainly used to show rotation axis
*/
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(gizmoObject) = objNull;
