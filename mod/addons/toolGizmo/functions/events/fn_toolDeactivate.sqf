/*
    Function:       ADA_toolGizmo_fnc_toolDeactivate
    Author:         Adanteh
    Description:    Run when tool is deactivated
*/
#include "macros.hpp"

if !(isNull GVAR(gizmoObject)) then {
    deleteVehicle GVAR(gizmoObject);
};

private _handle = GVAR(namespace) getVariable ["posUpdatePFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
GVAR(namespace) setVariable ["posUpdatePFH", -1];
