/*
    Function:       ADA_toolGizmo_fnc_toolActivate
    Author:         Adanteh
    Description:    Run when tool is activated
*/
#include "macros.hpp"

if (isNull GVAR(gizmoObject)) then {
    GVAR(gizmoObject) = "ADA_Gizmo" createVehicleLocal [0, 0, 0];
};
