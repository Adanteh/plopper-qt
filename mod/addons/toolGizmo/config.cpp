#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class ObjectPlacement {
    class Tools {
        class ADDON {
            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
            };
        };
    };
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main"};

			class clientInit { noImport = 1; };
			class updateGizmo;
            class gizmoHighlight;
			class Events {
				class toolActivate { noImport = 1; };
				class toolDeactivate { noImport = 1; };
			};
		};
	};
};

#include "cfgVehicles.hpp"
