#define MODULE toolGizmo
#define __GIZMONAME "ADA_Gizmo"

#define __REDTEX "#(rgb,8,8,3)color(0.53,0.08,0.02,0.2,co)"
#define __GREENTEX "#(rgb,8,8,3)color(0.05,0.55,0.01,0.2,co)"
#define __BLUETEX "#(rgb,8,8,3)color(0.06,0.2,0.55,0.2,co)"
