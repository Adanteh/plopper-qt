class CfgVehicles {
    class NonStrategic;
    class ADA_Gizmo: NonStrategic {
        displayName = "Gizmo";
        model = TEXTURE(ada_gizmo.p3d);
        hiddenSelections[] = {"arrow_x", "arrow_y", "arrow_z", "ring_x", "ring_y", "ring_z"};
        hiddenSelectionsTextures[] = {__REDTEX, __GREENTEX, __BLUETEX, __REDTEX, __GREENTEX, __BLUETEX};
        featureType = 2; // -- Show on max range
        author = "Mondkalb";
        scope = 2;
        scopeCurator = 2;
        accuracy = 1000;
    };

    class ADA_Bounding_Box: NonStrategic {
        scope = 2;
        scopeCurator = 2;
        displayName  = "Bounding Box";
        model        = TEXTURE(ada_bounding_box.p3d);
        author = "Mondkalb";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {__BLUETEX};
        class AnimationSources
        {
            class left
            {
                source     = "user";
                initPhase  = 0;
                animPeriod = 1; // Hier den wert 0 ausprobieren, schauen ob es dann auch noch funktioniert.
            };
            /*
            class right : left {};
            class front : left {};
            class rear : left {};
            class top : left {};
            class bottom : left {};
            */
        };
    };
};
