#define __INCLUDE_IDCS
#include "module.hpp"
#include "macros.hpp"


class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {};
    };
};


class PREFIX {
    #include "localModules.hpp"

    class MODULE {
        class cleanup {
            enabled = 1;
            speed = 120;
            schedule = 0;
        };
    };
};



// -- Add more functions through modular system, instead of here.
#define RECOMPILE recompile = 1;
#ifndef RECOMPILE
    #define RECOMPILE
#endif

class CfgFunctions {
    class ADDON {
        class Compiling {
            file = "x\plopper\addons\core\compile";
            class compile { RECOMPILE };
            class moduleLoad {
                RECOMPILE
                prestart = 1;
                preinit = 1;
            };
            class moduleQueue { RECOMPILE };
            class moduleQueueExecute {
                RECOMPILE
                preinit = 1;
                postinit = 1;
            };
        };
    };
};

class CfgLocationTypes {
    class Name;  // The commonly used FakeTown is not available in Buldozer
    class SVAR(Namespace): Name {
        size = 0;
    };
}

#include "ui\ui.hpp"
