/*
    Function:       ADA_Core_fnc_settingsWindow
    Author:         Adanteh
    Description:    Handles showing, adding and editing the settings windows
    Example 1:      ["show", "camera"] call ADA_Core_fnc_settingsWindow;
    Example 2:
        ['add', ['camera', "Camera", 'movement', ['Modifier Speed', 'Extra speed increase when holding shift',
                GQVAR(CtrlSlider), // Controlclassname
                { missionNamespace getVariable ["CAMERA_SHIFT_SPEED_MULTIPLIER", 3] }, // Value to set when loading
                { true; }, // Enabled
                { missionNamespace setVariable ["CAMERA_SHIFT_SPEED_MULTIPLIER", (_this select 1)] }, // Value to set when changing
                [[2, 10], [0.05, 1]] // Range for slider
        ]]] call FUNC(settingsWindow);

*/
/*#include __INCLUDE_IDCS*/
#include "macros.hpp"

disableSerialization;

#define __SPACING (getNumber (configFile >> _categoryCtrlClass >> "y"))
#define __SPACINGGROUP (getNumber (configFile >> _categoryCtrlClass >> "Controls" >> "Title" >> "h") * 1.5)
#define CTRL(x) (_paneCtrl controlsGroupCtrl x)

params ["_mode", ["_args", []]];
private _display = uiNamespace getVariable [QGVAR(SettingsWindow), displayNull];
//[_this, "lime", 15, 10] call CFUNC(debugMessage);

if (isNil QGVAR(settingsNamespace)) then {
    GVAR(settingsNamespace) = false call CFUNC(createNamespace);
};

switch (toLower _mode) do {
    case "show": {
        _args params ["_category", ["_type", "dialog"], ["_parentWindow", findDisplay 46], ["_parentControl", controlNull], ["_className", QGVAR(CtrlSettingsPopup)]];


        uiNamespace setVariable [QGVAR(SettingsCategory), toLower _category];
        uiNamespace setVariable [QGVAR(SettingsType), toLower _type];
        //[[_category, _type, _parentControl, _className], "purple", 15, 10] call CFUNC(debugMessage);

        switch (toLower _type) do {
            case "popupsmall": { // -- Small popup on mouse  position
                uiNamespace setVariable [QGVAR(SettingsWindow), _parentWindow];
                private _popup = _parentWindow ctrlCreate [_className, -1, controlNull];
            };

            case "embed": { //-- Creates it in another control group
                // -- Only allow 1 embed, delete previous if there is one
                private _hasEmbed = _parentControl getVariable [QGVAR(SettingsEmbed), controlNull];
                if !(isNull _hasEmbed) then { ctrlDelete _hasEmbed; true };

                uiNamespace setVariable [QGVAR(SettingsWindow), _parentWindow];
                private _settingsCtrl = _parentWindow ctrlCreate [_className, -1, _parentControl];
                _parentControl setVariable [QGVAR(SettingsEmbed), _settingsCtrl];
            };

            case "dialog"; // -- Separate dialog
            default {
                createDialog QGVAR(SettingsWindow);
            };
        };
    };

    case "add": {
        _args params ["_category", "_categoryTitle", "_subCategory", "_settingDetails"];
        _settingDetails params ["_displayName", "_tooltip", "_controlType", "_defaultValue", "_condition", "_action", ["_values", []], ["_settingTag", ""]];
        _action = [_action] call CFUNC(codeToString);

        // -- Find the category and subcategory within the saved array -- //
        private _categoryEntry = GVAR(settingsNamespace) getVariable [_category, locationNull];
        if (isNull _categoryEntry) then {
            _categoryEntry = false call CFUNC(createNamespace);
            GVAR(settingsNamespace) setVariable [_category, _categoryEntry];
            GVAR(settingsNamespace) setVariable [_category + ".title", _categoryTitle];
        };

        // -- Get entry for subcategory on the category settingsNamespace
        private _subCategoryEntry = _categoryEntry getVariable [_subCategory, []];
        _subCategoryEntry pushBack [_displayName, _tooltip, _controlType, _defaultValue, _condition, _action, _values, _settingTag];
        _categoryEntry setVariable [_subCategory, _subCategoryEntry];
    };

    case "load": {
        private _category = uiNamespace getVariable [QGVAR(SettingsCategory), ""];
        private _type = uiNamespace getVariable [QGVAR(SettingsType), ""];

        //[["Showing settings: ", _category, _type, _display, typeName _display], "purple", 15, 10] call CFUNC(debugMessage);

        private _categoryEntry = GVAR(settingsNamespace) getVariable [_category, locationNull];
        if (isNull _categoryEntry) exitWith { };

        private _categoryTitle = GVAR(settingsNamespace) getVariable [_category + ".title", _category];
        private _settingCategories = (allVariables _categoryEntry);

        private _isPopup = _type in ["popupsmall", "embed"];
        private _paneCtrl = [_display displayCtrl 116, uiNamespace getVariable [QGVAR(SettingsControlGroup), controlNull]] select _isPopup;
        private _categoryCtrlClass = [QGVAR(CtrlCategory), QGVAR(CtrlCategorySmall)] select _isPopup;
        CTRL(4000) ctrlSetText (toUpper _categoryTitle);
        private _settingControlsGroup = CTRL(4001);
        private _settingsCtrlPos = ctrlPosition _settingsControlGroup;
        _settingsCtrlPos set [3, 1];
        _settingsControlGroup ctrlSetPosition _settingsCtrlPos;
        _settingsControlGroup ctrlCommit 0;

        //[["_paneCtrl", _paneCtrl, ctrlPosition _paneCtrl], "white", 15, 10] call CFUNC(debugMessage);
        //[["_settingControlsGroup", ctrlPosition _settingControlsGroup], "white", 15, 10] call CFUNC(debugMessage);

        uiNamespace setVariable [QGVAR(controlGroup), _settingControlsGroup];

        private _yPosGlobal = 0;
        private _subCatIndex = 0;
        {
            private _settingsCategory = _x;
            private _settings = _categoryEntry getVariable _settingsCategory;
            private _subcategoryControlsgroupIDC = (10000 + (_subCatIndex * 1000));
            private _controlsGroup = _display ctrlCreate [_categoryCtrlClass, _subcategoryControlsgroupIDC, _settingControlsGroup];
            private _controlsGroupCategories = _controlsGroup controlsGroupCtrl 100;
            (_controlsGroup controlsGroupCtrl 1) ctrlSetText _x;

            //[['Add category', _settingControlsGroup, _settingsCategory, _controlsGroup], "white", 15, 10] call CFUNC(debugMessage);

            private _yPos = 0;
            private _hGroup = 0;

            {
                _x params ["", "", "_controlType"];
                _x call FUNC(settingsAddControl);

                // -- Prepare position for the next control, based on height of this control -- //
                _yPos = _yPos + (getNumber (configFile >> _controlType >> "h")) + __SPACING;
            } forEach _settings;

            _controlsGroup ctrlSetPosition [0, _yPosGlobal, (getNumber (configFile >> _categoryCtrlClass >> "w")), _yPos + ((getNumber (configFile >> _categoryCtrlClass >> "controls" >> "Attributes" >> "y")))];
            _controlsGroupCategories ctrlSetPosition [
                (getNumber (configFile >> _categoryCtrlClass >> "controls" >> "Attributes" >> "x")),
                (getNumber (configFile >> _categoryCtrlClass >> "controls" >> "Attributes" >> "y")),
                (getNumber (configFile >> _categoryCtrlClass >> "controls" >> "Attributes" >> "w")),
                (_yPos)];
            _controlsGroup ctrlCommit 0;
            _controlsGroupCategories ctrlCommit 0;
            //[['Positions' , ctrlPosition _controlsGroup, ctrlPosition _controlsGroupCategories], "white", 15, 10] call CFUNC(debugMessage);
            _yPosGlobal = _yPosGlobal + _yPos + __SPACINGGROUP;
            _subCatIndex = _subCatIndex + 1;
        } forEach _settingCategories;

        // -- Adjust the popup size based on the amount of availble settings
        private _varName = format [QGVAR(SettingsControlGroup_%1), _category];
        uiNamespace setVariable [_varName, _paneCtrl];

        if (_type in ["popupsmall", "embed"]) then {
            // -- Adjust the full popup
            private _baseHeight = [(configFile >> (ctrlClassName _paneCtrl) >> "h"), 0] call CFUNC(uiGetCfgSize);
            private _newHeight = _yPosGlobal + 2*_baseHeight;

            // -- Adjust control group size to be categories height + header height
            private _ctrlPos = ctrlPosition _paneCtrl;
            _ctrlPos set [3, _newHeight];
            _paneCtrl ctrlSetPosition _ctrlPos;
            _paneCtrl ctrlCommit 0;

            //[['Popup: ', _paneCtrl, ctrlPosition _paneCtrl, [_paneCtrl] call CFUNC(getCtrlPositionReal)], "white", 15] call CFUNC(debugMessage);
            //[[safeZoneX, safeZoneY], "white", 15] call CFUNC(debugMessage);

            // -- Adjust group size to be main control group minus header size
            _ctrlPos = ctrlPosition CTRL(4001);
            _ctrlPos set [3, _newHeight - (_ctrlPos select 1)];
            CTRL(4001) ctrlSetPosition _ctrlPos;
            CTRL(4001) ctrlCommit 0;
            //[['Group size: ', CTRL(4001), ctrlPosition CTRL(4001)], "white", 15] call CFUNC(debugMessage);


            // -- Adjust background size to be main control group minus header size
            _ctrlPos = ctrlPosition CTRL(3999);
            _ctrlPos set [3, _newHeight - (_ctrlPos select 1)];
            CTRL(3999) ctrlSetPosition _ctrlPos;
            CTRL(3999) ctrlCommit 0;
            //[['Background: ', CTRL(3999), ctrlPosition CTRL(3999)], "white", 15] call CFUNC(debugMessage);

            if (_type == "popupsmall") then {
                CTRL(1) ctrlSetEventHandler ["ButtonClick", format ["['close', ['%2']] call %1", QFUNC(settingsWindow), _category]];
                getMousePosition params ["_xPos", "_yPos"];
                [_paneCtrl, [_xPos + 0.015, _yPos - 0.15]] call CFUNC(setCtrlPosInScreen);
            };
        };

    };

    // -- Close through pressing the button
    case "close": {
        _args params ["_category"];
        private _varName = format [QGVAR(SettingsControlGroup_%1), _category];
        private _popup = uiNamespace getVariable [_varName, controlNull];
        ctrlDelete _popup;
    };

    case "unload": {
        uiNamespace setVariable [QGVAR(SettingsCategory), nil];
        saveProfileNamespace;
        //ctrlSetFocus __GUI_VIEWPORT;
    };

    default {
        // Invalid
    };
};

true;
