/*
    Function:       ADA_Core_fnc_settingsNumberEditScroll
    Author:         Adanteh
    Description:    This allows you to adjust the ctrlSettingNumber by scrolling when mouseover it
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["_display", "_delta"];
private _mouseoverControl = (_display getVariable [QGVAR(MouseZNumberControl), []]) param [0, controlNull];
if (isNull _mouseoverControl) exitWith { };

_delta = (_delta / 1.2); // -- by default zChanged is in 1.2 multiples
if ([DIK_LSHIFT] call CFUNC(keyPressed)) then {
    _delta = _delta * 5;
};

private _number = parseNumber (ctrlText _mouseoverControl);
(_mouseoverControl getVariable QGVAR(limits)) params [["_tick", 1], "_limitLow", "_limitHigh", ["_decimals", 0]];
_number = _number + (_tick * _delta);

if !(isNil "_limitLow") then {
    _number = _limitLow max _number;
};
if !(isNil "_limitHigh") then {
    _number = _limitHigh min _number;
};

_mouseoverControl ctrlSetText (_number toFixed _decimals);
private _callback = _mouseoverControl getVariable [QGVAR(valueChanged), {}];
[_mouseoverControl] call _callback;
