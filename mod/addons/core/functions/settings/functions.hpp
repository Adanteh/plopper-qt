class Settings {
    class clientInitSettings;
    class settingsNumberEditScroll;
    class settingsAddControl;
    class settingsWindow;
    class settingsCategoryExists;

    class Weather {
        class initWeather;
        class serverInitWeather;
        class weatherCommit;
        class weatherPreview;
        class weatherSetDate;
        class weatherSetFog;
        class weatherSetOvercast;
        class weatherSetRain;
        class weatherSetWind;
        class weatherStopRain;
    };
};
