/*
    Function:       ADA_Core_fnc_weatherStopRain
    Author:         Adanteh
    Description:    Function to force stop rain, which is automagically reenabled on higher overcast settings by BIS weather gods
*/
#include "macros.hpp"

if !(missionNamespace getVariable [QGVAR(weatherAllowRain), false]) then {
	if (rain != 0) then {
		0 setRain 0;
	};
};
