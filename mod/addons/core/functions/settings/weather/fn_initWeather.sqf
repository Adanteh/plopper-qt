/*
    Function:       ADA_Core_fnc_postInitWeather
    Author:         Adanteh
    Description:    Inits all the weather settings, plus makes the rain stop on high overcast
*/
#include "macros.hpp"

// -- Stops the rain from autostarting on high overcast settings -- //
if !(is3DEN) then {
    [{ call FUNC(weatherStopRain) }, 5, []] call CFUNC(addPerFramehandler);
};
