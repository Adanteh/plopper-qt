/*
    Function:       ADA_Core_fnc_settingsAddControl
    Author:         Adanteh
    Description:    Adds a control for given setting type
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params ["_displayName", "_tooltip", "_controlType", "_defaultValue", "_condition", "_action", ["_values", []], ["_settingTag", ""]];

[[_fnc_scriptNameShort, _displayName, _controlType], "cyan", 3] call CFUNC(debugMessage);

_condition = [_condition] call CFUNC(parseToBool);
private _controlsGroupIDC = (_subcategoryControlsgroupIDC + (_forEachIndex + 1));
private _control = _display ctrlCreate [_controlType, _controlsGroupIDC, _controlsGroupCategories];
(_control controlsGroupCtrl 1) ctrlSetText _displayName;
if (_tooltip != "") then { (_control controlsGroupCtrl 1) ctrlSetTooltip _tooltip; };

private _ctrlPos = ctrlPosition _control;
_ctrlPos set [1, _yPos];
_control ctrlSetPosition _ctrlPos;
_control ctrlCommit 0;

switch (toLower _controlType) do {
    case (toLower QGVAR(CtrlCheckbox)): {
        (_control controlsGroupCtrl 100) cbSetChecked ([_defaultValue] call CFUNC(parseToBool));
        if (_condition) then {
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['CheckedChanged', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlSlider)): {
        if (!(_defaultValue isEqualType 0)) then {
            if (_defaultValue isEqualType "") then {
                _defaultValue = compile _defaultValue;
            };
            _defaultValue = call _defaultValue;
        };

        (_control controlsGroupCtrl 100) sliderSetRange (_values select 0);
        (_control controlsGroupCtrl 100) sliderSetSpeed (_values select 1);
        (_control controlsGroupCtrl 100) sliderSetPosition _defaultValue;

        if (_condition) then {
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['SliderPosChanged', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlButton)): {
        (_control controlsGroupCtrl 100) ctrlSetText (_values select 0);
        if (_condition) then {
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['ButtonClick', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlButtonSmall)): { // -- No text label, but text goes on BUTTON itself
        (_control controlsGroupCtrl 100) ctrlSetText _displayName;
        (_control controlsGroupCtrl 100) ctrlSetTooltip _tooltip;
        if (_condition) then {
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['ButtonClick', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlToolbox3)): {
        private _toolbox = (_control controlsGroupCtrl 100);
        private _default = -1;
        _defaultValue = call (_defaultValue call CFUNC(parseToCode));
        _toolbox setVariable ["type", "listbox"];

        {
            _x params ["_text", "_value"];
            private _index = _toolbox lbAdd _text;
            _toolbox lbSetValue [_index, _value];
            if (_value isEqualTo _defaultValue) then {
                _default = _index;
            };
        } forEach _values;
        _toolbox lbSetCurSel _default;

        if (_condition) then {
            _action = format ["
                private _control = (_this select 0);
                private _value = (_this select 1);
                private _settingTag = '%2';
                [_control, _value, _settingTag] call { %1 }
            ", _action, _settingTag];
            _toolbox ctrlSetEventHandler ['ToolboxSelChanged', _action];
        } else {
            _toolbox ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlNumberEdit)): {
        (_control controlsGroupCtrl 100) ctrlSetText str(call _defaultValue);
        (_control controlsGroupCtrl 100) setVariable ["type", "text"];
        if (_condition) then {
            // -- Append this, so we can retrieve the values easily in scripts -- //
            _action = format ["
                private _control = (_this select 0);
                private _value = parseNumber (ctrlText _control);
                private _settingTag = '%2';
                [_control, _value, _settingTag] call { %1 }
            ", _action, _settingTag];
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['KeyUp', _action];

            _values = _values apply { if (_x isEqualType "") then { nil } else { _x }};
            // -- This stuff allows you to change numbers by selecting box and using mouse to scroll, so you don't need to type
            (_control controlsGroupCtrl 100) setVariable [QGVAR(valueChanged), _action call CFUNC(parseToCode)];
            (_control controlsGroupCtrl 100) setVariable [QGVAR(limits), _values]; // -- used for settingsNumberEditScroll;
            private _colors = getArray (configFile >> QGVAR(CtrlNumberEdit) >> "colorMouseoverScroll");
            (_control controlsGroupCtrl 100) setVariable [QGVAR(colors), _colors];
            (_control controlsGroupCtrl 100) ctrlAddEventHandler ["SetFocus", { ['ctrl.numberEdit.setFocus', _this] call CFUNC(localEvent); }];
            (_control controlsGroupCtrl 100) ctrlAddEventHandler ["KillFocus", { ['ctrl.numberEdit.killFocus', _this] call CFUNC(localEvent); }];
            //(_control controlsGroupCtrl 100) ctrlSetEventHandler ['SetFocus', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlTextEdit)): {
        (_control controlsGroupCtrl 100) ctrlSetText str(call _defaultValue);
        (_control controlsGroupCtrl 100) setVariable ["type", "text"];
        if (_condition) then {
            // -- Append this, so we can retrieve the values easily in scripts -- //
            _action = format ["
                private _control = (_this select 0);
                private _value = (ctrlText _control);
                private _settingTag = '%2';
                [_control, _value, _settingTag] call { %1 }
            ", _action, _settingTag];
            (_control controlsGroupCtrl 100) ctrlSetEventHandler ['KeyUp', _action];
            private _colors = getArray (configFile >> QGVAR(CtrlNumberEdit) >> "colorMouseoverScroll");
            (_control controlsGroupCtrl 100) setVariable [QGVAR(colors), _colors];
            (_control controlsGroupCtrl 100) ctrlAddEventHandler ["SetFocus", { ['ctrl.textEdit.setFocus', _this] call CFUNC(localEvent); }];
            (_control controlsGroupCtrl 100) ctrlAddEventHandler ["KillFocus", { ['ctrl.textEdit.killFocus', _this] call CFUNC(localEvent); }];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlValueset2)): {
        _defaultValue = _defaultValue call CFUNC(parseToBool);
        (_control controlsGroupCtrl 100) ctrlSetText str(_defaultValue select 0);
        (_control controlsGroupCtrl 101) ctrlSetText str(_defaultValue select 1);
        if (_condition) then {
            // -- Append this, so we can retrieve the values easily in scripts -- //
            _action = format ["
                disableSerialization;
                private _controlGroup = uiNamespace getVariable ['%1', controlNull];
                private _controlGroupParent = (((_controlGroup controlsGroupCtrl %2) controlsGroupCtrl 100) controlsGroupCtrl %3);
                private _value = [ctrlText (_controlGroupParent controlsGroupCtrl 100), ctrlText (_controlGroupParent controlsGroupCtrl 101)];
                %4
            ", QGVAR(controlGroup), _subcategoryControlsgroupIDC, _controlsGroupIDC, _action];
            (_control controlsGroupCtrl 102) ctrlSetEventHandler ['ButtonClick', _action];
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 101) ctrlEnable false;
            (_control controlsGroupCtrl 102) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlTextBoxButton)): {
        (_control controlsGroupCtrl 100) ctrlSetText "";
        if (_condition) then {
            // -- Append this, so we can retrieve the values easily in scripts -- //
            _action = format ["
                disableSerialization;
                private _controlGroup = uiNamespace getVariable ['%1', controlNull];
                private _controlGroupParent = (((_controlGroup controlsGroupCtrl %2) controlsGroupCtrl 100) controlsGroupCtrl %3);
                private _value = ctrlText (_controlGroupParent controlsGroupCtrl 100);
                %4
            ", QGVAR(controlGroup), _subcategoryControlsgroupIDC, _controlsGroupIDC, _action];
            (_control controlsGroupCtrl 102) ctrlSetEventHandler ['ButtonClick', _action];
            (_control controlsGroupCtrl 102) ctrlSetText _values
        } else {
            (_control controlsGroupCtrl 100) ctrlEnable false;
            (_control controlsGroupCtrl 101) ctrlEnable false;
            (_control controlsGroupCtrl 102) ctrlEnable false;
            (_control controlsGroupCtrl 1) ctrlSetTextColor [1, 1, 1, 0.4];
        };
    };

    case (toLower QGVAR(CtrlPlayerSelector)): {
        if (_condition) then {
            // -- Append this, so we can retrieve the values easily in scripts -- //
            _action = format ["
                params ['', '_index'];
                private _unit = (missionNamespace getVariable ['%1', []]) param [_index, objNull];
                missionNamespace setVariable ['%2', _unit];
            ", QGVAR(playerList), QGVAR(playerSelected)];

            private _dropdown = (_control controlsGroupCtrl 101);
            private _allPlayers = +allPlayers;
            lbClear _dropDown;
            _dropdown ctrlSetEventHandler ['LbSelChanged', _action];
            GVAR(playerList) = _allPlayers;
            GVAR(playerSelected) = objNull;
            {
                private _name = name _x;
                private _lbIndex = _dropdown lbAdd _name;
            } forEach _allPlayers;
        } else {
            (_control controlsGroupCtrl 101) ctrlEnable false;
        };
    };

    case (toLower QGVAR(CtrlCombo)): {
        if (_condition) then {
            private _dropdown = (_control controlsGroupCtrl 101);
            lbClear _dropDown;
            _dropdown ctrlSetEventHandler ['LbSelChanged', ([_action] call CFUNC(codeToString))];
            if (_values isEqualType {}) then {
                call _values;
            };
        } else {
            (_control controlsGroupCtrl 101) ctrlEnable false;
        };
    };

    case (toLower QGVAR(CtrlKeybind)): {
        if (_defaultValue isEqualType "") then {
            _defaultValue = compile _defaultValue;
        };

        (_control controlsGroupCtrl 100) ctrlSetText (call _defaultValue);
        (_control controlsGroupCtrl 100) setVariable ["values", _values];
        (_control controlsGroupCtrl 100) ctrlSetEventHandler ['ButtonClick', ({
            private _button = (_this select 0);
            private _window = ctrlParent _button;
            private _values = _button getVariable ["values", []];
            _values params ["_keybindIndex", "_category", "_tag"];
            _button setVariable ["text", ctrlText _button];
            _button ctrlSetText "Press keybind";
            _window setVariable ["keybindChangeCtrl", _button];


            // -- Handle the keybind
            _window displayRemoveAllEventHandlers "KeyUp"; // -- Remove the other ones
            _window displayRemoveAllEventHandlers "KeyDown"; // -- Remove the other ones

            _window displayAddEventHandler ["KeyDown", "true"];
            _window displayAddEventHandler ["KeyUp", ({
                disableSerialization;
                params ["_window", "_key", "_shift", "_ctrl", "_alt"];
                private _button = _window getVariable ["keybindChangeCtrl", controlNull];

                //#define __EXCEPTIONARRAY [DIK_LCONTROL, DIK_LALT, DIK_LSHIFT, DIK_RCONTROL, DIK_RALT, DIK_RSHIFT, DIK_ESC]
                #define __EXCEPTIONARRAY [DIK_ESC]
                if (_key in __EXCEPTIONARRAY) exitWith {
                    _button ctrlSetText "Key not supported";
                    _button ctrlSetTextColor [0.8, 0, 0, 1];
                };

                _button ctrlSetTextColor [1, 1, 1, 1];
                private _newKeybind = [_key, [_shift, _ctrl, _alt]];
                private _values = _button getVariable ["values", []];
                _values params ["_keybindIndex", "_category", "_tag"];

                _button ctrlSetText ([_newKeybind] call FUNC(getKeybindText));
                [_category, _tag, _newKeybind] call FUNC(changeKeybind);

                _window displayRemoveAllEventHandlers "KeyUp";
                _window displayRemoveAllEventHandlers "KeyDown";

                true
            }) call CFUNC(codeToString)];
        }) call CFUNC(codeToString)];
        (_control controlsGroupCtrl 100) setVariable ['keybindIndex', _values select 0];
    };

    default {
        // -- Custom class type, load in function for it's config entry. If class doesn't exist or no code, give error message
        private _controlClass = configFile >> _controlType;
        private _knownClass = false;
        if (isclass _controlClass) then {
            private _function = getText (_controlClass >> "settingLoadCode");
            if (_function != "") then {
                _knownClass = true;
                private _code = [_function] call CFUNC(parseToCode);
                [_this, _control, _subcategoryControlsgroupIDC, _controlsGroupIDC] call _code;
            };
        };
        if !(_knownClass) then {
            [format ["Unknown setting type: '%1'", _controlType], "error", 10, 1] call CFUNC(debugMessage);
        };
    };
};
