/*
    Function:       ADA_Core_fnc_initCoreEvents;
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _enabled = true;

if !([_enabled] call FUNC(parseToBool)) exitWith { };

["playMove", {
    (_this select 0) params ["_unit", "_move"];
    if (!local _unit) exitWith {
        ["playMove", _unit, _this select 0] call FUNC(targetEvent);
    };
    _unit playMove _move;
}] call FUNC(addEventHandler);

["playMoveNow", {
    (_this select 0) params ["_unit", "_move"];
    if (!local _unit) exitWith {
        ["playMoveNow", _unit, _this select 0] call FUNC(targetEvent);
    };
    _unit playMoveNow _move;
}] call FUNC(addEventHandler);

["playActionNow", {
    (_this select 0) params ["_unit", "_move"];
    if (!local _unit) exitWith {
        ["playActionNow", _unit, _this select 0] call FUNC(targetEvent);
    };
    _unit playActionNow _move;
}] call FUNC(addEventHandler);

["setVectorUp", {
    (_this select 0) params ["_object", "_vector"];
    if (!local _object) exitWith {
        ["setVectorUp", _object, _this select 0] call FUNC(targetEvent);
    };
    _object setVectorUp _vector;
}] call FUNC(addEventHandler);

["allowDamage", {
    (_this select 0) params ["_object", "_allow"];
    if (!local _object) exitWith {
        ["allowDamage", _object, _this select 0] call FUNC(targetEvent);
    };
    _object allowDamage _allow;
}] call FUNC(addEventHandler);

["setFuel", {
    (_this select 0) params ["_vehicle", "_value"];
    if (!local _vehicle) exitWith {
        ["setFuel", _vehicle, _this select 0] call FUNC(targetEvent);
    };
    _vehicle setFuel _value;
}] call FUNC(addEventHandler);

["removeMagazineTurret", {
    (_this select 0) params ["_vehicle", "_args"];
    if (!local _vehicle) exitWith {
        ["removeMagazineTurret", _vehicle, _this select 0] call FUNC(targetEvent);
    };
    _vehicle removeMagazineTurret _args;
}] call FUNC(addEventHandler);

["addMagazineTurret", {
    (_this select 0) params ["_vehicle", "_args"];
    if (!local _vehicle) exitWith {
        ["addMagazineTurret", _vehicle, _this select 0] call FUNC(targetEvent);
    };
    _vehicle addMagazineTurret _args;
}] call FUNC(addEventHandler);

["selectLeader", {
    (_this select 0) params ["_group", "_unit"];
    if (isNull _group) exitWith {};
    if (!local _group) exitWith {
        if (isServer) exitWith {
            ["selectLeader", groupOwner _group, [_group, _unit]] call FUNC(targetEvent);
        };
        ["selectLeader", _this select 0] call FUNC(serverEvent);
    };
    _group selectLeader _unit;
}] call FUNC(addEventHandler);

["leaveVehicle", {
    (_this select 0) params ["_group", "_vehicle"];
    if (isNull _group) exitWith {};
    if (!local _group) exitWith {
        if (isServer) exitWith {
            ["leaveVehicle", groupOwner _group, [_group, _vehicle]] call FUNC(targetEvent);
        };
        ["leaveVehicle", _this select 0] call FUNC(serverEvent);
    };
    _group leaveVehicle _vehicle;
}] call FUNC(addEventHandler);

["setOwner", {
    (_this select 0) params ["_objects", ["_clientID", clientOwner]];
    if !(isServer) exitWith {
        ["setOwner", [_objects, _clientID]] call FUNC(serverEvent);
    };

    if (_objects isEqualType objNull) then {
        _objects setOwner _clientID;
    } else {
        {
            _x setOwner _clientID;
        } forEach _objects;
    };
}] call FUNC(addEventHandler);

// Events for commands with local effect
["switchMove", {
    (_this select 0) params ["_unit", "_move"];
    _unit switchMove _move;
}] call FUNC(addEventHandler);

["say3D", {
    (_this select 0) params ["_args", "_sound"];
    _args say3D _sound;
}] call FUNC(addEventHandler);

["playSound", {
    (_this select 0) params ["_sound", ["_speech", true]];
    playSound [_sound, _speech];
}] call FUNC(addEventHandler);

["setDir", {
    (_this select 0) params ["_object", "_value"];
    _object setDir _value;
}] call FUNC(addEventhandler);

["setVelocity", {
    (_this select 0) params ["_object", "_velocity", ["_stop", false]];
    _object setVelocity _velocity;

    if (_stop) then {
        [{
            params ["_object"];
            _object setVelocity [0, 0, 0];
        }, 2, [_object]] call FUNC(wait);
    };
}] call FUNC(addEventHandler);

["unassignVehicle", { // -- Use with target event to a unit array
    (_this select 0) params ["_units"];
    if !(_units isEqualType []) then { _units = [_units] };
    { unassignVehicle _x } forEach (_units select { local _x });
}] call FUNC(addEventHandler);

// -- This handles detection of entities. Very useful, because its pretty much an all in one handler, that supports all mods
GVAR(entitiesNew) = [];
GVAR(entities) = [];
GVAR(entitiesNextLoad) = -1;
GVAR(entityVehicleID) = 0;

["entityCreated", {
    (_this select 0) params ["_unit"];
    if (_unit isKindOf "CaManBase") then {
        ["entityManCreated", _unit] call FUNC(localEvent);
    } else {
        if !(fullCrew [_unit, "", true] isEqualTo []) then {

            private _id = format [QSVAR(vehicleID_%1), GVAR(entityVehicleID)];
            GVAR(entityVehicleID) = GVAR(entityVehicleID) + 1;

            _unit setVariable [QSVAR(vehicleID), _id]; // THIS IS A LOCAL ID ONLY
            missionNamespace setVariable [_id, _unit];

            // -- Add a little delay, so any MP assigned variables have time to sync
            [{ ["vehicleCreated", _this] call FUNC(localEvent); }, 0.5, [_unit, _id]] call CFUNC(wait);

        };
    };
}] call FUNC(addEventHandler);


// GVAR(smEntities) = [] call CFUNC(smCreate);
// [GVAR(smEntities), "init", {
//     if (GVAR(entitiesNextLoad) > diag_frameNo) exitWith { "wait" };
//     GVAR(entitiesNew) = (entities [[], [], true, false]) - GVAR(entities);
//     GVAR(entitiesNextLoad) = diag_frameNo + 15;
//     ["walkEntities", "init"] select (GVAR(entitiesNew) isEqualTo []);
// }] call CFUNC(smAddState);

// [GVAR(smEntities), "walkEntities", {
//     private _entity = GVAR(entitiesNew) deleteAt 0;
//     GVAR(entities) pushBack _entity;
//     ["entityCreated", [_entity]] call CFUNC(localEvent);
//     ["walkEntities", "init"] select (GVAR(entitiesNew) isEqualTo []);
// }] call CFUNC(smAddState);

// [GVAR(smEntities), "wait", {
//     ["init", "wait"] select (GVAR(entitiesNextLoad) > diag_frameNo);
// }] call CFUNC(smAddState);

// [{
//     GVAR(smEntities) call CFUNC(smStep);
// }] call CFUNC(addPerFrameHandler);
