/*
    Function:       ADA_Core_fnc_startBatch
    Author:         Adanteh
    Description:    Starts the processing of batch stuffs
*/
#include "macros.hpp"

GVAR(scheduleQueue) = [];
[{
    {
        _x params ["_elements", "_code", "_codeEnd", "_args", "_processSpeed"];
        for "_i" from 1 to _processSpeed do {
            private _element = _elements deleteAt 0;
            [_element] call _code;

            if (_elements isEqualTo []) exitWith {
                [] call _codeEnd;
                GVAR(scheduleQueue) deleteAt (GVAR(scheduleQueue) find _x);
                nil;
            };
            nil;
        };
    } count GVAR(scheduleQueue);
}, 0] call CFUNC(addPerFrameHandler);
