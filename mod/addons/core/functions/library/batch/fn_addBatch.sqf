/*
    Function:       ADA_Core_fnc_addBatch
    Author:         Adanteh
    Description:    Adds an array of objects to process, process X per frame, useful to not kill the game if you have a lot of objects to handle

    ### WARNING
    You probably don't want to use this, generally speaking a statemachine approach is better and more versatile
*/
#include "macros.hpp"

params [["_elements", [], [[]]], ["_code", {}], ["_codeEnd", {}], ["_args", []], ["_processSpeed", 1]];

if (isNil QGVAR(scheduleQueue)) then {
    call FUNC(startBatch);
};

_code = _code call FUNC(parseToCode);
_codeEnd = _codeEnd call FUNC(parseToCode);
GVAR(scheduleQueue) pushBack [_elements, _code, _codeEnd, _args, _processSpeed];
