/*
    Function:       ADA_Core_fnc_localEvent
    Author:         Adanteh
    Description:    Calls a local event
*/
#include "macros.hpp"

if (isNil QGVAR(eventNamespace)) exitWith {
    GVAR(eventNamespace) = false call FUNC(createNamespace);
};

params [["_event", ""], ["_args", []], ["_namespace", GVAR(eventNamespace)]];

if !(toLower _event in ["eventadded", "drawmap"]) then {
    diag_log text format ["[%1] Event: '%2' - %3", QUOTE(PREFIX), _event, _args];
};

private _currentCode = _namespace getVariable [_event, []];
{
    private _eventIndex = _forEachIndex;
    [_args, _x select 1] call (_x select 0);
} forEach _currentCode;
