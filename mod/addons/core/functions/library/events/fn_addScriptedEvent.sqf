/*
    Function:       ADA_Core_fnc_addScriptedEvent
    Author:         Adanteh
    Description:    Framework for scripted event handlers, will check code block every frame, if value has changed from last data, then call it with new code
    Example:        ["playerChanged", { player }] call ADA_Core_fnc_addScriptedEvent
*/
#include "macros.hpp"
#define __CODETAG "##CODE"
params [["_tag", "scriptedEvent", [""]], ["_codeBlock", { true }]];


if (isNil QGVAR(eventScriptedNamespace)) then {
    GVAR(eventScriptedNamespace) = false call FUNC(createNamespace);
    GVAR(eventScriptedNamespaceCode) = false call FUNC(createNamespace);

    // -- This will go through each tag, then check if the value for that tag is changed, and if so invoke a local event
    [{
        [GVAR(eventScriptedNamespace), {
            private _newValue = call (GVAR(eventScriptedNamespaceCode) getVariable _key);
            if !(_newValue isEqualTo _value) then {
                [_key, [_newValue, _value]] call CFUNC(localEvent);
                GVAR(eventScriptedNamespace) setVariable [_key, _newValue];
            };
        }] call CFUNC(forEachVariable);

    }] call CFUNC(addPerFrameHandler);
};


_codeBlock = _codeBlock call CFUNC(parseToCode);
// -- Create a null value of the expected return, we do this so we always trigger the event properly when we add it, with the expected datatype,
// This makes the expected values for external functions a lot easier
private _currentValue = call _codeBlock;
private _defaultValue = switch (toLower typeName _currentValue) do {
    case "scalar": { 0 };
    case "string": { "" };
    case "bool": { false };
    case "array": { [] };
    case "code": { {} };
    case "object": { objNull };
    case "group": { grpNull };
    case "control": { controlNull };
    case "team_member": { teamMemberNull };
    case "display": { displayNull };
    case "task": { taskNull };
    case "location": { locationNull };
    case "side": { sideUnknown };
    case "text": { text "" };
    case "config": { configNull };
    case "namespace": { missionNamespace };
    default { false };
};

GVAR(eventScriptedNamespace) setVariable [_tag, _defaultValue];
GVAR(eventScriptedNamespaceCode) setVariable [_tag, _codeBlock]
