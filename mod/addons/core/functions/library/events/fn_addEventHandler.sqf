/*
    Function:       ADA_Core_fnc_addEventHandler
    Author:         Adanteh
    Description:    Creates a scripted event
    Example:        ["someEvent", { hint 'someEvent was triggered' }] call ADA_Core_fnc_addEventHandler;
*/
#include "macros.hpp"

if (isNil QGVAR(eventNamespace)) then {
    GVAR(eventNamespace) = false call FUNC(createNamespace);
};

params [["_key", ""], ["_code", { true }, ["", {}]], ["_args", []], ["_namespace", GVAR(eventNamespace)]];

_code = [_code] call FUNC(parseToCode);
private _currentEvent = _namespace getVariable [_key, []];
_currentEvent pushBack [_code, _args];
_namespace setVariable [_key, _currentEvent];

["eventAdded", [_key, _code, _args]] call CFUNC(localEvent);
