/*
    Function:       ADA_Core_fnc_targetEvent
    Author:         Adanteh
    Description:    Calls a target event
*/
#include "macros.hpp"

params [["_key", ""], ["_target", 2], ["_args", []]];

if ((_target isEqualType objNull) && { local _target }) exitWith {
    [_key, _args] call FUNC(localEvent);
};

[_key, _args] remoteExecCall [QFUNC(localEvent), _target, false];
