/*
    Function:       ADA_Core_fnc_persistentEventGlobal
    Author:         Adanteh
    Description:    Handles persistent global event, used to autocall any code that is added to the event later on
*/
#include "macros.hpp"

params [["_eventName", "", [""]], ["_args", []]];

if ((GVAR(persistentEventsGlobal) pushBackUnique toLower _eventName) == -1) then {
	// -- Already executed this event. Probably don't want a second call
} else {
	publicVariable QGVAR(persistentEventsGlobal);
};

_this call CFUNC(globalEvent);
GVAR(eventNamespace) setVariable [_eventName, nil];
