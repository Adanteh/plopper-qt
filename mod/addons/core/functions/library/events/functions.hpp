class Events {
    class addEventHandler;
    class addScriptedEvent;
    class globalEvent;
    class initEvents;
    class localEvent;
    class persistentEvent;
    class persistentEventGlobal;
    class serverEvent;
    class targetEvent;
};
