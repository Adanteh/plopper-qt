/*
    Function:       ADA_Core_fnc_persistentEvent
    Author:         Adanteh
    Description:    Handles persistent event, used to autocall any code that is added to the event later on
*/
#include "macros.hpp"

params [["_eventName", "", [""]], ["_args", []], ["_sender", "Local Called"]];

if ((GVAR(persistentEvents) pushBackUnique toLower _eventName) == -1) then {
	// -- Already executed this event. Probably don't want a second call
};

_this call CFUNC(localEvent);
GVAR(eventNamespace) setVariable [_eventName, nil];
