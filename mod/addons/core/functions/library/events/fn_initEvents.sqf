/*
    Function:       ADA_Core_fnc_initEvents
    Author:         Adanteh
    Description:    Inits event system
*/
#include "macros.hpp"

if (isNil QGVAR(persistentEvents)) then {
	GVAR(persistentEvents) = [];
};

if (isNil QGVAR(persistentEventsGlobal)) then {
	GVAR(persistentEventsGlobal) = [];
};

if (isNil QGVAR(eventNamespace)) exitWith {
    GVAR(eventNamespace) = false call FUNC(createNamespace);
};

["eventAdded", {
    params ["_arguments", "_data"];
    _arguments params ["_event", "_function", "_args"];
    if (toLower _event in GVAR(persistentEvents) || { tolower _event in GVAR(persistentEventsGlobal) }) then {
        if (_function isEqualType "") then {
            _function = parsingNamespace getVariable [_function, {}];
        };
        [nil, _args] call _function;
    };
}] call CFUNC(addEventHandler);
