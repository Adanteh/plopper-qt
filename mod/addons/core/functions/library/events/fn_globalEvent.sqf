/*
    Function:       ADA_Core_fnc_globalEvent
    Author:         Adanteh
    Description:    Calls a global event
*/
#include "macros.hpp"

if (isMultiplayer) then {
    _this remoteExecCall [QFUNC(localEvent), 0, false];
} else {
    _this call CFUNC(localEvent);
};
