/*
    Function:       ADA_Core_fnc_serverEvent
    Author:         Adanteh
    Description:    Calls a server event
*/
#include "macros.hpp"

params [["_key", ""], ["_args", []]];

[_key, _args] remoteExecCall [QFUNC(localEvent), 2, false];
