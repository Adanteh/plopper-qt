/*
    Function:       ADA_Core_fnc_name
    Author:         Adanteh
    Description:    Wrapper for name (Because A3 still doesn't support names on dead units through script) (Engine does though, BIS fucking lol)
    Example:        [player] call ADA_Core_fnc_name;
*/
#include "macros.hpp"

params ["_unit"];
private _name = _unit getVariable [QSVAR(name), ""];
if (_name == "") then {
    if (alive _unit) then {
        _name = name _unit;
        _unit setVariable [QSVAR(name), _name];
    };
};
_name
