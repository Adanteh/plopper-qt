#include "macros.hpp"
/*
    Function:       ADA_Core_fnc_hint
    Description:    Wrapper for hint, required because hint usages makes buldozer crash
    Author:         Adanteh
*/

params ["_hint", ["_silent", true]];

if !(_hint isEqualType "") then {
    _hint = str _hint;
};

if !(missionNamespace getVariable [QGVAR(disableHint), false]) then {
    if (_silent) then {
        hintSilent _hint;
    } else {
        hint _hint;
    };
} else {
    systemChat _hint;  // hint/hintSilent usage 
};