/*
    Function:       ADA_Core_fnc_waitUntil
    Author:         Based on CBA/ACE/Clib
    Description:    Unscheduled waitUntil call, first block is code to execute when second code block returns true.
*/
#include "macros.hpp"

params [["_callback", {}], ["_condition", {}], ["_args", []], ["_timeout", -1]];
GVAR(waitUntilArray) pushBack [_callback call CFUNC(parseToCode), _condition call CFUNC(parseToCode), _args, _timeout];
