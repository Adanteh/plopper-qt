/*
    Function:       ADA_Core_fnc_wait
    Author:         Based on CBA/ACE/Clib
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params [
    ["_code", {}, [{}]],
    ["_time", 0, [0]],
    ["_args", [], []]
];

_code = _code call CFUNC(parseToCode);
GVAR(waitArray) pushBack [_time + time, _code, _args];
GVAR(sortWaitArray) = true;
nil
