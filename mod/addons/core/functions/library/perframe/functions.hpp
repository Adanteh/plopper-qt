class perFrame {
    class addPerFrameHandler;
    class execNextFrame;
    class initPerFrameHandler;
    class prepareTimeHandling;
    class removePerFrameHandler;
    class wait;
    class waitUntil;
};
