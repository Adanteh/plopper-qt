/*
    Function:       ADA_Core_fnc_prepareTimeHandling
    Author:         Adanteh
    Description:    Compiles a suitable function to get good time variable, whether its SP, local MP or Dedi, JIP or whatever
                    (Thanks BIS, this should obviously be in the base game)
*/
#include "macros.hpp"

if (isMultiplayer) then {
    if (isServer) then {
        // multiplayer server
        FUNC(getTime) = compileFinal ([{
            if (time != GVAR(lastTime)) then {
                GVAR(time) = GVAR(time) + (_tickTime - GVAR(lastTickTime));
                GVAR(lastTime) = time; // used to detect paused game
            };
            GVAR(lastTickTime) = _tickTime;
        }] call CFUNC(codeToString));

        addMissionEventHandler ["PlayerConnected", {
            (_this select 4) publicVariableClient QGVAR(time);
        }];
    } else {
        GVAR(time) = -1;

        0 spawn { // Spawn required
            QGVAR(time) addPublicVariableEventHandler {
                GVAR(time) = _this select 1;
                GVAR(lastTickTime) = diag_tickTime; // prevent time skip on clients

                FUNC(getTime) = compileFinal ([{
                    if (time != GVAR(lastTime)) then {
                        GVAR(time) = GVAR(time) + (_tickTime - GVAR(lastTickTime));
                        GVAR(lastTime) = time; // used to detect paused game
                    };
                    GVAR(lastTickTime) = _tickTime;
                }] call CFUNC(codeToString));
            };
        };
    };
} else {
    FUNC(getTime) = compileFinal ([{
        if (time != GVAR(lastTime)) then {
            GVAR(time) = GVAR(time) + (_tickTime - GVAR(lastTickTime)) * accTime;
            GVAR(lastTime) = time; // used to detect paused game
        };
        GVAR(lastTickTime) = _tickTime;
    }] call CFUNC(codeToString));
};
