/*
    Function:       ADA_Core_fnc_execNextFrame
    Author:         Based on CBA/ACE/Clib
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

params [
    ["_func", {}, [{}]],
    ["_params", [], []]
];

if (diag_frameNo == GVAR(nextFrameNo)) then {
    GVAR(nextFrameBuffer) pushBack [_params, _func];
} else {
    GVAR(currentFrameBuffer) pushBack [_params, _func];
};
