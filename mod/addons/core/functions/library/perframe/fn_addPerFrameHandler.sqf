/*
    Function:       ADA_Core_fnc_addPerFrameHandler
    Author:         Based on CBA/ACE/Clib
    Description:    Adds perFrame handler
*/
#include "macros.hpp"

params [["_function", {}, [{},""]], ["_delay", 0, [0]], ["_args", []], ["_delayFirst", false]];

if (_function isEqualType "") then {
    _functionSaved = uiNamespace getVariable [_function, ""];
    _function = if (_functionSaved == "") then {
        compile _function;
    } else {
        _functionSaved;
    };
};

if (isNil QGVAR(perFrameHandleArray)) then {
    call FUNC(initPerFramehandler);
};

if (count GVAR(PFHhandles) >= 999999) exitWith {
    diag_log "Max PerFrame Handles reached (999999)";
    diag_log _function;
    -1
};

private _runAt = [diag_tickTime, diag_tickTime + _delay] select _delayFirst;
private _handle = GVAR(PFHhandles) pushBack count GVAR(perFrameHandleArray);
GVAR(perFrameHandleArray) pushBack [_function, _delay, _runAt, diag_tickTime, _args, _handle];
_handle
