/*
	Function:       ADA_Core_fnc_initPerFrameHandler
	Author:         Based on CBA/ACE/Clib
	Description:    Inits perframe handle (Simplified version)
*/
#include "macros.hpp"

if !(isNil QGVAR(perFrameHandleArray)) exitWith { };


GVAR(time) = 0;
GVAR(lastTime) = time;
call FUNC(prepareTimeHandling);


GVAR(perFrameHandleArray) = [];
GVAR(PFHhandles) = [];
GVAR(lastTickTime) = diag_tickTime;

GVAR(currentFrameBuffer) = [];
GVAR(nextFrameBuffer) = [];
GVAR(nextFrameNo) = diag_frameNo;

GVAR(waitUntilArray) = [];
GVAR(waitArray) = [];
GVAR(sortWaitArray) = false;

FUNC(perFrameFunction) = {
	if (getClientState == "GAME FINISHED") exitWith {
		removeMissionEventHandler ["EachFrame",_thisEventHandler];
	};

    private _tickTime = diag_tickTime;
    call FUNC(getTime);

	{
		_x params ["_function","_delay","_delta","","_args","_handle"];
		if (diag_tickTime > _delta) then {
			_x set [2,_delta + _delay];
			[_args,_handle] call _function;
		};
		nil;
	} count GVAR(perFrameHandleArray);

    if (GVAR(sortWaitArray)) then {
        GVAR(waitArray) sort true;
        GVAR(sortWaitArray) = false;
    };

    private _delete = false;

    {
        if (_x select 0 > time) exitWith {};
        (_x select 2) call (_x select 1);
        _delete = true;
        GVAR(waitArray) set [_forEachIndex, objNull];
    } forEach GVAR(waitArray);

    if (_delete) then {
        GVAR(waitArray) = GVAR(waitArray) - [objNull];
        _delete = false;
    };

    {
        if ((_x select 2) call (_x select 1)) then {
            (_x select 2) call (_x select 0);
            _delete = true;
            GVAR(waitUntilArray) set [_forEachIndex, objNull];
        } else {
            if (((_x select 3) > 0) && ((_x select 3) > time)) then {
                _delete = true;
                GVAR(waitUntilArray) set [_forEachIndex, objNull];
            };
        };
    } forEach GVAR(waitUntilArray);

    if (_delete) then {
        GVAR(waitUntilArray) = GVAR(waitUntilArray) - [objNull];
        _delete = false;
    };

    {
        (_x select 0) call (_x select 1);
        nil
    } count GVAR(currentFrameBuffer);

    GVAR(currentFrameBuffer) = +GVAR(nextFrameBuffer);
    GVAR(nextFrameBuffer) = [];
    GVAR(nextFrameNo) = diag_frameNo + 1;
};

addMissionEventHandler ["EachFrame", { call FUNC(perFrameFunction) }];
addMissionEventHandler ["Loaded", {
	{
		_x set [2, (_x select 2) - GVAR(lastTickTime) + diag_tickTime];
		nil;
	} count GVAR(perFrameHandleArray);
	GVAR(lastTickTime) = diag_tickTime;
}];
