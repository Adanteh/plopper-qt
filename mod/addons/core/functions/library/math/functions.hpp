class Math {
    class calcEdenAngles;
    class calcPitchBankYaw;
    class calcPolygonCoords;
    class calcRelativePosition;
    class calcVectorDirAndUp;
    class getEdgeCoordinateRectangle;
    class getRelDir;
    class getTerrainVector;
    class rotateAroundPoint;
    class setPitchBankYaw;

    class Matrix {
        class pbyToVector;
        class vectorToPby;
    };
};
