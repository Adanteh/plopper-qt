/*
    Function:       ADA_Core_fnc_pbyToVector
    Author:         Adanteh
    Description:    Converts pitch,bank,yaw rotation into vectorDirAndUp
*/
#include "macros.hpp"

params ["_roll", "_pitch", "_yaw"];

private _s3 = sin _roll;
private _c3 = cos _roll;
private _s2 = sin _pitch;
private _c2 = cos _pitch;
private _s1 = sin _yaw;
private _c1 = cos _yaw;

private _dcm = [
    [_c2 * _c1, (_s3 * _s2 * _c1) - (_c3 * _s1), (_c3 * _s2 * _c1) + (_s3 * _s1)],
    [_c2 * _s1, (_s3 * _s2 * _s1) + (_c3 * _c1), (_c3 * _s2 * _s1) - (_s3 * _c1)],
    [-1 * _s2, _s3 *_c2, _c3 * _c2]
];

private _arma_matrix = [
    [1, 0, 0],
    [0, 0, 1],
    [0, -1, 0]
];

private _V = _dcm matrixMultiply _arma_matrix;
private _vectorDir = [_V#1#0, _V#0#0, -1 * (_V#2#0)];
private _vectorUp = [_V#1#1, _V#0#1, -1 * (_V#2#1)];

[_vectorDir, _vectorUp]