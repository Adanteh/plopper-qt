/*
    Function:       ADA_Core_fnc_vectorToPby
    Author:         Adanteh
    Description:    Converts vectorDirAndUp into PitchBankYaw
                    [[[vectorUp cursorObject, vectorDir cursorObject]] call ada_core_fnc_vectorToPby] call ada_core_fnc_byToVector)
*/
#include "macros.hpp"

params ["_vectorDir", "_vectorUp"]; 
// -- Thsi is the nested array like [[VectorDir, VectorUp]]

private _aside = _vectorDir vectorCrossProduct _vectorUp;
private _reverse_matrix = [[1, 0, 0], [0, 0, -1], [0, 1, 0]];
private _vector = [
    [_vectorDir#1, _vectorUp#1, _aside#1], 
    [_vectorDir#0, _vectorUp#0, _aside#0], 
    [-1 * _vectorDir#2, -1 * _vectorUp#2, -1 * _aside#2]
];

private ["_pitch", "_bank", "_yaw"];
private _dcm = _vector matrixMultiply _reverse_matrix;
private _sy = sqrt (_dcm#0#0 * _dcm#0#0 + _dcm#1#0 * _dcm#1#0);
private _singular = _sy <= 1e-6;

if !(_singular) then {
    _bank = _dcm#2#1 atan2 _dcm#2#2;
    _pitch = (-1 * (_dcm#2#0)) atan2 _sy;
    _yaw = _dcm#1#0 atan2 _dcm#0#0;
} else {
    _bank = (-1 * (_dcm#1#2)) atan2 _dcm#1#1;
    _pitch = (-1 * (_dcm#2#0)) atan2 _sy;
    _yaw = 0;
};

[_bank, _pitch, _yaw];
