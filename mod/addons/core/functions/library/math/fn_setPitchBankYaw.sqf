/*
    Function:       ADA_Core_fnc_setPitchBankYaw
    Author:         Adanteh
    Description:    Sets pitch bank yaw on an object (Actuall vectorDirAndUp). I can't into vectors
    Eample:         [cursorObject, [100, 0, 100]] call ADA_Core_fnc_setPitchBankYaw
*/
#include "macros.hpp"

params ["_object", "_pitchBankYaw"];

private _vectorDirAndUp = _pitchBankYaw call FUNC(pbyToVector);
_object setVectorDirAndUp _vectorDirAndUp;