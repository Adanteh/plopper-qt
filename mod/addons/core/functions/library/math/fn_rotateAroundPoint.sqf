/*
    Function:       ADA_Core_fnc_rotateAroundPoint
    Author:         Adanteh
    Description:    Rotates an object around a point
*/
#include "macros.hpp"

params ["_pos", "_dir"];

_px = _pos select 0;
_py = _pos select 1;
_ma = _dir;

//Now, rotate point
_rpx = ( (_px) * cos(_ma) ) + ( (_py) * sin(_ma) );
_rpy = (-(_px) * sin(_ma) ) + ( (_py) * cos(_ma) );

[_rpx, _rpy, (_pos select 2)]
