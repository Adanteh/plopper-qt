/*
    Function:       ADA_Core_fnc_calcPitchBankYaw
    Author:         _SCAR & ianbanks
    Description:    Converts vectorDir and Up to arma-style rotation angles


    // github.com/ostinelli/SCAR_E2TB/blob/master/functions/fn_getModelLine.sqf
    // https://foxhound.international/sqf-snippets/export-arma-3-to-terrain-builder

    // object, [number _rotationX, number _rotationY, number _rotationZ]
    //
    // The rotation axes match those used by "getPos" in game, not the
    // Terrain Builder frame. In Terrain Builder, "Pitch (X): deg",
    // "Roll (Z): deg" and "Yaw (Y): deg" match up to "_rotationX",
    // "_rotationY" and "_rotationZ" in this function respectively.
    //
    // An object with a particular orientation in game will have the same
    // orientation in Terrain Builder if the three rotation angles returned
    // are pasted into the object properties UI. These fields are also
    // identical to the rotations found in an object list (.txt) file.
*/

#include "macros.hpp"

params [["_object", objNull, [objNull, []]]];
private ["_direction", "_up"];

// -- Object given, get vectoDir and vectorUp
if (_object isEqualType objNull) then {
    _direction = vectorDir _object;
    _up = vectorUp _object;
} else {
    // -- Array given, already vectorDir and vectorUp
    _direction = _object param [0, [0, 0, 0]];
    _up = _object param [1, [0, 0, 1]];
    if (_direction isEqualType 0) then {
        // -- Number given as vectorDir, convert dir in degrees to vectorDir
        _direction = [sin _direction, cos _direction, 0];
    };
};

private _aside = _direction vectorCrossProduct _up;
private ["_xRot", "_yRot", "_zRot"];

if (abs (_up select 1) < 0.999999) then {
    _xRot = -asin (_up select 1);

    private _signCosX = if (cos _xRot < 0) then { -1 } else { 1 };

    _yRot = ((_up select 0) * _signCosX) atan2 ((_up select 2) * _signCosX);
    _zRot = (-(_aside select 1) * _signCosX) atan2 ((_direction select 1) * _signCosX);
} else {
    _zRot = 0;

    if (_up select 1 < 0) then {
        _xRot = 90;
        _yRot = (_direction select 0) atan2 (_direction select 2);
    } else {
        _xRot = -90;
        _yRot = (-(_direction select 0)) atan2 (-(_direction select 2));
    };
};

[_xRot, _yRot, _zRot]
