/*
    Function:       ADA_Core_fnc_calcEdenAngles
    Author:         ianbanks
    Description:    Converts vectorDir and Up to arma-style rotation angles

    // https://foxhound.international/sqf-snippets/export-arma-3-to-terrain-builder
*/

#include "macros.hpp"

params [["_object", objNull, [objNull, []]]];
private ["_direction", "_up"];

// -- Object given, get vectoDir and vectorUp
if (_object isEqualType objNull) then {
    _direction = vectorDir _object;
    _up = vectorUp _object;
} else {
    // -- Array given, already vectorDir and vectorUp
    _direction = _object param [0, [0, 0, 0]];
    _up = _object param [1, [0, 0, 1]];
    if (_direction isEqualType 0) then {
        // -- Number given as vectorDir, convert dir in degrees to vectorDir
        _direction = [sin _direction, cos _direction, 0];
    };
};

private _aside = _direction vectorCrossProduct _up;
private ["_xRot", "_yRot", "_zRot"];

if (abs (_up select 0) < 0.999999) then {
    _yRot = -asin (_up select 0);

    private _signCosY = if (cos _yRot < 0) then { -1 } else { 1 };

    _xRot = (_up select 1 * _signCosY) atan2 (_up select 2 * _signCosY);
    _zRot = (_direction select 0 * _signCosY) atan2 (_aside select 0 * _signCosY);
} else {
    _zRot = 0;

    if (_up select 0 < 0) then {
        _yRot = 90;
        _xRot = (_aside select 1) atan2 (_aside select 2);
    } else {
        _yRot = -90;
        _xRot = (-(_aside select 1)) atan2 (-(_aside select 2));
    };
};

[_xRot, _yRot, _zRot]
