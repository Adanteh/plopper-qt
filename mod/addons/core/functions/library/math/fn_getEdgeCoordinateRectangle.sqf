/*
    Function:       ADA_Core_fnc_getEdgeCoordinateRectangle
    Author:         Adanteh
    Description:    This gets the coordinates of the edge a given [_width, _height] size rectangle, given a degree angle
                    If you use this with a 0 degree, you will get the east point, if you use it with 180 degrees, you will get the west side.
                    See: https://stackoverflow.com/questions/4061576/finding-points-on-a-rectangle-at-a-given-angle
*/
#include "macros.hpp"

params ["_width", "_height", "_degree"];

private _twoPi = pi * 2;
private _theta = _degree * pi / 180;

while { _theta < -pi } do {
    _theta = _theta + _twoPi;
};

while { _theta > pi } do {
    _theta = _theta - _twoPi;
};

private _rectAtan = _height atan2 _width;
private _rectAtan = _rectAtan * pi / 180;
private _tanTheta = tan (_theta * 180 / pi);
private _region = call {
    if ((_theta > -_rectAtan) && (_theta <= _rectAtan)) exitWith {
        1;
    };
    if ((_theta > _rectAtan) && (_theta <= (pi - _rectAtan))) exitWith {
        2;
    };

    if ((_theta > (pi - _rectAtan)) || (_theta <= -(pi - _rectAtan))) exitWith {
        3;
    };
    4
};

private _edgePointX = _width / 2;
private _edgePointY = _height / 2;
private _xFactor = 1;
private _yFactor = 1;

switch (_region) do {
    case 1: { _yFactor = -1; };
    case 2: { _yFactor = -1; };
    case 3: { _xFactor = -1; };
    case 4: { _xFactor = -1; };
};

if ((_region == 1) || (_region == 3)) then {
    _edgePointX = _edgePointX + _xFactor * (_width / 2);                                     // "Z0"
    _edgePointY = _edgePointY + _yFactor * (_width / 2) * _tanTheta;
} else {
    _edgePointX = _edgePointX + _xFactor * (_height / (2 * _tanTheta));                        // "Z1"
    _edgePointY = _edgePointY + _yFactor * (_height /  2);
};

[_edgePointX, _edgepointY, 0];
