/*
    Function:       ADA_Core_fnc_getClassFromModel
    Author:         Adanteh
    Description:    Gets a cfgVehicles class (if it exists) from a model path, useful to turn terrain object into classes wherever possible
*/
#include "macros.hpp"

params [["_modelPath", objNull, ["", objNull]]];

if (_modelPath isEqualType objNull) then {
    _modelPath = getModelInfo _modelPath select 1;
};

private _class = (missionNamespace getVariable [QGVAR(coreNamespace), objNull]) getVariable ["class.path#" + _modelPath, "##"];
if (_class == "##") then {
    private _condition = format ["([getText (_x >> 'model')] call %2 == '%1')", _modelPath, QFUNC(getReliableModelPath)];
    private _classes = _condition configClasses (configFile >> "CfgVehicles");
    _class = (_classes param [0, configNull]);
    _class = [configName _class, ""] select (isNull _class);
    (missionNamespace getVariable [QGVAR(coreNamespace), objNull]) setVariable ["class.path#" + _modelPath, _class];
};

_class;
