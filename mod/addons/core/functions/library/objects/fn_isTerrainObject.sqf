/*
    Function:       ADA_Core_fnc_isTerrainObject
    Author:         Adanteh
    Description:    Checks if given object is a terrain object
*/
#include "macros.hpp"


params ["_object"];

private _isTerrainObject = _object getVariable "terrainObject";
if !(isNil "_isTerrainObject") exitWith { _isTerrainObject };

private _nearestTerrainObject = nearestTerrainObjects [_object, [], 0.05];
_isTerrainObject = (_nearestTerrainObject find _object) != -1;

_object setVariable ["terrainObject", _isTerrainObject, true];
_isTerrainObject
