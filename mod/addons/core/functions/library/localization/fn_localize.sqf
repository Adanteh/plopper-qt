/*
    Function:       ADA_Core_fnc_localize
    Author:         Adanteh
    Description:    Does text localization, if it's available. Should be used through LOCALIZE() macro
*/
#include "macros.hpp"

params [["_text", ""], ["_prefix", QUOTE(PREFIX)], ["_module", QUOTE(MODULE)]];

private _stringLocalized = ["STR", _prefix, _module, _text] joinString "_";
if (isLocalized _stringLocalized) then {
    _text = localize _stringLocalized;
} else {
    // -- Do some logging to indicate missing localization
    [_stringLocalized] call FUNC(localizeMissingLog);
};

_text;
