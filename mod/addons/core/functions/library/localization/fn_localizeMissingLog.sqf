/*
    Function:       ADA_Core_fnc_localizeMissingLog
    Author:         Adanteh
    Description:    Keeps track of which missing localization strings there are
*/
#include "macros.hpp"

params ["_string"];
if (isNil QGVAR(localizeMissingLog)) then {
    GVAR(localizeMissingLog) = [];
};

GVAR(localizeMissingLog) pushBackUnique _string;
