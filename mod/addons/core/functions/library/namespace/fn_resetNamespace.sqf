/*
    Function:       ADA_Core_fnc_resetNamespace
    Author:         Adanteh
    Description:    Resets all variables on a namespace
*/

params [["_namespace", false], ["_global", false]];
private _allVariables = +(allVariables _namespace);
{
    _namespace setVariable [_x, nil];
} count _allVariables;
