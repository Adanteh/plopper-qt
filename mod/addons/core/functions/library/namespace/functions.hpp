class Namespace {
    class createNamespace;
    class deleteNamespace;
    class forEachVariable;
    class namespaceName;
    class resetNamespace;
};
