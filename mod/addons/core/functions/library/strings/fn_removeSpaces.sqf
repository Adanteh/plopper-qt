/*
    Function:       ADA_Core_fnc_removeSpaces
    Author:         Adanteh
    Description:    Removes leading and trailing spaces
    Example:        ["radio_channel"] call ADA_Core_fnc_removeSpaces;
*/
#include "macros.hpp"

#define _UNI_SPACE 32
#define _UNI_BREAK 10

params ["_input"];
private _array = toArray _input;
// -- Remove leading spaces -- //
while { ((_array select (count _array - 1)) in [_UNI_SPACE, _UNI_BREAK]) } do {
    _array deleteAt (count _array -1);
};

// -- Remove trailing spaces -- //
while { (_array select 0 in [_UNI_SPACE, _UNI_BREAK]) } do {
    _array deleteAt 0;
};

_input = toString _array;
_input;
