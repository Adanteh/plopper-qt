/*
    Function:       ADA_Core_fnc_getReliableModelPath
    Author:         Adanteh
    Description:    Gets a modelpath with extension and without leading slash. (Used to check config entries against getModelInfo returns)
    Example:        [getText (configFile >> "cfgVehicles" >> typeOf cursorObject >> "model")] call ADA_Core_fnc_getReliableModelPath
*/
#include "macros.hpp"

params ["_modelPath"];

if (_modelPath isEqualTo "") exitWith { "" };

// -- Add p3d
if !([_modelPath, ".p3d"] call CFUNC(endsWith)) then {
    _modelPath = _modelPath + ".p3d";
};
// -- Remove leading slash
if ([_modelPath, "\"] call CFUNC(startsWith)) then {
    _modelPath = _modelPath select [1, count _modelPath - 1];
};

_modelPath
