/*
    Function:       ADA_Core_fnc_startsWith
    Author:         Adanteh
    Description:    Checks if given string ends with other given string
    Example:        ["afile.p3d", ".p3d"] call ADA_Core_fnc_startsWith
*/
#include "macros.hpp"

params ["_string", "_check"];

if (count _string < (count _check)) exitWith { false };
(_string select [0, count _check]) == _check
