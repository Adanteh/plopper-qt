/*
    Function:       ADA_Core_fnc_endsWith
    Author:         Adanteh
    Description:    Checks if given string ends with other given string
    Example:        ["afile.p3d", ".p3d"] call ADA_Core_fnc_endsWith
*/
#include "macros.hpp"

params ["_string", "_check"];

if (count _string < (count _check)) exitWith { false };
(_string select [count _string - count _check, count _check]) == _check
