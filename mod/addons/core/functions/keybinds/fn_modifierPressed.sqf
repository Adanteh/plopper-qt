/*
    Function:       ADA_Core_fnc_modifierPressed
    Author:         Adanteh
    Description:    Handles checking if a modifier key is pressed, because not all event handlers support this (Thanks BIS)
*/
#include "macros.hpp"
#include "definedikcodes.hpp"

params ["_keydown", "_args"];
_args params ["", "_key"];

if (_keydown) then {
	if (_key in [DIK_LSHIFT, DIK_RSHIFT]) then { missionNamespace setVariable [QGVAR(shiftPressed), true]; };
	if (_key in [DIK_LCONTROL, DIK_RCONTROL]) then { missionNamespace setVariable [QGVAR(ctrlPressed), true]; };
	if (_key in [DIK_LMENU, DIK_RMENU]) then { missionNamespace setVariable [QGVAR(altPressed), true]; };
} else {
	if (_key in [DIK_LSHIFT, DIK_RSHIFT]) then { missionNamespace setVariable [QGVAR(shiftPressed), false]; };
	if (_key in [DIK_LCONTROL, DIK_RCONTROL]) then { missionNamespace setVariable [QGVAR(ctrlPressed), false]; };
	if (_key in [DIK_LMENU, DIK_RMENU]) then { missionNamespace setVariable [QGVAR(altPressed), false]; };
};
