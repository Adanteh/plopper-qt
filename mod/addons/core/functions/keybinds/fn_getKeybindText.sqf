/*
    Function:       ADA_Core_fnc_getKeybindText
    Author:         Adanteh
    Description:    Retrieves simple formatted keybind name (Without brackets or double quotes) based on standard keybind array
    Returns:        Name of keybind "Shift+A" <STRING>
    Example:        [35, [true, false, false]] call ADA_Core_fnc_getKeybindText;
*/
#include "macros.hpp"

params ["_keyArray"];

if (_keyArray isEqualTo []) exitWith { "None" };
if (_keyArray isEqualType -1) then { _keyArray = [_keyArray, [false, false, false]]; };

private _keyBindName = "Not set";
if (_keyArray isEqualTo [-1, [false, false, false]]) exitWith { _keyBindName };

_keyArray params [["_dikCode", 35], ["_modifiers", [false, false, false]]];
if (_dikCode == -1) exitWith { _keyBindName; };

_modifiers params [["_shift", false], ["_ctrl", false], ["_alt", false]];

private _keyBindName = [_dikCode] call FUNC(getKeyName);
if (_keyBindName == "") exitWith { _keyBindName = "Invalid Key"; };
if (_shift && _dikCode != 42) then { _keyBindName = "Shift+" + _keyBindName; };
if (_ctrl && _dikCode != 29) then { _keyBindName = "Ctrl+" + _keyBindName; };
if (_alt && _dikCode != 56) then { _keyBindName = "Alt+" + _keyBindName; };
_keyBindName
