/*
    Function:       ADA_Core_fnc_changeKeybind
    Author:         Adanteh
    Description:    Changes keybind
*/
#include "macros.hpp"

params ["_category", "_tag", "_newKeybind"];

private _varName = [_category, _tag] joinString ".";

private _keybindIndex = (GVAR(keybindArray) select 3) find (toLower _varName);
[[_fnc_scriptNameShort, _varname, _keybindIndex], "olive"] call CFUNC(debugMessage);
if (_keybindIndex == -1) exitWith { };


private _currentKeybind = (GVAR(keybindArray) select 0) select _keybindIndex;

{
    if (_x) then {
        (_currentKeybind select 1) set [_forEachIndex, _x];
    } else {
        private _current = (_currentKeybind select 1) select _forEachIndex;
        if !(isNil "_current") then {
            testvar2 = _forEachIndex;
            (_currentKeybind select 1) set [_forEachIndex, _x];
        };
    };
} forEach (_newKeybind select 1);
_currentKeybind set [0, _newKeybind select 0];
(GVAR(keybindArray) select 0) set [_keybindIndex, _currentKeybind];

private _profileSettingName = [QUOTE(PREFIX), QUOTE(MODULE), "keybind", _varName] joinString ".";
profileNamespace setVariable [_profileSettingName, _currentKeybind];
saveProfileNamespace;
