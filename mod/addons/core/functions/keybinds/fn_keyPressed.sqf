/*
    Function:       ADA_Core_fnc_keyPressed
    Author:         Adanteh
    Description:    Checks if a key is currently pressed
*/
#include "macros.hpp"

params ["_key"];
(GVAR(keybindNamespace) getVariable [format ["%1_pressed", _key], -1] > time);
