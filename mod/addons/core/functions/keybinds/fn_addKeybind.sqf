/*
    Function:       ADA_Core_fnc_addKeybind
    Author:         Adanteh
    Description:    Adds a keybind combo

    0: Category (Grouped up in settings dialog) <STRING>
    1: Keybind slug <STRING>
    2: Pretty name for keybind plus tooltip <ARRAY>
    3: Code to run when key is pressed <STRING/CODE>
    4: Code to run when key is released <STRING/CODE>
    5: Keybind to run the code <ARRAY>
        OR
       String name for the action to remove current keybind for (Use A3 keybind) <STRING>
    6: nil
    7: Whether to ignore modifiers to run '_upCode' <BOOL>
    (This is useful for ignoring things like Freelook. That way if you press alt for freelook while already speaking, you dont need to let go of alt to stop speaking)

    Returns:
    true <BOOL>

    Example:
    ["General", "map_names_toggle", ["Show names on map", "Shows all the player names on the map"], { true }, { false } [DIK_T, [false, false, false]], false, true] call CFUNC(addKeybind);
*/
#include "macros.hpp"
#include "definedikcodes.hpp"

params ["_category", "_tag", "_displayName", "_downCode", "_upCode", "_keybindDefault", "_something", ["_modifierRelease", true], ["_condition", { true }], ["_targetIDC", -1]];

// -- Load from A3 Keybinds. This is not really reliable, because we don't have good methods of determining modifier keys (LOL BIS!)
// Possible options: https://community.bistudio.com/wiki/inputAction/actions
if (_keybindDefault isEqualType "") then {
    private _actionKeys = actionKeys _keybindDefault;
    if (_actionKeys isEqualTo []) then {
        _actionKeys = [DIK_F];
    };
    _keybindDefault = [_actionKeys select 0, [nil, nil, nil]];
};

if (isNil QGVAR(keybindArray)) then { GVAR(keybindArray) = [[], [], [], [], [], []] };

// -- Load keybind from profile
private _varName = toLower ([_category, _tag] joinString ".");

// -- This keybind already exists, so don't readd it
private _alreadyExists = ((GVAR(keybindArray) select 3) find _varName) != -1;
if (_alreadyExists) exitWith { -1 };


private _profileSettingName = [QUOTE(PREFIX), QUOTE(MODULE), "keybind", _varName] joinString ".";
private _profileKeybind = profileNamespace getVariable _profileSettingName;
if (isNil "_profileKeybind") then {
    _profileKeybind = _keybindDefault;

    // profileNamespace setVariable [_profileSettingName, _keybindDefault];
    // saveProfileNamespace;
};

_keybindIndex = (GVAR(keybindArray) select 0) pushBack _profileKeybind;

_downCode = [_downCode] call FUNC(parseToCode);
_upCode = [_upCode] call FUNC(parseToCode);
_condition = [_condition] call FUNC(parseToCode);


(GVAR(keybindArray) select 1) set [_keybindIndex, [_downCode, _upCode]];
(GVAR(keybindArray) select 2) set [_keybindIndex, _modifierRelease];
(GVAR(keybindArray) select 3) set [_keybindIndex, _varName];
(GVAR(keybindArray) select 4) set [_keybindIndex, _condition];
(GVAR(keybindArray) select 5) set [_keybindIndex, _targetIDC];

[_category, _tag, _displayName, _keybindIndex] call FUNC(addKeybindSetting);

_keybindIndex;
