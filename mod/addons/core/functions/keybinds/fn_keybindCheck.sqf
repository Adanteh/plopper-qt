/*
    Function:       ADA_Core_fnc_keybindCheck
    Author:         Adanteh
    Description:    Checks connected code for given keycombo press or release
*/
#include "macros.hpp"


params ["_keydown", "_args", "_displayIDD"];
_args params ["", "_key", "_shift", "_ctrl", "_alt"];

_this call FUNC(modifierPressed);

// #define __DEBUG_KEYBIND
// [format ["%1: [%3 - %4] %2", _fnc_scriptNameShort, [_key, [_shift, _ctrl, _alt]], ["down", "up"] select !_keydown, _displayIDD], "red", 5, -1] call FUNC(debugMessage);

if (_keydown) then {
    GVAR(keybindNamespace) setVariable [format ["%1_pressed", _key], time + 0.5];
} else {
    GVAR(keybindNamespace) setVariable [format ["%1_pressed", _key], -1];
};
private _return = false;
{
    // -- Ignore if the target IDD is incorrect
    if ((GVAR(keybindArray) select 5 select _forEachIndex) != -1 && { _displayIDD != (GVAR(keybindArray) select 5 select _forEachIndex) }) exitWith { };

	// -- This allows us to use nil modifier, which ignores it -- //
	(_x select 1) params [["_shiftCheck", _shift], ["_ctrlCheck", _ctrl], ["_altCheck", _alt]];
	if (_keydown) then {
		if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
			if !(call (GVAR(keybindArray select 4) select _forEachIndex)) exitWith { }; // -- General condition
			_return = call (((GVAR(keybindArray) select 1) select _forEachIndex) select 0);
            #ifdef __DEBUG_KEYBIND
                [["Calling ", (GVAR(keybindArray) select 3 select _forEachIndex), "up"]] call CFUNC(debugMessage);
            #endif
		};
	} else {
		// -- Sometimes we don't want to check if the same modifiers are still held when releasing key. -- //
		if ((GVAR(keybindArray) select 2) select _forEachIndex) then {
			if (([_x select 0, [_shiftCheck, _ctrlCheck, _altCheck]]) isEqualTo [_key, [_shift, _ctrl, _alt]]) then {
				if !(call (GVAR(keybindArray select 4) select _forEachIndex)) exitWith { }; // -- General condition
				_return = call (((GVAR(keybindArray) select 1) select _forEachIndex) select 1);
                #ifdef __DEBUG_KEYBIND
                    [["Calling ", (GVAR(keybindArray) select 3 select _forEachIndex), "up"]] call CFUNC(debugMessage);
                #endif
			};
		} else {
			if ((_x select 0) isEqualTo _key) then {
				if !(call (GVAR(keybindArray select 4) select _forEachIndex)) exitWith { }; // -- General condition
				_return = call (((GVAR(keybindArray) select 1) select _forEachIndex) select 1);
                #ifdef __DEBUG_KEYBIND
                    [["Calling ", (GVAR(keybindArray) select 3 select _forEachIndex), "up"]] call CFUNC(debugMessage);
                #endif
			};
		};
	};
	if (_return) exitWith { };
	nil;
} forEach (GVAR(keybindArray) select 0);

_return
