class Keybinds {
    class addKeybind;
    class addKeybindSetting;
    class changeKeybind;
    class clientInitKeybinds;
    class getKeybindText;
    class getKeyName;
    class keybindCheck;
    class keyPressed;
    class modifierPressed;
};
