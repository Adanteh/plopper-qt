/*
    Function:       ADA_Core_fnc_addKeybindSetting
    Author:         Adanteh
    Description:    Adds a keybind entry to our settings window
*/
#include "macros.hpp"

params ["_category", "_tag", "_displayName", "_keybindIndex"];

_displayName params ["_label", "_tooltip"];

['add', [QGVAR(keybinds), 'Keybinds', _category, [
	_label,
    _tooltip,
	QGVAR(CtrlKeybind),
    format ["[%1 select 0 select %2] call %3", QGVAR(keybindArray), _keybindIndex, QFUNC(getkeybindText)],
	true,
	{ },
	[_keybindIndex, _category, _tag]
]]] call FUNC(settingsWindow);
