class Modules {

    class MODULE {
        defaultLoad = 1;
        class initCoreEvents;

        class Library {
            noHeader = 1;
            #include "functions\library\functions.hpp"

            #include "functions\library\batch\functions.hpp"
            #include "functions\library\debug\functions.hpp"
            #include "functions\library\events\functions.hpp"
            #include "functions\library\localization\functions.hpp"
            #include "functions\library\math\functions.hpp"
            #include "functions\library\namespace\functions.hpp"
            #include "functions\library\objects\functions.hpp"
            #include "functions\library\perframe\functions.hpp"
            #include "functions\library\strings\functions.hpp"
            #include "functions\library\ui\functions.hpp"
        };


        #include "functions\keybinds\functions.hpp"
        #include "functions\settings\functions.hpp"
    };

};
