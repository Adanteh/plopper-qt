/*
    Function:       ADA_Core_fnc_moduleQueue
    Author:         Adanteh
    Description:    Queues up a function for automatic execution based on the start of it's filename
*/
#include "macros.hpp"

params [["_functionVarName", "", [""]], ["_module", ""]];

if (isNil QGVAR(moduleUsage)) then {
	GVAR(moduleUsage) = true;
	GVAR(autoInitServer) = [];
	GVAR(autoInitClient) = [];
	GVAR(autoInit) = [];
	GVAR(autoInitPost) = [];
	GVAR(autoInitHc) = [];

	// -- Get the modules used in this mission -- //
	private _modules = (getArray (missionConfigFile >> QUOTE(PREFIX) >> "ModulesLoaded"));

	// -- Add modules that have 'defaultLoad' entry above 0
	private _modulesDefault = (("getNumber (_x >> 'defaultLoad') > 0" configClasses (configFile >> QUOTE(PREFIX) >> "Modules")) apply { configName _x });
    _modulesDefault append (("getNumber (_x >> 'defaultLoad') > 0" configClasses (missionConfigFile >> QUOTE(PREFIX) >> "Modules")) apply { configName _x });


	_modules append _modulesDefault;
	private _modulesAll =+ _modules;
	private _fnc_addDependencyModule = {
	    private _subModule = _this;
	    private _dependencies = getArray (configFile >> QUOTE(PREFIX) >> "Modules" >> _subModule >> "dependencies");
        _dependencies append (getArray (missionConfigFile >> QUOTE(PREFIX) >> "Modules" >> _subModule >> "dependencies"));

	    {
	        private _i = _modulesAll pushBackUnique _x;
	        if (_i != -1) then {
	            _x call _fnc_addDependencyModule;
	        };
	        nil;
	    } count _dependencies;
	};
	{
	    _x call _fnc_addDependencyModule;
	    nil
	} count _modules;


	GVAR(loadedModules) = _modulesAll apply { toLower _x };
    uiNamespace setVariable [QGVAR(loadedModules), GVAR(loadedModules)];
};

if !((toLower _module in GVAR(loadedModules))) exitWith { };

// -- Queue for execution -- //
private _functionNameLower = toLower _functionVarName;
if (_functionNameLower find "_fnc_clientinit" > 0) then {
    GVAR(autoInitClient) pushBackUnique _functionNameLower;
	uiNamespace setVariable [QGVAR(autoInitClient), GVAR(autoInitClient)];
};

if (_functionNameLower find "_fnc_serverinit" > 0) then {
    GVAR(autoInitServer) pushBackUnique _functionNameLower;
	uiNamespace setVariable [QGVAR(autoInitServer), GVAR(autoInitServer)];
};

if (_functionNameLower find "_fnc_hcinit" > 0) then {
    GVAR(autoInitHc) pushBackUnique _functionNameLower;
	uiNamespace setVariable [QGVAR(autoInitHc), GVAR(autoInitHc)];
};

if (_functionNameLower find "_fnc_init" > 0) then {
    GVAR(autoInit) pushBackUnique _functionNameLower;
	uiNamespace setVariable [QGVAR(autoInit), GVAR(autoInit)];
};

if (_functionNameLower find "_fnc_postinit" > 0) then {
    GVAR(autoInitPost) pushBackUnique _functionNameLower;
	uiNamespace setVariable [QGVAR(autoInitPost), GVAR(autoInitPost)];
};
