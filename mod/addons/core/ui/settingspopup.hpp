class GVAR(CtrlSettingsPopup): ctrlControlsGroupNoHScrollbars {
    onLoad = "uiNamespace setVariable ['ADA_core_settingsControlGroup', (_this select 0)]; ['load'] call ADA_Core_fnc_settingsWindow;";
    onUnload = "['unload'] call ADA_Core_fnc_settingsWindow;";
    idc = 116;
    x = -2;
    y = -2;
    w = __GUI_SETTING_W;
    h = __GUI_PANE_HEADER_H + __GUI_SETTING_SPACING_X; // -- This is the height if there would be no settings (Used for autosizing)
    class Controls {
        class Title: GVAR(DialogTitle) {
            idc = 4000;
            x = 0;
            y = 0;
            w = __GUI_SETTING_W;
            h = __GUI_PANE_HEADER_H;
            moving = 1;
            text = "SETTINGS";
        };

        class Background: ctrlStaticBackground {
            idc = 3999;
            colorBackground[] = __COLOR_BACKGROUND_SIDEBAR;
            x = 0;
            y = __GUI_PANE_HEADER_H;
            w = __GUI_SETTING_W;
            h = __GUI_SETTING_H - __GUI_PANE_HEADER_H;
        };

        class AttributeCategories: ctrlControlsGroupNoHScrollbars {
            idc = 4001;
            x = 0;
            y = __GUI_PANE_HEADER_H + __GUI_SETTING_SPACING_X;
            w = __GUI_SETTING_W;
            h = 0;
            class Controls {
                //--- Categories will be placed here using AttributeCategory template
            };

        };
    };
};
