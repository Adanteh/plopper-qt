#include "controls\default.hpp"
#include "controls\settingsCategory.hpp"
#include "controls\button.hpp"
#include "controls\checkbox.hpp"
#include "controls\slider.hpp"
#include "controls\textboxButton.hpp"
#include "controls\valueset.hpp"
#include "controls\playerselector.hpp"
#include "controls\keybind.hpp"

class GVAR(window) {
    idd = 115;
    enableSimulation = 1;
    enableDisplay = 1;
    movingEnable = 1;

    onLoad = "uiNamespace setVariable ['ADA_Core_window', _this select 0]; ['load'] call ADA_Core_fnc_settingsWindow;";
    onUnload = "['unload'] call ADA_Core_fnc_settingsWindow;";

    class ControlsBackground {};

    class Controls {
      class MainWindow: ctrlControlsGroupNoHScrollbars {
        x = __GUI_SETTING_X;
        y = __GUI_SETTING_Y;
        w = __GUI_SETTING_W;
        h = __GUI_SETTING_H;
        class Controls {
          class Title: GVAR(DialogTitle) {
              idc = 4000;
              x = 0;
              y = 0;
              w = __GUI_SETTING_W;
              h = __GUI_PANE_HEADER_H;
              moving = 1;
              text = "SETTINGS";
          };

          class Background: ctrlStaticBackground {
              colorBackground[] = __COLOR_BACKGROUND_SIDEBAR;
              x = 0;
              y = __GUI_PANE_HEADER_H;
              w = __GUI_SETTING_W;
              h = __GUI_SETTING_H - __GUI_PANE_HEADER_H;
          };

          class AttributeCategories: ctrlControlsGroupNoHScrollbars {
            idc = 4001;
            x = 0;
            y = __GUI_PANE_HEADER_H + __GUI_SETTING_SPACING_X;
            w = __GUI_SETTING_W;
            h = __GUI_SETTING_H - __GUI_PANE_HEADER_H - 2*__GUI_SETTING_SPACING_X - __GUI_PANE_BUTTON_H;
            class Controls {
              //--- Categories will be placed here using AttributeCategory template
            };

          };
          class ButtonOK: GVAR(ButtonAccent) {
            default = 1;
        		idc = 1;
        		text = "OK";
            y = __GUI_SETTING_H - __GUI_PANE_BUTTON_H;
            onButtonDown = "ctrlsetfocus (_this select 0);"; //--- Some values are saved only when they lose focus (e.g., slider's numerical value)
          };
        };
      };
    };
};
