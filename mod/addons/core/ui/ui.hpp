
class ctrlControlsGroupNoHScrollbars;
class ctrlControlsGroupNoScrollbars;
class ctrlControlsGroup;
class ctrlStaticBackground;
class ctrlStaticPictureKeepAspect;
class ctrlStaticPicture;
class ctrlStatic;
class ctrlStaticTitle;
class ctrlStructuredText;
class ctrlButton;
class ctrlCheckbox;
class ctrlXSliderH;
class ctrlEdit;
class ctrlCombo;
class ctrlToolbox;
class RscPicture;
class RscText;
class RscBackground;
class RscProgress;
class RscStructuredText;

class GVAR(RscStructuredText): RscStructuredText {
  x = 0;
	y = 0;
	h = 0.035;
	w = 0.1;
	text = "";
	size = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	colorText[] = {1,1,1,1};
	shadow = 1;
	class Attributes {
        font = "RobotoCondensed";
        color = "#ffffff";
        colorLink = "#D09B43";
        align = "left";
        shadow = 2;
        shadowColor = "#222222";
	};
};

#include "customclasses.hpp"

#include "controls\default.hpp"
#include "controls\settingsCategory.hpp"
#include "controls\button.hpp"
#include "controls\checkbox.hpp"
#include "controls\slider.hpp"
#include "controls\textboxButton.hpp"
#include "controls\valueset.hpp"
#include "controls\playerselector.hpp"
#include "controls\combo.hpp"
#include "controls\keybind.hpp"
#include "controls\toolbox.hpp"
#include "controls\CtrlNumberEdit.hpp"
#include "controls\CtrlTextEdit.hpp"

#include "emptyDialog.hpp"
#include "settingswindow.hpp"
#include "settingspopup.hpp"

class RscTitles {

    class GVAR(debugText)
    {
        idd = -1;
        movingEnable = 1;
        duration = 1e+011;
        fadein = 0;
        fadeout = 0;
        onLoad = "uiNamespace setVariable ['ada_core_debugText', (_this select 0)]";
        class controls
        {
            class Text: RscStructuredText
            {
                idc = 9999;
                style = "1 + 16";
                text = "";
                x = 0;
                y = 0.45;
                w = 1;
                h = 10000;
                colorText[] = {1, 1, 1, 1};
                colorBackground[] = {0, 0, 0, 0};
                size = "(0.05 / 1.17647) * safezoneH";
                sizeEx = 0.4;
                class Attributes {
                    color = "#ffffff";
                    align = "center";
                    shadow = 0;
                    valign = "top";
                };
            };
        };
    };
};
