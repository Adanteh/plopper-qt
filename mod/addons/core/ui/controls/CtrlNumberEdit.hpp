class GVAR(CtrlNumberEdit): GVAR(CtrlSetting) {
    colorMouseoverScroll[] = {__COLOR_EDIT_BG, __COLOR_BUTTON_BG_ACCENT}; // -- Used for settingsNumberEditScroll

	class Controls: Controls {
		class Title: Title {};

		class Value: ctrlEdit {
			idc = 100;
			tooltip = "";
            text = "0";
            font = __MONO;
            x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_GRIDX(5);
			h = __GUI_GRIDY(1.25);
            colorBackground[] = __COLOR_EDIT_BG;
            colorText[] = {0, 0, 0, 1};

            // -- This was meant to allow you to change numbers by just doing a mouseover and scrolling,
            // but mouseEnter only triggers once and mouseExit don't trigger at all. Aka shit don't work
            // onMouseEnter = "['ctrl.numberEdit.mousein', [_this]] call ADA_core_fnc_localEvent;";
            // onMouseExit = "['ctrl.numberEdit.mouseout', [_this]] call ADA_core_fnc_localEvent;"
		};
	};
};
