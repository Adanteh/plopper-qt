class GVAR(CtrlCheckbox): GVAR(CtrlSetting) {
	class Controls: Controls {
		class Title: Title{};
		class Value: ctrlCheckbox {
			idc = 100;
			textureChecked = ICON(toggle,check_box);
			textureUnchecked = ICON(toggle,check_box_outline_blank);
			textureFocusedChecked = ICON(toggle,check_box);
			textureFocusedUnchecked = ICON(toggle,check_box_outline_blank);
			textureHoverChecked = ICON(toggle,check_box);
			textureHoverUnchecked = ICON(toggle,check_box_outline_blank);
			texturePressedChecked = ICON(toggle,check_box);
			texturePressedUnchecked = ICON(toggle,check_box_outline_blank);
			textureDisabledChecked = ICON(toggle,check_box);
			textureDisabledUnchecked = ICON(toggle,check_box_outline_blank);
			colorBackground[] = {0, 0, 0, 0};
			x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_GRIDX(1.25);
			h = __GUI_GRIDY(1.25);
		};
	};
};
