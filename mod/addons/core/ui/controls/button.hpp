class GVAR(CtrlButton): GVAR(CtrlSetting) {
	class Controls: Controls {
		class Title: Title {};
		class Button: GVAR(ButtonMain) {
			idc = 100;
			x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
			w = __GUI_SETTING_BUTTON_WIDE;
			h = __GUI_SETTING_LINE_H;
		};
	};
};

class GVAR(CtrlButtonSmall): GVAR(CtrlSetting) {
	class Controls: Controls {
		delete Title;
		class Button: GVAR(ButtonMain) {
			idc = 100;
			x = __GUI_SETTING_SPACING_X;
			w = __GUI_SETTING_BUTTON_WIDE;
			h = __GUI_SETTING_LINE_H;
		};
	};
};
