class GVAR(CtrlKeybind): GVAR(CtrlSetting) {
  class Controls: Controls {
    class Title: Title{};
    class Button: GVAR(ButtonMain) {
      idc = 100;
      font = __FONT;
      x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
      w = __GUI_SETTING_BUTTON_WIDE;
      h = __GUI_SETTING_LINE_H;
    };
  };
};
