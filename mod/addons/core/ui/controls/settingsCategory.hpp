
class GVAR(CtrlCategory): ctrlControlsGroup {
	y = __GUI_GRIDY(1);  // 'y' property defines space between them
	w = __GUI_SETTING_W;
	h = __GUI_SETTING_CATEGORY_H;
	class Controls {
		class Title: CtrlStatic {
			text = "UWOT";
			idc = 1;
			x = 0;
			y = 0;
			w = __GUI_SETTING_W;
			h = __GUI_SETTING_LINE_H;
			colorBackground[] = {0, 0, 0, 0};
			colorText[] = {1, 1, 1, 1};
			colorShadow[] = {0, 0, 0, 0};
			sizeEx = __GUI_SETTING_TEXT_H;
			font = __FONT_TITLE;
		};

		class Attributes: ctrlControlsGroupNoScrollbars {
			y = __GUI_SETTING_LINE_H;
			w = __GUI_SETTING_W;
			h = __GUI_SETTING_CATEGORY_H;
			idc = 100;
			class Controls {
				//--- Attributes will be placed here
			};
		};
	};
};

class GVAR(CtrlCategorySmall): ctrlControlsGroup {
	y = __GUI_GRIDY(0.2);  // 'y' property defines space between them
	w = __GUI_SETTING_W;
	h = __GUI_SETTING_CATEGORY_H;
	class Controls {
		class Title: CtrlStatic {
			text = "UWOT";
			idc = 1;
			x = 0;
			y = 0;
            w = 0;
            h = 0;
			/*w = __GUI_SETTING_W;
			h = __GUI_SETTING_LINE_H;
			colorBackground[] = {0, 0, 0, 0};
			colorText[] = {1, 1, 1, 1};
			colorShadow[] = {0, 0, 0, 0};
			sizeEx = __GUI_SETTING_TEXT_H;
			font = __FONT_TITLE;*/
		};

		class Attributes: ctrlControlsGroupNoScrollbars {
			y = 0;
			w = __GUI_SETTING_W;
			h = __GUI_SETTING_CATEGORY_H;
			idc = 100;
			class Controls {
				//--- Attributes will be placed here
			};
		};
	};
};
