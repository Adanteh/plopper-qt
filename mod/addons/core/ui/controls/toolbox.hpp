class GVAR(CtrlToolbox3): GVAR(CtrlSetting) {
	class Controls: Controls {
		class Title: Title {};
		class Value: CtrlToolbox {
            colorBackground[] = {0.275, 0.275, 0.275, 1};
            colorText[] = __COLOR_BUTTON_TEXT;
            colorTextSelect[] = __COLOR_BUTTON_TEXT_ACCENT;
            font = __FONT;
            //color[] = {1, 0, 1, 1};
            colorSelect[] = {0, 0, 0, 0}; // Doesn't work. BIS!
            colorTextDisable[] = {0, 0, 0, 0}; // Doesn't work. BIS!
            colorDisable[] = {0, 0, 0, 0}; // Doesn't work. BIS!
            colorSelectedBg[] = __COLOR_BUTTON_BG_ACCENT;
            w = __GUI_GRIDX(10);
            h = __GUI_GRIDY(1.25);

            idc = 100;
            x = __GUI_SETTING_TEXT_W + __GUI_SETTING_SPACING_X;
            rows = 1;
            columns = 3;
		};
	};
};
