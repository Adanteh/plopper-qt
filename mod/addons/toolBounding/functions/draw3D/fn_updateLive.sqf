/*
    Function:       ADA_toolBounding_fnc_updateLive
    Author:         Adanteh
    Description:    Run when tool is activated
*/
#include "macros.hpp"

private _allVariables = +(allVariables GVAR(namespace));
{
    private _entry = GVAR(namespace) getVariable _x;
    if !(isNil "_entry") then {
        _entry params ["_object", "_corners", "_color"];
        (_corners select 0) params ["_x1", "_y1", "_z1"];
        (_corners select 1) params ["_x2", "_y2", "_z2"];
        drawLine3D [_object modelToWorld [_x2, _y2, _z2], _object modelToWorld [_x2, _y2, _z1], _color];
        drawLine3D [_object modelToWorld [_x1, _y1, _z2], _object modelToWorld [_x1, _y1, _z1], _color];
        drawLine3D [_object modelToWorld [_x2, -_y2, _z2], _object modelToWorld [_x2, -_y2, _z1], _color];
        drawLine3D [_object modelToWorld [_x1, -_y1, _z2], _object modelToWorld [_x1, -_y1, _z1], _color];

        drawLine3D [_object modelToWorld [_x1, -_y1, _z2], _object modelToWorld [_x2, _y2, _z2], _color];
        drawLine3D [_object modelToWorld [_x2, -_y2, _z2], _object modelToWorld [_x1, _y1, _z2], _color];
        drawLine3D [_object modelToWorld [_x2, -_y2, _z2], _object modelToWorld [_x2, _y2, _z2], _color];
        drawLine3D [_object modelToWorld [_x1, -_y1, _z2], _object modelToWorld [_x1, _y1, _z2], _color];
    };
    nil;
} count _allVariables;
