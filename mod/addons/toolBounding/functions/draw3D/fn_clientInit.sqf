/*
    Function:       ADA_ToolBounding_fnc_clientInit
    Author:         Adanteh
    Description:    Creates namespaces
*/
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);

["selection.reset", {
    call FUNC(resetBoxes);
}] call CFUNC(addEventHandler);

// -- Handles unique mouseover item
["object.mouseoverchanged", {
    (_this select 0) params ["_obj"];

    if (isNull _obj) exitWith {
        GVAR(namespace) setVariable ["boxMouseOver", nil];
    };

    private _color = [[0, 0, 1, 1], [1, 0, 0, 0.8]] select ([_obj] call CFUNC(isTerrainObject));
    [_obj, 1, _color, nil, "boxMouseOver"] call FUNC(createBoxLive);
}] call CFUNC(addEventHandler);
