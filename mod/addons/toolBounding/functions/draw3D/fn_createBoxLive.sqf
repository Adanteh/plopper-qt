/*
    Function:       ADA_toolBounding_fnc_createBoxLive
    Author:         Adanteh
    Description:    Create bounding box matching the item, but a 'live' version that always uses current position
    Excample:       [cursorTarget, 1, [1, 0, 0, 1], "base"];
*/
#include "macros.hpp"

params [["_object", objNull], ["_scale", 1], ["_color", [0.1, 0.1, 0.1, 1]], ["_tag", "base"], ["_varName", ""]];

if (isNull _object) exitWith { };

private _boundingBox = boundingBoxReal _object;
private _boundingBoxCenter = boundingCenter _object;
_boundingBox params ["_p1", "_p2"];

// add center
_p1 = _p1 vectorAdd _boundingBoxCenter;
_p2 = _p2 vectorAdd _boundingBoxCenter;

// scale
_p1 = _p1 vectorMultiply _scale;
_p2 = _p2 vectorMultiply _scale;

// reset center
_p1 = _p1 vectorAdd (_boundingBoxCenter vectorMultiply -1);
_p2 = _p2 vectorAdd (_boundingBoxCenter vectorMultiply -1);

if (_varName == "") then {
    _varName = str _object + _tag;
};
GVAR(namespace) setVariable [_varName, [_object, [_p1, _p2], _color]];
