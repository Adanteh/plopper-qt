/*
    Function:       ADA_toolBounding_fnc_resetBoxes
    Author:         Adanteh
    Description:    Resets all the boxes
*/
#include "macros.hpp"

// -- Setting all variables to nil doesn't clean up the array. To keep this fast, just create it
if (GVAR(namespace) isEqualType locationNull) then {
    deleteLocation GVAR(namespace);
} else {
    deleteVehicle GVAR(namespace);
};
GVAR(namespace) = false call CFUNC(createNamespace);

