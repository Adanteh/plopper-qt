#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_selectionBox
    Author:         Adanteh
    Description:    Handles creation of our boundingbox
*/
params ["_object"];

//[_object] call FUNC(selectionObject);
[_object] call FUNC(boxCreate);


private _color = [0, 0, 1, 1];
if ([_object] call CFUNC(isTerrainObject)) then {
    _color = [1, 0, 0, 0.8];
} else {
    if (_object getVariable [QMVAR(SlopeLandContact), false]) then {
        _color = [1, 0.1, 0, 1];
    };
};

// -- Draw a different box to indicate the scale
private _box = [_object, 1, _color] call FUNC(createBoxLive);
private _scale = _object getVariable [QMVAR(Scale), 1];
if (_scale != 1) then {
    [_object, _scale, [1, 0.5, 1, 0.5], "scale"] call FUNC(createBoxLive);
};