/*
    Function:       ADA_toolBounding_fnc_deleteBox;
    Author:         Adanteh
    Description:    Deletes bounding box for given object
*/
#include "macros.hpp"

params [["_object", objNull]];

// -- Find all the boxes for the given object
private _allVariables = +(allVariables GVAR(namespace));
{
    (GVAR(namespace) getVariable [_x, [objNull]]) params ["_objectEntry"];
    if (_objectEntry isEqualTo _object) then {
        GVAR(namespace) setVariable [_x, nil];
    };
    nil;
} count _allVariables;
