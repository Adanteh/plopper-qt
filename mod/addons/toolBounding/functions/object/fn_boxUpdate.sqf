#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxUpdate
    Author:         Adanteh
    Description:    Updates positions for our boxes
*/

private _allVariables = +(allVariables GVAR(namespaceObj));
{
    private _box = GVAR(namespaceObj) getVariable _x;
    if !(isNil "_box") then {
        private _obj = _box getVariable [QGVAR(obj), objNull];
        if !(isNull _obj) then {
            _box setVectorDirAndUp [vectorDir _obj, vectorUp _obj];
            _box setPosASL (AGLtoASL (_obj modelToWorld (_box getVariable QGVAR(boxOffset)))); 
        };
    };
    nil;
} count _allVariables;