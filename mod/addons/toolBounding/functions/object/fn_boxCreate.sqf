#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxCreate
    Author:         Adanteh
    Description:    Animated model way of creating box to indicate selection
*/

params ["_obj", ["_mode", 0]];

private _box = createSimpleObject [QSVAR(bounding_box), (getPosASL _obj), false];
(boundingBoxReal _obj) params ["_p1", "_p2"];

// -- Get our height offset to be able to set position properly that also works for pitch / banked

private _center_height = (boundingCenter _obj) select 2;
private _height = (_p2 select 2) - (_p1 select 2);
_h1 = (_p1 select 2) + _center_height;
_h2 = (_p2 select 2) + _center_height;
private _attach_height =  _h2 - _h1 - _height;

private _color = ["0,0,1", "1,0,0"] select ([_obj] call CFUNC(isTerrainObject));
if (_obj getVariable [QMVAR(slopeLandContact), false]) then {
    _color = "1,0.5,0";
};
private _varname = switch (_mode) do {
    case 0: { str _obj };
    case 1: { "mouseover"};
    case 2: { _color = "1,1,1"; str _obj + "_scale" };
};

_box setVariable [QGVAR(obj), _obj, false];
_box setObjectTexture [0, format["#(rgb,8,8,3)color(%1,1,co)", _color]];
_box setVariable [QGVAR(boxOffset), [0, 0, _attach_height], false];
[_obj, _box, (_mode == 2)] call FUNC(boxAnimate);
_box setVectorDirAndUp [vectorDir _obj, vectorUp _obj];
_box setPosASL (AGLtoASL (_obj modelToWorld (_box getVariable QGVAR(boxOffset))));

GVAR(namespaceObj) setVariable [_varname, _box];

if (_mode == 0) then {
    private _scale = _obj getVariable [QMVAR(scale), 1];
    if (_scale != 1) then {
        [_obj, 2] call FUNC(boxCreate);
    };
};

_box;