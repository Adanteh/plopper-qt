#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxScale
    Author:         Adanteh
    Description:    Resets all the boxes
*/

params ["_obj"];

private _box = GVAR(namespaceObj) getVariable [str _obj + "_scale", objNull];
if (isNull _box) then {
    _box = [_obj, 2] call FUNC(boxCreate);
} else {
    [_obj, _box, true] call FUNC(boxAnimate);
};

