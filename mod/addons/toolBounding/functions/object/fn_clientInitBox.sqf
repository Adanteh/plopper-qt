#include "macros.hpp"
/*
    Function:       ADA_ToolBounding_fnc_clientInitBox
    Author:         Adanteh
    Description:    Creates namespaces
*/

GVAR(namespaceObj) = false call CFUNC(createNamespace);

["selection.reset", { call FUNC(boxReset) }] call CFUNC(addEventHandler);
["selection.addobject", { (_this select 0) call FUNC(boxCreate) }] call CFUNC(addEventHandler);

// -- Handles unique mouseover item
["object.mouseoverchanged", {
    (_this select 0) params ["_obj"];

    deleteVehicle (GVAR(namespaceObj) getVariable ["boxMouseOver", objNull]);
    if (isNull _obj) exitWith { };

    GVAR(namespaceObj) setVariable ["boxMouseOver", [_obj, 1] call FUNC(boxCreate)];

}] call CFUNC(addEventHandler);

["object.predelete", {
    (_this select 0) params ["_obj"];

    private _box = GVAR(namespaceObj) getVariable [str _obj + "_scale", objNull];
    if !(isNull _box) then { deleteVehicle _box };

    private _box = GVAR(namespaceObj) getVariable [str _obj, objNull];
    if !(isNull _box) then { deleteVehicle _box };

}] call CFUNC(addEventHandler);