#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxDelete
    Author:         Adanteh
    Description:    Delete box for given object
*/

params ["_obj"];
private _box = GVAR(namespaceObj) getVariable [str _obj + "_scale", objNull];
deletevehicle _box;