#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxAnimate
    Author:         Adanteh
    Description:    Animate the model (Called on scale changes)
*/
params ["_obj", "_box", ["_scaled", false]];

(boundingBoxReal _obj) params ["_p1", "_p2"];

if (_scaled) then {
    private _scale = _obj getVariable [QMVAR(scale), 1];
    _p2 = _p2 vectorMultiply _scale;
    _p1 = _p1 vectorMultiply _scale;
};

(_p2 vectorDiff _p1) params ["_length", "_width", "_height"];
_box animate ["flt_z", -1*(_width/2), true];
_box animate ["rlt_z", 1*(_width/2), true];

//SizeY
_box animate ["flt_x", -1*(_length/2), true];
_box animate ["rlt_x", -1*(_length/2), true];
_box animate ["flb_x", -1*(_length/2), true];
_box animate ["rlb_x", -1*(_length/2), true];

_box animate ["frt_x", 1*(_length/2), true];
_box animate ["rrt_x", 1*(_length/2), true];
_box animate ["frb_x", 1*(_length/2), true];
_box animate ["rrb_x", 1*(_length/2), true];

_box animate ["flt_y",1*(_height/2), true];
_box animate ["flb_y",-1*(_height/2), true];