#include "macros.hpp"
/*
    Function:       ADA_toolBounding_fnc_boxReset
    Author:         Adanteh
    Description:    Resets all the boxes
*/

private _allVariables = +(allVariables GVAR(namespaceObj));

{
    private _box = GVAR(namespaceObj) getVariable _x;
    if !(isNil "_box") then {
        deleteVehicle _box;
    };
    nil;
} count _allVariables; 

// -- Recreate this
deleteLocation GVAR(namespaceObj);
GVAR(namespaceObj) = false call CFUNC(createNamespace);

