#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Tools {
        class ADDON {
            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
            };
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main"};

            // class draw3D {
            //     class clientInit;
            //     class createBoxLive;
            //     class deleteBox;
            //     class resetBoxes;
            //     class selectionBox;
            //     class updateLive;
            // };


            class Events {
                class toolActivate;
                class toolDeactivate;
            };

            class Object {
                class boxAnimate;
                class boxCreate;
                class boxDelete;
                class boxReset;
                class boxScale;
                class boxUpdate;
                class clientInitBox;
            };
        };
    };
};
