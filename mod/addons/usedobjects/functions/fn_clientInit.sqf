/*
    Function:       ADA_UsedObjects_fnc_clientInit
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

["object.created", {
    (_this select 0) params ["_object", "", "_client"];
    if !(_client isEqualTo player) exitWith { };
    ["add", (_this select 0)] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);

["object.predelete", {
    ["delete", (_this select 0)] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);

["project.clear", {
    ["clear"] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);

["object.item_changed", {
    ["item_changed", (_this select 0)] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);

["object.view_item", {
    ["item_changed", (_this select 0)] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);

["object.item_doubleclicked", {
    ["dblclick", (_this select 0)] call FUNC(handleUsedObjects);
}] call CFUNC(addEventHandler);
