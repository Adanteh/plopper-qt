/*
    Function:       ADA_UsedObjects_fnc_handleUsedObjects
    Author:         Adanteh
    Description:    Updates the listview for this module
*/
#include "macros.hpp"

params [["_mode", "add"], ["_args", []]];

private _return = true;

switch (toLower _mode) do {
    case "add": {
        _args params ["_object"];
        if (isNull _object) exitWith { };
        private _modelPath = getModelInfo _object select 1;
        private _objectIndex = _object getVariable [QMVAR(ObjectIndex), "##"];
        private _objectLayer = _object getVariable [QMVAR(ObjectLayer), ""];

        private _pos = getPosWorld _object;
        private _angles = _object getVariable [QMVAR(PitchBankYaw), [0, 0, 0]];
        ["plopper.plop", [
            "objects.add_object", 
            _modelPath, 
            _objectLayer, 
            _objectIndex,
            _pos#0,
            _pos#1,
            _pos#2,
            _object getVariable [QMVAR(scale), 1],
            _angles#0,
            _angles#1,
            _angles#2
        ]] call PY3_fnc_callExtension;
    };

    case "delete": { // -- Delete object entry (Look for the entry matching object name)
        _args params ["_object"];
        private _objectIndex = _object getVariable [QMVAR(ObjectIndex), "##"];
        ["plopper.plop", ["objects.remove_object", _objectIndex]] call PY3_fnc_callExtension;
    };

    case "clear": {
        ["plopper.plop", ["objects.clear"]] call PY3_fnc_callExtension;
    };

    case "item_changed": {
        _args params ["_ids", ["_inFrame", false]];
        private _objects = [];
        {
            private _object = MVAR(namespaceObjects) getVariable [_x, objNull];
            if !(isNull _object) then {

                [[_inFrame, worldToScreen (ASLToAGL getPosASL _object)], "blue"] call CFUNC(debugMessage);
                if (!_inFrame || { !(worldToScreen (ASLToAGL getPosASL _object) isEqualTo []) }) then {
                    _objects pushBack _object;
                };
            }
        } forEach _ids;

        if (_objects isEqualTo []) exitWith { };
        private _addToSelection = ["Selecting"] call MFUNC(modifierPressed);
        [_objects, _addToSelection] call MFUNC(selectObjects);
    };

    case "dblclick": { // -- Double click on an item, pan the camera
        _args params ["_objectIndex"];
        private _object = MVAR(namespaceObjects) getVariable [_objectIndex, objNull];
        if (isNull _object) exitWith { };

        // -- Moves camera to center on double clicked object in used objects pane
        private _camera = call MFUNC(getCamObject);
        private _positionOffset = (getPosWorld _object) vectorDiff (screenToWorld [0.5, 0.5]);
        private _newPos = (getPosWorld _camera) vectorAdd _positionOffset;
        _newPos set [2, getPosWorld _camera select 2];
        _camera setPosWorld _newPos;
    };
};

_return;
