#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            tools[] = {};
            class Events { };
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main"};
            class clientInit;
        };
    };
};
