/*
    Function:       ADA_Buldozer_fnc_clientInit
    Author:         Adanteh
    Description:    Inits mode where we don't really do anything
*/
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);

