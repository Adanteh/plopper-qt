/*
    Function:       ADA_Spal_fnc_fillDiscrete
    Author:         Mondkalb & Adanteh
    Description:    Generates objects along a line. Respects max rotation
*/
#include "macros.hpp"

GVAR(fillIndex) = GVAR(fillIndex) + 1;
GVAR(fillLayer) = format ["SPAL #%1", GVAR(fillIndex)];
private _data = ["plopper.plop", ["tools.spal.get_brush"]] call PY3_fnc_callExtension;
private _node_interval = MVAR(set_spal_node_interval);
private _max_distance = MVAR(set_spal_max_distance);
private _alignToTerrain = MVAR(set_slopeAlign);
private _weights = _data apply { _x select 2 };
GVAR(fillList) = _data;
GVAR(fillSettings) = [_node_interval, _max_distance, _alignToTerrain];

private _polypoints = call EFUNC(ToolPolyLine,getPolyLinePoints);
private _discretePath = [_polypoints, true] call FUNC(findDiscretePath);

//[_objectsPlaced] call MFUNC(selectObjects);
