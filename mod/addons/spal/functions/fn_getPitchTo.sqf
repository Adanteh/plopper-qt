/*
    Function:       ADA_Spal_fnc_getPitchTo
    Author:         Adanteh
    Description:    Gets slope pitch from one point to another
*/
#include "macros.hpp"

params ["_nodePos1", "_nodePos2"];

private _h1 = getTerrainHeightASL _nodePos1;
private _h2 = getTerrainHeightASL _nodePos2;
private _hdif = _h2 - _h1;
private _distance = _nodePos1 distance2D _nodePos2;

atan (_hdif/_distance);
