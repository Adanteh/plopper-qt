/*
    Function:       ADA_Spal_fnc_clientInit
    Author:         Adanteh
    Description:    Client init Filler mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(fillIndex) = 0;
GVAR(fillLayer) = "";

["spal.generate", {
    ["generate"] call FUNC(handleToolbarButtons);
}] call CFUNC(addEventHandler);