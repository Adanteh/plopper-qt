#include "macros.hpp"

private _nodeinterval = ["value.get", ["spal.nodeinterval", 0]] call FUNC(handleToolbarButtons);

private _fillList = +(GVAR(namespace) getVariable ["fill", []]);
private _values = _fillList select 0;

// assemble a list of allowed model paths
_allowedNodes = [];
_allowedSegments = [];

{
	_x params (call FUNC(getDataStructure));
	if (_isNode == 1) then
	{
		_allowedNodes pushBack _x;
	} else
	{
		_allowedSegments pushback _x;
	};
} forEach _values;

params ["_waypoints", "_distStep", "_discStep"];

_start = _waypoints select 0;
_end = _waypoints select 1;

_curDir = [_start, _end] call ada_spal_fnc_dirTo;

_nodes = [];
_discStep = [0];
_i = 1;
_limit = (count _waypoints);

_maxLoop = 1000;
_curLoop = 0;
private "_targetDir";
while {_i != _limit and _curLoop < _maxLoop} do
{
	// find the target pos
	_targetDir = [_start, _end] call ada_spal_fnc_dirTo;

	// select a template
	_type = selectRandom _allowedSegments;

	_type params (call FUNC(getDataStructure));

	_dirMod = 0;
	_distStep = if (_spacing == 0) then
	{
		// use the template's size as distance
		// doesnt work yet, force it to be 2m
		2
	} else
	{
		// user specified distance between segments
		_spacing
	};

	// Discrete step modification
	if (_dir_discrete != -1) then
	{
		// specific to discreteStepping
		_discStep2 = _discStep + [_dir_discrete];
		_offsetToCorrect = _targetDir - _curDir;
		_leftTurn = (_offsetToCorrect < 0);
		_dirCoef = if _leftTurn then {-1} else {1};

		_dirMod = ([abs _offsetToCorrect, _discStep2] call ada_spal_fnc_discreteDir) * _dirCoef;
		_newDir = _curDir + _dirMod;

		_targetDir = _newDir;

		_distoffset = _spacing - _offsetX;

		// The rotation the object around its center causes a mismatch of the connections. Calculate the deviance and offset by it to make it match again:
		// This would be the place to add matrices and do rotation matrices, but i cant be arsed. so just some more trig
		// So first find our pivot position
		_pivotX = (_start select 0)+(_distoffset)*sin(_targetDir);
		_pivotY = (_start select 1)+(_distoffset)*cos(_targetDir);

		_px = _start select 0;
		_py = _start select 1;
		_rpx = ( (_px - _pivotX) * cos(-_dirMod) ) + ( (_py - _pivotY) * sin(-_dirMod) ) + _pivotX;
		_rpy = (-(_px - _pivotX) * sin(-_dirMod) ) + ( (_py - _pivotY) * cos(-_dirMod) ) + _pivotY;

		_start = [_rpx, _rpy, 0];
	};

	// modify the stored postion of the object by specified offset (that doesnt influence the path)
	// Adjust by X
	_tempPos = [(_start select 0)+(_offsetX*0.99)*sin(_targetDir),(_start select 1)+(_offsetX*0.99)*cos(_targetDir),0];
	// Adjust by Y
	_tempPos = [(_tempPos select 0)+(_offsetY*0.99)*sin(_targetDir+90),(_tempPos select 1)+(_offsetY*0.99)*cos(_targetDir+90),0];

	// Generate item at this position
	_nodes pushBack [_tempPos, _targetDir + _Dir_Offset + _dirMod * 0.5, 0, _type];

	// Increment by step in new dir
	_newpos = [(_start select 0)+(_distStep)*sin(_targetDir),(_start select 1)+(_distStep)*cos(_targetDir),0];

	// if newpos is sufficiently close to the targetpos, readjust target
	if ((_newPos distance _end) < _distStep) then
	{
		_i = _i +1;
		_end = _waypoints select _i;
	};

	// reloop
	_curLoop = _curLoop + 1;
	_curDir = _targetDir;
	_start = _newpos;

};
// add last pylon at end
// _nodes pushBack [[_start select 0,_start select 1,0], _targetDir, 0];

_nodes
