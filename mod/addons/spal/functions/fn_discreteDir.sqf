

params ["_in", "_steps"];

_lidx = 0;
_prev = 1000000;
{
	_test = abs (_in - _x);
	if (_test < _prev) then {
		_lidx = _forEachIndex;
		_prev = _test;
	};
} forEach _steps;
_steps select _lidx
