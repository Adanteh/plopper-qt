/*
    Function:       ADA_Spal_fnc_getDataStructure
    Author:         Adanteh
    Description:    Gets the named local variables to export and use for params command
*/
#include "macros.hpp"

[
    "_templateName",
    "_modelPath",
    ["_probability", 1],
    ["_min_scale", 1],
    ["_max_scale", 1],
    ["_dir_random", 0],
    ["_dir_offset", 0],
    ["_dir_discrete", -1],
    ["_spacing", -1],
    ["_rotation_random", 0],
    ["_offsetX", 0],
    ["_offsetY", 0],
    ["_isNode", 0],
    ["_allowPitch", 0],
    ["_allowBank", 0],
    ["_memoryStart", ""],
    ["_memoryEnd", ""]
];
