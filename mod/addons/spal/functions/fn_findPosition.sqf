/*
    Function:       ADA_Spal_fnc_findPosition
    Author:         Mondkalb
    Description:    Finds position for spal entry
*/
#include "macros.hpp"

params [
    "_posStart", // -- Position we should start from (Object will always snap to this point)
    "_posEnd" // -- End position that we try and works toward
];

private _targetDir = [_posStart, _posEnd] call CFUNC(getRelDir);

// assemble a list of allowed model paths
private _fillList = +GVAR(fillList);
private _allowedNodes = [];
private _allowedSegments = [[], []];
{
	_x params (call FUNC(getDataStructure));
	if (_isNode == 1) then
	{
		_allowedNodes pushBack _x;
	} else
	{
		(_allowedSegments select 0) pushback _x;
		(_allowedSegments select 1) pushback _probability;
	};
} forEach _fillList;

private _objectType = (_allowedSegments select 0) selectRandomWeighted (_allowedSegments select 1);
_objectType params (call FUNC(getDataStructure));

private _dirMod = 0;
private _itemSize = if (_spacing == -1) then { // use the template's size as distance
	_nodeinterval
} else { // user specified distance between segments
	_spacing
};

[[_fnc_scriptNameShort, _curDir, _targetDir], "lime"] call CFUNC(debugMessage);

// Discrete step modification
if (_dir_discrete != -1) then
{
	// specific to discreteStepping
	private _discStep2 = [0, _dir_discrete];
	private _offsetToCorrect = _targetDir - _curDir;
	private _dirCoef = [1, -1] select (_offsetToCorrect < 0);
	_dirMod = ([abs _offsetToCorrect, _discStep2] call FUNC(discreteDir)) * _dirCoef;
	private _newDir = _curDir + _dirMod;

	_targetDir = _newDir;
	_distoffset = _spacing - _offsetX;

	// The rotation the object around its center causes a mismatch of the connections. Calculate the deviance and offset by it to make it match again:
	// This would be the place to add matrices and do rotation matrices, but i cant be arsed. so just some more trig
	// So first find our pivot position
	private _pivotX = (_posStart select 0) + (_distoffset)*sin(_targetDir);
	private _pivotY = (_posStart select 1) + (_distoffset)*cos(_targetDir);

	private _px = _posStart select 0;
	private _py = _posStart select 1;
	private _rpx = ( (_px - _pivotX) * cos(-_dirMod) ) + ( (_py - _pivotY) * sin(-_dirMod) ) + _pivotX;
	private _rpy = (-(_px - _pivotX) * sin(-_dirMod) ) + ( (_py - _pivotY) * cos(-_dirMod) ) + _pivotY;

	_posStart = [_rpx, _rpy, 0];
};

[[_fnc_scriptNameShort, _dirMod, _targetDir, _posStart], "magenta"] call CFUNC(debugMessage);

// modify the stored postion of the object by specified offset (that doesnt influence the path)
// Adjust by X
private _objectPos = [(_posStart select 0) + (_offsetX*0.99)*sin(_targetDir),(_posStart select 1) + (_offsetX*0.99)*cos(_targetDir), 0];
// Adjust by Y

_objectPos = [(_objectPos select 0) + (_offsetY*0.99)*sin(_targetDir+90),(_objectPos select 1) + (_offsetY*0.99)*cos(_targetDir+90), 0];

// -- This is the position the next object should start from
private _objectEndPos = [(_posStart select 0) + (_itemSize)*sin(_targetDir),(_posStart select 1) + (_itemSize)*cos(_targetDir), 0];
private _objectDirection = _targetDir + _dir_offset + _dirMod * 0.5;
private _objectPitch = _allowPitch min ([_posStart, _objectEndPos] call FUNC(getPitchTo));
private _pitchBankYaw = [_objectPitch, 0, _objectDirection];

//[[_fnc_scriptNameShort, _objectPitch, _objectDirection], "cyan"] call CFUNC(debugMessage);

// reloop
_curDir = _targetDir;

[_objectPos, _objectType, _pitchBankYaw, _objectEndPos]
