/*
    Function:       ADA_Spal_fnc_placeRiver
    Author:         Mondkalb
    Description:    WIP file, to make sure we don't need to restart for adding new files
*/
#include "macros.hpp"

params ["_nodePos1", "_nodePos2", "_initDir"];

private _findRelPitch = {
    private _posObj1 = +(_this select 0);
    private _posObj2 = +(_this select 1);

    private _h1 = _posObj1 select 2;
    private _h2 = _posObj2 select 2;
    _posObj1 set [2, 0];
    _posObj2 set [2, 0];

    private _hdif = _h2 - _h1;
    private _distance = _posObj1 distance _posObj2;

    atan (_hdif/_distance);
};

private _curPos = _nodepos1;
private _curDir = _initDir;
private _newPos = [0, 0, 0];
private _dirChange = 0;
private _pitch = [_nodepos1, _nodepos2] call _findRelPitch;
private _dold = 100000;
private _nextPart = "";

systemChat str _pitch;


private _fillList = +(GVAR(namespace) getVariable ["fill", []]);
private _values = _fillList select 0;

// loop through all possible parts to see which one brings us closer to the target
{

    _x params (call FUNC(getDataStructure));
    private _temp = _modelPath createSimpleObject [1, 1, 1];

    _temp setPosASL _curPos;
    _temp setDir _curDir;
    [_temp, _pitch, 0] call BIS_fnc_setPitchBank;
    private _projectedNewPos = ATLtoASL (_temp modelToWorld (_temp selectionPosition _memory));
    private _d = (_projectedNewPos distance _nodePos2);

    if (_d < _dold) then
    {
        _dold = _d;
        _nextPart = _modelPath;
        _newPos = _projectedNewPos;
        _dirChange = _dir_discrete;
    };
    deleteVehicle _temp;

} forEach _values;

private _newSegment = _nextPart createSimpleObject _nodepos1;
private _newDir = (_initDir + _dirChange);
_newSegment setPosASL _nodepos1;
_newSegment setDir _initDir;
[_newSegment, _pitch, 0] call BIS_fnc_setPitchBank;

gm_mkriver_initpos = _newPos;
gm_mkriver_initdir = _newDir;

[_newPos, _newDir]
