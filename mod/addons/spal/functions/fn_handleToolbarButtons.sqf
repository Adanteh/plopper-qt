/*
    Function:       ADA_Spal_fnc_handleToolbarButtons
    Author:         Adanteh & Mondkalb
    Description:    Shared fucntion for random snips for code called from buttons in toolbar
*/
#include "macros.hpp"

params [["_mode", "start"], ["_args", []]];
[[_fnc_scriptNameShort, _this], "cyan"] call CFUNC(debugMessage);

private _return = false;
switch (toLower _mode) do {
	case "generate" : {
        call FUNC(fillDiscrete);
	};
	case "generatestep" : {
        call FUNC(fillDiscrete);
	};
};

_return;
