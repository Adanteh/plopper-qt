/*
    Function:       ADA_Spal_fnc_findDiscretePath
    Author:         Mondkalb
    Description:    Finds the locations for our path
*/
#include "macros.hpp"

params ["_waypointsBase", ["_create", true]];

if (count _waypointsBase < 2) exitWith {
    [LOCALIZE("NOT_ENOUGH_WAYPOINTS"), "red", 5, -1] call CFUNC(debugMessage);
};

private _nodeinterval = GVAR(FillSettings) select 0;
private _maxDistanceFromPoint = GVAR(FillSettings) select 1;
private _alignToTerrain = GVAR(FillSettings) select 2;

private _createdObjects = [];
private _waypoints = +_waypointsBase;

{ _x set [2, getTerrainHeightASL _x]; } forEach _waypoints;
private _posStart = _waypoints deleteAt 0;
private _posEnd = _waypoints deleteAt 0;
private _curDir = [_posStart, _posEnd] call CFUNC(getRelDir);

private _nodes = [];
private _maxLoop = 1000;
private _curLoop = 0;

while {_curLoop < _maxLoop} do {
    private _stepEntry = [_posStart, _posEnd] call FUNC(findPosition);
    _stepEntry params ["_objectPos", "_objectType", "_pitchBankYaw", "_objectEndPos"];

    _curLoop = _curLoop + 1;
    _nodes pushBack [_objectPos, _objectType, _pitchBankYaw];

    if (_create) then {
        _objectType params (call FUNC(getDataStructure));
        private _scale = _min_scale + random (_max_scale - _min_scale);
        private _direction = (_pitchBankYaw param [2, 0]) + _dir_random;
        _objectPos set [2, getTerrainHeightASL _objectPos];
        private _object = [
            _modelPath,
            _objectPos,
            _direction,
            _scale,
            !_alignToTerrain,
            _pitchBankYaw,
            false,
            [[QMVAR(ObjectLayer), GVAR(fillLayer)]]
        ] call MFUNC(createObject);
        _createdObjects pushBack _object;

        //private _posDiff = _posStart vectorDiff _correctPos;
        //_object setPosWorld (getPosWorld _object vectorAdd _posDiff);
        //[[_fnc_scriptNameShort, _offset, _correctPos, _posDiff, _spacing], "magenta"] call CFUNC(debugMessage);
        //_objectEndPos = AGLtoASL (_object modelToWorld [0, (_spacing) - _offsetX, 0]);
    } else {
        _objectEndPos set [2, getTerrainHeightASL _objectEndPos];
    };

    _posStart = _objectEndPos;

    [[_fnc_scriptNameShort, _objectEndPos, _posEnd, _maxDistanceFromPoint], "orange"] call CFUNC(debugMessage);

    // -- If the end position of the object is close enough, move on to the next waypoint
    if ((_objectEndPos distance2D _posEnd) < _maxDistanceFromPoint) then {
        if (_waypoints isEqualTo []) exitWith { // -- Last point reached
            [[_fnc_scriptNameShort, "EXIT"], "red"] call CFUNC(debugMessage);
            breakto (_fnc_scriptName + '_Main');
        };
        _posEnd = _waypoints deleteAt 0;
        [[_fnc_scriptNameShort, "NEXT WAYPOINT", _posEnd], "red"] call CFUNC(debugMessage);
    };
};

if (count _createdObjects > 0) then {
    [_createdObjects] call MFUNC(selectObjects);
};

_nodes
