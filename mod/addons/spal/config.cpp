#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class ObjectPlacement {
	class Modes {
		class Default;
		class MODULE: Default {
			tools[] = {QSVAR(toolPolyLine), QSVAR(toolBounding)};
			class Events {
				class OnActivate { function = QFUNC(toolActivate); };
				class OnDeactivate { function = QFUNC(toolDeactivate); };
			};
		};
	};
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main"};
            class testFunc;

			class clientInit;
			class discreteDir;
			class fillDiscrete;
			class findDiscretePath;
            class findPosition;
            class getDataStructure;
            class getPitchTo;
            class handleToolbarButtons;
		};
	};
};
