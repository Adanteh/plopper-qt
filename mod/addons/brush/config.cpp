#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            tools[] = {QSVAR(toolCircle), QSVAR(toolBounding)};
            class Events {
                class MouseDragStart { function = QFUNC(brushStart); };
                class MouseDragEnd { function = QFUNC(brushEnd); };
                class BDPlaceStart { function = QFUNC(brushStart); };
                class BDPlaceEnd { function = QFUNC(brushEnd); };
            };
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main", "toolCircle", "toolBounding"};

            class brushEnd;
            class brushLoop;
            class brushSelectionAction;
            class brushStart;
            class clientInit;
            class getDataStructure;

        };
    };
};
