/*
    Function:       ADA_Brush_fnc_brushSelectionAction
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#define __INCLUDE_DIK
#include "macros.hpp"

private _currentPos = getMousePosition;
private _lastPos = GVAR(namespace) getVariable ["mouseDragPosLast", [-1, -1]];

 // -- Don't place when mouse not moving
if (_lastPos isEqualTo _currentPos) exitWith { };

private _circleSize = call EFUNC(toolCircle,getCircleSize);
private _selectTerrainObjects = true;  //GVAR(namespace) getVariable ["setting.brush.selectTerrainObjects", false];
private _position = screenToWorld _currentPos;
private _objects = (call MFUNC(getObjectsPlaced)) - (call MFUNC(getSelectedObjects));
if (_selectTerrainObjects) then {
    _objects append (nearestTerrainObjects [_position, [], _circleSize]);
};

private _objectsInBrush = _objects select { _x inArea [_position, _circleSize, _circleSize, 0, false]; };
[_objectsInBrush, true] call MFUNC(selectObjects);

GVAR(namespace) setVariable ["mouseDragPosLast", _currentPos];
