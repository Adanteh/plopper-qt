/*
    Function:       ADA_Brush_fnc_clientInit
    Author:         Adanteh
    Description:    Client init brush mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(index) = 0;
GVAR(layer) = "";
GVAR(objectsPlaced) = [];  // Used to allow spacing only to current brush action, plus select them at the end


["brush.add_from_selection", { 
    private _data = [(_this select 0)] call MFUNC(getSelectionData);
    ["plopper.plop", ["tools.brush.add_from_selection", _data]] call PY3_fnc_callExtension;
}] call CFUNC(addEventHandler);

/*

            _pos = ASLToAGL (getPosASL MVAR(bdCursor));
            setMousePosition (worldToScreen _pos);
            [0, nil, _pos] call FUNC(brushStart);
*/