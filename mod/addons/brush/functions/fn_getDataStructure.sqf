/*
    Function:       ADA_Brush_fnc_getDataStructure
    Author:         Adanteh
    Description:    Gets the named local variables to export and use for params command
*/
#include "macros.hpp"

[
    "_templateName",
    "_modelPath",
    ["_probability", 1],
    ["_dir_random", 360],
    ["_min_scale", 1],
    ["_max_scale", 1],
    ["_rotation_random", 0],
    ["_spacing", 0.5],
    ["_keepHorizontal", 0],
    ["_z_offset", 0]
];
