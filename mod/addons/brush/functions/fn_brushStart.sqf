/*
    Function:       ADA_Brush_fnc_brushStart
    Author:         Adanteh
    Description:    Event for mouse dragging start (When button is hold) for brush mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld"];
if (_button != 0) exitWith { };

GVAR(namespace) setVariable ["lastPosMove", _startPosWorld];
GVAR(namespace) setVariable ["drag.startPosWorld", _startPosWorld];

// -- Selection brush
private ["_code", "_mode"];
if (["Selecting"] call MFUNC(modifierPressed)) then {
    _mode = "select";
    _code = { _this call FUNC(brushSelectionAction) };
} else {
    [[]] call MFUNC(selectObjects);

    // -- Set / get the brush settings
    GVAR(index) = GVAR(index) + 1;
    GVAR(layer) = format ["Brush #%1", GVAR(index)];
    GVAR(objectsPlaced) = [];

    private _data = ["plopper.plop", ["tools.brush.get_brush"]] call PY3_fnc_callExtension;
    private _weights = _data apply { _x select 2 };
    GVAR(brush) = [_weights, _data];
    GVAR(clusterCounter) = 50;

    if (GVAR(brush) isEqualTo [[], []]) exitWith {
        [LOCALIZE("NO_BRUSH_SELECTED"), "orange", nil, -1] call CFUNC(debugMessage);
    };

    // -- Start loop
    _mode = "brush";
    _code = { _this call FUNC(brushLoop) };
};

private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
private _handle = [_code, 0] call CFUNC(addPerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", _handle];
GVAR(namespace) setVariable ["dragMode", _mode];

true