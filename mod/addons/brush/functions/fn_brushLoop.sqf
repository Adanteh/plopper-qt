/*
    Function:       ADA_Brush_fnc_brushLoop
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _currentPos = getMousePosition;

// -- How often the brush should add / remove objects
private _flow = 1 / MVAR(set_brush_flow);
if (((GVAR(namespace) getVariable ["lastUsedAt", -1]) + _flow) > diag_tickTime) exitWith { };

private _deleting = ["Deselect"] call MFUNC(modifierPressed);
if (_deleting) then {

    private _circleSize = call EFUNC(toolCircle,getCircleSize);
    private _objects = (call MFUNC(getObjectsPlaced));
    private _inCircle = ((_objects select { _x inArea [screenToWorld _currentPos, _circleSize, _circleSize, 0, false]; }) select { [_x] call MFUNC(canEditObject) });

    if (count _inCircle > 0) then {
        private _object = selectRandom _inCircle;
        ["object.predelete", [_object]] call CFUNC(globalEvent); // Keep this in front, so we dont end up with null
        deleteVehicle _object;
    };


} else {

    private _circleSize = call EFUNC(toolCircle,getCircleSize);
    private _pos = AGLtoASL (screenToWorld _currentPos);
    private _placePos = _pos getPos [random _circleSize, random 360];
    _placePos set [2, _pos select 2];
    if (!MVAR(set_brush_allowOnWater) && {surfaceIsWater ASLtoATL _placePos}) exitWith { };
    if (!MVAR(set_brush_allowOnRoad) && {isOnRoad ASLtoATL _placePos}) exitWith { };

    private _entry = (GVAR(brush) select 1) selectRandomWeighted (GVAR(brush) select 0);
    _entry params (call FUNC(getDataStructure));

    // -- Do some weird clustering thing, where if we place one object, the chance an object gets placed next time increases
 	if (MVAR(set_brush_clustering) != -1) then {
        private _chanceToPlace = GVAR(clusterCounter);
        GVAR(clusterCounter) = GVAR(clusterCounter) - ((GVAR(set_clustering) min 100) / 10);

        if (GVAR(clusterCounter) <= (0 - GVAR(set_clustering))) then {
            GVAR(clusterCounter) = (100 + GVAR(set_clustering));
        };
        if (random 100 > GVAR(clusterCounter)) then {
            breakTo (_fnc_scriptName + "_main");
        };
    };

    private _spaceList = if (MVAR(set_brush_spaceToCurrent)) then {
        GVAR(objectsPlaced)
    } else {
        call MFUNC(getObjectsPlaced)
    };
    if (count (_spaceList inAreaArray [_placePos, _spacing, _spacing, 0, false, -1]) > 0) exitWith { };

    // [_placePos] call CFUNC(debugMessage);
    if (MVAR(set_brush_spaceToTerrain) && { (count (nearestTerrainObjects [ASLtoAGL _placePos, [], _spacing]) > 0) }) exitWith { };

    private _direction = (0) + random _dir_random;
    private _scale = _min_scale + random (_max_scale - _min_scale);
    private _pitchBankYaw = [random _rotation_random, random _rotation_random, _direction];

    _placePos set [2, (getTerrainHeightASL _placePos) + _z_offset];
    private _vars = [[QMVAR(ObjectLayer), GVAR(layer)]];

    // 0 horizon setting uses global align, 1 sets it to horizontal, 2 to slope align
    private _horizontal = !MVAR(set_slopeAlign);
    if (_keepHorizontal > 0) then {
        _horizontal = (_keepHorizontal == 1);
    };
    if !(_horizontal) then {
        _pitchBankYaw = [];
    };

    GVAR(objectsPlaced) pushBack ([_modelPath, _placePos, _direction, _scale, _horizontal, _pitchBankYaw, false, _vars] call MFUNC(createObject));
};

GVAR(namespace) setVariable ["lastUsedAt", diag_tickTime];
GVAR(namespace) setVariable ["mouseDragPosLast", _currentPos];
