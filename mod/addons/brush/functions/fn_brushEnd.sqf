/*
    Function:       ADA_Brush_fnc_brushEnd
    Author:         Adanteh
    Description:    End of drag event for brush mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld", "_endPos", "_endPosWorld"];
if (_button != 0) exitWith { };


// -- End loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
[_handle] call CFUNC(removePerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", -1];

if (MVAR(set_brush_selectOnDone)) then {
    if ((GVAR(namespace) getVariable ["dragMode", ""]) == "brush") then {
        [GVAR(objectsPlaced)] call MFUNC(selectObjects);
    };
};
