#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
	class ADDON {
		author = AUTHOR;
		name = ADDONNAME;
		units[] = {};
		weapons[] = {};
		version = VERSION;
		requiredaddons[] = {QSVAR(Main)};
	};
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            tools[] = {QSVAR(toolCircle), QSVAR(toolBounding)};
            class Events {
                class MouseDragStart { function = QFUNC(mouseDragStart); };
                class MouseDragEnd { function = QFUNC(mouseDragEnd); };
                class MouseClick { function = QFUNC(placeComposition); };
            };
        };
    };
};

class PREFIX {
	class Modules {
		class MODULE {
			defaultLoad = 1;
			dependencies[] = {"Core", "Main", "toolCircle", "toolBounding"};

			class clientInit;
            class getDataStructure;
            class placeComposition;
            class selectionCircle;

            class Events {
				class mouseDragEnd;
				class mouseDragStart;
			};
		};
	};
};
