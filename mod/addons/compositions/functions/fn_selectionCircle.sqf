/*
    Function:       ADA_Compositions_fnc_selectionCircle
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _currentPos = getMousePosition;
private _lastPos = GVAR(namespace) getVariable ["mouseDragPosLast", [-1, -1]];

 // -- Don't place when mouse not moving. Maybe make this a setting
if (_lastPos isEqualTo _currentPos) exitWith { };

private _position = screenToWorld _currentPos;
private _circleSize = call EFUNC(toolCircle,getCircleSize);
private _deselect = ["Deselect"] call MFUNC(modifierPressed);
private "_objects";
if (_deselect) then {
    _objects = call MFUNC(getSelectedObjects);
} else {
    _objects = (call MFUNC(getObjectsPlaced)) - (call MFUNC(getSelectedObjects));
    _objects append (nearestTerrainObjects [_position, [], _circleSize]);
};

private _objectsInBrush = _objects select { _x inArea [_position, _circleSize, _circleSize, 0, false]; };

if (_deselect) then {
    [_objects - _objectsInBrush, false] call MFUNC(selectObjects);
} else {
    [_objectsInBrush, true] call MFUNC(selectObjects);
};


GVAR(namespace) setVariable ["mouseDragPosLast", _currentPos];
