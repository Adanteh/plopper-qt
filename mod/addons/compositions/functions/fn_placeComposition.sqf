/*
    Function:       ADA_Compositions_fnc_placeComposition
    Author:         Adanteh
    Description:    Places the composition
*/
#include "macros.hpp"

params ["_button", "_mousePosition", "_mousePositionWorld"];
if (_button != 0) exitWith { };

GVAR(compIndex) = GVAR(compIndex) + 1;
GVAR(compLayer) = format ["Composition #%1", GVAR(compIndex)];

private _data = ["plopper.plop", ["tools.compositions.get_brush"]] call PY3_fnc_callExtension;
private _keys = call FUNC(getDataStructure);
private _basePosition = +_mousePositionWorld;
private _objects = [];

_basePosition set [2, getTerrainHeightASL _basePosition];
{
    _x params _keys;

    private _pos = _basePosition vectorAdd [_offsetX, _offsetY, _offsetZ];

    // 0 uses global setting, 1 follows terrain, 2 ignores terrain height
    if ((_absolute_height == 1) || {_absolute_height == 0 && MVAR(set_followTerrain)}) then {
        _pos set [2, (getTerrainHeightASL _pos) + _offsetZ];
    };

    private _scale = _min_scale + random (_max_scale - _min_scale);
    private _direction = RANDOMOFFSET(_dir,_dir_random);
    private _pitchBankYaw = [_pitch + random _rotation_random, _bank + random _rotation_random, _direction];

    // 0 horizon setting uses global align, 1 sets it to horizontal, 2 to slope align
    private _horizontal = !MVAR(set_slopeAlign);
    if (_keepHorizontal > 0) then {
        _horizontal = (_keepHorizontal == 1);
    };
    if !(_horizontal) then {
        _pitchBankYaw = [];
    };

    private _vars = [[QMVAR(ObjectLayer), GVAR(compLayer)]];
    private _object = [_modelPath, _pos, _direction, _scale, _horizontal, _pitchBankYaw, false, _vars] call MFUNC(createObject);
    _objects pushBack _object;

} forEach _data;

[_objects, false] call MFUNC(selectObjects);
