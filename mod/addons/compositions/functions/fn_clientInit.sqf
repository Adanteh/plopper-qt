/*
    Function:       ADA_Compositions_fnc_clientInit
    Author:         Adanteh
    Description:    Client init brush mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(compIndex) = 0;
GVAR(compLayer) = "";


["compositions.add_from_selection", { 
    private _data = [(_this select 0)] call MFUNC(getSelectionData);
    ["plopper.plop", ["tools.compositions.add_from_selection", _data]] call PY3_fnc_callExtension;
}] call CFUNC(addEventHandler);
