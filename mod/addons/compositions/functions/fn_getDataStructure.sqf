/*
    Function:       ADA_Compositions_fnc_getDataStructure
    Author:         Adanteh
    Description:    Gets the named local variables to export and use for params command
*/
#include "macros.hpp"

[
    "_templateName",
    "_modelPath",
    ["_probability", 1], // -- Chance of object appearing
    ["_offsetX", 0], // -- OffsetX
    ["_offsetY", 0], // -- OffsetY
    ["_offsetZ", 0], // -- OffsetZ
    ["_min_scale", 1], // -- minimu scale (for tree shit)
    ["_max_scale", 1], // -- max scale
    ["_dir", 0], // -- Direction compared to 0 dir
    ["_dir_random", 0], // -- Extra random dir applied (not have the tree always pinting same dir)
    ["_rotation_random", 0], // -- pitchbankyaw rand offset
    ["_absolute_height", 0], // -- whether Z offset is relative to horizon or to terrain (For example a couple items should always be at the same height, regardless of terrain height)
    ["_keepHorizontal", 0], // -- Keep object horizontal always (for example on a house, whereas a wheelbarrow or so would follow slope)
    ["_pitch", 0],
    ["_bank", 0]
];
