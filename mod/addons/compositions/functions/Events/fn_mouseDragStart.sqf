/*
    Function:       ADA_Compositions_fnc_mouseDragStart
    Author:         Adanteh
    Description:    Event for mouse dragging start (When button is hold) for composition mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld"];
if (_button != 0) exitWith { };

GVAR(namespace) setVariable ["lastPosMove", _startPosWorld];
GVAR(namespace) setVariable ["drag.startPosWorld", _startPosWorld];


// -- Start loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
private _handle = [{ _this call FUNC(selectionCircle) }, 0] call CFUNC(addPerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", _handle];
