#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Modes {
        class Default;
        class MODULE: Default {
            tools[] = {QSVAR(toolBounding)};
            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
                class mouseDragStart { function = QFUNC(mouseDragStart); };
                class mouseDragEnd { function = QFUNC(mouseDragEnd); };
                class mouseClick { function = QFUNC(mouseButtonClick); };
                class onmouseMoving { function = QFUNC(mouseMoving); };
            };
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main"};

            class clientInit;
            class getClassData;
            class getDataStructure;
            class move;
            class placeObject;
            class replaceSelected;
            class selectDragging;

            class Copy {
                class clientInitCopy;
                class copyObject;
                class copyDirection;
                class loadCopySettings;
                class drawCopyDirection;
            };

            class Events {
                class mouseButtonClick;
                class mouseDragEnd;
                class mouseDragMoving;
                class mouseDragStart;
                class mouseMoving;
                class toolActivate;
                class toolDeactivate;
            };
        };
    };
};
