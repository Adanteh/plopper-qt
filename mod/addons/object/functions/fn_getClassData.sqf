/*
    Function:       ADA_Object_fnc_getClassData
    Author:         Adanteh
    Description:    Array order of received python code
*/
#include "macros.hpp"

params ["_template"];
["plopper.plop", ["tools.object.get_data", _template]] call PY3_fnc_callExtension;
