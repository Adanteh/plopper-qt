/*
    Function:       ADA_Object_fnc_getDataStructure
    Author:         Adanteh
    Description:    Array order of received python code
*/
#include "macros.hpp"

[
    ["_offsetX", 0],
    ["_offsetY", 0],
    ["_offsetZ", 0],
    ["_offsetXRandom", 0],
    ["_offsetYRandom", 0],
    ["_offsetZRandom", 0],
    ["_scaleMin", 1],
    ["_scaleMax", 1],
    ["_dirRandom", 0],
    ["_rotationOffsetX", 0],
    ["_rotationOffsetY", 0],
    ["_rotationOffsetZ", 0]
]
