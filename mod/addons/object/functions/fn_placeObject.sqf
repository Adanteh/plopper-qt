/*
    Function:       ADA_Object_fnc_placeObject
    Author:         Adanteh
    Description:    Places an object (This is the object.doubleclick action)
*/
#include "macros.hpp"

params ["_position"];

if (is3DEN) exitWith { };

_position = AGLtoASL _position;
private _selected = call MFUNC(getClassSelected);
if (_selected isEqualTo []) exitWith {
    [LOCALIZE("NO_CLASS_SELECTED"), "orange", 5, -1] call CFUNC(debugMessage);
};

_selected params ["_templateName", "_modelpath"];

private _classData = [_templateName] call FUNC(getClassData);
private _keys = [] call FUNC(getDataStructure);
_classData params _keys;


private _direction = random _dirRandom;
private _scale = _scaleMin + random (_scaleMax - _scaleMin);
// private _vars = [[QMVAR(ObjectLayer), "Manual"]];
private _vars = [];
private _object = [_modelpath, _position, _direction, _scale, !MVAR(set_slopeAlign), nil, nil, _vars] call MFUNC(createObject);



