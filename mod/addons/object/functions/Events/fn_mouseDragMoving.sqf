/*
    Function:       ADA_Object_fnc_mouseDragMoving
    Author:         Adanteh
    Description:    Called whenever the mouse moves
*/
#include "macros.hpp"

private ["_delta", "_currentPos"];
if MVAR(buldozer) then {
    private _lastPos = GVAR(namespace) getVariable ["mouseDragPosLast", getPosASL MVAR(bdCUrsor)];
    _currentPos = getPosASL MVAR(bdCUrsor);
    _delta = [(_currentPos select 0) - (_lastPos select 0), (_currentPos select 1) - (_lastPos select 1)];
} else {
    private _lastPos = GVAR(namespace) getVariable ["mouseDragPosLast", getMousePosition];
    _currentPos = getMousePosition;
    _delta = [(_currentPos select 0) - (_lastPos select 0), (_currentPos select 1) - (_lastPos select 1) * 5];
};

private _axisMovement = call {
    if (__AXISRESTRAINT("axis.rotate.z")) exitWith { true };
    if (__AXISRESTRAINT("axis.move.x")) exitWith { true };
    if (__AXISRESTRAINT("axis.move.y")) exitWith { true };
    if (__AXISRESTRAINT("axis.move.z")) exitWith { true };
    if (__AXISRESTRAINT("axis.rotate.x")) exitWith { true };
    if (__AXISRESTRAINT("axis.rotate.y")) exitWith { true };
    false;
};

// TODO: STICKY
if (_axisMovement || (GVAR(namespace) getVariable ["draggingOnObject", false])) then {
    GVAR(namespace) setVariable ["draggingOnObject", true];
    call {

        private _exit = false;
        if (__AXISRESTRAINT("axis.rotate.z")) then {
            _exit = true;
            [_delta, nil, "drag"] call MFUNC(rotateToPoint);
        };

        if (__AXISRESTRAINT("axis.move.z")) then {
            _exit = true;
            [_delta, nil, "drag"] call MFUNC(changeHeight);
        };

        if (__AXISRESTRAINT("axis.rotate.x")) then {
            _exit = true;
            [_delta, nil, "drag"] call MFUNC(pitch);
        };

        if (__AXISRESTRAINT("axis.rotate.y")) then {
            _exit = true;
            [_delta, nil, "drag"] call MFUNC(bank);
        };

        if (_exit) exitWith { };
        [_delta, nil] call FUNC(move);
    };
} else {
    [false] call FUNC(selectDragging);
};

GVAR(namespace) setVariable ["mouseDragPosLast", _currentPos];
