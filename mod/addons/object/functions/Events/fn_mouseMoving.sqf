/*
    Function:       ADA_Object_fnc_mouseMoving
    Author:         Adanteh
    Description:    Called whenever the mouse moves
*/
#include "macros.hpp"

if (MVAR(namespaceMouse) getVariable ["mouseDragging", false]) exitWith { false };

if !(MVAR(set_highlight_mouseover)) exitWith { false };

// -- Detect object under cursor, so we can highlight when its moving
private _mouseOverCurrent = GVAR(namespace) getVariable ["mouseOverBounding", objNull];
private _mouseOverNew = [nil] call MFUNC(detectUnderCursor);
if !(_mouseOverCurrent isEqualTo _mouseOverNew) then {
    ["object.mouseoverchanged", [_mouseOverNew, _mouseOverCurrent]] call CFUNC(localEvent);
    GVAR(namespace) setVariable ["mouseOverBounding", _mouseOverNew];
};

false;
