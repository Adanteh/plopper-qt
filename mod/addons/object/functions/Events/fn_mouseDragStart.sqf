/*
    Function:       ADA_Object_fnc_mouseDragStart
    Author:         Adanteh
    Description:    Event for mouse dragging start (When button is hold) for object mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld"];

if (_button != 0) exitWith { };

GVAR(namespace) setVariable ["lastPosMove", _startPosWorld];
GVAR(namespace) setVariable ["drag.startPosWorld", _startPosWorld];

// -- Determine if we started this drag with a mouse on an object, if so, we want to move/rotate/scale items, if not then we want to create a selection box
private _dragAction = if (MVAR(set_object_sticky_selection) && { !([] call MFUNC(getSelectedObjects) isEqualTo []) }) then {
    true;
} else {
    !isNull ([_startPos] call MFUNC(detectUnderCursor))
};

if (_dragAction) then {
    GVAR(namespace) setVariable ["draggingOnObject", true];
} else {
    GVAR(namespace) setVariable ["draggingOnObject", false];
};

// -- Used to identify between different dragging actions within other functions
GVAR(namespace) setVariable ["drag.dragIndex", GVAR(dragIndex)];
GVAR(dragIndex) = GVAR(dragIndex) + 1;

if (_dragAction) then {
    ["move"] call MFUNC(commandStart);
};

// -- Start loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
private _handle = [{ _this call FUNC(mouseDragMoving) }, 0] call CFUNC(addPerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", _handle];
