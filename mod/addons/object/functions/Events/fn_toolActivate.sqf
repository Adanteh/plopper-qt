/*
    Function:       ADA_Object_fnc_toolActivate
    Author:         Adanteh
    Description:    Run when tool is selected
*/
#include "macros.hpp"

// -- Call selection again, so we update gizmo properly for the objects we already had selection
["selection.changed", [call MFUNC(getSelectedObjects)]] call CFUNC(localEvent);
