/*
    Function:       ADA_Object_fnc_mouseButtonClick
    Author:         Adanteh
    Description:    Handles mouse button click
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params [["_button", 0], "_mousePosition", "_positionWorld"];
if (_button != 0) exitWith { };

// -- See if we click on an object, if so, select that object, LSHIFT or LCONTROL will add to selection
private _mouseOver = [nil] call MFUNC(detectUnderCursor);
private _removeFromSelection = ["Deselect"] call MFUNC(modifierPressed);
if (_removeFromSelection) exitWith {
    if (isNull _mouseOver) exitWith { };
    private _selection = call MFUNC(getSelectedObjects);
    [_selection - [_mouseOver], false] call MFUNC(selectObjects);
};

private _addToSelection = ["Selecting"] call MFUNC(modifierPressed);
[_mouseOver, _addToSelection] call MFUNC(selectObjects);
