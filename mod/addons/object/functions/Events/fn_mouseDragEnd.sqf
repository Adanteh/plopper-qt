/*
    Function:       ADA_Object_fnc_mouseDragEnd
    Author:         Adanteh
    Description:    Big event for mouse dragging options, while holding mouse button
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld", "_endPos", "_endPosWorld"];
if (_button != 0) exitWith { };

// -- If this was a drag that started without mouseover, then it was a drag selection.
if !(GVAR(namespace) getVariable ["draggingOnObject", true]) then {
    [true] call FUNC(selectDragging);
} else {
    ["move", [] call MFUNC(getSelectedObjects)] call MFUNC(commandEnd);
};

// -- End loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
[_handle] call CFUNC(removePerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", -1]
