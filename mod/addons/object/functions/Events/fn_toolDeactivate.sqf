/*
    Function:       ADA_Object_fnc_toolDeactivate
    Author:         Adanteh
    Description:    Run when tool is Deselected
*/
#include "macros.hpp"

// -- Reset some settings that might break, and we at least want to fix by toggling modules
GVAR(namespace) setVariable ["mouseDown", -1];
GVAR(namespace) setVariable ["selectionBox", []];

// -- Reset the mouseover box
["object.mouseoverchanged", [objNull, (GVAR(namespace) getVariable ["mouseOverBounding", objNull])]] call CFUNC(localEvent);
GVAR(namespace) setVariable ["mouseOverBounding", objNull];

[[objNull]] call FUNC(drawCopyDirection); // -- Reset the copy arrow on top of objects
