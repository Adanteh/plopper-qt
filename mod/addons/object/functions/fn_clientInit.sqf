/*
    Function:       ADA_Object_fnc_clientInit
    Author:         Adanteh
    Description:    Client init object mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(dragIndex) = 0;

[{
    // -- Draws selection box for dragging
    private _box = +(GVAR(namespace) getVariable ["selectionBox", []]);
    if !(_box isEqualTo []) then {
        #define __BOXCOLOR [0, 1, 1, 1]
        drawLine3D [_box select 0, _box select 1, __BOXCOLOR];
        drawLine3D [_box select 1, _box select 2, __BOXCOLOR];
        drawLine3D [_box select 2, _box select 3, __BOXCOLOR];
        drawLine3D [_box select 3, _box select 0, __BOXCOLOR];
    };
}, 0] call CFUNC(addPerFrameHandler);

["object.created", {
    (_this select 0) params ["_object", "_select", "_client"];
    if !(_select) exitWith { };
    if !(player isEqualTo _client) exitWith { };

    [_object] call MFUNC(selectObjects);
}] call CFUNC(addEventHandler);

// -- Double clicking will create an object
["mouse.doubleclick", {
    (_this select 0) params [["_button", 0], ["_mousePosition", getMousePosition]];
    if !(GVAR(modeActive)) exitWith { };
    if (_button != 0) exitWith { };

    [screenToWorld _mousePosition] call FUNC(placeObject);
}] call CFUNC(addEventHandler);

// -- Will update translation gizmo (Small model to indicate rotation axis and so)
["selection.changed", {
    if !(GVAR(modeActive)) exitWith { };

    (_this select 0) params ["_objects"];
    _objects = _objects select { [_x] call MFUNC(canEditObject) };
    if (count _objects == 0) exitWith { [nil] call EFUNC(toolGizmo,updateGizmo) };

    // -- Selecting multiple objects will use the middle of all objects as offset
    private _object = [_objects] call CFUNC(arrayPeek);
    private "_offset";
    if (isNull _object) exitWith { [nil] call EFUNC(toolGizmo,updateGizmo) };
    if (count _objects == 1) then {
        private _data = [[_object] call MFUNC(getTemplateName)] call FUNC(getClassData);
        _data params (call FUNC(getDataStructure));
        _offset = [_rotationOffsetX, _rotationOffsetY, _rotationOffsetZ];
    } else {
        private _centerPoint = [0, 0, 0];
        _objects apply { _centerPoint = _centerPoint vectorAdd (getPosWorld _x ) };
        _centerPoint = _centerPoint vectorMultiply (1 / (count _objects));
        _offset = _object worldToModel _centerPoint;
    };

    [_object, _offset, count _objects > 1] call EFUNC(toolGizmo,updateGizmo);
}] call CFUNC(addEventHandler);

// -- Update gizmo when rotation offsets are changed
["object.setting_changed", {
    // if !(GVAR(modeActive)) exitWith { };

    (_this select 0) params ["_setting", "_value"];
    if (({ _setting == _x } count ["rotationOffsetX", "rotationOffsetY", "rotationOffsetZ"]) > 0) then {

        private _objects = call MFUNC(getSelectedObjects);
        if (_objects isEqualTo []) exitWith { };
        if (count _objects > 1) exitWith { };

        private _object = [_objects] call CFUNC(arrayPeek);
        if (isNull _object) exitWith { };

        
        private _data = [[_object] call MFUNC(getTemplateName)] call FUNC(getClassData);
        _data params (call FUNC(getDataStructure));

        [_object, [_rotationOffsetX, _rotationOffsetY, _rotationOffsetZ]] call EFUNC(toolGizmo,updateGizmo);
    };
}] call CFUNC(addEventHandler);


//    Description:    This handles the xcam mod mode of doing things, which is that you have a preview state
//                    In the preview state changing object class will change your object, you need some confirm button actually place things
["object.mainclass.changed", {
    if !(GVAR(modeActive)) exitWith { };
    if !(MVAR(set_live_mode)) exitWith { };

    (_this select 0) params ["_newClass"];
    _newClass params ["_templateName", "_modelPath"];

    private _selected = call MFUNC(getSelectedObjects);
    if (_selected isEqualTo []) exitWith { };

    private _last = [_selected] call CFUNC(arrayPeek);
    [_templateName, _modelPath, [_last]] call FUNC(replaceSelected);

}] call CFUNC(addEventHandler);
