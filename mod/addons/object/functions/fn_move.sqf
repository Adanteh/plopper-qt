/*
    Function:       ADA_Object_fnc_move
    Author:         Adanteh
    Description:    Moves an object
*/
#include "macros.hpp"

params ["", ["_selection", []], "_offset", "_axis"];

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };

// -- Move based on the world position change for the cursor
private _previousPos = GVAR(namespace) getVariable ["lastPosMove", screenToWorld getMousePosition];
private _currentPos = screenToWorld getMousePosition;
if (isNil "_offset") then { // -- Given param when using mouse scroll
    _offset = _currentPos vectorDiff _previousPos;
};

if (__AXISRESTRAINT("precisionMode")) then {
    _offset = _offset vectorMultiply (1/10);
};

GVAR(namespace) setVariable ["lastPosMove", _currentPos];

private _followTerrain = MVAR(set_followTerrain);
private _objectsToManipulate = _selection select { [_x] call MFUNC(canEditObject) };

// -- If one of the axis buttons is pressed, only move along the pressd axis, if none are pressed, to freeform movement
if (isNil "_axis") then {
    private _xMove = __AXISRESTRAINT("axis.move.x");
    private _yMove = __AXISRESTRAINT("axis.move.y");
    private _zMove = __AXISRESTRAINT("axis.move.z");
    _axis = [_xMove, _yMove, _zMove];
};

_axis params [["_xMove", false], ["_yMove", false], ["_zMove", false]];
private _multiSelect = count _objectsToManipulate > 1;
private _restrainAxis = !([_xMove, _yMove, _zMove] isEqualTo [false, false, false]);

if (_restrainAxis) then {
    if (_multiSelect) then {
        private _direction = getDir (_selection select 0);
        if !(_xMove) then { _offset set [0, 0]; };
        if !(_yMove) then { _offset set [1, 0]; };
        if !(_zMove) then {
            _offset set [2, 0];
        } else {
            _followTerrain = false;
        };
        _offset = [_offset, _direction + 180] call CFUNC(rotateAroundPoint);
    };
};

// -- Move object
{
    private _oldPos = (getPosWorld _x);
    private "_newPos";
    // -- Multiple objects will be moved on world axis, single objects on local axis
    if (!_multiSelect && _restrainAxis) then {
        private _modelOffset = +_offset;
        if !(_xMove) then { _modelOffset set [0, 0]; };
        if !(_yMove) then { _modelOffset set [1, 0]; };
        if !(_zMove) then { _modelOffset set [2, 0]; };

        _newPos = _x modelToWorldWorld (_modelOffset apply { _x * -1 });
    } else {
        _newPos = (_oldPos vectorAdd _offset);
    };

    if (_followTerrain) then {
        private _heightDifference = (getTerrainHeightASL _newPos) - (getTerrainHeightASL _oldPos);
        _newPos set [2, (_oldPos select 2) + _heightDifference];
    };

    _x setPosWorld _newPos;
} forEach _objectsToManipulate;
