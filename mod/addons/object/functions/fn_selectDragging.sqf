/*
    Function:       ADA_Object_fnc_selectDragging
    Author:         Adanteh
    Description:    Does a dragging selection box
*/
#define __INCLUDE_DIK
#include "macros.hpp"
#define __HEIGHTADD 0.5

params [["_end", false]];

// -- Use AGL positions~
private _startPos = +(MVAR(namespaceMouse) getVariable ["mouse.0.DownPosWorld", [0, 0, 0]]);
private _currentPos = +(MVAR(namespaceMouse) getVariable ["dragPosWorld", [0, 0, 0]]);

// TODO: Allow selection in camera direction, instead of always north
private _cameraDirection = direction (call MFUNC(getCamObject));

// -- End of the dragging action. Get all objects that area in the box created
if (_end) exitWith {
    private _inArea = [[_startPos, _currentPos], call MFUNC(getObjectsPlaced),  direction (call MFUNC(getCamObject))] call CFUNC(getObjectsInArea);
    private _removeFromSelection = ["Deselect"] call MFUNC(modifierPressed);
    if (_removeFromSelection) then {
        private _selection = call MFUNC(getSelectedObjects);
        [_selection - _inArea, false] call MFUNC(selectObjects);
    } else {
        private _addToSelection = ["Selecting"] call MFUNC(modifierPressed);
        [_inArea, _addToSelection] call MFUNC(selectObjects);
    };
    GVAR(namespace) setVariable ["selectionBox", []];
};

// -- Add some height, so it becomes easier to see the selection box
_startPos set [2, (_startPos select 2) + __HEIGHTADD]; // -- Red
_currentPos set [2, (_startPos select 2) + __HEIGHTADD]; // -- Cyan

private _posStart = [_startPos, -_cameraDirection] call CFUNC(rotateAroundPoint);
private _posCurrent = [_currentPos, -_cameraDirection] call CFUNC(rotateAroundPoint);
(_posCurrent vectorDiff _posStart) params ["_xDimension", "_yDimension"];
private _corner2 = _startPos vectorAdd ([[_xDimension, 0, __HEIGHTADD], _cameraDirection] call CFUNC(rotateAroundPoint));
private _corner4 = _startPos vectorAdd ([[0, _yDimension, __HEIGHTADD], _cameraDirection] call CFUNC(rotateAroundPoint));

private _rectangle = [_startPos, _corner2, _currentPos, _corner4];
GVAR(namespace) setVariable ["selectionBox", _rectangle];
