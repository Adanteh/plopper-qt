/*
    Function:       ADA_Object_fnc_copyObject
    Author:         Adanteh
    Description:    Does a quick copy on object, from a keypress
*/
#include "macros.hpp"

// -- Use the selection to copy from, if it's empty then use last created object. If there is no last object, do nothing
params [["_selection", []]];
if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = get3DENselected "object"
    };
};


// -- Select first object in selection
private _copyObject = _selection param [0, objNull];
if (isNull _copyObject) exitWith {
    [LOCALIZE("NO_OBJECT_COPY"), "orange", 5, -1] call CFUNC(debugMessage);
    nil
};

// -- Classless object, use modelInfo instead
private _class = typeOf _copyObject;
if (_class == "") then {
    _class = (getModelInfo _copyObject) param [1, ""];
};

if (_class == "") exitWith {
    [LOCALIZE("CANT_COPY_THIS_OBJECT"), "red", 5, -1] call CFUNC(debugMessage);
};

//[format ["%1: %2", _fnc_scriptNameShort, _class], "magenta", 5, __DEBUGTOOL] call CFUNC(debugMessage);
private _copyDirection = GVAR(namespace) getVariable ["copy.direction", 0];

// -- Gets the size of the object in the direction we are copying, then offset the position with that
(boundingBoxReal _copyObject) params ["_size1", "_size2"];
private _maxWidth = abs ((_size2 select 0) - (_size1 select 0));
private _maxLength = abs ((_size2 select 1) - (_size1 select 1));
private _maxHeight = abs ((_size2 select 2) - (_size1 select 2));

private _classData = [([_copyObject] call MFUNC(getTemplateName))] call FUNC(getClassData);
private _keys = [] call FUNC(getDataStructure);
_classData params _keys;

_offsetX = RANDOMOFFSET(_offsetX,_offsetXRandom);
_offsetY = RANDOMOFFSET(_offsetY,_offsetYRandom);
_offsetZ = RANDOMOFFSET(_offsetZ,_offsetZRandom);

private _currentDir = getDir _copyObject;
private _currentPos = getPosASL _copyObject;
private _position = call ([
    {[_currentPos, [_maxWidth + _offsetX, 0, 0], _currentDir] call CFUNC(calcRelativePosition)}, // West
    {[_currentPos, [0, _maxLength + _offsetY, 0], _currentDir] call CFUNC(calcRelativePosition)}, // North
    {[_currentPos, [-_maxWidth - _offsetX, 0, 0], _currentDir] call CFUNC(calcRelativePosition)}, // East
    {[_currentPos, [0, -_maxLength - _offsetY, 0], _currentDir] call CFUNC(calcRelativePosition)}, // South
    {(getPosWorld _copyObject) vectorAdd [0, 0, _maxHeight + _offsetZ]}, // Above
    {(getPosWorld _copyObject) vectorAdd [0, 0, -_maxHeight - _offsetZ]} // Below
] select _copyDirection);

//[[_fnc_scriptNameShort, _position, _copyDirection], "red"] call CFUNC(debugMessage);

private _direction = RANDOMOFFSET(_currentDir,_dirRandom);
private _copyHeight = (getPosATL _copyObject) select 2;

// -- Add height offset and remove boundingcenter height (modelToWorld returns posWorld)
if !(MVAR(set_followTerrain)) then {
    //_position set [2, _copyHeight];
} else {
    _position set [2, (getTerrainHeightASL _position) + _copyHeight + _offsetZ];
};

// -- If no randomized scale, copy from object
private _scale = if (_scaleMin == 1 && _scaleMax == 1) then {
    _copyObject getVariable [QMVAR(Scale), 1];
} else {
    _scaleMin + random (_scaleMax - _scaleMin);
};

private _object = [_class, _position, _direction, _scale, !MVAR(set_slopeAlign)] call MFUNC(createObject);
["object.copied", [_object, _copyObject]] call CFUNC(localEvent);
