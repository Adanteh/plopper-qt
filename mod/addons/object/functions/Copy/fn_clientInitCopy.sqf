/*
    Function:       ADA_Object_fnc_clientInitCopy
    Author:         Adanteh
    Description:    Inits some copying options
*/
#include "macros.hpp"

[{
    // -- Draws the copy direction for an object
    private _direction = +(GVAR(namespace) getVariable ["copy.directionLine", []]);
    if !(_direction isEqualTo []) then {
        _direction params ["_object", "_centerPoint", "_color"];
        private _copyDirection = GVAR(namespace) getVariable ["copy.direction", 0];

        // -- Get position pointing into direction of copy (3 points to form an arrow)
        private _offset = [
            [[-2, 0, 0], [-1.5, -0.5, 0], [-1.5, 0.5, 0]],
            [[0, 2, 0], [0.5, 1.5, 0], [-0.5, 1.5, 0]],
            [[2, 0, 0], [1.5, -0.5, 0], [1.5, 0.5, 0]],
            [[0, -2, 0], [0.5, -1.5, 0], [-0.5, -1.5, 0]],
            [[0, 0, 2], [0.5, 0, 1.5], [-0.5, 0, 1.5]],
            [[0, 0, -2], [0.5, 0, -1.5], [-0.5, 0, -1.5]]
        ] select _copyDirection;

        private _endPoint = _object modelToWorld (_centerPoint vectorAdd (_offset select 0));
        private _endPointLeft = _object modelToWorld (_centerPoint vectorAdd (_offset select 1));
        private _endPointRight = _object modelToWorld (_centerPoint vectorAdd (_offset select 2));

        // -- Draw arrow pointing from the middle
        drawLine3D [_object modelToWorld _centerPoint, _endPoint, _color];
        drawLine3D [_endPointLeft, _endPoint, _color];
        drawLine3D [_endPointRight, _endPoint, _color];
        drawLine3D [_endPointLeft, _endPointRight, _color];
    };
}, 0] call CFUNC(addPerFrameHandler);


// -- Update the copy direction indicator when selection changes
["selection.changed", { _this call FUNC(drawCopyDirection) }] call CFUNC(addEventHandler);

// -- Loads in the copy settings for this object
["selection.changed", { _this call FUNC(loadCopySettings) }] call CFUNC(addEventHandler);
