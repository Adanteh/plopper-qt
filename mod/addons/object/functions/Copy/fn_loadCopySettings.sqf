/*
    Function:       ADA_Object_fnc_copySettings
    Author:         Adanteh
    Description:    Handles copy settings, whether to follow terrain, copy offset and things like that
*/
#include "macros.hpp"

(_this select 0) params ["_newSelection"];

private _object = _newSelection param [0, objNull];
if (isNull _object) exitWith {
    GVAR(namespace) setVariable ["copy.currentObject", ""];
};

private _objectType = (getModelInfo _object) select 1;

// -- If object type has changed, load in the copy settings for this object type
private _previousData = GVAR(namespace) getVariable ["copy.currentObject", ""];
if (_objectType != "") then {
    GVAR(namespace) setVariable ["copy.currentObject", _objectType];

    private _directionRandomization = 0;
    GVAR(namespace) setVariable ["copy.dirRandom", _directionRandomization];

    private _xOffset = 0;
    GVAR(namespace) setVariable ["copy.xOffset", _xOffset];

    private _yOffset = 0;
    GVAR(namespace) setVariable ["copy.yOffset", _yOffset];

    private _zOffset = 0;
    GVAR(namespace) setVariable ["copy.zOffset", _zOffset];

};
