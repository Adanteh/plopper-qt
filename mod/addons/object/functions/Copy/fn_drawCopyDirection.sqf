/*
    Function:       ADA_Object_fnc_drawCopyDirection
    Author:         Adanteh
    Description:    Prepares copy position for a changed selection
*/
#include "macros.hpp"

(_this select 0) params ["_newSelection"];

private _object = _newSelection param [0, objNull];
if (isNull _object) exitWith {
    GVAR(namespace) setVariable ["copy.directionLine", []];
};


private _boundingBox = boundingBoxReal _object;
private _centerPoint = (boundingCenter _object);
_centerPoint set [2, ((_boundingBox select 1) select 2) + 0.5];
private _color = [1, 0, 0, 1];
GVAR(namespace) setVariable ["copy.directionLine", [_object, _centerPoint, _color]];
