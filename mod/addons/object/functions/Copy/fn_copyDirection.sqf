/*
    Function:       ADA_Object_fnc_copyDirection
    Author:         Adanteh
    Description:    Changes the copy direction
*/
#include "macros.hpp"

private _direction = GVAR(namespace) getVariable ["copy.direction", 0];
private _newDir = _direction + 1;
if (_newDir > 5) then { _newDir = 0 };
GVAR(namespace) setVariable ["copy.direction", _newDir];

[format ["%1: %2", _fnc_scriptName, _newDir], "magenta", 5, __DEBUGTOOL] call CFUNC(debugMessage);

false;
// [0, left]
// [1, north]
// [2, right]
// [3, south]
// [4, above]
// [5, below]
