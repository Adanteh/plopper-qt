/*
    Function:       ADA_Object_fnc_replaceSelected
    Author:         Adanteh
    Description:    Replaces given objects or selection by given class
*/
#include "macros.hpp"

params ["_templateName", "_modelPath", ["_selection", []]];

if (_selection isEqualTo []) then {
    _selection = call MFUNC(getSelectedObjects);
    if (_selection isEqualTo []) then {
        _selection = [objNull];
    };
};

if (_selection isEqualTo [objNull]) exitWith { };

private _selection = _selection select { [_x] call MFUNC(canEditObject) };
private _objectsCreated = [];
{
    private _newPos = getPosWorld _x;
    private _objectLayer = _x getVariable [QMVAR(ObjectLayer), ""];
    private _direction = getDir _x;
    _newPos set [2, (_newPos select 2) - (boundingCenter _x select 2)];

    private _testObject = createSimpleObject [_modelPath, [10, 10, 1000]];
    if (isNull _testObject) then {
        ["debugMessage", [format ["%1:  %2", LOCALIZE("COULDNT_CREATE"), _modelPath], "red", 10, -1]] call CFUNC(localEvent);
    } else {
        deleteVehicle _testObject;
        _objectsCreated pushBack ([
            _modelPath,
            _newPos,
            _direction,
            _x getVariable [QMVAR(Scale), 1],
            false,
            _x getVariable [QMVAR(PitchBankYaw), [0, 0, _direction]],
            false,
            [QMVAR(ObjectLayer), _objectLayer]
        ] call MFUNC(createObject));
        ["object.predelete", [_x]] call CFUNC(globalEvent);
    };

} forEach _selection;

if !(_objectsCreated isEqualTo []) then {
    [_objectsCreated] call MFUNC(selectObjects);
};
