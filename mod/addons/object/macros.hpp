#include "module.hpp"
#include "..\core\macros.hpp"
#ifdef __INCLUDE_IDCS
    #include "..\core\idcs.hpp"
#endif

#define __LOCALMOVERTYPE "Sign_Sphere10cm_F"
#define __DEBUGEH 800
#define __DEBUGTOOL 300
#define __DEBUGSELECT 400
