/*
    Function:       ADA_Surface_fnc_getAllPixels
    Author:         Adanteh
    Description:    Gets all current pixels
*/
#include "macros.hpp"

private _pixels = allVariables GVAR(namespacePixels);
private _objects = [];
{
    private _pixel = GVAR(namespacePixels) getVariable _x;
    if (!(isNil "_pixel") && { !isNull _pixel }) then {
        _objects pushBack _pixel;
    };
} forEach _pixels;
_objects;
