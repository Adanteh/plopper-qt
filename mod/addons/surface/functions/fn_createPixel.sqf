/*
    Function:       ADA_Surface_fnc_createPixel
    Author:         zgmrvn
    Description:    Creates surfaces pixel. Animates to proper terrain size
*/
#include "macros.hpp"

params [["_position", [0, 0, 0]]];

private _positionPixel = _position;

_positionPixel resize 2;
_positionPixel = _positionPixel apply {
    (_x - (_x mod MVAR(set_surface_grid_size))) + (MVAR(set_surface_grid_size) * 0.5)
};
_positionPixel pushBack MVAR(set_surface_zMargin);

_pixel = createSimpleObject [__PIXELNAME, _positionPixel];
GVAR(queue) pushBack [_pixel, _positionPixel];

// [[_fnc_scriptNameShort, _positionPixel, GVAR(surfaceTexture), GVAR(surfaceName)], "green"] call CFUNC(debugMessage);


_pixel
