/*
    Function:       ADA_Surface_fnc_brushSurface
    Author:         Adanteh
    Description:    Fills position with size of circle in surface types
*/
#define __INCLUDE_DIK
#include "macros.hpp"

params [["_position", [0, 0, 0]], ["_detection", false]];

private _circleSize = call EFUNC(toolCircle,getCircleSize);
private _steps = (floor (_circleSize / MVAR(set_surface_grid_size))) max 0.5;
private _area = [_position, _circleSize, _circleSize, 0, false, -1];

// [[_fnc_scriptNameShort, _circleSize], "blue"] call CFUNC(debugMessage);


// -- Get the different code only one, so we don't need to check it on every step in our circle
private "_code";
private _deleting = ["Deselect"] call MFUNC(modifierPressed);
if (_deleting) then {
    _code = {
        deleteVehicle _pixel;
    };
} else {
    // -- If holding Lconrol paint in current surface
    if !(_detection) then {
        _detection = ["Selecting"] call MFUNC(modifierPressed);
    };
    _code = {
        if (_detection) then {
            // NOT IMPLEMENTED IN PLOPPER VERSION
            // _surface = (surfaceType _placePos);
            // _surface = _surface select [1, (count _surface - 1)]; // -- Remove leading #-sign in front of this command's return
            // private _data = [_surface] call FUNC(getSurfaceColor);
            // _color = _data select 0;
            // _colorTex = _data select 1;
        };

        if (isNull _pixel) then {
            _pixel = [_placePos] call FUNC(createPixel);
            GVAR(namespacePixels) setVariable [str _gridPos, _pixel];
        };
        _pixel setObjectTexture [0, GVAR(surfaceTexture)];
        _pixel setVariable [QGVAR(surface), GVAR(surfaceName)];
        _pixel setVariable [QGVAR(grid), _gridPos];
    };
};


// -- Go throguh all positions within brush area
// -- The actual position we want to test is in the middle of a pixel, so shift by 0.5
for "_gridX" from (0.5 - _steps) to (_steps + 0.5) do {
    for "_gridY" from (0.5 - _steps) to (_steps + 0.5) do {
        private _placePos = _position vectorAdd [_gridX * MVAR(set_surface_grid_size), _gridY * MVAR(set_surface_grid_size), 0];
        if (_placePos inArea _area) then {
            _placePos = _placePos apply {
                (_x - (_x mod MVAR(set_surface_grid_size))) + (MVAR(set_surface_grid_size) * 0.5)
            };
            private _gridPos = [_placePos] call FUNC(getGridPos);
            private _pixel = GVAR(namespacePixels) getVariable [str _gridPos, objNull];
            call _code;
        };
    };
};
