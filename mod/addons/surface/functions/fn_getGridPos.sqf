/*
    Function:       ADA_Surface_fnc_getGridPos
    Author:         Adanteh
    Description:    Gets index coodinates of position according to gridSize
*/
#include "macros.hpp"

params [["_position", [0, 0]]];
private _posGrid = _position apply { floor (_x / MVAR(set_surface_grid_size)) };

_posGrid resize 2;
_posGrid;
