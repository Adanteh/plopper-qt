/*
    Function:       ADA_Surface_fnc_export
    Author:         Adanteh
    Description:    Exports all pixel data to Pythia and create a nice picture
*/
#include "macros.hpp"

private _resolution = worldSize / MVAR(set_surface_grid_size);
private _exportData = [_resolution];
private _pixels = call FUNC(getAllPixels);
{
    private _pixel = _x;
    private _surface = _pixel getVariable [QGVAR(surface), ""];
    private _grid = _pixel getVariable [QGVAR(grid), []];
    call {
        if (_surface isEqualTo []) exitWith { };
        if (_grid select 0 < 0 || {_grid select 0 > (_resolution - 1)}) exitWith { };
        if (_grid select 1 < 0 || {_grid select 1 > (_resolution - 1)}) exitWith { };
        _exportData pushBack [_grid select 0, _grid select 1, _surface];
    };
} forEach _pixels;

private _success = ["plopper.plop", ["tools.surface.export", _exportData]] call py3_fnc_callExtension;
