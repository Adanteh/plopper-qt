/*
    Function:       ADA_Surface_fnc_mouseDragStart
    Author:         Adanteh
    Description:    Event for mouse dragging start (When button is hold) for Surface mode
*/
#include "macros.hpp"

params ["_button", "_startPos", "_startPosWorld"];
if (_button != 0) exitWith { };

GVAR(namespace) setVariable ["lastPosMove", _startPosWorld];
GVAR(namespace) setVariable ["drag.startPosWorld", _startPosWorld];

private _data = ["plopper.plop", ["tools.surface.get_settings"]] call PY3_fnc_callExtension;
GVAR(surfaceName) = _data deleteAt 0;
GVAR(surfaceTexture) = _data deleteAt 0;

if (GVAR(surfaceTexture) isEqualTo "") exitWith {
    [LOCALIZE("NO_SURFACE_SELECTED"), "red", 5, -1] call CFUNC(debugMessage);
};

// -- Start loop
private _handle = GVAR(namespace) getVariable ["mouseDraggingPFH", -1];
if (_handle != -1) then { [_handle] call CFUNC(removePerFrameHandler) };
private _handle = [{ call FUNC(brushAction) }, 0] call CFUNC(addPerFrameHandler);
GVAR(namespace) setVariable ["mouseDraggingPFH", _handle];
