/*
    Function:       ADA_Surface_fnc_toolDeactivate
    Author:         Adanteh
    Description:    Run when tool is Deselected
*/
#include "macros.hpp"

private _pixels = GVAR(namespace) getVariable ["pixels", []];
{
    deleteVehicle _x;
} forEach _pixels;
GVAR(namespace) setVariable ["pixels", []];


// -- Hide all pixels
{
    _x hideObject true;
} forEach (call FUNC(getAllPixels));

[false] call FUNC(showGrid);
