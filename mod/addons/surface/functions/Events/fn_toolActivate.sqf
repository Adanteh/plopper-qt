/*
    Function:       ADA_Surface_fnc_toolActivate
    Author:         Adanteh
    Description:    Run when tool is selected
*/
#include "macros.hpp"

// -- Show all pixels
{
    _x hideObject false;
} forEach (call FUNC(getAllPixels));
