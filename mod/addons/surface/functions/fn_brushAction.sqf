/*
    Function:       ADA_Surface_fnc_brushAction
    Author:         Adanteh
    Description:    A Function That Does stuff
*/
#include "macros.hpp"

private _currentPos = MVAR(namespaceMouse) getVariable ["dragPosWorld", []];
private _lastPos = GVAR(namespace) getVariable ["dragPosLast", []];

 // -- Don't place when mouse not moving. Maybe make this a setting
if (_lastPos isEqualTo _currentPos) exitWith { };

[_currentPos] call FUNC(brushSurface);

GVAR(namespace) setVariable ["lastUsedAt", diag_tickTime];
GVAR(namespace) setVariable ["dragPosLast", _currentPos];
