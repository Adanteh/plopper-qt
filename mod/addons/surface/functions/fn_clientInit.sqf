/*
    Function:       ADA_Surface_fnc_clientInit
    Author:         Adanteh
    Description:    Client init Surface mode
*/
#define __INCLUDE_DIK
#include "macros.hpp"

GVAR(namespace) = false call CFUNC(createNamespace);
GVAR(namespacePixels) = false call CFUNC(createNamespace);

["surface.export", {
    ["export"] call FUNC(export);
}] call CFUNC(addEventHandler);

["surface.clear", {
    private _pixels = call FUNC(getAllPixels);
    {
        deleteVehicle _x;
    } forEach _pixels;
}] call CFUNC(addEventHandler);

// -- This will handle creating and animating 10 pixels per x Time
GVAR(queue) = [];
#define PIXEL_BORDER_PERCENT	1
[{
    private _max = count GVAR(queue);
    if (_max == 0) exitWith { };

    private _mapGridSize = MVAR(set_surface_grid_size);
    private _queue = [999, 9] select MVAR(set_surface_cachedpainting);
    for "_i" from 0 to (_max min _queue) do {
        (GVAR(queue) deleteAt 0) params ["_pixel", "_positionPixel"];
        _pixel setPosATL _positionPixel;
        private _border	 = _mapGridSize / 100 * PIXEL_BORDER_PERCENT;
        {
        	_pixel animate [_x, (_mapGridSize - 1) / 2 - _border, true];
        } forEach [
        	"fl_x",
        	"fr_x",
        	"fl_y",
        	"rl_y"
        ];

        // z translations
        {
            _x params ["_anim", "_xy"];
        	private _cornerElevation = _pixel modelToWorldVisualWorld (_xy vectorMultiply _mapGridSize / 2 - _border);
        	_cornerElevation set [2, 0];
        	_cornerElevation = ATLToASL _cornerElevation;
        	private _difference = ((ATLToASL _positionPixel) select 2) - (_cornerElevation select 2);
        	_pixel animate [_anim, _difference + MVAR(set_surface_zMargin), true];
        } forEach [
        	["fl_z", [1, 1, 0]],
        	["fr_z", [-1, 1, 0]],
        	["rl_z", [1, -1, 0]],
        	["rr_z", [-1, -1, 0]]
        ];
    };

}, 0] call CFUNC(addPerFrameHandler);
