#define __INCLUDE_IDCS
#include "macros.hpp"

class CfgPatches {
    class ADDON {
        author = AUTHOR;
        name = ADDONNAME;
        units[] = {};
        weapons[] = {};
        version = VERSION;
        requiredaddons[] = {QSVAR(Main)};
    };
};

class ObjectPlacement {
    class Modes {
        class Default;
        class Surface: Default {
            tools[] = {QSVAR(toolCircle)};
            class Events {
                class OnActivate { function = QFUNC(toolActivate); };
                class OnDeactivate { function = QFUNC(toolDeactivate); };
                class OnMouseZChanged { function = QFUNC(mouseZChanged); };
                class mouseDragEnd { function = QFUNC(mouseDragEnd); };
                class mouseDragStart { function = QFUNC(mouseDragStart); };
            };
        };
    };
};

class PREFIX {
    class Modules {
        class MODULE {
            defaultLoad = 1;
            dependencies[] = {"Core", "Main", "toolCircle"};

            class brushAction;
            class brushSurface;
            class clientInit;
            class createPixel;
            class export;
            class getAllPixels;
            class getGridPos;

            class Events {
                class mouseDragEnd;
                class mouseDragStart;
                class toolActivate;
                class toolDeactivate;
            };
        };
    };
};

#include "cfgVehicles.hpp"
