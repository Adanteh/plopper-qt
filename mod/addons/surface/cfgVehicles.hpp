class CfgVehicles {
	class HeliH;
	class Land_ADA_SurfacePixel: HeliH {
		scope = 2;
		_generalMacro = __PIXELNAME;
		displayName = "Surface Mask Pixel";
		model = TEXTURE(pixel.p3d);
		hiddenSelections[] = {"camo"};
        featureType = 2; // -- Show on max range
	};
	class Land_ADA_SurfaceLine: HeliH {
		scope = 2;
		_generalMacro = "Land_ADA_SurfaceLine";
		displayName = "Surface Mask Line";
		model = TEXTURE(ada_line.p3d);
		hiddenSelections[] = {"camo"};
        featureType = 2; // -- Show on max range
	};
};
