# Manual
This is a somewhat basic manual, seeing I could write a 500 page one but no one will read it.
This mod assumes you are willing invest time into making an terrain and have some clue about things. 
Things might seem complicated to 3DEN, but serious terrain makers will know that the tools in this make sense for serious terrain production. 
If you expect to just randomly get good results when clicking random buttons, you will be disappointed.


## For starters
* Run the mod WITHOUT battleeye, and with the -noPause and -window startup parameters

## General
Some general terms that will be used:
- Menu. The top bar with Project, Settings, etc
- Panel. One of the moveable panes that can be opened/closed and attached to the sidebar
- Mode. The mod that is selected on the left bar, for example Brush, Surface or Object mode
- Tool. Used by a mode, for example Brush uses the circle tool.
- Toggle. One of the icons on the grey horizontal bar. Toggles a certain setting
- Toolbar. The bar containing the toggles
  
Saving/loading files for each mode or panel is largely the same. **Saving only happens when you press the save button!**
You can either select an existing file in the dropdown, and press save, or select `<New File>` to create a new file.
Selecting an entry in the dropdown and pressing Load will load a file

### Panels
In this mod things like Projects, Library view, favorites and so can be opened and closed separately.
Most of them can be resized and docked either at the Right, bottom or kept floating. Resizing is also possible,
but you should probably use one of the Resolution options in Settings menu after doing this. They can also be turned off
when right clicking any of the panels or toolbar and selecting the panel you want to hide.

#### Library
Probably the most important one. This will load in any .tml file (Terrain Builder template files from TemplateLibs folder)
If you want to place objects that aren't available in the list, add them as Template Builder and copy the .tml file from the TemplateLibs folder in
the `Exports\Library` folder. Clicking any item in the list will select that class.
**I highly recommend using these same template files in Terrain Builder, so you don't get missing template when importing**

#### Projects
Under the menubar. When you Save a project it will saved **all** placed objects to a file.
Loading will load all objects into your current running mission.  Plopper doesn't have sub projects
Use Export for importing into terrain builder. **Absolute height should be selected when importing in TB**

Ctrl+S (When the Plopper UI is active) will save to current project 

#### Objects
List of objects in the current project. Clicking an entry in the list will select the object. Double clicking will pan the camera to it.
You can right click entries for some extra options in context menu (Like Selecting all of the same class in your current view)

#### Favorites
Easy access saved classes. When you work on a terrain project I first recommend to figure out some objects you like and then save them here.
Pressing Add will add the current selected class (Either from library or selected object class). You can also add the classes of selected objects,
I recommend using the Brush mode for that and making sure Terrain Object in mode setting is turned on, and using Shift+Drag to select them

### Modes
These are your general modes, each one has a specific task. I won't go over all keybinds, I recommend taking a look under `Settings > Keybinds` for that


## Everything below this point still needs to be updated to changes made in Plopper

#### Object Mode
* Pressing *F* will set your library class to selected object
* Double click in 3D space will place an object of last selected class (Either object, library or favorites)
* To turn off alignment to terrain and keep objects horizontal, use the option in the togglebar
* To ignore height differences in terrain when moving / copying objects, use the option in the togglebar
* Hold one of the Axis buttons to limit movement to that axis only (Check keybinds!).
* Copying objects will clone the last selected object, to wherever the arrow is pointing.
* Offsets for copying can be controlled in the left sidebar. This will automatically save per class, for future usage.
* Selected objects can be replaced by double clicking in the library (Or favorites list)

#### Brush Mode
* Brushes are a collection of objects, that get randomly placed within the size of circle.
* All items in the left sidebar are used, you can add items one by one and change each entry.
* You can have multiple entries of the same class with different settings (For example 1 class with low scale and low probability and one entry with high scale and high probability)
* You can create a brush from selected entries, select objects by doing *Shift+LMB drag* and use the button next to the list
* Mouse wheel is used to change the circle size
* Brushes can be saved and loaded to `Exports\Projects\<projectname>\Brush`

### Surface Painting
* Open up a `layers.cfg` in the selected tool
* Set your resolution ahead of time. This is the amount of pixels per meter. Most uses is 1m/px
* You can do *LMB drag* to Brush with the selected color in the List
* You can do *Ctrl+LMB drag* to brush in the current surface (If the surface of terrain is available in selected layer set)
* Pressing Export in the left bar will allow you to save a .png
* I recommend turning background off and adding the .png to a Photoshop/gimp project to combine them all
* For performance reasons I recommend Exporting and then clearing quite often

#### Compositions
* *LMB doubleclick* to place
* Select terrain objects by doing *Shift+LMB Drag* and create Composition

#### Polyfill
* *LMB click* to add a point
* *LALT+LMB click* to remove a point
* *LALT+LMB double click* to remove all points
* Press generate to fill the polygon with objects

#### SPAL
This stuff is advanced and only meant for very specific usage, if you want a better tool to build walls with, please consider becoming a developer :)

* To use put a polyline down with left mouse click, objects will be placed following this line.
* Max Distance is the distance an object is allowed to be, from a point in the polyline before moving onto the next point (Generally speaking this is a little above half the spacing)
* Spacing is the distance between each object, if there's gaps, lower this, if there is too much overlap increase it.
* Dir Random is randomized direction for each object.
* Dir offset is direction offset to the direction of the line
* OffsetX and OffsetY is a rotation offset, to have walls connect properly when going in corners adjust these values, usually offsetX is half the spacing



### General Notes and Known Issues
This is a very experimental mod, that goes beyond what's "normal" in arma. Because of this bug may appear.
Some are known issues that can't simply can't be fixed (Or no way of fixing them has been discovered yet)

* When going into Video settings, the Arma window detaches. Use `Settings > Debug > Fix Embedding` for this
* Sometimes after restarting the mission, arma disappears. End the arma process manually in task manager
* Drag actions don't get detected when clicking between the Plopper UI and the camera view. Can't fix
