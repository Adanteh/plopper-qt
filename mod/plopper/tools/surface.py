from collections import OrderedDict
from pathlib import Path
from typing import List, Tuple, Dict
from dataclasses import dataclass

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QColor, QIcon, QImage, QPixmap
from PyQt5.QtWidgets import QFileDialog, QListWidget, QListWidgetItem, QPushButton

from plopper import Plop, PlopperSequence, MODFOLDER
from plopper.keybinds import add_keybind
from plopper.settings import PlopperSettings
from plopper.settings.props import BoolProperty, FloatProperty
from plopper.toolsettings import Checkbox, NumberEdit, ToolSettings
from plopper.toolsettings.fileselector import FileSelector

from plopper.arma import armaclass

from . import PlopperTool

# from https://gist.github.com/JokerMartini/c3a38069020480727e5e
# Adjusted version with support added for orderedDict
def renameKeysToLower(iterable):
    """Renames all key in orderdeddict recursively to lowercase"""
    newdict = type(iterable)()
    if type(iterable) in (dict, OrderedDict):
        for key in iterable.keys():
            newdict[key.lower()] = iterable[key]
            if type(iterable[key]) in (dict, list, OrderedDict):
                newdict[key.lower()] = renameKeysToLower(iterable[key])
    elif type(iterable) is list:
        for item in iterable:
            item = renameKeysToLower(item)
            newdict.append(item)
    else:
        return iterable
    return newdict

# Name, Procedural Texture
SurfaceType = tuple[str, str]

@dataclass
class SurfaceEntry(PlopperSequence):
    name: str = ""
    color: Tuple[int, int, int] = (255, 0, 0)

class SurfaceList(QListWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.surfaces: Dict[str, SurfaceEntry] = {}
        self.current: SurfaceType = ("", "")
        self.currentItemChanged.connect(self._selchanged)
        self.setMaximumHeight(600)

    def add_entry(self, entry: SurfaceEntry()):
        try:
            _map = QPixmap(30, 30)
            _map.fill(QColor(*entry.color))
            _icon = QIcon(_map)
            _item = QListWidgetItem(_icon, entry["name"], parent=self)
            _item.entry = entry
            self.surfaces[entry["name"]] = entry
        except TypeError:
            print(f"Failed to import surface: {entry}")

    def clear(self):
        super().clear()
        self.surfaces.clear()

    def get_current(self) -> SurfaceType:
        return self.current

    @pyqtSlot(QListWidgetItem, QListWidgetItem)
    def _selchanged(self, new, old):
        try:
            entry = new.entry
            self.current = (entry["name"], entry["color"])
        except (AttributeError, TypeError):
            pass


class SurfaceTool(PlopperTool):
    """
        Allows painting surfaces. Comparable to vertex, splatmaps, attribute mask or whatever you want to call them
        Should only be used for minor touchups, simply because each pixel will be an object.
        Allows exporting to BMP for further handling in image editors
    """

    ICON = "fa5s.palette"
    DIRECTORY = MODFOLDER / "exports"
    NAME = "surface"
    ALLOW_BULDOZER = True

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._class = SurfaceEntry
        self.settings = self._load_settings()
        self.add_preferences()

    def add_keybinds(self):
        add_keybind("4", self.activate, name=self.NAME)

    def add_preferences(self):
        """Adds options to general setting window"""
        zmargin = FloatProperty(
            widget=NumberEdit, name="Z Margin", tooltip="Height the pixels should be placed above terrain", default=0.2
        )
        cache = BoolProperty(
            widget=Checkbox,
            name="Cached painting",
            tooltip="This will cache pixel creation. Allows you to have higher FPS while painting, but will create a delay",
            default=False,
        )
        PlopperSettings().register_setting(self.NAME + "/zmargin", zmargin)
        PlopperSettings().register_setting(self.NAME + "/cachedpainting", cache)

    def _load_settings(self):
        settings = ToolSettings(tool=self)

        settings.add(
            FloatProperty(NumberEdit, "Resolution", "", 1, 0, None, 0.5, 10, 0.5, 2), name="grid_size", default=1
        )
        settings.add(
            FloatProperty(NumberEdit, "Tile Size", "", 512, 0, None, 16, 2048, 16, 0), name="tile_size", default=512
        )
        settings.add(
            FloatProperty(NumberEdit, "Tile Overlap", "", 64, 4, None, 16, 2048, 16, 0), name="tile_overlap", default=64
        )
        settings.add(Checkbox("Add background", settings), "add_background", default=False)

        setting = settings.add(FileSelector(extension=".cfg"), "fileselector")
        setting.file_load.connect(self.load_from_file)
        setting._layout.removeWidget(setting._save)
        setting._save.deleteLater()

        button = settings.add(QPushButton("Export"), "export", priority=1, persistent=False)
        button.clicked.connect(self._export)

        button = settings.add(QPushButton("Clear"), "clear", priority=2, persistent=False)
        button.clicked.connect(self._clear)
        self.list = SurfaceList(parent=settings)
        settings.add(self.list, "surface_list", persistent=False)
        return settings

    def surfaces(self) -> List[SurfaceEntry]:
        return list(self.list.surfaces.values())

    def add_entries(self, data: List[SurfaceEntry]):
        self.list.clear()
        for entry in data:
            self.list.add_entry(entry)

    def get_settings(self) -> SurfaceType:
        name, color = self.list.current
        if not name:
            return ("", "")
        else:
            return (name, self._texture_arma(color))

    @staticmethod
    def _texture_arma(color: Tuple[int, int, int]) -> str:
        """Creates arma procedural texture  from rgb color"""
        r, g, b = [round(x / 255, 2) for x in color]
        return f"#(rgb,8,8,3)color({r},{g},{b},0.75,co)"

    def _export(self):
        """Fills the polygon with objects"""
        exportpath = self._export_path()
        if exportpath:
            self.exportpath = exportpath
            Plop().cache_add(self.NAME + ".export", unique=True)

    def _clear(self):
        """Fills the polygon with objects"""
        Plop().cache_add(self.NAME + ".clear", unique=True)

    def create_image(self, resolution: int, pixels: list) -> QImage:
        image = QImage(resolution, resolution, QImage.Format_ARGB32)
        if self.settings["add_background"]:
            image.fill(QColor(0, 0, 0))
        else:
            image.fill(QColor(0, 0, 0, 0))

        for _x_coord, _y_coord, _surface in pixels:
            color = self.list.surfaces[_surface].color
            image.setPixelColor(_x_coord, resolution - 1 - _y_coord, QColor(*color))
        return image

    def export(self, data: list) -> bool:
        resolution = data.pop(0)
        image = self.create_image(resolution, data)
        success = image.save(self.exportpath)
        return success

    def _export_path(self) -> str:
        extension = ".png"
        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        folder = str(self.DIRECTORY / "mask")
        data = QFileDialog().getSaveFileName(
            parent=self,
            caption="Save file",
            directory=folder,
            filter="Bitmap (*.bmp);;PNG (*.png)",
            initialFilter="PNG (*.png)",
            options=options,
        )
        file = data[0]
        if file:
            if Path(file).suffix == "":
                file = str(Path(file).with_suffix(extension))
        return file

    def load_from_file(self, path: Path):
        """Loads layer mask"""

        with path.open(mode="r") as file:
            data = armaclass.parse(file.read(), keep_order=True)

        lowered_data = renameKeysToLower(data)
        colorsdict = lowered_data["legend"]["colors"]
        colors = []
        for surface in colorsdict:
            color = list(colorsdict[surface])
            if len(color) > 0:  # Only add surfaces with color
                if isinstance(color[0], list):  # Only add the primary color of surface if it's nested
                    color = color[0]
                try:
                    color = tuple(int(f) for f in color)
                    colors.append(SurfaceEntry(surface, color))
                except ValueError:
                    print(f"Surface {surface} has invalid color in file: {path}")

        self.add_entries(colors)
        PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)

    def reloadFile(self, *args, **kwargs):
        """Reloads our last saved/loaded file from this. Called on app startup"""
        last_file = PlopperSettings().get_(self.NAME + "/lastfile", default="", _type=str)
        if last_file:
            last_path = Path(last_file)
            if last_path.exists():
                self.load_from_file(last_path)
