import json
from dataclasses import dataclass

from plopper import Plop, PlopperSequence, MODFOLDER
from plopper.keybinds import add_keybind
from plopper.settings import FloatProperty
from plopper.toolsettings import NumberEdit, ToolSettings

from . import PlopperTool


@dataclass
class ObjectEntry(PlopperSequence):
    offsetX: float = 0
    offsetY: float = 0
    offsetZ: float = 0
    offsetXRandom: float = 0
    offsetYRandom: float = 0
    offsetZRandom: float = 0
    scaleMin: float = 1
    scaleMax: float = 1
    dirRandom: float = 0
    rotationOffsetX: float = 0
    rotationOffsetY: float = 0
    rotationOffsetZ: float = 0


class ObjectTool(PlopperTool):
    """
        Mode to clone objects with T keybind in a selected direction
        or create single objects by double clicking. Has settings to randomize
        most properties when creating new object
    """

    ICON = "fa5s.map-marker-alt"
    NAME = "object"
    ALLOW_BULDOZER = True

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.current = ""
        self.currentdata = ObjectEntry()
        self.settings = self._load_settings()
        self.__ignore = False

        Plop().class_changed.connect(self.load_new_class)

    def add_keybinds(self):
        add_keybind("1", self.activate, name=self.NAME)
        add_keybind(
            "t",
            "[] call ADA_Object_fnc_copyObject",
            name="Clone",
            category=self.NAME,
            condition="ada_object_modeActive",
        )
        add_keybind(
            "ctrl+t",
            "[] call ADA_Object_fnc_copyDirection",
            name="Clone Direction",
            category=self.NAME,
            condition="ada_object_modeActive",
        )

    def __getitem__(self, key):
        """Keys are used to set to the current entry"""
        return self.currentdata[key]

    def __setitem__(self, key, value):
        """Keys are used to set to the current entry"""
        self.currentdata[key] = value
        self.setting_changed(key, value)

    def _load_settings(self):
        settings = self.settings = ToolSettings(tool=self)

        # Brush entry + Attributes list
        self._add_setting(FloatProperty(NumberEdit, "X Offset", "", 0, None, None, None, None, 0.1, 3), name="offsetX")
        self._add_setting(FloatProperty(NumberEdit, "Y Offset", "", 0, None, None, None, None, 0.1, 3), name="offsetY")
        self._add_setting(FloatProperty(NumberEdit, "Z Offset", "", 0, None, None, None, None, 0.1, 3), name="offsetZ")
        self._add_setting(
            FloatProperty(NumberEdit, "X Random", "", 0, None, None, None, None, 0.1, 3), name="offsetXRandom"
        )
        self._add_setting(
            FloatProperty(NumberEdit, "Y Random", "", 0, None, None, None, None, 0.1, 3), name="offsetYRandom"
        )
        self._add_setting(
            FloatProperty(NumberEdit, "Z Random", "", 0, None, None, None, None, 0.1, 3), name="offsetZRandom"
        )
        self._add_setting(FloatProperty(NumberEdit, "Min scale", "", 0, 0, None, 0, 2, 0.1, 3), name="scaleMin")
        self._add_setting(FloatProperty(NumberEdit, "Max scale", "", 0, 0, None, 0, 2, 0.1, 3), name="scaleMax")
        self._add_setting(FloatProperty(NumberEdit, "Dir random", "", 0, 0, 360, 0, 360, 1, 3), name="dirRandom")
        self._add_setting(
            FloatProperty(NumberEdit, "X Rotation offset", "", 0, None, None, None, None, 0.05, 3),
            name="rotationOffsetX",
        )
        self._add_setting(
            FloatProperty(NumberEdit, "Y Rotation offset", "", 0, None, None, None, None, 0.05, 3),
            name="rotationOffsetY",
        )
        self._add_setting(
            FloatProperty(NumberEdit, "Z Rotation offset", "", 0, None, None, None, None, 0.05, 3),
            name="rotationOffsetZ",
        )
        return settings

    def _add_setting(self, item, name):
        default = self[name]
        self.settings.add(item, name=name, default=default, persistent=False, parent=self)

    def update_attributes(self):
        """Updates the UI attribute view when another class is selected"""
        for key, value in self.currentdata:
            self.settings.set_value(self.settings.get_setting(key), value)
            self.settings[key] = value

    def load_data(self, template):
        filepath = MODFOLDER / "exports" / "classes" / template
        filepath = filepath.with_suffix(".json")
        if filepath.exists():
            with filepath.open() as file:
                data = json.load(file)
        else:
            data = []

        return ObjectEntry(*data)

    def save_current(self):
        if not self.current:
            return
        filepath = MODFOLDER / "exports" / "classes"
        filepath.mkdir(parents=True, exist_ok=True)
        filepath = filepath.joinpath(self.current).with_suffix(".json")
        with filepath.open(mode="w") as file:
            data = json.dump(self.currentdata.values(), file)

    def setting_changed(self, setting, value):
        if not self.__ignore:
            Plop().cache_add(self.NAME + ".setting_changed", [setting, value])
            self.save_current()

    def get_settings(self):
        return self.settings

    def load_new_class(self, new: dict):
        template = new["Name"]
        self.get_data(template)

    def get_data(self, template):
        if template is not self.current:
            self.save_current()
            self.current = template
            self.currentdata = self.load_data(template)

            self.__ignore = True  # Ignore the setting_changed events
            self.update_attributes()
            self.__ignore = False

        data = self.currentdata.values()
        return data
