import json
from pathlib import Path
from dataclasses import dataclass

import qtawesome as qta
from PyQt5.QtWidgets import QPushButton

from plopper import Plop, PlopperSequence
from plopper.keybinds import add_keybind
from plopper.settings.props import FloatProperty
from plopper.toolsettings import ModelAttributes, NumberEdit, Toolbox, ToolSettings
from plopper.toolsettings.fileselector import FileSelector

from .brush import BrushTool
from plopper.settings import PlopperSettings


@dataclass
class SpalEntry(PlopperSequence):
    PLACEMENT_GLOBAL = 0
    PLACEMENT_HORIZONTAL = 1
    PLACEMENT_SLOPE = 2

    name: str
    model: str
    probability: int = 1
    min_scale: float = 1
    max_scale: float = 1
    dir_random: float = 360
    dir_offset: float = 0
    dir_discrete: float = -1
    spacing: float = 1
    pitchbank_random: float = 0
    offsetx: float = 0
    offsety: float = 0
    isNode: int = 0
    allowPitch: int = 0
    allowBank: int = 0
    memoryStart: float = 0
    memoryEnd: float = 0



class SpalTool(BrushTool):
    ICON = "mdi.chart-line-variant"
    NAME = "spal"
    ALLOW_BULDOZER = False

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._class = SpalEntry
        # self.setMinimumSize(300, 400)

    def add_keybinds(self):
        add_keybind("6", self.activate, name=self.NAME)
        add_keybind(
            "ctrl+i",
            "[] call ADA_SPAL_fnc_fillDiscrete",
            name="Generate",
            category=self.NAME,
            condition="ada_spal_modeActive",
        )

    def _load_settings(self):
        settings = ToolSettings(tool=self)
        setting = settings.add(
            FloatProperty(NumberEdit, "Node interval", "", 5, 0, None, 0, None, 1, 2), "node_interval", default=5.0
        )
        setting = settings.add(
            FloatProperty(NumberEdit, "Max distance", "", 5, 0.5, None, 1, None, 0.5, 2), "max_distance", default=5.0
        )

        button = QPushButton("Generate")
        button.clicked.connect(self.generate)
        settings.add(button, "generate", priority=1, persistent=False)

        setting = self.fileselector = settings.add(
            FileSelector(subfolder=self.NAME, extension=(".json", self.LEGACY)), "fileselector"
        )
        setting.file_load.connect(self.load_from_file)
        setting.file_save.connect(self.save_to_file)

        # Brush entry + Attributes list
        setting = self.modellist = ModelAttributes()
        settings.add(setting, "modellist", persistent=False)
        setting.add_attribute_view(Toolbox("Node", setting, (("Segment", 0), ("Node", 1))), name="isNode")

        prob = FloatProperty(NumberEdit, "probability", "", 1, 0, None, 0, None, 1, 0)
        dir_random = FloatProperty(NumberEdit, "Direction randomization", "", 0, 0, 360, 0, 360, 5, 0)
        min_scale = FloatProperty(NumberEdit, "Min scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        max_scale = FloatProperty(NumberEdit, "Max scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        pitch_bank = FloatProperty(NumberEdit, "Pitch/Bank Randomization", "", 0, 0, 360, 0, 3, 0.1, 2)
        x_offset = FloatProperty(NumberEdit, "X Offset", "", 1, None, None, None, None, 0.05, 4)
        y_offset = FloatProperty(NumberEdit, "Y Offset", "", 1, None, None, None, None, 0.05, 4)
        z_offset = FloatProperty(NumberEdit, "Z Offset", "", 1, None, None, None, None, 0.05, 4)
        dir_ = FloatProperty(NumberEdit, "Dir offset", "", 1, None, None, None, None, 0.05, 4)
        spacing = FloatProperty(NumberEdit, "Spacing", "", 2, 0, None, 0.3, 10, 0.25, 3)
        allow_pitch = FloatProperty(NumberEdit, "Allowed pitch", "", 0, 0, None, 0, 10, 1, 3)
        allow_bank = FloatProperty(NumberEdit, "Allowed bank", "", 0, 0, None, 0, 10, 1, 3)

        setting.add_attribute_view(prob, name="probability")
        setting.add_attribute_view(spacing, name="spacing")
        setting.add_attribute_view(dir_random, name="dir_random")
        setting.add_attribute_view(dir_, name="dir_offset")
        setting.add_attribute_view(min_scale, name="min_scale")
        setting.add_attribute_view(max_scale, name="max_scale")
        setting.add_attribute_view(x_offset, name="offsetx")
        setting.add_attribute_view(y_offset, name="offsety")
        setting.add_attribute_view(pitch_bank, name="pitchbank_random")
        setting.add_attribute_view(allow_pitch, name="allowPitch")
        setting.add_attribute_view(allow_bank, name="allowBank")
        setting.changed.connect(self.instantsave)

        button = setting.add_button(qta.icon("fa5s.plus-square"), "")
        button.clicked.connect(self.add_entry_from_current)
        button = setting.add_button(qta.icon("fa5s.minus-square"), "")
        button.clicked.connect(self.remove_entry)
        button = setting.add_button(qta.icon("fa5s.window-close"), "")
        button.clicked.connect(self.clear_entries)

        return settings

    def get_brush(self):
        data = []
        data.extend([entry.values() for entry in self.entries])
        return data

    def _add_doubleclick(self):
        pass  # Remove this

    def generate(self):
        """Fills the polygon with objects"""
        Plop().cache_add(self.NAME + ".generate", self.get_brush(), unique=True)

    def save_to_file(self, path: Path):
        data = {
            "max_distance": self.settings["max_distance"],
            "node_interval": self.settings["node_interval"],
            "entries": [entry.values() for entry in self.entries],
        }
        self._saveFunc(path, data)

    def load_from_file(self, path: Path):
        """Loads a saved brush from a file, replaces the current one"""

        data = self.read_file(path)
        try:
            self.modellist.clear()
            if "max_distance" in data.keys():
                self.settings.set_value(self.settings.get_setting("max_distance"), data["max_distance"])
            if "node_interval" in data.keys():
                self.settings.set_value(self.settings.get_setting("node_interval"), data["node_interval"])

            self.entries = [self._class(*entry) for entry in data["entries"]]
            for entry in self.entries:
                item = self.modellist.add_entry(entry)

            PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)
            return data
        except TypeError as e:
            print(e)

    def legacy_processing(self, data: str):
        """Original objectplacementTBH format"""

        from ast import literal_eval

        data = literal_eval(data)[1:]
        for f in data:
            value = f.pop(1)  # Move modelname to first index
            f.insert(0, value)
        return {"entries": data}
