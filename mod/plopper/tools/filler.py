from pathlib import Path

from PyQt5.QtWidgets import QPushButton

from plopper import Plop
from plopper.keybinds import add_keybind
from plopper.settings import props
from plopper.toolsettings import Checkbox, NumberEdit, ToolSettings

from .brush import BrushTool


class FillTool(BrushTool):
    ICON = "fa5s.paint-roller"
    NAME = "filler"
    ALLOW_BULDOZER = False

    def __init__(self, parent=None):
        return super().__init__(parent=parent)

    def add_keybinds(self):
        add_keybind("3", self.activate, name=self.NAME)

    def _load_topsettings(self):
        settings = ToolSettings(tool=self)
        button = QPushButton("Generate")
        button.clicked.connect(self.generate)
        settings.add(button, "generate", priority=1, persistent=False)

        settings.add(
            Checkbox(text="Allow on road"),
            "allowOnRoad",
            tooltip="Check to allow creation of objects on roads",
            default=False,
        )

        settings.add(
            Checkbox(text="Allow on water"),
            "allowOnWater",
            tooltip="Check to allow creation of objects in water",
            default=False,
        )

        settings.add(
            Checkbox(text="Space to terrain"),
            "spaceToTerrain",
            tooltip="Include terrain objects in minimum distance calculation",
            default=True,
        )

        settings.add(
            Checkbox(text="Select after fill used"),
            "selectOnDone",
            tooltip="Selects placed objects after completed fill action",
            default=False,
        )


        settings.add(props.FloatProperty(NumberEdit, "Flow", "", 40, 1, None, 1, 100, 5, 0), name="flow", default=40)
        return settings

    def _add_doubleclick(self):
        pass  # Remove this

    def generate(self):
        """Fills the polygon with objects"""
        Plop().cache_add(self.NAME + ".generate", self.get_brush(), unique=True)

    def save_to_file(self, path: Path):
        data = {"flow": self.settings["flow"], "entries": [entry.values() for entry in self.entries]}
        self._saveFunc(path, data)

    def get_brush(self):
        data = [self.settings["flow"]]
        data.extend([entry.values() for entry in self.entries])
        return data
