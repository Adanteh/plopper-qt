"""Handles arma shapefiles (Roads, mask areas and whatever"""

import os
from pathlib import Path

import shapefile

TESTMODE = False


def get_projection_format(filepath):
    """Gets the projection format for this shapefile"""
    # Check if .prj file exists. pyshp doesn't suppor this, but we can just read the whole file and use that EZPZ
    # That file will automatically be detected by TerrainBuilder and give the correct coordinate systems
    if os.path.isfile("{}.prj".format(filepath)):
        prjpath = "{}.prj".format(filepath)
    else:
        prjpath = str(Path(__file__).parent / "defaultprojection.prj")
        if not Path(prjpath).exists():
            raise FileNotFoundError

    if TESTMODE:
        print(prjpath)
    with open(prjpath, "r") as projection_file:
        projection_data = projection_file.read()

    if TESTMODE:
        print(projection_data)

    return projection_data


def read_shapefile(filepath, offset=200000):
    """Reads a shapefile and returns the coordinates for it in array format"""

    testpath = Path(filepath + ".shp")
    if not Path(filepath + ".shp").exists():
        raise FileNotFoundError

    shapef = shapefile.Reader(filepath)
    shaperecords = shapef.shapeRecords()
    fields = shapef.fields[1:]  # Don't pass the deletion flag

    if TESTMODE:
        shaperecords = shaperecords[:10]

    data = []
    for _shapeindex, shaperecord in enumerate(shaperecords):
        record = shaperecord.record
        points = shaperecord.shape.points
        pointsadjusted = []
        # Offset coordinates by TB mapframe. I have absolutely no clue why this needs to be done, but whatever.
        # Faster to do it here than in SQF
        for point in points:
            pointsadjusted.append([point[0] - offset, point[1]])
        data.append([record, pointsadjusted])

    return (shapef.shapeType, fields, data)


def write_shapefile(filepath, datainput, offset=200000):
    """Writes a shapefile"""

    shapetype, fields, data = datainput

    if TESTMODE:
        print(shapetype)
        print(fields)

    try:
        with shapefile.Writer(target=filepath, shapeType=shapetype) as writer:
            for field in fields:
                writer.field(*field)

            for record, points in data:
                for point in points:
                    point[0] += offset

                if TESTMODE:
                    print(record)
                    print(points)

                writer.line([points])
                writer.record(*record)

            # Write the projection file
            if not os.path.isfile("{}.prj".format(filepath)):
                projection_data = get_projection_format(filepath)
                with open("{}.prj".format(filepath), "w") as projection_file:
                    projection_file.write(projection_data)

        return 0
    except Exception as e:
        print(e)
        return e
