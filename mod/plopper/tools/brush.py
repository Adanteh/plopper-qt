import json
from pathlib import Path
from typing import Dict
from dataclasses import dataclass

import qtawesome as qta

from plopper import Plop, PlopperSequence
from plopper.keybinds import add_keybind
from plopper.settings import PlopperSettings
from plopper.settings.props import FloatProperty
from plopper.tools import PlopperTool
from plopper.toolsettings import Checkbox, ModelAttributes, NumberEdit, Toolbox, ToolSettings
from plopper.toolsettings.fileselector import FileSelector


@dataclass
class BrushEntry(PlopperSequence):
    PLACEMENT_GLOBAL = 0
    PLACEMENT_HORIZONTAL = 1
    PLACEMENT_SLOPE = 2

    name: str = ""
    model: str = ""
    probability: int = 1
    dir_random: int = 360
    min_scale: float = 1
    max_scale: float = 1
    pitchbank_random: float = 0
    spacing: float = 1
    alignment: int = PLACEMENT_GLOBAL
    z_offset: float = 0


class BrushTool(PlopperTool):
    """
        Brush tool to create randomized groups of objects, from a list
    """

    ICON = "fa5s.paint-brush"
    NAME = "brush"
    AUTOLOAD_LAST = True
    LEGACY = ".sqf"
    ALLOW_BULDOZER = True

    def __init__(self, name=None, icon=None, parent=None):
        super().__init__(icon=icon, parent=parent)
        self._class = BrushEntry
        self.settings = self._load_topsettings()
        self.settings = self._load_settings()
        self.entries = []
        self.add_preferences()

    def add_keybinds(self):
        """Function called from init"""
        add_keybind("2", self.activate, name=self.NAME)
        add_keybind(
            "bracketleft",
            "[nil, -1] call ADA_toolCircle_fnc_mouseZChanged",
            name="Decrease size",
            category="circle",
            condition="ada_toolcircle_modeActive",
        )
        add_keybind(
            "bracketright",
            "[nil, 1] call ADA_toolCircle_fnc_mouseZChanged",
            name="Increase size",
            category="circle",
            condition="ada_toolcircle_modeActive",
        )

    # TODO split up settings and GUI in this
    def _load_topsettings(self):
        """Settings on top, these are general settings for our tool, not connected to currently loaded file"""
        settings = ToolSettings(tool=self)
        settings.add(
            Checkbox(text="Allow on road"),
            "allowOnRoad",
            tooltip="Check to allow creation of objects on roads",
            default=False,
        )

        settings.add(
            Checkbox(text="Allow on water"),
            "allowOnWater",
            tooltip="Check to allow creation of objects in water",
            default=False,
        )

        settings.add(
            Checkbox(text="Space to terrain"),
            "spaceToTerrain",
            tooltip="Include terrain objects in minimum distance calculation",
            default=True,
        )

        settings.add(
            Checkbox(text="Space to current"),
            "spaceToCurrent",
            tooltip="Check to only space to your current brush action.\nTurning this off allows you to add extra density easily",
            default=True,
        )

        settings.add(
            Checkbox(text="Select on brush end"),
            "selectOnDone",
            tooltip="Selects placed objects after completed brush action",
            default=False,
        )

        settings.add(FloatProperty(NumberEdit, "Flow", "", 40, 1, None, 1, 100, 5, 0), name="flow", default=40)
        settings.add(
            FloatProperty(NumberEdit, "Clustering", "", 1, 1, None, 1, 100, 5, 0), name="clustering", default=40
        )
        return settings

    def _load_settings(self):
        """Loads the settings connected to loaded files"""
        settings = self.settings
        setting = self.fileselector = settings.add(
            FileSelector(subfolder=self.NAME, extension=(".json", self.LEGACY)), "fileselector"
        )
        setting.file_load.connect(self.load_from_file)
        setting.file_save.connect(self.save_to_file)

        # Brush entry + Attributes list
        setting = self.modellist = ModelAttributes()
        settings.add(setting, "modellist", persistent=False)

        prob = FloatProperty(NumberEdit, "probability", "", 1, 0, None, 0, None, 1, 0)
        dir_random = FloatProperty(NumberEdit, "Direction randomization", "", 360, 0, 360, 0, 360, 5, 0)
        min_scale = FloatProperty(NumberEdit, "Min scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        max_scale = FloatProperty(NumberEdit, "Max scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        spacing = FloatProperty(NumberEdit, "Spacing", "", 2, 0, None, 0.3, 10, 0.25, 3)
        pitch_bank = FloatProperty(NumberEdit, "Pitch/Bank Randomization", "", 0, 0, 360, 0, 3, 0.1, 2)
        z_offset = FloatProperty(NumberEdit, "Height Offset", "", 0, None, None, -2, 2, 0.1, 2)

        setting.add_attribute_view(prob, name="probability")
        setting.add_attribute_view(dir_random, name="dir_random")
        setting.add_attribute_view(min_scale, name="min_scale")
        setting.add_attribute_view(max_scale, name="max_scale")
        setting.add_attribute_view(spacing, name="spacing")
        setting.add_attribute_view(pitch_bank, name="pitchbank_random")
        setting.add_attribute_view(z_offset, name="z_offset")
        setting.changed.connect(self.instantsave)

        setting.add_attribute_view(
            Toolbox(
                "Alignment",
                setting,
                (
                    ("Use global", self._class.PLACEMENT_GLOBAL),
                    ("Horizontal", self._class.PLACEMENT_HORIZONTAL),
                    ("Slope", self._class.PLACEMENT_SLOPE),
                ),
            ),
            name="alignment",
        )
        button = setting.add_button(qta.icon("fa5s.plus-square"), "")
        button.clicked.connect(self.add_entry_from_current)

        button = setting.add_button(qta.icon("fa5s.minus-square"), "")
        button.clicked.connect(self.remove_entry)

        button = setting.add_button(qta.icon("fa5s.folder-plus"), "")
        button.clicked.connect(self._add_from_selection)

        button = setting.add_button(qta.icon("fa5s.window-close"), "")
        button.clicked.connect(self.clear_entries)
        return settings

    def add_preferences(self):
        _brushsize = FloatProperty(
            widget=NumberEdit,
            name="Circle Size",
            tooltip="Size of the brush circle",
            default=1.5,
            min=0.5,
            max=None,
            soft_min=1,
            soft_max=25,
            step=1,
            precision=2,
        )

        PlopperSettings().register_setting("brush/circlesize", _brushsize)

    def add_entry(self, *args, entry=None, **kwargs):
        """Creates a new brush entry and adds it to our list"""
        if entry is None:
            entry = self._class(*args, **kwargs)
        self.entries.append(entry)
        item = self.modellist.add_entry(entry)
        self.modellist.select(item)

        self.changed.emit()

    def _add_from_selection(self):
        """Will ask arma to send us data on current selected items to compile brush from"""
        Plop().cache_add(self.NAME + ".add_from_selection", args=("model", "scale", "size"))

    def add_from_selection(self, data: list):
        """Receives data from currently selected objects, the return of _add_from_selection"""

        if not data:
            return

        entries: Dict[str, BrushEntry] = {}
        position = [0, 0, 0]
        for obj in (dict(f) for f in data):
            model = obj["model"]
            scale = obj["scale"]

            try:
                entry = entries[model]
                entry.probability += 1
                if scale < entry.min_scale:
                    entry.min_scale = scale

                if scale > entry.max_scale:
                    entry.max_scale = scale

            except KeyError:
                name = Path(model).stem
                spacing = obj["size"] / 2.3
                entry = entries[model] = self._class(name, model, min_scale=scale, max_scale=scale, spacing=spacing)

            self.add_entry(entry=entry)
        return True

    def add_entry_from_current(self, *args):
        """Adds the currently selected item to brush"""
        entry = Plop().main_class
        name = entry["Name"]
        if name:
            model = entry["File"]
            self.add_entry(name, model)

    def remove_entry(self, *args):
        """Removes currently selected entry in the list"""
        item = self.modellist.list.currentItem()
        if item is not None:
            # Need to delete by row, removeWidgetItem doesn't work as expected (Qt issue)
            row = self.modellist.list.row(item)
            self.entries.remove(item.entry)
            self.modellist.list.takeItem(row)
            del item

    def clear_entries(self):
        """Removes currently selected entry in the list"""
        self.modellist.clear()
        self.entries = []

    def get_settings(self):
        return self.settings

    def get_brush(self):
        data = []
        data.extend([entry.values() for entry in self.entries])
        return data

    def instantsave(self):
        if self.fileselector.file:
            self.save_to_file(self.fileselector.file)

    def save_to_file(self, path: Path):
        data = {
            "flow": self.settings["flow"],
            "clustering": self.settings["clustering"],
            "entries": [entry.values() for entry in self.entries],
        }
        self._saveFunc(path, data)

    def read_file(self, path: Path):
        """Reads the file"""
        legacy = path.suffix == self.LEGACY
        try:
            with path.open(mode="r") as file:
                contents = file.read()
                if legacy:
                    try:
                        return self.legacy_processing(contents)
                    except Exception:
                        pass
                data = json.loads(contents)
        except Exception as e:
            print(e)  # WARNING
            return {"entries": []}

        return data

    def load_from_file(self, path: Path):
        """Loads a saved brush from a file, replaces the current one"""

        data = self.read_file(path)

        try:
            self.modellist.clear()
            if "flow" in data.keys():
                self.settings.set_value(self.settings.get_setting("flow"), data["flow"])
            if "clustering" in data.keys():
                self.settings.set_value(self.settings.get_setting("clustering"), data["clustering"])

            self.entries = [self._class(*entry) for entry in data["entries"]]
            for entry in self.entries:
                item = self.modellist.add_entry(entry)

            PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)
            return data
        except TypeError as e:
            print(e)

    def legacy_processing(self, data: str):
        """Original objectplacementTBH format"""

        from ast import literal_eval

        data = literal_eval(data)[1:]
        for f in data:
            value = f.pop(1)  # Move modelname to first index
            f.insert(0, value)
            value = f.pop(5)  # Move probability entry to 3rd index
            f.insert(3, value)
            value = f.pop(7)  # Move pitchbank random forward one
            f.insert(6, value)

        return {"entries": data}

    def _saveFunc(self, path: Path, data: dict):
        """Sets the filepath as last loaded for this tool, and writes to file"""
        PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)
        with path.open(mode="w") as file:
            json.dump(data, file)
        return True

    def reloadFile(self, *args, **kwargs):
        """Reloads our last saved/loaded file from this. Called on app startup"""
        if not self.AUTOLOAD_LAST:
            return

        last_file = PlopperSettings().get_(self.NAME + "/lastfile", default="", _type=str)
        if last_file:
            last_path = Path(last_file)
            if last_path.exists():
                self.load_from_file(last_path)
