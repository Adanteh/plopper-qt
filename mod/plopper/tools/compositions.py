from pathlib import Path
from dataclasses import dataclass

import qtawesome as qta
from PyQt5.QtCore import Qt


from plopper import PlopperSequence, Plop
from plopper.keybinds import add_keybind
from plopper.vector import Vector3D
from plopper.settings.props import FloatProperty
from plopper.toolsettings import ModelAttributes, NumberEdit, Toolbox, ToolSettings
from plopper.toolsettings.fileselector import FileSelector
from .brush import BrushTool


@dataclass
class CompositionEntry(PlopperSequence):
    PLACEMENT_GLOBAL = 0
    PLACEMENT_HORIZONTAL = 1
    PLACEMENT_SLOPE = 2

    name: str
    model: str
    probability: int = 1
    offsetx: float = 0
    offsety: float = 0
    offsetz: float = 0
    min_scale: float = 1
    max_scale: float = 1
    dir: float = 0
    dir_random: float = 0
    pitchbank_random: float = 0
    absolute: int = PLACEMENT_GLOBAL
    alignment: int = PLACEMENT_GLOBAL
    pitch: float = 0
    bank: float = 0


class CompositionTool(BrushTool):
    """
        Allows saving premade compositions, with some randomized settings
        Double click to create them.
    """

    ICON = "fa5s.city"
    NAME = "compositions"
    ALLOW_BULDOZER = False

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._class = CompositionEntry

    def add_keybinds(self):
        add_keybind("5", self.activate, name=self.NAME)

    def _load_topsettings(self):
        return None

    def _load_settings(self):
        settings = ToolSettings(tool=self)
        setting = self.fileselector = settings.add(
            FileSelector(subfolder=self.NAME, extension=(".json", self.LEGACY)), "fileselector"
        )
        setting.file_load.connect(self.load_from_file)
        setting.file_save.connect(self.save_to_file)

        # Brush entry + Attributes list
        setting = self.modellist = ModelAttributes()
        setting.list.itemClicked.connect(self.remove_all_of_type)
        settings.add(setting, "modellist", persistent=False)

        prob = FloatProperty(NumberEdit, "probability", "", 1, 0, None, 0, None, 1, 0)
        dir_random = FloatProperty(NumberEdit, "Direction randomization", "", 0, 0, 360, 0, 360, 5, 0)
        min_scale = FloatProperty(NumberEdit, "Min scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        max_scale = FloatProperty(NumberEdit, "Max scale", "", 1, 0, None, 0.1, 5, 0.05, 3)
        pitch_bank = FloatProperty(NumberEdit, "Pitch/Bank Randomization", "", 0, 0, 360, 0, 3, 0.1, 2)

        setting.add_attribute_view(prob, name="probability")
        setting.add_attribute_view(dir_random, name="dir_random")
        setting.add_attribute_view(min_scale, name="min_scale")
        setting.add_attribute_view(max_scale, name="max_scale")
        setting.add_attribute_view(pitch_bank, name="pitchbank_random")
        setting.add_attribute_view(
            Toolbox(
                "Alignment",
                setting,
                (
                    ("Use global", self._class.PLACEMENT_GLOBAL),
                    ("Horizontal", self._class.PLACEMENT_HORIZONTAL),
                    ("Slope", self._class.PLACEMENT_SLOPE),
                ),
            ),
            name="alignment",
        )

        setting.add_attribute_view(
            Toolbox(
                "Absolute height",
                setting,
                (
                    ("Use global", self._class.PLACEMENT_GLOBAL),
                    ("Horizontal", self._class.PLACEMENT_HORIZONTAL),
                    ("Slope", self._class.PLACEMENT_SLOPE),
                ),
            ),
            name="absolute",
            priority=6,
        )
        setting.changed.connect(self.instantsave)

        button = setting.add_button(qta.icon("fa5s.plus-square"), "")
        button.clicked.connect(self.add_entry_from_current)
        button = setting.add_button(qta.icon("fa5s.minus-square"), "")
        button.clicked.connect(self.remove_entry)
        button = setting.add_button(qta.icon("fa5s.folder-plus"), "")
        button.clicked.connect(self._add_from_selection)
        button = setting.add_button(qta.icon("fa5s.window-close"), "")
        button.clicked.connect(self.clear_entries)

        x_offset = FloatProperty(NumberEdit, "X Offset", "", 1, None, None, None, None, 0.05, 4)
        y_offset = FloatProperty(NumberEdit, "Y Offset", "", 1, None, None, None, None, 0.05, 4)
        z_offset = FloatProperty(NumberEdit, "Z Offset", "", 1, None, None, None, None, 0.05, 4)
        dir_ = FloatProperty(NumberEdit, "Dir offset", "", 1, None, None, None, None, 0.05, 4)
        pitch = FloatProperty(NumberEdit, "Pitch", "", 0, None, None, 0, 180, 1, 4)
        bank = FloatProperty(NumberEdit, "Bank", "", 0, None, None, 0, 180, 1, 4)

        setting.add_attribute_view(x_offset, name="offsetx", priority=10)
        setting.add_attribute_view(y_offset, name="offsety", priority=10)
        setting.add_attribute_view(z_offset, name="offsetz", priority=10)
        setting.add_attribute_view(dir_, name="dir", priority=10)
        setting.add_attribute_view(pitch, name="pitch", priority=10)
        setting.add_attribute_view(bank, name="bank", priority=10)

        return settings

    def legacy_processing(self, data: str):
        """Original objectplacementTBH format"""

        from ast import literal_eval

        data = literal_eval(data)[1:]
        for f in data:
            value = f.pop(1)  # Move template to first entry
            f.insert(0, value)
        return {"entries": data}

    def remove_all_of_type(self, item):
        """This removes all entries with the same model as the one we alt-clicked on"""
        from plopper.main import PlopperApp

        modifiers = PlopperApp().queryKeyboardModifiers()
        if Qt.AltModifier == modifiers:

            model = item.entry["model"]
            list_ = self.modellist.list

            for item in self.modellist.entries():
                if item.entry["model"] == model:
                    list_.takeItem(list_.row(item))

    def get_brush(self):
        data = []
        data.extend([entry.values() for entry in self.entries])
        return data

    def save_to_file(self, path: Path):
        data = {"entries": [entry.values() for entry in self.entries]}
        self._saveFunc(path, data)

    def _add_doubleclick(self):
        pass  # Remove this

    def _add_from_selection(self):
        """Will ask arma to send us data on current selected items to compile brush from"""
        Plop().cache_add(
            self.NAME + ".add_from_selection", args=("model", "scale", "posw", "posw_t", "pitchbank")
        )

    def add_from_selection(self, data: list):
        """Receives data from currently selected objects, the return of _add_from_selection"""

        if not data:
            return

        data = [dict(f) for f in data]
        center_point = sum(Vector3D(*f["posw"]) for f in data) / len(data)  # type: Vector3D
        center_point.z = 0
        for obj in data:
            name = Path(obj["model"]).stem
            scale = obj["scale"]
            offset = Vector3D(*obj["posw_t"]) - center_point
            pitch, bank, yaw = obj["pitchbank"]
            entry = dict(
                name=name,
                model=obj["model"],
                offsetx=offset.x,
                offsety=offset.y,
                offsetz=offset.z,
                dir=yaw,
                min_scale=scale,
                max_scale=scale,
                pitch=pitch,
                bank=bank,
            )
            self.add_entry(**entry)
