from plopper.tools import PlopperTool


class BuldozerTool(PlopperTool):
    """
        Buldozer tool, mostly just to disable all normal Plopper behavior
    """

    ICON = "fa5s.circle"
    NAME = "buldozer"

    def __init__(self, name=None, icon=None, parent=None):
        super().__init__(icon=icon, parent=parent)

    @classmethod
    def condition(cls, mode: str):
        if mode == "buldozer":
            return True
        return False
