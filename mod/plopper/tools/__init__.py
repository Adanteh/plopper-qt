import qtawesome as qta
from PyQt5 import QtCore
from PyQt5.QtWidgets import QButtonGroup, QPushButton

from plopper import Plop, plop
from plopper.settings import PlopperSettings


class PlopperToolGroup(QButtonGroup):
    def __init__(self, parent=None):
        super().__init__(parent=parent)


class PlopperTool(QPushButton):
    """Mode with a whole bunch of options"""

    activated = QtCore.pyqtSignal(object)
    changed = QtCore.pyqtSignal()
    ICON = ""
    NAME = ""
    ALLOW_BULDOZER = False

    def __init__(self, icon=None, parent=None):
        displayname = self.NAME
        if icon is None:
            if self.ICON:
                icon = qta.icon(self.ICON, color_active="white")
                displayname = ""
            else:
                icon = self.NAME
        else:
            displayname = ""
        super().__init__(icon, displayname, parent=parent)

        self.settings = None
        self.priority = 1
        self.setCheckable(True)
        self.setChecked(False)
        self.setFixedSize(30, 30)
        self.setToolTip(self.NAME)
        self.setIconSize(QtCore.QSize(28, 28))
        # self.pressed.connect(partial(self.activated.emit, self))
        # self.released.connect(partial(self.deactivated.emit, self))
        self.toggled.connect(self._toggled)
        self.add_keybinds()
        # self.settings = PlopperToolSettings(parent=self)

    def __repr__(self):
        return "{} <{}>".format(self.__class__.__name__, self.NAME)

    @classmethod
    def condition(cls, mode: str):
        if mode != "buldozer":
            return True

        if cls.ALLOW_BULDOZER:
            return True

        return False

    def add_keybinds(self):
        pass

    def activate(self):
        from plopper.main import PlopperApp

        PlopperApp.instance().main_event.emit(lambda: self.setChecked(True))
        # self.on_activate()

    def active(self):
        return self.isChecked()

    def _toggled(self, toggle):
        if toggle:
            self.on_activate()
        else:
            self.on_deactivate()

    def on_activate(self):
        print("Activated tool {}".format(self.NAME))
        self.activated.emit(self)
        Plop().set_tool(self.NAME)
        PlopperSettings().set("tool", self.NAME, str)

    def on_deactivate(self):
        pass

    def on_mouse_z_changed(self):
        pass

    def on_mouse_click(self):
        pass

    def on_mouse_drag(self):
        pass

    def on_mouse_moving(self):
        pass

    def on_mouse_down(self):
        pass

    def on_mouse_up(self):
        pass

    def reloadFile(self, *args, **kwargs):
        """Reloads previously loaded files, called on app startup"""
        pass
