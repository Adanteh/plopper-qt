from collections import OrderedDict
from typing import Any, Optional, Dict

from PyQt5.QtCore import QObject, QSettings, pyqtSignal

from plopper.connect import Singleton
from .props import PlopperProperty, FloatProperty, BoolProperty  # noqa: F401


class PlopperSettings(QObject, metaclass=Singleton):
    """Our singleton instance that contains all our settings"""

    settingchanged = pyqtSignal(str, object)
    props: Dict[str, PlopperProperty]

    def __init__(self, mode, parent=None):
        super().__init__(parent=parent)

        self.mode = mode
        self.settings = QSettings("Adanteh", "Plopper")
        self.settings.beginGroup(mode)
        self.__accesedsettings = OrderedDict()
        self.props = {}

    def get(self, name: str, default: Optional[Any] = None, _type: Optional[Any] = bool):
        """Gets a setting 
        
        Args:
            name (str): `some/setting` will be available as `MVAR(set_some_setting)`
            default (Optional[Any], optional): Default if setting isn't in profile. Defaults to None.
            _type (Optional[Any], optional): The type our value should be. Defaults to bool.
        """
        value = self.get_(name, default, _type)
        self.__accesedsettings[name] = value
        return value

    def get_(self, name: str, default: Optional[Any] = None, _type: Optional[Any] = bool):
        """Getter (For Qt only, doesn't send it to embedded application)"""
        try:
            value = self.settings.value(name, default, _type)
        except TypeError:
            value = default
        return value

    def set(self, name: str, value: Any, _type: Optional[Any] = None):
        """Sets a setting
        
        Args:
            name (str): `some/setting` will be available as `MVAR(set_some_setting)`
            default (Optional[Any], optional): Default if setting isn't in profile. Defaults to None.
            _type (Optional[Any], optional): The type our value should be. Defaults to bool.
        """
        value = self.set_(name, value, _type)
        _property = self.get_property(name)
        if _property:
            _property.set_value(value)

        self.__accesedsettings[name] = value
        self.settingchanged.emit(name, value)

    def set_(self, name: str, value: Any, _type: Optional[Any] = None):
        """Sets a setting"""
        if _type is None:
            _type = type(value)
        self.settings.setValue(name, value)
        return value

    def destroy(self):

        self.settings.endGroup()
        del self.settings
        self.__class__._instance = None

    def get_settings(self) -> list:
        """Returns all the access settings in zipped (key, value) list"""
        return list(zip(self.__accesedsettings.keys(), self.__accesedsettings.values()))

    def get_property(self, key):
        return self.props.get(key, None)

    def register_setting(self, name: str, prop: object, default: Optional[Any] = None, _type: Optional[Any] = None):
        """Register a setting, loads the current value and saves the property class to link it to a widget"""
        if default is None:
            default = prop.default

        if _type is None:
            _type = prop.TYPE

        value = self.get(name, default, _type)
        prop.set_value(value)
        self.props[name] = prop
        return value

    def show_ui(self):
        from .ui import PlopperSettingsDialog

        self.view = PlopperSettingsDialog()
        print("Opened settings view")
