from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt, QAbstractAnimation
from PyQt5.QtWidgets import QSizePolicy


class CollapisbleGroup(QtWidgets.QWidget):
    def __init__(self, parent=None, title="", anim_duration=100, collapsed=True):
        """
        This is a collapse widget group that we can collapse, used for Settings menu

        References:
            # Adapted from c++ version
            http://stackoverflow.com/questions/32476006/how-to-make-an-expandable-collapsable-section-widget-in-qt
        """
        super().__init__(parent=parent)

        self.anim_duration = anim_duration
        self.toggle_anim = QtCore.QParallelAnimationGroup()
        self.content = QtWidgets.QScrollArea()
        self.headerLine = QtWidgets.QFrame()
        self.toggle_button = QtWidgets.QToolButton()
        self.mainLayout = QtWidgets.QGridLayout()

        toggle_button = self.toggle_button
        toggle_button.setStyleSheet("QToolButton { border: none; }")
        toggle_button.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        toggle_button.setArrowType(Qt.RightArrow)
        toggle_button.setText(str(title))
        toggle_button.setCheckable(True)
        toggle_button.setChecked(False)

        headerLine = self.headerLine
        headerLine.setFrameShape(QtWidgets.QFrame.HLine)
        headerLine.setFrameShadow(QtWidgets.QFrame.Sunken)
        headerLine.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)

        self._createlayout()

        # let the entire widget grow and shrink with its content
        toggle_anim = self.toggle_anim
        toggle_anim.addAnimation(QtCore.QPropertyAnimation(self, b"minimumHeight"))
        toggle_anim.addAnimation(QtCore.QPropertyAnimation(self, b"maximumHeight"))
        toggle_anim.addAnimation(QtCore.QPropertyAnimation(self.content, b"maximumHeight"))

        def start_animation(checked):
            arrow_type = Qt.DownArrow if checked else Qt.RightArrow
            direction = QAbstractAnimation.Forward if checked else QAbstractAnimation.Backward
            toggle_button.setArrowType(arrow_type)
            self.toggle_anim.setDirection(direction)
            self.toggle_anim.start()

        self.toggle_button.clicked.connect(start_animation)

    def _createlayout(self):
        layout = QtWidgets.QVBoxLayout()
        # layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignTop)

        self.content.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        # self.content.setStyleSheet("QScrollArea { background-color: white; border: none; }")
        self.content.setMaximumHeight(0)
        self.content.setMinimumHeight(0)
        self.content.setLayout(layout)

        # don't waste space
        mainLayout = self.mainLayout
        mainLayout.setVerticalSpacing(0)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        row = 0
        mainLayout.addWidget(self.toggle_button, row, 0, 1, 1, Qt.AlignLeft)
        mainLayout.addWidget(self.headerLine, row, 2, 1, 1)
        row += 1
        mainLayout.addWidget(self.content, row, 0, 1, 3)
        self.setLayout(self.mainLayout)

    def add_widget(self, widget):
        # Not sure if this is equivalent to self.contentArea.destroy()
        self.content.layout().addWidget(widget)
        self.update_animation()

    def update_animation(self):
        layout = self.content.layout()
        collapsedHeight = self.sizeHint().height() - self.content.maximumHeight()
        contentHeight = layout.sizeHint().height()

        for i in range(self.toggle_anim.animationCount() - 1):
            spoiler_anim = self.toggle_anim.animationAt(i)
            spoiler_anim.setDuration(self.anim_duration)
            spoiler_anim.setStartValue(collapsedHeight)
            spoiler_anim.setEndValue(collapsedHeight + contentHeight)
        anim = self.toggle_anim.animationAt(self.toggle_anim.animationCount() - 1)
        anim.setDuration(self.anim_duration)
        anim.setStartValue(0)
        anim.setEndValue(contentHeight)
