from functools import partial
from collections import OrderedDict

from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QSizePolicy

from plopper.settings import PlopperSettings
from plopper.settings.collapsible import CollapisbleGroup


class PlopperSettingsDialog(QtWidgets.QDialog):
    """The dialog showing our settings / preferences"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__settings = []

        layout = QtWidgets.QVBoxLayout()
        dialog = self
        dialog.resize(600, 400)
        dialog.setLayout(layout)

        self.scroll = QtWidgets.QScrollArea(self)
        self.scroll.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.scroll.setWidgetResizable(True)
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.inner = QtWidgets.QFrame(self.scroll)
        self.inner.setLayout(QtWidgets.QVBoxLayout())
        self.inner.layout().setAlignment(Qt.AlignTop)
        self.scroll.setWidget(self.inner)

        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.buttonBox.accepted.connect(dialog.accept)
        self.buttonBox.rejected.connect(dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(dialog)

        layout.addWidget(self.scroll)
        layout.addWidget(self.buttonBox)

        self.add_settings()
        self.scroll.show()
        self.setWindowTitle("Preferences")
        self.show()

        # self.layout = QtWidgets.QVBoxLayout()
        # dialog.setLayout(self.layout)

    def add_settings(self):
        """Creates all the widgets for our settings"""
        allsettings = PlopperSettings().get_settings()
        self.categories = OrderedDict()
        for key, value in allsettings:
            prop = PlopperSettings().get_property(key)
            if prop:
                parents = key.split("/")
                if len(parents) > 1:
                    parentwidget = self.add_category(parents)
                    widget = prop.create_widget(parentwidget)
                    parentwidget.add_widget(widget)
                else:
                    parentwidget = self.inner
                    widget = prop.create_widget(parentwidget)
                    self.inner.layout().addWidget(widget)

                widget.value_changed.connect(partial(PlopperSettings().set, key))

    def add_setting(self, _class):
        pass
        # self.inner.setMinimumSize(self.inner.size())

    def add_category(self, parents):
        parent = parents[0]
        parentwidget = self.categories.get(parent, None)
        if parentwidget:
            return parentwidget

        category = CollapisbleGroup(self.inner, parent.capitalize())
        self.inner.layout().addWidget(category)
        self.categories[parent] = category
        return category

    def on_buttonBox_triggered(self, *args):
        print(*args)

    def reject(self):
        return super().reject()

    def accept(self):
        return super().accept()

    def register_setting(self, name, default, type=None):
        pass
