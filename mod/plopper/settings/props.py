"""
    Settings/properties.
    Basically inspired by the Blender style of settings
    Could see these as Models as well, with create_widget being our UI representation
"""


from PyQt5.QtWidgets import QWidget
from plopper import PlopperSequence
from dataclasses import dataclass


class PlopperProperty(PlopperSequence):
    """
        Base property, each one of these refers to a single Setting that will be saved to profile
        and has a widget, so it can be adjusted from UI
    """

    TYPE = object
    _value: float = 0

    def __init__(self, *a, **kw):
        self._value = None
        super().__init__(*a, **kw)

    def value(self):
        return self._value

    def set_value(self, value):
        self._value = value

    def create_widget(self, parent=None) -> QWidget:
        try:
            if self.value() is None:
                self.set_value(self.default)

            widget = self.widget(self.name, parent=parent)
            widget.setToolTip(self.tooltip)
            widget.setText(str(self.value()))
            widget.property = self
            return widget
        except Exception as e:
            raise NotImplementedError(e)


@dataclass
class FloatProperty(PlopperProperty):
    TYPE = float

    widget: QWidget
    name: str = ""
    tooltip: str = ""
    default: float = 1
    min: float = None
    max: float = None
    soft_min: float = None
    soft_max: float = None
    step: float = 1
    precision: float = 2


@dataclass
class BoolProperty(PlopperProperty):
    TYPE = bool

    widget: QWidget
    name: str = ""
    tooltip: str = ""
    default: str = 1

    def create_widget(self, parent=None):
        try:
            widget = self.widget(self.name, parent=parent)
            widget.setToolTip(self.tooltip)
            widget.setChecked(self.value())
            return widget
        except Exception as e:
            raise NotImplementedError(e)
