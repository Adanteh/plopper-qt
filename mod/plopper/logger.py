import logging
from plopper import MODFOLDER

# Create the logger for object placement mod
def init():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    logpath = MODFOLDER / "plopper.log"
    hdlr = logging.FileHandler(str(logpath))
    hdlr.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s", "%H:%M:%S")
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)


init()
log = logging.getLogger(__name__)
