from pathlib import Path
import sys

INSTALLED = False
FOLDER = Path(__file__).parent
MODFOLDER = FOLDER.parent  # type: Path


def debug():
    "Debugger attaching" ""
    print("Waiting for debugger attach")
    import debugpy  # noqa: E402
    debugpy.listen(("localhost", 5678))
    debugpy.wait_for_client()
    print("Debugger attached")


if len(sys.argv) > 1 and sys.argv[1] == "debug":
    debug()
elif __name__ == "__main__":
    debug()

from plopper.connect import PlopperSequence  # noqa: F401
from plopper.connect import Plop, plop  # noqa: E402, F401
from .core import deinit, running, start, PlopperApp  # noqa: F401


def get_event_name(event):
    _type = event.type()
    for name in dir(event):
        if _type == getattr(event, name, 0):
            return name
    return "<unknown event>"


def print_(*args, **kwargs):
    print(*args, **kwargs)


IMPORTED = True
