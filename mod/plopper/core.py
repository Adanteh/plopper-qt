from plopper.main import PlopperApp  # noqa: E402
from plopper.sample import sample_data  # noqa: E402, F401
from plopper.embed import hwnd_functions  # noqa: E402,F401
from plopper import arma  # noqa: F401
from . import Plop


def start(program="Arma 3", mode="arma", callback=None, keep_alive=False):
    """Main function to start our Qt program"""
    with PlopperApp(mode=mode, Liststr=[]) as app:
        app.initUI()
        arma.main(app)

        if callback is not None:
            callback()

        if keep_alive:
            while app.alive:
                app.processEvents()
            # app.cleanup()


def running():
    """Checks if our Qt application is running"""
    try:
        instance = PlopperApp._instance
        if instance is None:
            raise Exception
        return (True, instance.alive)
    except Exception:
        return (False, False)


def deinit():
    """Properly unloads our Qt application without needing to restart Python interpreter"""
    try:
        instance = PlopperApp._instance  # type: PlopperApp
        if instance is not None:
            instance.cleanup()
            # instance.alive = False
            # PlopperApp._instance = None
            # del instance
    except LookupError:
        pass


def restart():
    """Restarts the Plopper UI without restarting the QApplication"""
    from time import sleep

    deinit()
    sleep(3)
    start()


def test():
    instance = PlopperApp.instance()
    data = [
        repr(getattr(instance, "ui", None)),
        repr(getattr(instance, "plop", None)),
        repr(getattr(instance, "settings", None)),
        repr(getattr(instance, "tracker", None)),
    ]
    return data


def test2():
    from plopper.settings import PlopperSettings

    # from plopper.embed.windowtrack import WindowTracker

    data = [
        repr(PlopperApp.instance()),
        repr(PlopperApp._instance),
        repr(Plop._instance),
        repr(PlopperSettings._instance),
    ]
    return data
