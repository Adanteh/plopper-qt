from typing import Any

from PyQt5 import QtCore
from PyQt5.QtCore import Qt  # noqa: F401
from PyQt5.QtWidgets import QApplication, QLineEdit

from plopper import Plop

#  REWORK from plopper.embed import windowtrack  # noqa: F401
from .connect import Singleton
from .embed import hwnd_functions
from .settings import PlopperSettings
from .ui.plopperwindow import PlopperWindow
from .embed.hwnd_functions import hwnd_from_exe, hwnd_from_name

Executables = {
    "arma": [
        "arma3.exe",
        "arma3_x64.exe",
        "arma3diag_x64.exe",
    ],
    "test": [
        "powershell.exe"
    ]
}

class PlopperApp(QApplication, metaclass=Singleton):
    """
        Our application. This can't be destroyed without restarting our Python interpreter, which
        will be impossible for Arma mods. That's why this App is quite basic and basically everything
        should be implement in child objects that we can reinit instead

        Also check the note for __init__
    """

    set_position = QtCore.pyqtSignal(object, object, str)
    main_event = QtCore.pyqtSignal(object)
    ui: PlopperWindow

    def __init__(self, mode="arma", Liststr=[]):
        """
            Dev note: Everything in here will only run once per interpreter, seeing we can't properly exit this.
            Everything that should restart when we close the UI, should be in a separate method
        """
        super().__init__(Liststr)
        self.__keys = []
        self._threads = []
        self.alive = True
        self.cleaned = False

        self.mode = mode

        hwnd = hwnd_from_exe(Executables[mode])
        if hwnd is None:
            hwnd = hwnd_from_name("Arma3")
            if hwnd is None:
                raise Exception("Hwnd can't be found both through process and window title method")
        self.hwnd = hwnd
        # self.aboutToQuit.connect(self.cleanup)

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        pass

    def initUI(self):
        self.alive = True

        self.plop = Plop(mode=self.mode, parent=self)
        self.settings = PlopperSettings(self.mode, parent=self)
        self.settings.settingchanged.connect(self.__settingchanged)

        self.ui = PlopperWindow(self)
        self.ui.closing_window.connect(self._quit)
        self.main_event.connect(self._main_event)

    def _main_event(self, event):
        """Used as convenience function to call funtions in main app (Required in Qt sometimes)"""
        if callable(event):
            event()

    @staticmethod
    def instance() -> "PlopperApp":
        return PlopperApp._instance

    def cleanup(self, *args):
        """This is cleanup that happens when we close the program. This actual QApplication won't shut down"""
        self.alive = False
        for thread in self._threads:
            thread.worker.destroy()

        self.ui.destroy()
        self.settings.destroy()
        self.plop.destroy()

        del self.ui
        del self.plop.ui
        del self.settings
        del self.plop
        self.alive = False
        self.cleaned = True

    def _quit(self):
        print("Running _quit")
        # self.plop.cache_add("plop.quit")
        self.alive = False
        self.exit(0)

    def has_focus(self):
        """Checks if our UI or the target application has focus"""
        if self.focusWidget() is not None:
            if isinstance(self.focusWidget(), QLineEdit):  # Don't pass keybinds when typing
                return False
            return True
        if hwnd_functions.has_focus(self.hwnd):
            return True
        return False

    @QtCore.pyqtSlot(str, object)
    def __settingchanged(self, name: str, value: Any):
        self.plop.cache_add("plop.setting_changed", [name, value])

    def create_thread_for(self, target, func, autostart=True):
        thread = QtCore.QThread()
        thread.setObjectName(repr(target))
        target.moveToThread(thread)
        thread.started.connect(getattr(target, func))
        self._threads.append(thread)
        thread.worker = target
        if autostart:
            thread.start()
        return thread

    @QtCore.pyqtSlot(bool)
    def externalKilled(self, alive):
        print("The external program went missing")
