from PyQt5.QtCore import QEvent, QCoreApplication, QObject

from functools import partial


from plopper.logger import log
from plopper import main

handles = []


class KeyboardFilter(QObject):
    def __init__(self, *args, **kwargs):
        self._event = KeyboardEvent.registerEventType()
        super().__init__(*args, **kwargs)

    def eventFilter(self, obj, event):
        """Used as wrapper for keyboard package, into normal events. Helps with Qt critical threading"""
        if obj == self:
            if event.type() == self._event:
                if callable(event.callback):
                    event.callback()
                return False
        return False


class KeyboardEvent(QEvent):
    pass


def add_keybind(
    key, callback: callable, *args, name: str = "", condition="true", category="plop", modifierRelease=True, **kwargs
):

    # Arma suppressed keyboard events, so can't use the keyboard package
    mode = main.PlopperApp.instance().mode
    if mode == "arma" or mode == "buldozer":
        from plopper.arma.keybinds import add_keybind as add_keybind_arma

        handle = add_keybind_arma(
            key, name, callback, *args, condition=condition, category=category, modifierRelease=True
        )
    else:
        import keyboard
        handle = keyboard.add_hotkey(key, partial(_keybindwrapper, callback), *args, **kwargs)
    handles.append(handle)


def _keybindwrapper(callback, *args, **kwargs):
    """See if our global OS keybind should pass through"""
    if main.PlopperApp.instance().has_focus():
        if isinstance(callback, str):
            print(callback)
            return

        kb = main.PlopperApp.instance().ui.kb  # type: KeyboardFilter
        event = QEvent(kb._event)
        event.callback = callback
        QCoreApplication.sendEvent(kb, event)
        return None

if __name__ == "__main__":
    # add_keybind("f11")
    pass
