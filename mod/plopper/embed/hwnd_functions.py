"""
    win32gui related functions to interact with windows UI
"""

from functools import partial
from typing import Tuple
from pathlib import Path
from win32 import win32gui, win32process, win32api
from win32.lib import win32con


def hwnd_from_name(name: str) -> int:
    """Gets the hwnd ID from partial match of the window title"""

    def callback(hwnd, hwnds):
        if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
            text = win32gui.GetWindowText(hwnd)
            if text.find(name) >= 0:
                hwnds.append(hwnd)
        return True

    hwnds = []
    win32gui.EnumWindows(callback, hwnds)
    if not hwnds:
        return None
    else:
        return hwnds[0]


def hwnd_from_exe(names: Tuple[str]) -> int:
    """
        Gets the hwnd ID from match of process name.
        This exists on first match
    """

    hwnds = []

    def callback(names: Tuple[str], hwnd, hwnds: list):
        if hwnds:
            return True

        try:
            if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
                _, pid = win32process.GetWindowThreadProcessId(hwnd)
                handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, pid)
                exe_name = win32process.GetModuleFileNameEx(handle, 0)
                if Path(exe_name).name in names:
                    hwnds.append(hwnd)

        except win32process.error as e:
            pass
        return True

    win32gui.EnumWindows(partial(callback, names), hwnds)
    if not hwnds:
        return None
    else:
        return hwnds[0]


def flash(hwnd):
    """Turns the icon on the taskbar for the window orange"""
    win32gui.FlashWindowEx(hwnd)


def set_parent(target: str, parent: str):
    win32gui.SetParent(hwnd_from_name(target), hwnd_from_name(parent))


def hwnd_from_pid(pid):
    """Gets hwnd from the process ID"""

    def callback(hwnd, hwnds):
        if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
            _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
            if found_pid == pid:
                hwnds.append(hwnd)
        return True

    hwnds = []
    win32gui.EnumWindows(callback, hwnds)
    if not hwnds:
        return None
    else:
        return hwnds[0]


def is_alive(window):
    return bool(win32gui.IsWindow(window))


def set_pos(window, x, y, w, h):
    """Sets hwnd window position"""
    win32gui.SetWindowPos(window, win32con.HWND_NOTOPMOST, x, y, w, h, win32con.SWP_SHOWWINDOW)


def make_fullscreen(window):
    pass
    # from win32api import GetSystemMetrics
    # width = GetSystemMetrics(0)
    # height = GetSystemMetrics(1)
    # set_pos(window, 0, 0, width, height)


def get_pos(window) -> Tuple[int]:
    # This should be offset by the resize area
    return win32gui.GetWindowRect(window)


def set_focus(window):
    # return None
    try:
        win32gui.SetForegroundWindow(window)
    except Exception:
        pass


def has_focus(window):
    # print("Focussed window", win32gui.GetWindowText(win32gui.GetForegroundWindow()))
    return win32gui.GetForegroundWindow() == window


def pull_forward(window):
    try:
        win32gui.BringWindowToTop(window)
    except Exception:
        pass


def get_frame(hwnd):
    style = win32gui.GetWindowLong(hwnd, win32con.GWL_STYLE)
    style2 = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    rect = win32gui.GetWindowRect(hwnd)
    frame = (style, style2, rect)
    return frame


def set_frame(hwnd, frame=(0, 0, 1920, 1080)):
    style = 349044736  # Default arma
    ex_style = 262400  # Default arma window
    # win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, frame[1])
    win32gui.ShowWindow(hwnd, win32con.SW_HIDE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_STYLE, style)
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, ex_style)

    x, y, xd, yd = frame
    win32gui.ShowWindow(hwnd, win32con.SW_SHOW)
    win32gui.SetWindowPos(hwnd, win32con.HWND_TOP, x, y, xd, yd, win32con.SWP_SHOWWINDOW)
    # win32gui.ShowWindow(hwnd, win32con.SW_SHOW)


def remove_frame(hwnd: int):
    """
        Sets the window as a bordless. Also fixes our inability for input in Arma 3 when using
        Qt's QWindow.fromWinId(hwnd)
    """
    style = win32gui.GetWindowLong(hwnd, win32con.GWL_STYLE)
    style2 = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    style = win32gui.SetWindowLong(
        hwnd, win32con.GWL_STYLE, style & ~(win32con.WS_BORDER | win32con.WS_DLGFRAME | win32con.WS_THICKFRAME)
    )
    style = win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, style2 & ~win32con.WS_EX_DLGMODALFRAME)

    style = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    style = style | win32con.WS_POPUP
    style = style & ~win32con.WS_OVERLAPPEDWINDOW
    win32gui.ShowWindow(hwnd, win32con.SW_HIDE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_STYLE, style)
    win32gui.ShowWindow(hwnd, win32con.SW_SHOW)


if __name__ == "__main__":

    # hwnd = hwnd_from_name("powershell.exe")
    hwnd = 8523354
    rect = win32gui.GetWindowRect(hwnd)
    top, left = win32gui.ScreenToClient(hwnd, (rect[0], rect[1]))
    bottom, right = win32gui.ScreenToClient(hwnd, (rect[2], rect[3]))

    frame = (rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1])
    frame_client = (top, left, bottom - top, right - left)
    print(frame)
    print(frame_client)
    # print(hwnd, get_frame(hwnd))
    # set_frame(hwnd)
    print("Exit")
