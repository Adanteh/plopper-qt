"""
    Functions to embed a windows application into a Qt application.

"""

import threading

from PyQt5.QtCore import QEvent, QObject, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QWindow
from PyQt5.QtWidgets import QWidget
from win32 import win32gui

from .hwnd_functions import get_frame, remove_frame, set_frame


class EventFilter(QObject):
    def eventFilter(self, obj, event):
        if event.type() == QEvent.WindowActivate or event.type() == QEvent.WindowDeactivate:
            from plopper import get_event_name

            print(get_event_name(event))
        return super().eventFilter(obj, event)


class EmbedWrapper(QObject):
    """Wrapper class for our embedded QWidget. Seeing the class is hardcoded in the createWindowContainer classmethod"""

    focus_toggle = pyqtSignal(bool)
    hwnd: int
    container: QWindow
    original_frame: tuple
    widget: QWidget

    def __init__(self, parent=None):
        self.focused = None
        self.alive = True
        super().__init__(parent=parent)

    @classmethod
    def createEmbed(cls, hwnd, parent):
        """Turns a windows GUI into a borderless Qt widget"""

        self = cls(parent=parent)

        container = QWindow.fromWinId(hwnd)  # type: QWindow
        container.setFlags(Qt.FramelessWindowHint)
        self.widget = QWidget.createWindowContainer(container, parent=parent)  # type: QWidget
        self.container = container
        self.hwnd = hwnd
        self.__frame = get_frame(hwnd)  # Qt doesn't properly set our required frame status, so do that here.
        remove_frame(hwnd)
        if parent:
            parent.embed = self

        self.focus_toggle.connect(self.focusEvent)
        t = threading.Thread(target=self.trackFocus, args=())
        t.start()
        return self

    def setFocus(self):
        """Sets input focus on our embedded application"""
        result = win32gui.SetFocus(self.hwnd)
        print(f"Setting focus: {result}")

    def trackFocus(self):
        """
        Thread to detect if our embed program currently has focus. 
        GetForegroundWindow loop required because focusEvents dont work with createWindowContainer
        """
        while self.parent().parent().alive:
            focused = win32gui.GetForegroundWindow() == self.hwnd
            if focused != self.focused:
                self.focus_toggle.emit(focused)
                self.focused = focused

    @pyqtSlot(bool)
    def focusEvent(self, focused: bool):
        """
        Focus our widget to pull it forward. By default when we click the embedded program, 
        it won't pull itself forward, so we detect it being focused then pull it forward
        """
        if focused:
            # TODO: Doesn't work for arma right now. Gets stuck in loop
            # Using activateWindow pulls it forward, but will make it lose input again
            # self.widget.activateWindow()
            self.parent().parent().raise_()
            win32gui.BringWindowToTop(self.hwnd)

    def enterEvent(self, event):
        """DOESNT WORK"""
        pass

    def detach(self, *args, **kwargs):
        """Detachs the windows GUI from our Qt application and restores to original frame"""

        rect = win32gui.GetWindowRect(self.hwnd)
        frame = (rect[0], rect[1], rect[2] - rect[0], rect[3] - rect[1])
        self.widget.setParent(None)
        self.widget.setWindowFlags(Qt.Window)
        win32gui.SetParent(self.hwnd, 0)

        # Do the detaching in separate thread, the arma window won't properly detached
        # while it's stuck here calling extension
        def restore_frame(hwnd, frame):
            from time import sleep

            sleep(1)
            set_frame(hwnd, frame)

        t = threading.Thread(target=restore_frame, args=(self.hwnd, frame))
        t.start()


def embed_program(hwnd: int, parent: QWidget) -> EmbedWrapper:
    """Embeds an external windows program as a widget inside Qt GUI
    
    Args:
        hwnd (int): HWND id for the 'real' program
        layout ([type]): The Qt widget layout to add program intoself
    """

    # val = hwnd_functions.hwnd_id(hwnd)
    embed = EmbedWrapper.createEmbed(hwnd, parent)  # type: QWidget
    geom = parent.geometry()
    embed.widget.setGeometry(0, 0, geom.width(), geom.height())
    return embed
