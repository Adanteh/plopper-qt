from typing import List

from .panels.library import LibraryPanel
from .panels.output import OutputPanel
from .panels.objects import ObjectsPanel
from .panels.properties import PropertiesPanel
from .panels.favorites import FavoritesPanel
from .panels.details import DetailsPanel

from .tools import PlopperTool
from .tools.objects import ObjectTool
from .tools.brush import BrushTool
from .tools.compositions import CompositionTool
from .tools.filler import FillTool
from .tools.spal import SpalTool
from .tools.surface import SurfaceTool
from .tools.buldozer import BuldozerTool

# fmt: off
Modules = [
    ("library", LibraryPanel),
    ("output", OutputPanel),
    ("objects", ObjectsPanel),
    ("properties", PropertiesPanel),
    ("favorites", FavoritesPanel),
    ("details", DetailsPanel),
]
# fmt: on

Tools = [
    ObjectTool,
    BrushTool,
    FillTool,
    SurfaceTool,
    CompositionTool,
    SpalTool,
    BuldozerTool,
]  # type: List[PlopperTool]
