"""
    Buttons to load projects (Serialized files, with editable positions)
    Also have the export button to export to the target format we want (TB .txt import)
"""


import json
from pathlib import Path
from typing import List

from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, QObject
from PyQt5.QtWidgets import QMessageBox

from plopper import Plop, MODFOLDER
from plopper.keybinds import add_keybind
from plopper.toolsettings.fileselector import FileLoading
from plopper.settings import PlopperSettings
from plopper.arma.tb import tb_iterator

# from plopper.armaterrains import tb_library
class Projects(QObject):
    DIRECTORY = MODFOLDER / "exports"
    EXTENSION = ".json"
    LEGACY = ".sqf"

    NORMAL = 0
    AUTOSAVE = 1
    QUICKSAVE = 2
    NAME = "project"

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.openprojects: List[Path] = []
        self.currentproject = ""
        self.savetype = self.NORMAL

        # self.add_keybinds()
        self.handle_autosave(first=True)

    def add_keybinds(self):
        add_keybind("ctrl+s", self.quicksave, name="Quicksave")

    def menuActions(self, ui=None):
        """Called from plopper init, will add a `Project` entry on the menubar"""

        submenu = "Project"
        prio = 0
        for args in (
            ("Save", "Ctrl+S", self._save_project, (submenu)),
            ("Load", "", self.load_project, (submenu)),
            ("Clear", "", self._clear, (submenu)),
            ("Export", "", self._export_project, (submenu)),
            ("Import TB", "", self._import_project, (submenu)),
        ):
            ui.create_action(*args, priority=prio)

    @QtCore.pyqtSlot()
    def handle_autosave(self, first=False):
        enabled = PlopperSettings().get("project/autosave", default=True)
        if not first and enabled:
            self._save_project(savetype=self.AUTOSAVE)

        delay = 1000 * 60 * 15
        QTimer.singleShot(delay, self.handle_autosave)

    def quicksave(self):
        if self.currentproject:
            self._save_project(savetype=self.QUICKSAVE)

        return True

    def get_incremental_savename(self, basename, extension=None, maxsaves=10) -> Path:
        """Gets incremental save name, with a max number Autosave_01, autosave_02, etc"""
        if extension is None:
            extension = self.EXTENSION

        folder = self.DIRECTORY / self.NAME
        filenumber = 1
        while Path(folder / "{}_{}{}".format(basename, str(filenumber).zfill(2), extension)).exists():
            filenumber += 1

        # Shift all filenames upward, so _01 will now be _02. Delete any files above max saves
        for number in range(filenumber, 1, -1):
            old_path = folder / "{}_{}{}".format(basename, str(number - 1).zfill(2), extension)
            if number > maxsaves:
                old_path.unlink()
            else:
                newname = folder / "{}_{}{}".format(basename, str(number).zfill(2), extension)
                old_path.rename(newname)

        return folder / "{}_{}{}".format(basename, str(1).zfill(2), extension)

    def _save_project(self, *args, savetype=None):
        """Will ask arma to send us the project data"""
        if savetype is None:
            savetype = self.NORMAL

        self.savetype = savetype
        Plop().cache_add(self.NAME + ".save_project")

    def _export_project(self, *args):
        """Will ask arma to send us the project data"""
        Plop().cache_add(self.NAME + ".export_project")

    def export_project(self, data):
        """Exports a project in Arma Terrain builder format"""
        if not data:
            return False

        path = self.get_save_path(extension=".txt")
        if path is not None:
            with open(path, "w") as path:
                for line in data:
                    path.write(line + "\n")
            return True
        else:
            return False

    def save_project(self, data, worldname=""):
        """Saves a plopper project in JSON format, for future editing in the tool"""

        if not data:
            if self.savetype == self.AUTOSAVE:  # Never autosave empty projects
                return False

            saveempty = self._ask_empty_save()
            if not saveempty:
                return False

        if self.savetype == self.AUTOSAVE:
            path = self.get_incremental_savename(worldname)
        elif self.savetype == self.QUICKSAVE:
            path = self.currentproject
        else:
            path = self.get_save_path()

        if path is not None:
            if self.savetype != self.AUTOSAVE:
                if path not in self.openprojects:
                    self.openprojects.append(path)
                self.currentproject = path

            if path.exists():
                path.unlink()

            with path.open(mode="w") as file:
                json.dump(data, file)
            return True
        else:
            return False

    def _import_project(self, *args):
        """Imports a TB project into current mission"""
        path = FileLoading.loadfile(self.DIRECTORY / self.NAME, (".txt",))
        if path is not None:
            try:
                data = [entry.as_tuple() for entry in tb_iterator(path)]
                Plop().cache_add(self.NAME + ".import_tb", args=data)
            except Exception as e:
                print("ERROR loading project")
                print(e)

    def load_project(self, *args):
        """Will ask arma to send us data on current selected items to compile brush from"""
        path = FileLoading.loadfile(self.DIRECTORY / self.NAME, (self.EXTENSION, self.LEGACY))
        if path is not None:
            if path in self.openprojects:
                if not self._ask_duplicate_load():
                    return None
            else:
                self.openprojects.append(path)

            try:
                with path.open(mode="r") as file:
                    if path.suffix == self.LEGACY:
                        data = self.legacy_processing(file.read())
                    else:
                        data = json.load(file)

                Plop().cache_add(self.NAME + ".load_project", args=data)
            except json.decoder.JSONDecodeError as e:
                print("ERROR loading project")
                print(e)

    def get_save_path(self, extension=None):
        if extension is None:
            extension = self.EXTENSION
        return FileLoading.savefile(self.DIRECTORY / self.NAME, extension)

    def legacy_processing(self, data: str):
        """Original objectplacementTBH format"""

        from ast import literal_eval

        data = literal_eval(data)[1:]
        return data

    def _clear(self):
        """Clears project"""
        Plop().cache_add(self.NAME + ".clear")
        self.openprojects = []

    def _ask_empty_save(self):
        """Creates confirmation dialog when attempting to save an empty project"""
        title = "Saving project"
        reply = QMessageBox.question(
            self.parent(), title, "Are you sure you want to save an empty project?", QMessageBox.Yes | QMessageBox.No
        )
        return reply == QMessageBox.Yes

    def _ask_duplicate_load(self):
        """Creates confirmation when loading a project that already is loaded"""
        title = "Loading project"
        reply = QMessageBox.question(
            self.parent(),
            title,
            "Are you sure you want to load the same project twice??",
            QMessageBox.Yes | QMessageBox.No,
        )
        return reply == QMessageBox.Yes
