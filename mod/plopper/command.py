from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from plopper.connect import Plop


def _command(plop: "Plop", key: str, data: list):
    """Command wrapper, keeps Python updated when objects are adjusted in the engine"""
    pass