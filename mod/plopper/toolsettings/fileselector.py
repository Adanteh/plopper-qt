from pathlib import Path
from typing import Union

import qtawesome as qta
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QFileDialog

from plopper import Plop, MODFOLDER
from plopper.settings import PlopperSettings


class FileSelector(QtWidgets.QWidget):

    file_load = QtCore.pyqtSignal(Path)  # After selecting file <before write>
    file_save = QtCore.pyqtSignal(Path)  # After selecting file <before load>

    DIRECTORY = MODFOLDER / "exports"
    file: Path
    folder: Path

    def __init__(self, parent=None, extension=".json", subfolder=""):
        """UI Element to save and load files easily
        
        Args:
            parent (QWidget, optional): Parent widget. Defaults to None.
            extension (Union[str, tuple], optional): Accepted extensions. Defaults to ".json".
            subfolder (str, optional): Where to open the initial browser, subfolder of root project. Defaults to "".
        """
        super().__init__(parent=parent, flags=Qt.WindowFlags())

        self.extension: Union[str, tuple] = extension
        self.file = None
        self._layout = QtWidgets.QHBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self._layout)
        self._layout.setAlignment(Qt.AlignRight)

        self.__text = QtWidgets.QLabel("Select file", parent=self)
        self._layout.addWidget(self.__text)

        self.subfolder = subfolder
        self.folder = self._load_last_location(subfolder)
        self._save = QtWidgets.QPushButton(qta.icon("mdi.content-save"), "", parent=self)
        self._save.clicked.connect(self._savefile)
        self._layout.addWidget(self._save)

        # Load
        self._load = QtWidgets.QPushButton(qta.icon("mdi.folder-open"), "", parent=self)
        self._load.clicked.connect(self._loadfile)
        self._layout.addWidget(self._load)

    def _load_last_location(self, subfolder):
        target_dir = None
        try:
            folder = PlopperSettings().get_(f"{subfolder}/folder", default="", _type=str)
            if folder:
                folder = Path(folder)
                if folder.exists():
                    target_dir = folder
        except Exception:
            pass

        if target_dir is None:
            target_dir = self.DIRECTORY / subfolder
            target_dir.mkdir(0o755, True, exist_ok=True)
        return target_dir

    def _save_last_location(self, path: Path):
        subfolder = self.subfolder
        folder = path.parent
        PlopperSettings().set_(f"{subfolder}/folder", str(folder), _type=str)
        self.folder = folder

    def _loadfile(self, *args):
        path = FileLoading.loadfile(self.folder, self.extension)
        if path:
            self._save_last_location(path)
            self.file_load.emit(path)
            self.file = path
            self.__text.setText("..{}".format(str(Path(*path.parts[-3:]))))

    def _savefile(self, *args):
        path = FileLoading.savefile(self.folder, self.extension)
        if path:
            self._save_last_location(path)
            self.file_save.emit(path)
            self.file = path
            self.__text.setText("..{}".format(str(Path(*path.parts[-3:]))))


class FileLoading:
    """Some classmethods to load files with QFileDialog"""

    DIRECTORY = MODFOLDER / "exports"
    OPTIONS = QFileDialog.Options() | QFileDialog.DontUseNativeDialog

    @classmethod
    def loadfile(cls, folder: Path, extension) -> Union[Path, None]:
        if isinstance(extension, tuple):
            files = " ".join("*" + f for f in extension)
        else:
            files = f"*{extension}"

        parent = Plop().ui
        file, _ = QFileDialog().getOpenFileName(
            parent=parent, caption="Load file", directory=str(folder), filter=f"Plopper ({files})", options=cls.OPTIONS
        )
        if file:
            return Path(file)
        return None

    @classmethod
    def savefile(cls, folder: Path, extension) -> Union[Path, None]:
        if isinstance(extension, tuple):
            files = " ".join("*" + f for f in extension)
            suffix = extension[0]
        else:
            files = f"*{extension}"
            suffix = extension

        parent = Plop().ui
        file, _ = QFileDialog().getSaveFileName(
            parent=parent, caption="Save file", directory=str(folder), filter=f"Plopper ({files})", options=cls.OPTIONS
        )
        if file:
            path = Path(file)
            if path.suffix == "":
                path = path.with_suffix(suffix)
            return path
        return None
