from functools import partial
from collections import OrderedDict
from operator import attrgetter

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtWidgets import QListWidgetItem, QSizePolicy
from PyQt5.QtGui import QDoubleValidator

from plopper.settings import PlopperSettings, props


def behavior(func, parent, *args, **keywords):
    def newfunc(*fargs, **fkeywords):
        newkeywords = keywords.copy()
        newkeywords.update(fkeywords)
        _ret = func(*args, *fargs, **newkeywords)
        parent.changed.emit()
        return _ret

    newfunc.func = func
    newfunc.args = args
    newfunc.keywords = keywords
    return newfunc


def add_behavior(setting, parent, name, tooltip="", persistent=False):
    """Wrapper to connect behavior to button"""

    if isinstance(setting, QtWidgets.QCheckBox):
        setting.toggled.connect(behavior(parent._toggled, parent, name))
        setting.setToolTip(tooltip)
        if persistent:
            setting.toggled.connect(partial(parent._persistent_setting, name))

    elif isinstance(setting, QtWidgets.QLineEdit):
        setting.textChanged.connect(behavior(parent._textedit, name))
        setting.setToolTip(tooltip)
        if persistent:
            setting.textChanged.connect(partial(parent._persistent_setting, name))

    elif isinstance(setting, NumberEdit):
        setting.value_changed.connect(behavior(setting._value_changed, parent, name))
        if persistent:
            setting.value_changed.connect(partial(parent._persistent_setting, name))

    elif isinstance(setting, Toolbox):
        setting.value_changed.connect(behavior(setting._value_changed, parent, name))
        if persistent:
            setting.value_changed.connect(partial(parent._persistent_setting, name))


class DuplicateSetting(Exception):
    """Raised when adding a setting to a tool that already exists"""

    pass


class ToolSettings(QtWidgets.QWidget):
    """Container for all settings shown in the sidebar regarding a selected tool"""

    changed = QtCore.pyqtSignal()

    def __init__(self, tool=None, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=None, flags=flags)

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignTop)
        self.resize(600, 400)
        self.setLayout(layout)

        self._init_scrollArea()
        self.__settings = OrderedDict()
        self.__settingclasses = OrderedDict()  # Need ordered for if priorities are the same
        self.tool = tool

        self.changed.connect(lambda: print("Changed on toolsettings"))

    def _init_scrollArea(self):
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.setAlignment(Qt.AlignTop)
        self.scroll = QtWidgets.QScrollArea(self)
        self.scroll.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.scroll.setWidgetResizable(True)
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.inner = QtWidgets.QFrame(self.scroll)
        self.inner.setLayout(self._layout)
        self.inner.layout().setAlignment(Qt.AlignTop)
        self.scroll.setWidget(self.inner)

        self.scroll.show()
        self.layout().addWidget(self.scroll)

    def add(self, setting: object, name: str, tooltip="", default=True, priority=5, persistent=True, parent=None):
        if parent is None:
            parent = self

        if isinstance(setting, props.PlopperProperty):
            setting = setting.create_widget(parent=parent)

        if name in self.__settings.keys():
            raise DuplicateSetting("This setting already exists in this group: {}".format(name))

        if persistent:
            default = PlopperSettings().get("{}/{}".format(self.tool.NAME, name), default, _type=type(default))

        self.__settings[name] = default
        self.__settingclasses[name] = setting
        self.set_value(setting, default)

        # Priority. Lower means higher up in the list
        setting._priority = priority
        newlist = sorted(self.__settingclasses.values(), key=attrgetter("_priority"), reverse=False)
        index = newlist.index(setting)

        self._layout.insertWidget(index, setting)
        add_behavior(setting, parent, name, tooltip, persistent=persistent)
        return setting

    def _toggled(self, name, state):
        self[name] = state

    def _textedit(self, name, text):
        self[name] = text

    def _persistent_setting(self, name, value):
        PlopperSettings().set("{}/{}".format(self.tool.NAME, name), value)

    def __getitem__(self, key):
        return self.__settings[key]

    def __setitem__(self, key, value):
        self.__settings[key] = value

    def get_setting(self, name):
        return self.__settingclasses.get(name, None)

    def set_value(self, setting, value):
        if isinstance(setting, QtWidgets.QCheckBox):
            setting.setChecked(bool(value))
        elif isinstance(setting, QtWidgets.QLineEdit):
            setting.setText(str(value))
        elif isinstance(setting, NumberEdit):
            setting.setText(value)
        elif isinstance(setting, Toolbox):
            setting.set_from_value(value)


class Checkbox(QtWidgets.QCheckBox):
    value_changed = QtCore.pyqtSignal(bool)

    def __init__(self, text="", parent=None):
        super().__init__(text, parent=parent)
        self.parent = parent
        self.toggled.connect(self.value_changed.emit)


class NumberEdit(QtWidgets.QWidget):
    value_changed = QtCore.pyqtSignal(float)

    def __init__(self, text="", parent=None):
        super().__init__(parent=parent)
        self.parent = parent
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel(text, parent=self)
        self.field = QtWidgets.QLineEdit("", parent=self)
        self.field.setValidator(QDoubleValidator(parent=self.field))
        self.setMinimumSize(0, 0)

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.field)
        self.setLayout(self.layout)
        self.field.textChanged.connect(partial(self._changed, self.field))

        self.property = None
        self.installEventFilter(self)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.Wheel:
            if self.property is not None:
                angle = event.angleDelta().y() / 120
                if angle:
                    offset = angle * self.property.step
                    new = offset + self.property.value()
                    value = self.property_filter(new, soft=True)
                    self.setText(value)
        return False

    def property_filter(self, value, soft=False):
        if self.property.min is not None:
            if value < self.property.min:
                value = self.property.min

        if self.property.max is not None:
            if value > self.property.max:
                value = self.property.max

        # Respect soft boundaries (Limit scroll / drag behavior, but typing in lower/higher values is allowed)
        if soft:
            if self.property.soft_min is not None:
                if value < self.property.soft_min:
                    value = self.property.soft_min

            if self.property.soft_max is not None:
                if value > self.property.soft_max:
                    value = self.property.soft_max

        if self.property.precision is not None:
            value = round(value, self.property.precision)
        return value

    def setText(self, value, *args, **kwargs):
        """Pass through text change to the field"""

        if self.property is not None:
            self.property.set_value(value)

        if not isinstance(value, str):
            value = str(value)

        self.field.setText(value, *args, **kwargs)

    def _changed(self, cls, text):
        """Called from signal when text is changed"""

        if getattr(self, "block_signal", False):
            return
        try:
            value = float(text)
            self.field.setStyleSheet("")
            self.value_changed.emit(value)

        except ValueError:
            self.field.setStyleSheet("QLineEdit { background-color: red }")

    def _validate(self):
        pass

    def _value_changed(self, name, value):
        self.parent[name] = float(value)


class Toolbox(QtWidgets.QWidget):
    """Horizontal unique checkable buttons, think a radiolist, but with buttons"""

    value_changed = QtCore.pyqtSignal(object)

    def __init__(self, text="", parent=None, data=()):
        super().__init__(parent=parent)
        self.parent = parent
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel(text, parent=self)
        self.__group = QtWidgets.QButtonGroup(self)
        self.__buttons = []
        self.value = None

        self.layout.addWidget(self.label)
        self.setLayout(self.layout)
        for entry in data:
            self._addButton(*entry)

    def set_from_value(self, value):
        """Sets the currently active button from given value"""
        button = [button for button in self.__buttons if button._value == value]
        if not button:
            raise Exception("Trying to set button from invalid value")
        button[0].setChecked(True)

    def _addButton(self, text, value):
        """Pass through text change to the field"""
        button = QtWidgets.QPushButton(text, parent=self)
        button._value = value
        button.toggled.connect(partial(self._clicked, value))
        button.setCheckable(True)
        self.layout.addWidget(button)
        self.__group.addButton(button)
        self.__buttons.append(button)

        if self.value is None:
            button.setChecked(True)

    def _clicked(self, value, toggle):
        """Called from signal when text is changed"""
        if toggle:
            self.value = value
            self.value_changed.emit(value)

    # @staticmethod
    def _value_changed(self, name, value):
        self.parent[name] = value


class ModelAttributes(QtWidgets.QWidget):
    changed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.setContentsMargins(0, 0, 0, 0)

        # Prepare for horizontal layout too
        layout_h = self.layout_h = QtWidgets.QHBoxLayout()
        layout_h.setContentsMargins(0, 0, 0, 0)
        layout_h.addLayout(self.layout)
        layout_h.setAlignment(Qt.AlignTop)
        self.setLayout(layout_h)

        self.list = QtWidgets.QListWidget(parent=self)
        self.list.setMaximumHeight(700)
        self.list.currentItemChanged.connect(self.select_changed)
        # self.list.setMinimumHeight(700)
        self.list._priority = -1
        # self.list.setMinimumSize(300, 400)
        self.layout.insertWidget(0, self.list)
        self.__attributes = OrderedDict()
        self.__buttons = []
        self.__buttonslayout = None
        self._selectedentry = None

    def __setitem__(self, key, value):
        if self._selectedentry is not None:
            self._selectedentry[key] = value

    def __getitem__(self, key):
        if self._selectedentry is not None:
            return self._selectedentry[key]
        else:
            return None

    def add_attribute_view(self, setting, name, priority=5):
        if isinstance(setting, props.PlopperProperty):
            setting = setting.create_widget(parent=self)

        self.__attributes[name] = setting

        # Priority. Lower means higher up in the list
        setting._priority = priority
        newlist = sorted(self.__attributes.values(), key=attrgetter("_priority"), reverse=False)
        index = newlist.index(setting) + 1  # Add the list on top

        self.layout.insertWidget(index, setting)
        add_behavior(setting, self, name)

        return setting

    def remove_attribute_view(self, name):
        try:
            setting = self.__attributes[name]
            self.layout.removeWidget(setting)
            setting.deleteLater()
            del self.__attributes[name]
        except KeyError:
            pass

    def add_entry(self, entry):
        item = QListWidgetItem(entry.name, parent=self.list)
        item.setToolTip(entry.model)
        item.entry = entry
        return item

    def select(self, item):
        self.list.setCurrentItem(item)

    def clear(self):
        self.list.clear()

    def remove_current(self):
        current = self.list.currentItem()
        if current is not None:
            self.list.takeItem(current)
            del current

    def entries(self):
        entries = []
        for x in range(self.list.count() - 1):
            entries.append(self.list.item(x))
        return entries

    def add_button(self, *args, **kwargs):
        """Adds a button of to the side"""
        button = QtWidgets.QPushButton(*args, parent=self, **kwargs)
        button.setFixedSize(20, 20)
        button.setIconSize(QtCore.QSize(18, 18))
        if not self.__buttons:
            layout = self.__buttonslayout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setAlignment(Qt.AlignTop)
            self.layout_h.addLayout(layout)

        self.__buttons.append(button)
        self.__buttonslayout.addWidget(button)
        return button

    def _toggled(self, state, name):
        self[name] = state
        pass

    def _textedit(self, state, name):
        self[name] = state
        pass

    @QtCore.pyqtSlot(QtWidgets.QListWidgetItem, QtWidgets.QListWidgetItem)
    def select_changed(self, current, previous):
        try:
            entry = current.entry
            self._selectedentry = entry
            self._show_attributes_for(entry)
        except AttributeError:
            pass

    def _show_attributes_for(self, entry):
        for attr_name in self.__attributes.keys():
            attr_widget = self.__attributes[attr_name]
            try:
                attr_value = entry[attr_name]
                if attr_value is None:
                    continue

                # attr_widget.blockSignals(True)
                attr_widget.block_signal = True
                if isinstance(attr_widget, QtWidgets.QCheckBox):
                    attr_widget.setChecked(bool(attr_value))

                elif isinstance(attr_widget, NumberEdit):
                    attr_widget.setText(attr_value)

                elif isinstance(attr_widget, Toolbox):
                    attr_widget.set_from_value(attr_value)

            except (AttributeError, TypeError) as e:
                print(e)
            finally:
                attr_widget.block_signal = False
