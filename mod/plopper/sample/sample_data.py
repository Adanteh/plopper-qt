import sys
from random import uniform
from functools import partial

from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QObject

from plopper import Plop
from plopper.panels.objects import ObjectEntry


def random(value):
    return uniform(0, value)


def sample_surface_export(*args):
    data = [1024]
    surfaces = Plop().tools.surface.surfaces()
    for _x, surface in enumerate(surfaces):
        for _y in range(1, 15):
            data.append([_x + 1, _y, surface["name"]])
    return ("tools.surface.export", data)


def sample_selection_data(event, tags):
    data = []
    for i in range(1, 5):
        values = []
        pos = [20, 20, 10]
        for tag in tags:
            if tag == "model":
                value = f"a3\\something\\model{i}.p3d"
            elif tag == "size":
                value = uniform(3, 5.5)
            elif tag == "scale":
                value = uniform(0.6, 1.5)
            elif tag == "dir":
                value = random(360)
            elif tag in ("posw", "posw_t"):
                value = pos

            values.append([tag, value])
        data.append(values)
    return (f"{event}.add_from_selection", data)


def sample_save_project(*args):
    data = []
    for i in range(1, 10):
        data.append(
            [
                "a3\\something\\model{}.p3d".format(i),  # Model
                [10, 10, 0],  # Pos
                uniform(0.5, 1),  # Scale
                [0, 0, 1],  # Pitchbankyaw
                "",  # Layer
                0,  # isHidden
                0,  # isLocked
            ]
        )
    return ("project.save_project", data, "Fakeworld")


def sample_load_project(data):
    for index, entry in enumerate(data):
        model = entry[0]
        layer = entry[4]
        id_ = "0_" + str(index + 500)
        pos = [float(f) for f in entry[1]]
        pby = [float(f) for f in entry[3]]
        scale = float(entry[2])

        entry = ObjectEntry(model, layer, id_, *pos, scale, *pby)
        Plop().poll_return.emit("objects.add_object", entry.values())


def sample_load_tb(data):
    from plopper import plop
    for index, entry in enumerate(data):
        model = plop("library.get_model", entry[0])
        layer = ""
        id_ = "0_" + str(index + 200)
        Plop().poll_return.emit("objects.add_object", (model, layer, id_))


def sample_clear_project(*args):
    return ("objects.clear", ())


def add_sample_data():
    return

    def sample_objects(*args):
        data = (
            ("model1.p3d", ""),
            ("model2.p3d", ""),
            ("model3.p3d", ""),
            ("model4.p3d", "brush1"),
            ("model5.p3d", "brush1"),
            ("model6.p3d", "brush2"),
        )
        for index, entry in enumerate(data):
            Plop().objects.add_object(entry[0], id="0_" + str(index + 1), layer=entry[1])

    sample_objects()


def sample_object_delete(params, *args):
    for idx in params[0]:
        Plop().poll_return.emit("objects.remove_object", (idx,))
    return None


SAMPLEEVENTS = {
    "surface.export": sample_surface_export,
    "favorites.add_from_selection": partial(sample_selection_data, "favorites"),
    "brush.add_from_selection": partial(sample_selection_data, "tools.brush"),
    "compositions.add_from_selection": partial(sample_selection_data, "tools.compositions"),
    "filler.add_from_selection": partial(sample_selection_data, "tools.filler"),
    "project.import_tb": sample_load_tb,
    "project.save_project": sample_save_project,
    "project.load_project": sample_load_project,
    "project.clear": sample_clear_project,
    "object.delete": sample_object_delete,
}


class CacheTester(QObject):
    """
        Used to simulate the cached events when running sample programself.
        This is run in a separate QThread, so breakpoints will not work
    """

    def __init__(self, parent=None):
        self.app = parent
        self.alive = True
        self._sampleevents = {}
        super().__init__(parent=None)

    def poll(self):
        while self.alive:
            cache = Plop().cache_out()
            if cache:
                for event, sets in cache:
                    for args in sets:
                        self.invoke(event, args)
            self.app.processEvents()

    def invoke(self, event, args):
        # print(event, args)
        self.test_event(event, args)

    def test_event(self, event, *args):
        try:
            code = SAMPLEEVENTS[event]
        except (KeyError) as e:
            code = None
            Plop().poll_return.emit("debug", ())

        if code is not None:
            _return = code(*args)
            if _return:
                print("Returning '{event}' with {args}".format(event=event, args=_return))
                Plop().poll_return.emit(_return[0], _return[1:])

    def destroy(self):
        self.app = None
        self.alive = False
        del self.app
        # self.deleteLater()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.exec_()
