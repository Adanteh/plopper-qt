"""
    Class and methods used to process outgoing and incomming data from whatever game you're using this tool for
    Any custom extensions should inherit from this
"""

from dataclasses import asdict, astuple
from collections import OrderedDict
from types import SimpleNamespace
from typing import List, TYPE_CHECKING
import traceback

from PyQt5 import QtCore

if TYPE_CHECKING:
    from plopper.tools import PlopperTool
    from plopper.modules import LibraryPanel, OutputPanel, ObjectsPanel
    from plopper.modules import PropertiesPanel, FavoritesPanel, DetailsPanel


class Singleton(type(QtCore.QObject)):
    _instance = None

    def __init__(self, name, bases, dict):
        # print("Creating class {}".format(cls.__name__))
        super().__init__(name, bases, dict)

    def __call__(self, *args, **kw):
        if self._instance is None:
            print("Init class {}".format(self.__name__))
            self._instance = super().__call__(*args, **kw)

        return self._instance

    def instance(self):
        if self._instance is None:
            raise LookupError("No instance of class created yet")
        return self._instance

    def __del__(self):
        self._instance = None


class PlopperSequence:
    """
        All our stuff that gets send to or received from Arma will be positional
    """

    def values(self):
        return astuple(self)

    def asdict(self):
        return asdict(self)

    def __getitem__(self, key: str):
        return getattr(self, key)

    def __setitem__(self, key: str, value):
        return setattr(self, key, value)

    def __iter__(self):
        for key, value in self.asdict().items():
            yield (key, value)


class Plop(QtCore.QObject, metaclass=Singleton):
    """Base object handling shared settings we want to i/o with the game"""

    class_changed = QtCore.pyqtSignal(dict, dict)
    tool_changed = QtCore.pyqtSignal(object, object)
    poll_return = QtCore.pyqtSignal(str, tuple)  # Used for proper threading

    objects: "LibraryPanel"
    output: "OutputPanel"
    objects: "ObjectsPanel"
    properties: "PropertiesPanel"
    favorites: "FavoritesPanel"
    details: "DetailsPanel"

    def __init__(self, mode="generic", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_class = {"Name": "", "File": ""}
        self.tool = ""
        self.mode = mode
        self.cache = OrderedDict()
        self.tools = SimpleNamespace()
        self.poll_return.connect(self.__poll_return)

    def set_main_class(self, new: dict):
        """Main function to share the 'main' selected class between multiple modules"""
        self.class_changed.emit(new, self.main_class)
        args = ((new["Name"], new["File"]), (self.main_class["Name"], self.main_class["File"]))
        self.cache_add("plop.class_changed", args=args, unique=True)
        self.main_class = new

    def set_main_alt(self, name, file):
        self.set_main_class({"Name": name, "File": file})

    def set_tool(self, tool: str):
        self.tool_changed.emit(tool, self.tool)
        self.cache_add("plop.tool_changed", args=(tool, self.tool))
        self.tool = tool

    def register_tool(self, tool: "PlopperTool"):
        """Register a tool for use in arma,mostly sets some variable names related to tool"""
        name = tool.NAME
        setattr(self.tools, name, tool)
        self.cache_add("plop.tool_register", (name,))

    def cache_add(self, event: str, args: tuple = (), unique=False):
        """Adds an entry to the cache. This will be polled by our extension every 0.1sec"""
        print(event, args)

        if unique and event in self.cache.keys():
            self.cache[event] = [args]
            return

        current = self.cache.get(event, [])
        current.append(args)
        self.cache[event] = current

    def cache_out(self):
        """Returns the cached events we want to get from the game"""
        data = self.cache.copy()
        data = list(zip(data.keys(), data.values()))
        self.cache.clear()
        return data

    def get_settings(self, args: List[str]) -> list:
        """Collects global settings (Unique names)"""
        from plopper.settings import PlopperSettings

        data = []
        for arg in args:
            data.append(PlopperSettings().get(arg, False))
        return data

    def set_setting(self, setting, value):
        from plopper.settings import PlopperSettings

        PlopperSettings().set(setting, value)

    def send_settings(self):
        """Sends all the settings"""
        from plopper.settings import PlopperSettings

        data = PlopperSettings().get_settings()
        self.cache_add("plop.send_settings", data)

    def destroy(self):
        self.__class__._instance = None

    def selection(self, obj_ids: List[str]):
        """Called when selection is changed ingame"""
        self.objects.tree.set_selected(obj_ids)

    def command(self, key: str, data: list):
        if key == "move":
            for obj_id, pos in data:
                obj = self.objects.get_object(obj_id)
                if obj is not None:
                    obj.set_pos(pos)
        self.details.refresh()

    @QtCore.pyqtSlot(str, tuple)
    def __poll_return(self, event, args):
        """Signal is used to return back to the same thread, mainly for debuggin purposes"""
        if event == "debug":
            pass
            # print("Waiting for dbg")
            # print("Done")
        else:
            plop(event, *args)

    @QtCore.pyqtSlot(str, list)
    def __dbg_return(self, event, args):
        """Signal is used to return back to the same thread, mainly for debuggin purposes"""
        plop(event, args)


def plop(func: str, *args):
    r"""
        Call as 'plop('brush.get_current_brush', [1, 2, 3])
    """
    current = Plop.instance()
    parts = func.split(".")
    for part in parts:
        current = getattr(current, part)
    if callable(current):
        try:
            return current(*args)
        except Exception as e:
            print("EXCEPTION", traceback.print_exc(4))
            return e
    else:
        return current
