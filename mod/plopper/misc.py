from typing import Union
from pathlib import Path
import subprocess

from plopper import Plop
from plopper.embed.embed import EmbedWrapper
from plopper.embed import hwnd_functions
from PyQt5.QtCore import QSettings


def reset_settings(*args, **kwargs):
    """Resets settings without initializing any of the QApplication stuff"""
    settings = QSettings("Adanteh", "Plopper")
    settings.remove("geometry")
    settings.remove("windowState")
    settings.sync()
    return True


def open_in_explorer(*args, **kwargs):
    """Opens explorer window for given path if it exists"""

    path = Plop().main_class["File"]
    if not path:
        return False

    if isinstance(path, str):
        path = Path(path)
    if not path.drive:
        path = "P:/" / Path(path)

    if path.exists():
        subprocess.Popen(r"explorer /select,{}".format(path))
        print(f"Opened file at path: {path}")
        return True
    else:
        print(f"Failed opening: {path}. This only works when you have proper P drive")
        return False


def fix_embedding(embed: EmbedWrapper):
    """Sometimes the embedded state can be corrupted, use this to restore it"""
    hwnd_functions.remove_frame(embed.hwnd)
    size = embed.parent().size()
    embed.widget.setGeometry(0, 0, size.width(), size.height())
