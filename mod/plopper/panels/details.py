"""
    Shows 
"""

from typing import List
from functools import partial
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QLineEdit

from plopper import Plop
from . import PlopperDockWidget
from .uic.details import Ui_Form
from .objects import ObjectEntry, UsedObjects


class DetailsView(QtWidgets.QWidget):
    _object: ObjectEntry
    fields: List[QLineEdit]

    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self._object = None
        self.form = Ui_Form()
        self.form.setupUi(self)
        self.fields = []
        self.load_fields()

        objects: UsedObjects = Plop().objects.tree
        objects.selectionChanged.connect(self.update_details)

    def load_fields(self):
        """Connects our QLineEdit fields with the value we have in ObjectEntry"""
        f = self.form
        for field, key in (
            (f.lPosX, "posx"),
            (f.lPosY, "posy"),
            (f.lPosZ, "posz"),
            (f.lScale, "scale"),
            (f.lPitch, "pitch"),
            (f.lBank, "bank"),
            (f.lYaw, "yaw"),
        ):
            field._key = key
            field.editingFinished.connect(partial(self.value_edited, field))
            self.fields.append(field)

    @pyqtSlot()
    def value_edited(self, field: QLineEdit):
        """Called after pressing Enter with a QLineEdit focused"""
        if self._object is None:
            return
        try:
            value = float(field.text())
            self._object[field._key] = value
            Plop().cache_add("object.value_change", args=(self._object.id, field._key, value))
        except ValueError:
            pass

    @pyqtSlot(object)
    def update_details(self, obj: ObjectEntry):
        """Sets our QLineEdit fields to the values set to the `obj`"""
        self._object = obj
        for field in self.fields:
            field.setText(str(getattr(obj, field._key, 0.0)))

    def refresh(self):
        """Updates the details for current object"""
        if self._object is not None:
            self.update_details(self._object)


class DetailsPanel(PlopperDockWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Details", parent=parent, flags=flags)

        self._widget = DetailsView(parent=self)
        self.setWidget(self._widget)

    def refresh(self):
        """Updates the details for current object"""
        self._widget.refresh()
