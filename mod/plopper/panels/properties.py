"""
    View indicating all objects in the current project. Groups them by layer
    Use left/right arrow keys to jump between different objects
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from plopper import Plop
from . import PlopperDockWidget
from .uic.properties import Ui_Form



class PropertiesView(QtWidgets.QWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.form = Ui_Form()
        self.form.setupUi(self)
        self.add_actions()

    def add_actions(self):
        self.form.bAlignTerrain.clicked.connect(lambda: self._cache_add("alignterrain"))
        self.form.bAlignHorizon.clicked.connect(lambda: self._cache_add("alignhorizon"))
        self.form.bHeightReset.clicked.connect(lambda: self._cache_add("resetheight"))
        self.form.bScaleReset.clicked.connect(lambda: self._cache_add("scalereset"))
        self.form.bAlignHeight.clicked.connect(lambda: self._cache_add("alignheight"))

    def _cache_add(self, event):
        Plop().cache_add("plop.property_event", args=[event])


class PropertiesPanel(PlopperDockWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Properties", parent=parent, flags=flags)

        self._widget = PropertiesView(parent=self)
        self.setWidget(self._widget)
