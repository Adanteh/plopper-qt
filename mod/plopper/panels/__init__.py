from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QEvent, QSettings
from PyQt5.QtWidgets import QDockWidget, QPushButton, QAction, QStyle, QAbstractButton, QSizePolicy

from plopper.settings import PlopperSettings


class PlopperDockTitle(QtWidgets.QWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.setBaseSize(100, 30)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)


class PlopperDockWidget(QDockWidget):
    def __init__(self, str, parent=None, flags=Qt.WindowFlags()):
        super().__init__(str, parent=parent, flags=flags)

        self.setAutoFillBackground(True)
        self.setAttribute(Qt.WA_StyledBackground)
        self.setAllowedAreas(Qt.RightDockWidgetArea)

        # self.setFeatures(QDockWidget.DockWidgetFloatable | QDockWidget.DockWidgetMovable)

        # self.__title = PlopperDockTitle(parent=self)
        # title = self.setTitleBarWidget(self.__title)
        icon = self.style().standardIcon(QStyle.SP_TitleBarMaxButton, None, self)
        close = self.findChild(QAbstractButton, name="qt_dockwidget_closebutton")
        close.setIcon(icon)
        close.installEventFilter(self)

        self.__close = close
        self._oldmax = 1000
        self._oldmin = 30
        self.__expanded = True

    # Disabled because it doesn't currently work (REEEEEEEEE)
    # def setWidget(self, widget):
    #     from plopper.main import PlopperApp
    #     super().setWidget(widget)

    #     settings = PlopperSettings.instance().settings
    #     self.__expanded = settings.value(
    #         "ui/{}/collapsed".format(self.windowTitle()),
    #         True,
    #         bool
    #     )
    #     # This no worky. Don't know why
    #     PlopperApp.instance().processEvents()
    #     PlopperApp.instance().main_event.emit(lambda: self.setExpanded(self.__expanded))

    def collapse(self):
        if not self.__expanded:
            return
        # self.widget().hide()

        widget = self.widget()
        self._oldmin = widget.minimumHeight()
        self._oldmax = widget.maximumHeight()
        self._oldheight = widget.height()
        widget.setFixedHeight(0)
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

    def expand(self):
        if self.__expanded:
            return

        self.widget().setMinimumHeight(self._oldmin)
        self.widget().setMaximumHeight(self._oldmax)
        QtWidgets.QWidget.size
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Expanding)

    def eventFilter(self, obj, event):
        if obj is self.__close:
            etype = event.type()
            if etype == QEvent.MouseButtonPress:
                self.expand_toggle()
                return True
            elif etype == QEvent.MouseButtonDblClick or etype == QEvent.MouseButtonRelease:
                return True
            # TODO: which other events can trigger the button (is the button
            # focusable).

        return super().eventFilter(obj, event)

    def expanded(self):
        return self.__expanded

    def expand_toggle(self, *args, **kwargs):
        self.setExpanded(not self.expanded())

        # Save state
        settings = PlopperSettings.instance().settings
        settings.setValue("ui/{}/collapsed".format(self.windowTitle()), self.expanded())
        settings.sync()

    def setExpanded(self, state=True):
        if state:
            self.expand()
        else:
            self.collapse()
        self.__expanded = state
