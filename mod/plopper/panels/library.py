"""
    Main object library, shows treeview for the TML files
    Has a nice search option and so on
"""

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt

from plopper import Plop, FOLDER
from plopper.arma.library import TbLibraryCollection, ModelEntry
from . import PlopperDockWidget
from .required import TreeviewSearchable

try:
    import pydevd
    DEBUG = True
except ImportError:
    DEBUG = False

class PlopperLibrary(TreeviewSearchable):
    """Library viewer for xml-like files"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_search()
        self.library = TbLibraryCollection(FOLDER.parent / "library", limit=DEBUG)
        self.populate()

    def populate(self):
        for library in self.library:
            cat = self.add_item(library.name, library)
            for entry in library:
                ent = self.add_item(entry.name, entry, cat)
        self.tree.sortByColumn(0, Qt.AscendingOrder)


class LibraryPanel(PlopperDockWidget):
    selected: ModelEntry

    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Library", parent=parent, flags=flags)

        self.tree = PlopperLibrary(parent=self)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.addWidget(self.tree)
        self.setWidget(self.tree)
        self.selected = None

        # Signals
        self.tree.item_changed.connect(self.current_item_changed)
        self.tree.item_doubleclicked.connect(self.double_click)

    @QtCore.pyqtSlot(ModelEntry, ModelEntry)
    def current_item_changed(self, item: ModelEntry, previous: ModelEntry):
        Plop().set_main_class(item.as_dict())
        self.selected = item

    @QtCore.pyqtSlot(ModelEntry)
    def double_click(self, item: ModelEntry):
        print("Double clicked: {}".format(item.name))

    def get(self):
        """Gets currently selected item"""
        mode = Plop().mode
        if mode == "arma" or mode == "buldozer":
            return [self.selected.name, self.selected.file]
        raise NotImplementedError

    def in_library(self, template: str) -> bool:
        """Check if template name is a valid library entry"""
        return (self.tree.library.get_entry(template.lower()) is not None)

    def get_model(self, template: str) -> str:
        """Gets the full model path from a template name"""
        entry = self.tree.library.get_entry(template.lower())
        if entry is None:
            return ""
        else:
            return entry.model
