"""
    Console window, showing debug / info messages
"""

import sys

from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import QHBoxLayout, QTextEdit, QWidget, QTextBrowser

from plopper.logger import log

from . import PlopperDockWidget


class EmittingStream(QObject):
    text_written = pyqtSignal(str)

    def __init__(self, parent=None):
        self.text = ""
        super().__init__(parent=parent)

    def write(self, text):
        text = str(text)

        # Put everything till newline character as one log message
        if text == "\n":
            log.info(self.text)
            self.text_written.emit(self.text)
            self.text = ""
        else:
            self.text += text
        try:
            sys.__stdout__.write(text)
        except Exception:
            pass

    def flush(self, *args, **kwargs):
        pass


class Output(QWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.layout = QHBoxLayout()
        self._text = QTextBrowser(parent=self)
        self.setLayout(self.layout)
        self.layout.addWidget(self._text)

        sys.stdout = self.out = EmittingStream()
        self.out.text_written.connect(self.add_text)
        self.setMinimumHeight(100)

    # def __del__(self):
    #     sys.stdout = sys.__stdout__

    def add_text(self, text):
        cursor = self._text.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertHtml(text + "<br>")
        self._text.setTextCursor(cursor)
        self._text.ensureCursorVisible()


class OutputPanel(PlopperDockWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Output", parent=parent, flags=flags)

        self._widget = Output(parent=parent)
        self.setWidget(self._widget)
        self.setAllowedAreas(Qt.RightDockWidgetArea | Qt.BottomDockWidgetArea)
