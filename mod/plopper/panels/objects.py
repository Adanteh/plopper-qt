"""
    View indicating all objects in the current project. Groups them by layer
    Use left/right arrow keys to jump between different objects
"""
from typing import List
from functools import partial
from dataclasses import dataclass

from PyQt5 import QtWidgets
from PyQt5.QtCore import QEvent, Qt, QObject, pyqtSignal, pyqtSlot
from PyQt5.QtCore import QModelIndex
from PyQt5.QtGui import QKeyEvent, QStandardItem
from PyQt5.QtCore import QItemSelectionModel

from plopper import Plop, PlopperSequence

from . import PlopperDockWidget
from .required import TreeviewSearchable
# from plopper.sample.sample_data import sample_load_project


@dataclass
class ObjectEntry(PlopperSequence):
    model: str = ""
    layer: str = ""
    id: str = ""
    posx: float = 0
    posy: float = 0
    posz: float = 0
    scale: float = 1
    pitch: float = 0
    bank: float = 0
    yaw: float = 0
    hidden: float = 0
    locked: float = 0

    def get_template(self) -> str:
        return self.model.split("\\")[-1]

    def position_round(self):
        return [round(f, 0) for f in [self.posx, self.posy, self.posz]]

    def set_pos(self, pos):
        """Sets the position from `getPositionWorld`"""
        self.posx = pos[0]
        self.posy = pos[1]
        self.posz = pos[2]

    def __repr__(self):
        return "({} at pos {})".format(self.get_template(), self.position_round())

    def __str__(self):
        return self.get_template().replace(".p3d", "")


class UsedObjects(TreeviewSearchable):
    """Library viewer for xml-like files"""

    selectionChanged = pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_search()
        self.entries = {}
        self._layers = {}
        self.item_changed.connect(self._item_changed)
        self.obj_id: str = None
        self.selected = ()

        self.add_context_options()
        self.event_filter = EventFilter(parent=self)
        self.installEventFilter(self.event_filter)
        self.item_doubleclicked.connect(self.double_click)

    def add_context_options(self):
        self.add_contextmenu()

        action = QtWidgets.QAction("Select All", parent=self)
        action.triggered.connect(partial(self.select_all_of_type, False))
        self.add_action(action)

        action = QtWidgets.QAction("Select All in view", parent=self)
        action.triggered.connect(partial(self.select_all_of_type, True))
        self.add_action(action)

    def select_all_of_type(self, in_view: bool, state):
        """Context menu option to select items of similar class (Global or in view)"""
        selected = self.selected_data()
        if not selected:
            return

        # Don't do this when we're selecting a layer (Causes crash)
        target = selected[0]
        if not isinstance(target, ObjectEntry):
            return

        model = target["model"]
        items = self.items_filtered(lambda entry: entry["model"] == model, ignore_error=True)
        ids = [item["id"] for item in items]
        Plop().cache_add("object.item_changed", args=(ids, in_view))

    def get_layer(self, name):
        if name != "":
            parent = self._layers.get(name, None)
            if parent is None:
                parent = self.add_item(name, name, category=self.CATEGORY)
                parent.children = []
                parent.name = name
                self._layers[name] = parent
            return parent
        else:
            return None
        return

    def clear(self, *args):
        super().clear(*args)
        self._layers.clear()

    def add_object(self, obj: ObjectEntry):
        """Adds object entry to the treeview"""

        parent = self.get_layer(obj.layer)
        item = self.add_item(str(obj), obj, parent, category=self.ENTRY)
        if parent is not None:
            parent.children.append(item)
        item.parent = parent
        self.entries[obj.id] = item

    def remove_object(self, object_id: str):
        """Removes object with given ID from treeview"""
        try:
            item = self.entries[object_id]
            self.remove_item(item)

            # Reset the layer name

            parent_item = item.parent
            if parent_item:
                parent_item.children.remove(item)
                if not parent_item.children:
                    del self._layers[parent_item.name]
        except KeyError as e:
            print("Error", e)
            pass

    def selected_data(self) -> list:
        """Gets the currently selected items (Data, not the useless QModelIndex)"""
        items = []
        for idx in self.tree.selectedIndexes():
            items.append(self._get_data(idx))
        return items

    def set_selected(self, obj_ids: List[str]):
        return
        
        items: List[QStandardItem] = [self.entries[f] for f in obj_ids]
        for item in items:
            self.tree.selectionModel().select(item.index(), QItemSelectionModel.Select)

    def _get_data(self, midx: QModelIndex):
        """Override of TreeviewSearchable. Here we actually care about the data for layers"""
        role = midx.data(role=Qt.UserRole)
        return self._data[role]

    @pyqtSlot(object)
    def double_click(self, item: ObjectEntry):
        """Pan camera on double clicking item"""
        if isinstance(item, ObjectEntry):
            Plop().cache_add("object.item_doubleclicked", args=(item.id,))

    @pyqtSlot(object, object)
    def _item_changed(self, item: ObjectEntry, previous: ObjectEntry):
        """Item changed, does not trigger if selection is from previous selection being removed!"""
        # item = self._get_data(model_idx)
        items: List[str] = []
        if isinstance(item, ObjectEntry):
            self.obj_id = item.id
            self.selectionChanged.emit(item)
            items = [self.obj_id]
        else:
            for child in self._layers[item].children:
                item = self._get_data(child)
                if isinstance(item, ObjectEntry):
                    items.append(item.id)
        self.selected = tuple(items)
        if items:
            Plop().cache_add("object.item_changed", args=(items, False))


class ObjectsPanel(PlopperDockWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Objects", parent=parent, flags=flags)

        self.tree = UsedObjects(parent=self)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.addWidget(self.tree)
        self.setWidget(self.tree)

        self.tree.setBaseSize(100, 200)
        self.setBaseSize(100, 200)

        # self.tree.tree.installEventFilter(event_filter)
        # self.init_keybinds()
        # self.__load_sample_objects()

    def add_object(self, *args, **kwargs):
        obj = ObjectEntry(*args, **kwargs)
        self.tree.add_object(obj)

    def remove_object(self, object_id: str):
        self.tree.remove_object(object_id)

    def clear(self, *args):
        self.tree.clear()

    def get_object(self, obj_id: str) -> ObjectEntry:
        """Gets `ObjectEntry` with given ID"""
        try:
            item: QStandardItem = self.tree.entries[obj_id]
            return self.tree._data[item.data(Qt.UserRole)]
        except KeyError:
            return None


class EventFilter(QObject):
    def eventFilter(self, tree: UsedObjects, event):
        """Event filter to delete selected objects when you press Del"""
        # from plopper import get_event_name
        # print(get_event_name(event))
        _super = super().eventFilter(tree, event)

        if _super:
            return _super
        # Delete key should delete the currently selected objects in-game
        if isinstance(event, QKeyEvent):
            if event.key() == Qt.Key_Delete:  # Del
                if event.type() == QEvent.KeyRelease:
                    from plopper import Plop

                    if tree.selected:
                        Plop().cache_add("object.delete", args=(tree.selected,))

        # Double click should focus camera on clicked object

        return False
