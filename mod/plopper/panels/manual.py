from PyQt5.QtWidgets import QDialog, QTextBrowser, QVBoxLayout

from plopper import MODFOLDER
from PyQt5.QtCore import QSettings


class ManualPanel(QDialog):
    NAME = "manual_dialog"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        layout = QVBoxLayout()
        self.resize(800, 600)
        self.setLayout(layout)
        self.setObjectName(self.NAME)

        md = self.get_manual()
        widget = QTextBrowser(parent=self)
        widget.setMarkdown(md)
        widget.setObjectName("manual_text")
        layout.addWidget(widget)

        settings = QSettings("Adanteh", "Plopper")
        geom = settings.value(self.NAME)
        if geom:
            self.restoreGeometry(geom)

        self.setWindowTitle("Manual")
        self.show()

    def get_manual(self) -> str:
        file = MODFOLDER / "manual.md"
        with file.open(mode="r") as fp:
            data = fp.read()
        return data

    def closeEvent(self, *args, **kwargs):
        geom = self.saveGeometry()
        settings = QSettings("Adanteh", "Plopper")
        settings.setValue(self.NAME, geom)
        return super().closeEvent(*args, **kwargs)


def show_manual(parent, *args, **kwargs):
    view = ManualPanel(parent=parent)
