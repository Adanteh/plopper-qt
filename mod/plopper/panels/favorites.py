"""
    Saved list of favorite objects
"""
import json
from pathlib import Path
from dataclasses import dataclass

import qtawesome as qta
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItem

from plopper import Plop, PlopperSequence
from plopper.toolsettings.fileselector import FileSelector

from . import PlopperDockWidget
from .required import TreeviewSearchable
from plopper.settings import PlopperSettings


@dataclass
class FavoritesEntry(PlopperSequence):
    Name: str = ""
    File: str = ""

    def get_template(self) -> str:
        return self.Name

    def __repr__(self):
        return "<{} '{}'>".format(self.__class__.__name__, self.Name)

    def __str__(self):
        return self.File


class FavoritesList(TreeviewSearchable):
    """List to keep track of favorite classes in separate files"""

    NAME = "Favorites"
    LEGACY = ".sqf"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.add_search()
        self.item_changed.connect(self._item_changed)
        self.__add_settings()

    def __add_settings(self):
        self.selector = FileSelector(parent=self, subfolder="favorites", extension=(".json", self.LEGACY))
        self.layout.insertWidget(0, self.selector)
        self.selector.file_load.connect(self.load_from_file)
        self.selector.file_save.connect(self.save_to_file)

        button = self.add_button(qta.icon("fa5s.plus-square"), "")
        button.clicked.connect(self.add_entry_from_current)

        button = self.add_button(qta.icon("fa5s.minus-square"), "")
        button.clicked.connect(self._remove_current)

        button = self.add_button(qta.icon("fa5s.folder-plus"), "")
        button.clicked.connect(self._add_from_selection)

        button = self.add_button(qta.icon("fa5s.window-close"), "")
        button.clicked.connect(self.clear)

    def add_entry_from_current(self, *args):
        entry = Plop().main_class
        if entry["Name"]:
            self.add_entry([entry["Name"], entry["File"]])

    def add_entry(self, entry: list) -> QStandardItem:
        name, file = entry
        currentnames = [entry["Name"] for entry in self._data.values()]
        if name in currentnames:
            return None
        else:
            obj = FavoritesEntry(*entry)
            item = self.add_item(name, obj, category=self.ENTRY)
            return obj

    def _add_from_selection(self, *args):
        Plop().cache_add("favorites.add_from_selection", args=("model",))

    def add_from_selection(self, data: list):
        if not data:
            return
        for obj in (dict(f) for f in data):
            file = obj["model"]
            name = Path(file).stem
            self.add_entry([name, file])

    def _item_changed(self, item, previous):
        if isinstance(item, FavoritesEntry):
            Plop().set_main_class(item.asdict())

    def save_to_file(self, path: Path):
        data = [entry.values() for entry in self._data.values()]
        with path.open(mode="w") as file:
            json.dump(data, file)
        PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)

    def read_file(self, path: Path):
        """Reads the file"""
        legacy = path.suffix == self.LEGACY
        try:
            with path.open(mode="r") as file:
                contents = file.read()
                if legacy:
                    try:
                        return self.legacy_processing(contents)
                    except Exception:
                        pass
                data = json.loads(contents)
        except Exception as e:
            print(e)  # WARNING
            return None
        return data

    def load_from_file(self, path: Path):
        """Loads a saved brush from a file, replaces the current one"""
        data = self.read_file(path)

        try:
            self.clear()
            for entry in data:
                self.add_entry(entry)

            PlopperSettings().set_(self.NAME + "/lastfile", str(path), _type=str)
            return data
        except TypeError as e:
            print(e)

    def legacy_processing(self, data: str):
        """Original objectplacementTBH format"""

        from ast import literal_eval

        data = literal_eval(data)[1:]
        return data

    def reloadFile(self, *args, **kwargs):
        """Reloads our last saved/loaded file from this. Called on app startup"""
        last_file = PlopperSettings().get_(self.NAME + "/lastfile", default="", _type=str)
        if last_file:
            last_path = Path(last_file)
            if last_path.exists():
                self.load_from_file(last_path)


class FavoritesPanel(PlopperDockWidget):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__("Favorites", parent=parent, flags=flags)

        self.tree = FavoritesList(parent=self)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.addWidget(self.tree)
        self.setWidget(self.tree)

        self.tree.setBaseSize(100, 200)
        self.setBaseSize(100, 200)

    def clear(self):
        pass

    def reloadFile(self, *args, **kwargs):
        self.tree.reloadFile(*args, **kwargs)

    def add_from_selection(self, *args, **kwargs):
        self.tree.add_from_selection(*args, **kwargs)
