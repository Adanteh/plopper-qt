from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt, QModelIndex, QItemSelectionModel, QItemSelection, pyqtSlot
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QAction, QWidget


class SearchProxyModel(QtCore.QSortFilterProxyModel):
    def setFilterRegExp(self, pattern):
        if isinstance(pattern, str):
            pattern = QtCore.QRegExp(pattern, QtCore.Qt.CaseInsensitive, QtCore.QRegExp.FixedString)
        super(SearchProxyModel, self).setFilterRegExp(pattern)

    def _accept_index(self, idx):
        if idx.isValid():
            text = idx.data(QtCore.Qt.DisplayRole)
            if self.filterRegExp().indexIn(text) >= 0:
                return True

            # Show parent if child matches search
            for row in range(idx.model().rowCount(idx)):
                if self._accept_index(idx.model().index(row, 0, idx)):
                    return True

            # Show children if parent matches search
            parent = idx.parent()
            while parent.isValid():
                text = parent.data(QtCore.Qt.DisplayRole)
                if self.filterRegExp().indexIn(text) >= 0:
                    return True
                parent = parent.parent()

        return False

    def filterAcceptsRow(self, sourceRow, sourceParent):
        idx = self.sourceModel().index(sourceRow, 0, sourceParent)
        return self._accept_index(idx)


class TreeviewSearchable(QWidget):
    """QTreeWidget like thing with nested searching on item name"""

    ENTRY = 1
    CATEGORY = 2

    item_changed = QtCore.pyqtSignal(object, object)
    item_doubleclicked = QtCore.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._current = None
        self._data = {}
        self._items = {}
        self.__dataindex = 0
        self.__buttons = []
        self.__buttonslayout = None

        self.layout = QtWidgets.QVBoxLayout()

        # Prepare for horizontal layout too
        self.setLayout(self.layout)
        self._add_tree()
        self.show()

    def setSortingEnabled(self, on=True):
        self.tree.setSortingEnabled(on)

    def sortByColumn(self, column: int, order=Qt.AscendingOrder):
        self.tree.sortByColumn(column, order)

    def _add_tree(self):
        self.tree = QtWidgets.QTreeView(parent=self)
        self.tree.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tree.setHeaderHidden(True)
        self.tree.setRootIsDecorated(True)
        self.tree.setUniformRowHeights(True)

        # Create layotus
        layout = self.layout_h = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout_v = QtWidgets.QVBoxLayout()
        layout_v.setContentsMargins(0, 0, 0, 0)
        layout_v.addWidget(self.tree)
        layout.addLayout(layout_v)
        self.layout.addLayout(layout)

        self.tree.clicked.connect(self._clicked)
        self.tree.doubleClicked.connect(self._doubleclicked)

        self.model = QStandardItemModel(parent=self)
        self.tags_model = SearchProxyModel()
        self.tags_model.setSourceModel(self.model)
        self.tags_model.setDynamicSortFilter(True)
        self.tree.setModel(self.tags_model)
        self.tree.selectionModel().selectionChanged.connect(self._selected)

    @pyqtSlot(QModelIndex)
    def _clicked(self, midx: QModelIndex):
        pass

    @pyqtSlot(QModelIndex)
    def _doubleclicked(self, midx: QModelIndex):
        data = self._get_data(midx)
        if data is not None:
            self.item_doubleclicked.emit(data)

    @pyqtSlot(QItemSelection, QItemSelection)
    def _selected(self, selected, deselected):
        sel = selected.indexes()
        if not sel:
            return

        midx = sel[0]
        data = self._get_data(midx)
        if data is not None:
            if data != self._current:
                self.item_changed.emit(data, self._current)
                self._current = data

    def selected_data(self) -> list:
        """Gets the currently selected items (Data, not the useless QModelIndex)"""
        items = []
        for idx in self.tree.selectedIndexes():
            items.append(self._get_data(idx))
        return items

    def add_contextmenu(self):
        """Sets the context menu policy"""
        self.tree.setContextMenuPolicy(Qt.ActionsContextMenu)

    def add_action(self, action: QAction):
        """Adds a context menu action"""
        self.tree.addAction(action)

    def add_search(self):
        """Adds the default search based on"""
        self.search = QtWidgets.QLineEdit()
        self.search.setPlaceholderText("Search...")
        self.search.setClearButtonEnabled(True)
        self.search.textChanged.connect(self._search_text_changed)
        self.layout.insertWidget(0, self.search)

    def add_button(self, *args, **kwargs):
        """Adds a button to the side of the list"""
        button = QtWidgets.QPushButton(*args, parent=self, **kwargs)
        button.setFixedSize(20, 20)
        button.setIconSize(QtCore.QSize(18, 18))
        if not self.__buttons:
            layout = self.__buttonslayout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setAlignment(Qt.AlignTop)
            self.layout_h.addLayout(layout)

        self.__buttons.append(button)
        self.__buttonslayout.addWidget(button)
        return button

    def clear(self, *args):
        """Removes all entries from the list"""
        model = self.tree.model()
        for i in range(0, model.rowCount()):
            model.removeRow(0)
        self._data.clear()
        self._items.clear()

    def _get_data(self, midx: QModelIndex):
        """Finds the connected data to the given QModelIndex"""
        role = midx.data(role=Qt.UserRole)
        _type = midx.data(role=Qt.UserRole + 1)
        if _type == self.ENTRY:  # Only bother with actual entry selections
            return self._data[role]
        else:
            return None

    def _remove_current(self):
        for idx in self.tree.selectedIndexes():
            self.remove_item(idx)

    def _search_text_changed(self, text=None):
        self.tags_model.setFilterRegExp(self.search.text())
        if len(self.search.text()) >= 1 and self.tags_model.rowCount() > 0:
            self.tree.expandAll()
        else:
            self.tree.collapseAll()

    def items_filtered(self, filter_, ignore_error=False) -> list:
        """Gets data in the treeview matching the given filter"""
        if not callable(filter_):
            raise TypeError("Filter needs to be a callable, with first argument for the item")

        items = []
        for entry in self._data.values():
            try:
                if filter_(entry):
                    items.append(entry)
            except Exception as e:
                if not ignore_error:
                    raise Exception(e)

        return items

    def add_item(self, text, data, parent=None, category=None):
        """Adds an item to the treeview."""
        self.__dataindex += 1
        if parent is None:
            model = self.tree.model().sourceModel()
            parent = model.invisibleRootItem()
            if category is None:
                category = self.CATEGORY
        else:
            if category is None:
                category = self.ENTRY

        item = QStandardItem(text)
        item.setData(self.__dataindex, role=Qt.UserRole)
        item.setData(category, role=Qt.UserRole + 1)
        self._data[self.__dataindex] = data
        self._items[self.__dataindex] = item
        parent.appendRow(item)
        return item

    def remove_item(self, midx: QModelIndex):
        if not isinstance(midx, QModelIndex):
            midx = midx.index()

        if midx.isValid():
            role = midx.data(role=Qt.UserRole)
            parent = midx.parent()
            
            row = midx.row()
            self.model.removeRow(row, parent)
            del self._data[role]
            del self._items[role]

            # Cleanup parent
            if parent.isValid() and not self.model.hasChildren(parent):
                self.remove_item(parent)

    def get_children(self, midx):
        pass

    def __selection_offset(self, offset=1):
        sel = self.tree.selectedIndexes()
        model = self.tree.model()
        rowcount = model.rowCount()

        # If there is no selection, select the first item (If there are any)
        if not sel:
            if rowcount == 0:
                return None

            item = model.index(0, 0)
            self.tree.selectionModel().select(item, QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows)
            return item

        current = sel[-1]
        if current.parent().isValid():
            rowcountparent = model.rowCount(current.parent())
            newindex = current.row() + offset
            if newindex < 0:
                current = current.parent()
            elif newindex >= rowcountparent:
                current = current.parent()
            else:
                item = model.index(0, 0)
                self.tree.selectionModel().select(item, QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows)
                return item

        newindex = current.row() + offset
        if newindex < 0:
            newindex = rowcount - 1
        elif newindex >= rowcount:
            newindex = 0

        model = self.tree.model()
        item = model.index(newindex, 0)
        self.tree.selectionModel().select(item, QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows)

        return item

    def select_previous(self):
        item = self.__selection_offset(offset=-1)
        from plopper.main import PlopperApp

        PlopperApp.instance().processEvents()
        return item

    def select_next(self):
        item = self.__selection_offset(offset=1)
        from plopper.main import PlopperApp

        PlopperApp.instance().processEvents()
        return item
