from functools import partial
from pathlib import Path

from plopper import Plop, debug
from plopper.misc import fix_embedding, open_in_explorer
from plopper.project import Projects
from plopper.panels.manual import show_manual

from ..settings import PlopperSettings
from .mainwindow import MainWindow


class PlopperWindow(MainWindow):
    """Plopper implemtation of your basic MainWindow framework"""

    def __init__(self, app):
        super().__init__(app)
        style = PlopperSettings.instance().get_("ui/stylesheet", "style_1.qss", str)
        self._load_stylesheet(name=style)
        self.projects = Projects(parent=self)
        self.projects.menuActions(ui=self)
        Plop().project = self.projects

    def _load_stylesheet(self, name="style.qss"):
        from .styles import style_1  # noqa: 401

        with (Path(__file__).parents[1] / "ui" / "styles" / name).open() as file:
            self.setStyleSheet(file.read())
            PlopperSettings.instance().set_("ui/stylesheet", name, str)

    def break_fnc(self):
        from plopper.main import PlopperApp  # noqa: F401
        from win32 import win32gui, win32process  # noqa: F401
        from win32.lib import win32con  # noqa: F401
        from plopper.embed import hwnd_functions  # noqa: F401

        app = PlopperApp()
        ui = app.ui
        hwnd = app.hwnd
        print("Breakpoint")

    def set_focus(self, *args, **kwargs):
        """Called from `ui.view.mousein`"""
        return
        # DISABLE FOR NOW: For some reason the mousein is only detected when we have QDockWidget selected
        # it does not work when we have the sidebar, toolbar or menubar selected, making the whole thing incredibly unreliable
        print("Setting focus")
        self.embed.setFocus()

    def createActions(self):
        """Adds menubar actions"""
        from plopper.core import restart

        def add_resolution(x, y):
            def set_resolution(x, y, *args):
                from plopper.main import PlopperApp

                app = PlopperApp()
                app.ui.centralWidgetResize(x, y)

            self.create_action(f"{x}x{y}", "", partial(set_resolution, x, y), ("Settings", "Resolution"))

        self.create_action("Preferences", "Ctrl+K", PlopperSettings().show_ui, "Settings")
        self.create_action("Light Stylesheet", "Ctrl+R", lambda: self._load_stylesheet(), "Settings")
        self.create_action("Dark Stylesheet", "Ctrl+T", lambda: self._load_stylesheet("style_1.qss"), "Settings")
        self.create_action("Hide UI", "Ctrl+Y", lambda: self.toggle_visibility(), "Settings")

        # Debug options, because this is a hacky mod
        self.create_action("Select objects", "F5", self.select_objects, ("Settings", "Debug"))
        self.create_action("End move", "F4", self.command_test, ("Settings", "Debug"))
        self.create_action("Breakpoint", "F6", self.break_fnc, ("Settings", "Debug", "Dev"))
        self.create_action("Restart", "F9", restart, ("Settings", "Debug", "Dev"))
        self.create_action("DEBUGPY", "", debug, ("Settings", "Debug", "Dev"))
        self.create_action("Reset UI", "", lambda: self.readSettings(reset=True), ("Settings", "Debug"))
        self.create_action("Fix embedding", "", lambda: fix_embedding(self.embed), ("Settings", "Debug"))
        self.create_action("Toggle UI", "F11", lambda: self.toggle_visibility(), ("Settings", "Debug"))
        self.create_action("Set Focus", "F12", self.set_focus, ("Settings", "Debug", "Dev"))

        self.create_action("Open in Explorer", "", open_in_explorer, ("Tools"))
        self.create_action("Manual", "", lambda: show_manual(self), "")

        for reso in (
            (1366, 768),  # HD
            (1440, 900),
            (1600, 900),  # HD+
            (1680, 1050),  # WSXGA+
            (1920, 1080),  # FHD
            (2560, 1080),  # Widescreen HD
            (2560, 1440),  # QHD
            (3440, 1440),  # Widescreen
            (3840, 2160),  # UHD
        ):
            add_resolution(*reso)

    def select_objects(self, *args, **kwargs):
        # from plopper import plop

        Plop().poll_return.emit("selection", (("0_502", "0_503"),))

    def command_test(self, *args, **kwargs):
        # from ploppper import plop
        # fmt: off
        Plop().poll_return.emit("command", ("move", [
            ("0_506", [100, 100, 10]),
        ]))
        # fmt: on
