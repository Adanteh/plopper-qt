from typing import Union

import qtawesome as qta
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QSettings, Qt, QThread
from PyQt5.QtGui import QColor, QCursor, QIcon
from PyQt5.QtWidgets import QAction, QMainWindow, QMenu, QMenuBar, QSizePolicy, QWidget

from plopper import Plop, FOLDER

from ..embed.embed import EmbedWrapper, embed_program
from ..keybinds import KeyboardFilter
from ..modules import Modules, Tools
from .sidebar import Sidebar, SidebarFloating

qta.set_defaults(scale_factor=1.0, color=QColor(255, 255, 255), color_disabled=QColor(150, 150, 150))

class CentralWidget(QWidget):
    embed: EmbedWrapper
    sidebar_dock: Sidebar

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.embed = None
        self.sidebar_dock = None

    def resizeEvent(self, event):
        if self.embed:
            self.embed.widget.setFixedSize(event.size())
            self.sidebar_dock.setLocation(self)
        event.accept()


class MainWindow(QMainWindow):

    closing_window = QtCore.pyqtSignal()
    menubar: QMenuBar
    sidebar: Sidebar

    def __init__(self, app, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)
        QThread.currentThread().setObjectName("main")

        self.__toolbuttons = []
        self.__toolbars = {}
        self.__menubuttons = {}
        self._shown = True
        self.app = app
        self.sidebar_dock = None
        self.alive = True

        self.setWindowIcon(QIcon(str(FOLDER / "ui" / "icon.ico")))

        self.initUI()
        self._initCentralWidget()
        self._initSidebar()
        self._initTools()
        self._initKeybinds()

        self.menubar = self.menuBar()

        self.createActions()
        self.show()
        self.toggle_visibility(True, restore=True)

    def initUI(self):
        """Sets the required attributes and flags for our main UI"""
        self.setWindowTitle("Plopper")
        self.setCursor(QCursor(Qt.ClosedHandCursor))
        self.setAcceptDrops(True)
        # self.setAttribute(Qt.WA_TranslucentBackground)  # Required for the central transparency
        # self.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.statusbar = self.statusBar()
        self.statusbar.show()
        self.statusbar.setAutoFillBackground(True)

    def _initCentralWidget(self):
        # Central widget area
        # layout = self.layout = QHBoxLayout()
        # layout.setContentsMargins(0, 0, 0, 0)

        c = self.central = CentralWidget(parent=self)
        c.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        c.setGeometry(0, 0, 1920, 1080)
        c.setBaseSize(1920, 1080)
        c.setObjectName("central")  # Used for stylesheet
        self.setCentralWidget(c)
        self.embed = embed_program(self.app.hwnd, c)

    def _initSidebar(self):
        # It is impossible to use basic setGeometry, raise_, stackUnder to overlay
        # sibling widget on top of a createWindowContainer one, so instead we make floating QDockWidget
        # which we then put in the correct location using central.mapToGlobal

        self.app.plop.ui = self
        self.sidebar_dock = SidebarFloating(self.central, parent=self, flags=Qt.FramelessWindowHint)
        self.sidebar = self.sidebar_dock.sidebar

    def _initTools(self):
        """Adds the tools for our object placement program"""
        for name, module in Modules:
            panel = module(parent=self)
            panel.setObjectName(name)
            self.addDockWidget(Qt.RightDockWidgetArea, panel)
            setattr(Plop(), name, panel)

            # Reload previous files if it still exists
            if hasattr(panel, "reloadFile"):
                panel.reloadFile()

        self.setDockOptions(self.dockOptions() | QMainWindow.GroupedDragging)

        for Tool in Tools:
            if Tool.condition(self.app.mode):
                tool = Tool(parent=self.sidebar)
                Plop().register_tool(tool)
                self.sidebar.add_tool(tool)

                # Reload previous files if it still exists
                if hasattr(tool, "reloadFile"):
                    tool.reloadFile()

    def add_toolbutton(self, action: QAction, toolbar: Union[str, tuple], **kwargs):
        """Adds an option to the given toolbar, if it doens't exist yet it'll be created"""

        tb = self.__toolbars.get(toolbar, None)
        if not tb:
            tb = QtWidgets.QToolBar(toolbar, parent=self)
            tb.setFloatable(False)
            tb.setObjectName(f"toolbar_{toolbar}")
            self.addToolBar(tb)
            self.__toolbars[toolbar] = tb

        tb.addAction(action)
        self.__toolbuttons.append(action)
        return tb

    def _initKeybinds(self):
        self.kb = KeyboardFilter(parent=self)

    def centralWidgetResize(self, x: int, y: int):
        """Resizes the window to result in wanted size of our centralWidget"""
        # If the window is not visible, it doesn't keep its layout up to date, so force it.
        if not self.isVisible():
            self.layout().update()

        self.layout().activate()
        size = self.size()
        childsize = self.centralWidget().size()
        dx = size.width() - childsize.width()
        dy = size.height() - childsize.height()
        self.resize(x + dx, y + dy)

    def destroy(self, *args, **kwargs):
        """Called when destroying the UI, called from either app cleanup (Arma) or closeEvent"""
        # from time import sleep

        if self._shown:
            self.saveSettings()
        self.embed.detach()
        self.sidebar_dock.destroy()
        self.alive = False
        return super().destroy(*args, **kwargs)

    def moveEvent(self, event):
        """Move along our floating QDockWidget sidebar overlay"""
        if self.sidebar_dock:
            self.sidebar_dock.setLocation(self.central)
        event.accept()

    def add_menubutton(self, action, parent: Union[str, tuple] = "", priority=None, **kwargs):
        """Adds an option to the menubar in the very top (Give parent for nested options)"""

        targetlevel = self.menubar
        if parent:
            if isinstance(parent, str):
                parent = (parent,)

            current = ""
            for p in parent:
                current += f"{p}."
                parentmenu = self.__menubuttons.get(p, None)
                if not parentmenu:
                    if priority is not None:
                        insert_before = targetlevel.actions()[priority]
                        parentmenu = QMenu(p)
                        targetlevel.insertMenu(insert_before, parentmenu)
                    else:
                        parentmenu = targetlevel.addMenu(p)
                    self.__menubuttons[p] = parentmenu
                targetlevel = parentmenu

        action.setParent(targetlevel)
        menu = targetlevel.addAction(action)
        self.__menubuttons[action.text()] = menu
        return menu

    def create_action(
        self, text: str, keybind: str, callback: callable, parent: Union[tuple, str] = "", priority=None
    ) -> QAction:
        """Allows creating basic QAction with a callable function on the menubar"""
        action = QAction(text)
        if keybind:
            action.setShortcut(keybind)
        action.triggered.connect(callback)
        self.add_menubutton(action, parent, priority=priority)
        return action

    def createActions(self):
        """Loads some default stuff in"""
        pass

    def closeEvent(self, event):
        """Event that gets called when we close our UI by clicking the close button"""
        Plop().cache_add("plop.closeEvent")

        super().closeEvent(event)
        self.destroy()
        self.closing_window.emit()

    def readSettings(self, reset=False):
        """Reads saved UI settings to restore our QDockWidgets to saved locations and sizes"""
        from plopper.settings.defaults import GEOMETRY, STATES

        settings = QSettings("Adanteh", "Plopper")
        geom = settings.value("geometry")
        if not geom or reset:
            geom = GEOMETRY
        self.restoreGeometry(geom)

        # If for some reason the width becomes completely corrupted, try and fix it here
        if self.width() < 500:
            self.setGeometry(50, 50, 1000, 500)

        states = settings.value("windowState")
        if not states or reset:
            states = STATES
        self.restoreState(states)

    def saveSettings(self):
        settings = QSettings("Adanteh", "Plopper")
        settings.setValue("geometry", self.saveGeometry())
        settings.setValue("windowState", self.saveState())

    def toggle_visibility(self, show=None, restore=True, **kwargs):
        """Toggles visibility for the app. This just makes all widgets but the central one hidden"""

        if not self.alive:
            return

        def child_iter(ui):
            exception = [ui.centralWidget(), ui.menuBar()]
            for child in ui.children():
                if child in exception:
                    # Leave our central embed widget visible
                    continue
                if hasattr(child, "setVisible"):
                    yield child
            yield ui.sidebar

        if show is None:
            show = not self._shown
        if not show:
            if restore:
                self.saveSettings()

            [f.setVisible(False) for f in child_iter(self)]
            self._shown = False
        else:
            [f.setVisible(True) for f in child_iter(self)]
            if restore:
                self.readSettings()
            self.sidebar_dock.setLocation(self.central)  # Required to restore proper height
            self._shown = True
