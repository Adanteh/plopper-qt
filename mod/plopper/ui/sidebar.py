import qtawesome as qta

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QEvent, QPoint, Qt, QSize
from PyQt5.QtWidgets import QDockWidget, QHBoxLayout, QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout, QWidget

from plopper.settings import PlopperSettings
from plopper.tools import PlopperToolGroup


class LockButton(QPushButton):
    def __init__(self, locked, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCheckable(True)
        self.setChecked(locked)
        self.setFixedSize(30, 30)
        self.setIconSize(QSize(28, 28))
        self.setIcon(qta.icon("mdi.lock"))


class Sidebar(QWidget):

    """
        Tools sidebar on the left side
    """

    WIDTH_SMALL = 30
    WIDTH_WIDE = 300

    hover_enter = QtCore.pyqtSignal()
    hover_exit = QtCore.pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.locked = PlopperSettings().get_("sidebar/locked", default=False, _type=bool)
        self.installEventFilter(self)
        self.setAutoFillBackground(True)

        self.hover_enter.connect(self._hover_enter)
        self.hover_exit.connect(self._hover_exit)

        self.setLayout(QHBoxLayout())
        self.layout().setAlignment(Qt.AlignTop)
        self.tools_layout = QVBoxLayout()
        self.tools_layout.setAlignment(Qt.AlignTop)
        self.tools_layout.setContentsMargins(0, 0, 0, 0)
        self.layout().addLayout(self.tools_layout)

        layout = QVBoxLayout()
        lock_button = LockButton(self.locked, parent=self)
        lock_button.toggled.connect(self._lock_toggle)
        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.tools_layout.addSpacerItem(spacer)
        self.tools_layout.addWidget(lock_button, alignment=Qt.AlignBottom)

        self.__toolgroup = PlopperToolGroup()
        self.__tools = []
        self.show()

        self.stack = QtWidgets.QStackedWidget()
        self.setBaseSize(self.WIDTH_WIDE, 400)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.layout().addWidget(self.stack)
        self.setAttribute(Qt.WA_StyledBackground, True)
        if not self.locked:
            self.stack.hide()

    def _lock_toggle(self, toggle: bool):
        PlopperSettings().set_("sidebar/locked", toggle, _type=bool)
        self.locked = toggle

    def add_tool(self, tool):
        """Adds a tool to the sidebar"""
        self.__tools.append(tool)
        self.__toolgroup.addButton(tool)
        self.tools_layout.insertWidget(len(self.__tools) - 1, tool)
        if tool.settings is not None:
            self.stack.addWidget(tool.settings)
            tool.activated.connect(self.activate_settings)

        _lastactive = PlopperSettings().get("tool", "", _type=str)
        if _lastactive == tool.NAME:
            tool.setChecked(True)

    def activate_settings(self, tool):
        self.stack.setCurrentWidget(tool.settings)

    def _hover_enter(self):
        self.stack.show()

    def _hover_exit(self):
        if not self.locked:
            self.stack.hide()

    def eventFilter(self, obj, event):
        if obj == self:
            # eventname = helper_qt.get_event_name(event, event.type())
            # print(eventname)
            if event.type() == QEvent.Enter:
                self.hover_enter.emit()
            elif event.type() == QEvent.Leave:
                self.hover_exit.emit()
        return super().eventFilter(obj, event)


class SidebarFloating(QDockWidget):
    """Use a QDockWidget so we can use parent properly, easily remove the frame but still allow overlay on a WinID window"""

    def __init__(self, fake_parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fake_parent.sidebar_dock = self
        self.setObjectName(self.__class__.__name__)

        self.sidebar = Sidebar()
        self.setWidget(self.sidebar)
        self.setWindowFlag(Qt.FramelessWindowHint, True)
        self.setFloating(True)  # Forces floating
        self.setTitleBarWidget(QWidget())  # Remove the title bar properly
        self.setAllowedAreas(Qt.NoDockWidgetArea)
        # self.setLocation(fake_parent)

        self.show()

    def setLocation(self, parent: QWidget):
        """Updates our floating widget to be placed as overlay location for other widget"""
        point = parent.mapToGlobal(QPoint(0, 0))
        height = parent.geometry().height()
        self.setGeometry(point.x(), point.y(), self.sidebar.WIDTH_WIDE, height)

    def restoreGeometry(self, *args, **kwargs):
        return True

    def restoreState(self, *args, **kwargs):
        return True
