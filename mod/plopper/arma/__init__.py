from functools import partial
from typing import TYPE_CHECKING

import qtawesome as qta
from PyQt5.QtWidgets import QAction

from plopper.settings import PlopperSettings, props
from plopper.toolsettings import Checkbox, NumberEdit

if TYPE_CHECKING:
    from plopper.main import PlopperApp


class PlopToggle(QAction):
    """On/Off toggle for important settings"""

    def __init__(self, icon, name, parent=None, text="", tooltip="", default=False):
        if isinstance(icon, str):
            icon = qta.icon(icon)
        super().__init__(icon, "", parent=parent)

        settings = PlopperSettings()
        prop = props.BoolProperty(widget=Checkbox, name=text, tooltip=tooltip, default=default)
        value = settings.register_setting(name, prop)
        self.setCheckable(True)
        self.setChecked(value)
        self.toggled.connect(partial(settings.set, name))
        if tooltip:
            self.setToolTip(tooltip)

    def __bool__(self):
        self.isChecked()

    def get(self):
        self.isChecked()


def main(app: "PlopperApp"):
    """Main implementation for arma"""

    def _main_toggles():
        action = PlopToggle(
            "fa5s.mountain", "slopeAlign", text="Slope Align", tooltip="Align objects to terrain when placing"
        )
        app.ui.add_toolbutton(action, "Objects")

        action = PlopToggle(
            "mdi.terrain",
            "followTerrain",
            text="Follow Height",
            tooltip="Follow terrain height when moving objects",
            default=True,
        )
        app.ui.add_toolbutton(action, "Objects")

        action = PlopToggle(
            "mdi6.cast-variant",
            "live_mode",
            text="Live Mode",
            tooltip="Will change selected object instantly, when selecting another class",
        )
        app.ui.add_toolbutton(action, "Selection")

        action = PlopToggle(
            "fa5s.sync-alt",
            "slopecontact_rotate",
            text="Slopecontact Rotate",
            tooltip="Allow rotating slopeContact items. Enable for missions, turn off when making a terrain",
        )
        app.ui.add_toolbutton(action, "Objects")

        action = PlopToggle(
            "mdi.format-rotate-90",
            "grouped_rotate",
            text="Grouped Rotation",
            tooltip="Rotates objects around middle of selection, instead of around its individual axis",
            default=True,
        )
        app.ui.add_toolbutton(action, "Objects")

        action = PlopToggle(
            "mdi.image-filter-center-focus",
            "highlight_mouseover",
            text="Mouseover highlight",
            tooltip="Highlights object when doing mouseover",
            default=True,
        )
        app.ui.add_toolbutton(action, "Selection")

        action = PlopToggle(
            "mdi.select-all",
            "object/sticky_selection",
            text="Sticky Selection",
            tooltip="Will not deselect when doing things like drag",
            default=True,
        )
        app.ui.add_toolbutton(action, "Selection")

    def _cam_settings():
        action = PlopToggle(
            "mdi.camera-switch",
            "cam/moveoncopy",
            text="Move on copy",
            tooltip="Follows objects when copying",
            default=True,
        )
        app.ui.add_toolbutton(action, "Camera")

        action = PlopToggle(
            "mdi.camera-party-mode",
            "cam/dynamicspeed",
            text="Dynamic Speed",
            tooltip="Changes camera movement speed based on altitude",
            default=True,
        )
        app.ui.add_toolbutton(action, "Camera")

        action = PlopToggle(
            "mdi6.cctv",
            "cam/dollymode",
            text="Dolly mode",
            tooltip="Forward/backward movement will be in camera direction, instead of relative to terrain",
        )
        app.ui.add_toolbutton(action, "Camera")

        action = PlopToggle(
            "mdi.camera-image",
            "cam/followterrain",
            text="Follow height",
            tooltip="Follows the terrain height when moving camera",
        )
        app.ui.add_toolbutton(action, "Camera")

        _speed = props.FloatProperty(
            widget=NumberEdit,
            name="Base speed",
            tooltip="The speed at which the camera moves (In MagicUnits/second)",
            default=1,
            min=0.01,
            max=None,
            soft_min=0.1,
            soft_max=5,
            step=0.1,
            precision=2,
        )

        _modifier = props.FloatProperty(
            widget=NumberEdit,
            name="Turbo modifier",
            tooltip="Modifier for speed when holding shift",
            default=3,
            min=0.1,
            max=None,
            soft_min=2,
            soft_max=15,
            step=1,
            precision=2,
        )

        _easing = props.BoolProperty(
            widget=Checkbox, name="Easing", tooltip="Eases camera movement (Accelarate camera slowly)", default=False
        )

        _dupe = props.BoolProperty(
            widget=Checkbox,
            name="Prevent Duplicates",
            tooltip="Will skip entries with same class and position on exporting",
            default=True,
        )
        _autosave = props.BoolProperty(
            widget=Checkbox,
            name="Autosave",
            tooltip="Autosaves every X minutes. Will use terrain name and autoincrement names",
            default=True,
        )
        _dblclick = props.BoolProperty(
            widget=Checkbox, name="Dbl Click Close", tooltip="Ends linetool placement with double click", default=False
        )

        PlopperSettings().register_setting("cam/speed", _speed)
        PlopperSettings().register_setting("cam/speed_modifier", _modifier)
        PlopperSettings().register_setting("cam/easing", _easing)
        PlopperSettings().register_setting("project/preventduplicates", _dupe)
        PlopperSettings().register_setting("project/autosave", _autosave)
        PlopperSettings().register_setting("line/doubleClickEnd", _dblclick)

    def _menu_options():
        """Adds the menubar options, mostly passthroughf or arma"""

        bar = app.ui.menubar
        plop = app.plop

        def windowopener(name):
            return lambda state, name=name, event="plop.open_window": plop.cache_add(event, name)

        def functionrunner(name):
            return lambda state, name=name, event="plop.run_function": plop.cache_add(event, name)

        # TODO: Reimplement this in Python to configure them
        action = QAction("Keybinds")
        action.triggered.connect(windowopener("ada_core_keybinds"))
        app.ui.add_menubutton(action, "Settings")

        # TODO: Reimplement this
        action = QAction("Add Existing Objects")
        action.triggered.connect(functionrunner("ADA_Main_fnc_addExistingObjects"))
        app.ui.add_menubutton(action, "Tools")

    _main_toggles()
    _cam_settings()
    _menu_options()

    action = PlopToggle(
        "mdi.information-outline",
        "debug/showmessages",
        text="Debug Messages",
        tooltip="Show debug messages",
        default=False,
    )
    app.ui.add_toolbutton(action, "Debug")

    app.plop.send_settings()
