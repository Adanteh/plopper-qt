from types import FunctionType

import keyboard

from plopper import Plop

# Patch the core to be able to instantly call python methods from arma
def keybind_callback(name):
    print("Keybind callback: ", name)
    try:
        callback = Plop.instance()._keybindlist[name]
        callback()
    except KeyError as e:
        print("Missing keybind", e)


def add_keybind(key, tag: str, callback: list, *args, modifierRelease=True, condition="", category="plop", **kwargs):
    key = parse_hotkey(key)
    up, down = parse_callback(callback, category, tag)

    # if condition == "":
    # condition = "_displayIDD == 14001"

    Plop.instance().cache_add("plop.add_keybind", args=[category, key, tag, up, down, modifierRelease, condition])
    return None


def parse_hotkey(key) -> list:
    """Changes a hotkey package keybind identifier to an Arma keybind array"""
    parsed = keyboard.parse_hotkey(key)
    if len(parsed) > 1:
        raise Exception("Arma does not support multi-step keybinds")

    keyarray = parsed[0]
    ctrl = (29, 57629, 57373) in keyarray
    shift = (42, 54) in keyarray
    alt = (56, 57400) in keyarray

    scancode = keyarray[-1][0]
    return (scancode, (shift, ctrl, alt))


def parse_callback(callback, category, tag) -> str:
    if isinstance(callback, (list, tuple)):
        return callback

    if isinstance(callback, str):
        return ("", callback)

    if callable(callback):
        if not hasattr(Plop.instance(), "_keybindlist"):
            Plop.instance()._keybindlist = {}
            Plop.instance()._keybind_call = keybind_callback

        name = "{}.{}".format(category, tag)
        Plop.instance()._keybindlist[name] = callback
        return ("", '["plopper.plop", ["_keybind_call", "{name}"]] call PY3_fnc_callExtension'.format(name=name))

    return ("", callback)
