from pathlib import Path
from dataclasses import dataclass, astuple
from typing import Generator


@dataclass
class TbRow:
    """
        Row of tb contains
        `("model", "x", "y", "dir", "pitch", "bank", "scale", "z", "end")`
    """

    model: str
    x: float = 0.0
    y: float = 0.0
    dir: float = 0.0
    pitch: float = 0.0
    bank: float = 0.0
    scale: float = 1.0
    z: float = 0.0
    end: str = ";"

    def __post_init__(self):
        """Change all values to the set type in our field definitions"""
        for name, field_type in self.__annotations__.items():
            if not isinstance(self.__dict__[name], field_type):
                setattr(self, name, field_type(self.__dict__[name]))

    def as_line(self) -> str:
        """Properly formatted line for a TB file"""
        values = list(self)
        values[0] = f'"{values[0]}"'  # Quotes around the model
        return ";".join((str(f) for f in values)) + self.end

    @classmethod
    def from_line(cls, line: str) -> "TbRow":
        """Creates TB entry from line in a TB file"""
        values = line.strip("\n").replace('"', "").split(";")
        return cls(*values)

    def __iter__(self):
        return iter(astuple(self)[:-1])

    def as_tuple(self):
        return astuple(self)[:-1]


def tb_iterator(path: Path) -> Generator[TbRow, None, None]:
    """Loads in an object file, and yields `TbRow` objects"""
    with path.open(mode="r") as fp:
        for line in fp:
            yield TbRow.from_line(line)
