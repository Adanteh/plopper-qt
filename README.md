# Plopper

This is a mod used to place objects on A3 Terrains. It's a 'new' version of ObjectPlacementTBH,
where all the UI was moved into an external PyQt5 program. Spending a lot of time making an Arma UI,
which is not a useful skill for any other programming is a waste of time, so I figured I'd write it in an actual language.
Another advantage of this is that it easier to make versions for other games, using the same UI + Tools

Please report all issues at [https://gitlab.com/Adanteh/plopper-qt/issues](https://gitlab.com/Adanteh/plopper-qt/issues)

**Everything below this point is probably incorrect, the Readme.MD still needs to be updated**

## USAGE
**Do not use BattleEye with this mod, it will not work.**

# Development
** This is instructions for dev-stuff. User manual is available at `Manual\Manual.md` **

### Modules
This addon sticks with SP's modular approach for tools/modes and Mapbuilder approach for Panes(windows). **Core** contains framework and basic functions not related to this mod itself.
**Main** contains the shared functions specifically for this mod. All variables that actually belong to the object placing specifically should belong to Main, not Core.

Module structure is as follows:
* `$PBOPREFIX$` for path prefixing. If it doesn't exist `x\plopper\addons\<folder name>` is used (Usually you dont need it)
* `Module.hpp` to set module name. This should generally be the same as folder name. Used for variable and function naming
* `Macros.hpp` to include the main macro file from core
* `config.cpp` as addon config file, with the module config, where module config is structured as:
  ```cpp
    class PREFIX { // Leave as is
      class Modules { // Leave as is
        class MODULE { // This module config.used from module.hpp
          DefaultLoad = 1; // Always execute this module. Otherwise module needs to be in description.ext\"ADA"\"Modules[]"
          dependencies[] = {"Core"}; // Autoload given modules if this module is enabled
          path = ""; // Optional, path to function files. Default: `x\plopper\addons\<MODULE>\funtions`

          class functionName; // Function file `fn_<functionName>.sqf` in path. Function name will be `<PREFIX>_<MODULE>_fnc_functionName`

          // -- Some examples of how filenames automatically execute functions on mission start
          class AutoExecution { // Subfolder of path. Use to keep folders clean.
            class initEverything; // STARTING with `init` will automatically run on server AND client AND HC
            class serverInitSystem; // STARTING with `server` will automatically run on server when starting mission
            class clientInitSomething; // STARTING with `clientInit` will automatically run on client when joining mission
          };

        };
      };
    };
  ```

Keep in mind that when you mave a subfolder, it should contain a `macros.hpp` with contents `#include ..\macros.hpp` to include the core macro file

Modules can be recompiled at any moment by running `["settings"] call ADA_Core_fnc_moduleLoad;` where you can use any module name. `""` to recompile everything


### Macro usage
This helps to make sure we don't get overlapping function and variable names. It might take some getting used to, but I highly recommend it

All examples given are in a module called 'example' (in Module.hpp)
* `ICON(action,settings)` :  `\x\plopper\addons\core\icons\action\settings.paa` Path to material.io icon in category Action with name Settings
* `GVAR(something)` :  ADA_Example_something
* `QGVAR(something)`  :  "ADA_Example_something"
* `FUNC(someFunction)`  :  ADA_Example_fnc_someFunction
* `QFUNC(someFunction)` :  "ADA_Example_fnc_someFunction"
* `CFUNC(libraryFunc)`  :  ADA_Core_fnc_libraryFunc
* `QCFUNC(libraryFunc)` :  "ADA_Core_fnc_libraryFunc"
* `MFUNC(libraryFunc)`  :  ADA_Main_fnc_libraryFunc
* `QMFUNC(libraryFunc)` :  "ADA_Main_fnc_libraryFunc"
* `LOCALIZE('some_text')` :  returns localized "$STR_ADA_Language_some_text" if it exists, else just keeps `some_text` and logs missing localization


# Tools
* Install latest Python version to run it.
* Open up CMD in the tools folder (Shift+Rightclick on the folder and do Open CMD Window here)
* Alternatively you can just open up CMD and use `cd <path to repo>\tools`

You can also double click the python files, but for more obvious reporting i recommend using CMD/PowerShell

#### Setup
```
python .\setup.py
```
After this in your arma folder you should have `x\plopper` present in Arma 3 folder
After you build this is what you want to launch as mod. Use something like Arma3Sync for dev launching
Otherwise use a shortcut wtih `-mod=\x\plopper;`


#### Building
This is for local testing copies only

```
python .\build.py
```

This will build any updated PBOs for ADA. Folders that contain a `$NOPACK$` file are ignored


### Poetry
`poetry install`
`poetry export -f requirements.txt --output requirements.txt --without-hashes` 

`keyboard` package must be manually added to Requirements, or Poetry will create broken file.