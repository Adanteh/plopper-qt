```sqf
["pythia.enable_reloader", [true]] call py3_fnc_callExtension; 
["plopper.plop", ["currentclass"]] call py3_fnc_callExtension; 

["plopper.deinit"] call py3_fnc_callextension; 
testvar1 = ["plopper.test"] call py3_fnc_callextension; 
testvar2 = ["plopper.running"] call py3_fnc_callextension; 
testvar3 = ["plopper.test2"] call py3_fnwc_callextension;
```

#### BULDOZER
-buldozer+addons -name=Buldozer -window -disableSteam -noAsserts -cfg=p:\buldozer.cfg -showScriptErrors -mod=@Pythia;@Plopper

* [ ] Readd map
* [ ] Update library files (slopeContact + BB)
* [ ] Change keybinds to python

* [ ] Launch mod
* [ ] Add BDPlaceStart / BDPlaceEnd tool events
* Interface

* [ ] Parse our world cfg into the DefaultWorld config.cpp
* Attempt worldCfg= launch parameter
