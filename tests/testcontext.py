from pathlib import Path
import os  # noqa: F401
import sys

sys.path.insert(0, str(Path(__file__).parents[1]))

import plopper  # noqa: F401
