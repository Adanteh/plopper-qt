import unittest
import context  # noqa: F401

from plopper import plop


class TestObjects(unittest.TestCase):
    def test_add_list(self):
        pos = (100, 100, 10)
        angles = (90, 0, 0)
        scale = 1.0
        path = "a3\\rocks_f_argo\\limestone\\limestone_01_01_lc_f.p3d"
        layer = "Sample #1"
        object_index = "500_1"
        plop("objects.add.object", path, layer, object_index, *pos, scale, *angles)


if __name__ == "__main__":
    unittest.main()
