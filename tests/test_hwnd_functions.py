import subprocess
import time
import unittest

import context  # noqa: F401
from plopper.embed import hwnd_functions


class TestHwndFunctions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        program = "powershell.exe"
        process = subprocess.Popen([program], creationflags=subprocess.CREATE_NEW_CONSOLE)
        time.sleep(2.5)  # Wait for window to open

        cls._name = program
        cls._process = process
        cls._pid = process.pid

    def test_hwnd_from_name(self):
        hwnd = hwnd_functions.hwnd_from_name(self._name)
        self.assertIsNotNone(hwnd)

    @unittest.skip("Not required anymore with win32gui")
    def test_id_from_name(self):
        hwnd = hwnd_functions.hwnd_from_name(self._name)
        hwnd_id = hwnd_functions.hwnd_id(hwnd)
        self.assertIsInstance(hwnd_id, int)

    def test_hwnd_from_pid(self):
        hwnd = hwnd_functions.hwnd_from_pid(self._pid)
        self.assertIsNotNone(hwnd)

    def test_hwnd_move(self):
        hwnd = hwnd_functions.hwnd_from_name(self._name)
        hwnd_functions.set_pos(hwnd, 0, 0, 320, 240)
        self.assertIsNotNone(hwnd)

    def test_none(self):
        self.assertIsNone(hwnd_functions.hwnd_from_name("a1b2c3"))

    @classmethod
    def tearDownClass(cls):
        cls._process.kill()


if __name__ == "__main__":
    unittest.main()
