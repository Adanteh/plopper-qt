"""Debug testing, launches terminal, waits a bit then attaches"""

import subprocess
import time
from pathlib import Path

def start_program(program):
    process = subprocess.Popen([program], creationflags=subprocess.CREATE_NEW_CONSOLE)
    time.sleep(1)


def compile_ui_files():
    file: Path
    for file in (Path(__file__).parent / "mod" / "plopper").glob("**/*.ui"):
        output = file.with_suffix(".py")
        subprocess.run(["pyuic5", str(file), "-o", str(output)])
        print(f"Compiled ui file: '{file}'")


if __name__ == "__main__":
    from plopper.main import PlopperApp
    from plopper.core import sample_data
    from plopper import start

    # Create a thread simulating event callbacks
    def callback():
        sample_data.add_sample_data()
        app = PlopperApp.instance()
        app.tester = sample_data.CacheTester(parent=app)
        app.create_thread_for(app.tester, "poll")
        # app.exec()

    compile_ui_files()
    program = "powershell.exe"
    start_program(program)
    # program = "Buldozer"
    start(program, mode="test", callback=callback, keep_alive=True)
    time.sleep(1)
    print("Exit")
